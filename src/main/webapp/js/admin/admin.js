		var isUserNameExist=false;
		var isUserEmailExist=false;
		var isCompanyNameExist=false;
		var isCompanyCodeExist=false;
/**
		 * Created By Bhagya On june 02nd, 2020
		 * Function for to validate the provider registration form
		 */
		function validateAdminRegistration(){
			var result=false;
			var a,b,c,d,e,f,g=true;
			var nameFilter=/^(?=.*[a-z])/; //matches only letters
			var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
				
			var companyName=$('#companyName').val();
			var contactEmail=$('#contactEmail').val();
			var contactNumber=$('#contactNumber').val();
			var companyAddress=$('#companyAddress').val();
			var username=$('#username').val();
			var password=$('#password').val();
			var companyCode=$('#companyCode').val();
			
			
			if(companyName=="" || companyName==null){
				a=false;
				$('#companyName_error').html('*Required');
			}
		
			else{
				a=true;
				$('#companyName_error').html('');
				checkCompanyNameExistance(companyName);
			}
			if(contactEmail=="" || contactEmail==null){
				b=false;
				$('#contactEmail_error').html('*Required');
			}
			else if(contactEmail!="" && !emailFilter.test(contactEmail)){
				b=false;
				$('#contactEmail_error').html('* Company Email Address is not Valid');
			}
			
			else{
				b=true;
				$('#contactEmail').html('');
				checkUserEmailExistance(contactEmail);
				
			}
			if(contactNumber=="" || contactNumber==null){
				c=false;
				$('#contactNumber_error').html('*Required');
			}
			else{
				c=true;
				$('#contactNumber_error').html('');
			}
			
			if(companyAddress=="" || companyAddress==null){
				d=false;
				$('#companyAddress_error').html('*Required');
			}
			else{
				d=true;
				$('#companyAddress_error').html('');
				
			}

			if(username=="" || username==null){
				e=false;
				$('#username_error').html('*Required');
			}
			else if(!nameFilter.test(username)){
				e=false;
				$('#username_error').html('*Should have atleast one Alphabet');
			}
			else{
				e=true;
				$('#username_error').html('');
				checkUserNameExistance(username);
			}
									
			if(password.trim().length<1){
				$('#password_error').html('*Required');
				f=false;
			}
			else if(password.trim().length<8 || password.trim().lenght>20){
				$('#password_error').html('* Password Should have atleast 8 characters and maximum 20 characters');
				f=false;
			}
			else{
				$('#password_error').html('');
				f=true;
			}
			if(companyCode=="" || companyCode==null){
				g=false;
				$('#companyCode_error').html('*Required');
			}
		
			else{
				g=true;
				$('#companyCode_error').html('');
				checkCompanyCodeExistance(companyCode);
			}
			if(a==true && b==true && c==true && d==true && e==true &&f==true && g==true){
				result=true;
			}
			else{
				result=false;
			}		
		
			 if(isUserNameExist==true || isUserEmailExist==true || isCompanyNameExist==true || isCompanyCodeExist==true ){
				 result=false;
			 }
								 
			/*if(isUserEmailExist==true){
				result=false;
			}
			*/
			return result;
			
		}
		
		
		/**
		 * Created By bHagya On June 02nd, 2020
		 * @param user
		 * 
		 * Function for to check the user Existance by username
		 */
		function checkUserNameExistance(user){
			var a=false;
			$.ajax({
				url:"/idlemonitor/user/checkuserexistance.do",
				type:'GET',
				async:false,
				data:{
					user:user
				},
				success:function(data){
					if(data==="success"){
						isUserNameExist=false;	
						$('#username_error').html('');
					}		
					else{
						isUserNameExist=true;		
						$('#username_error').html('*User Name Already Exist');
					}	
										
				},
			complete:{
			}
			});		
			$.ajaxSetup({async: true});
		}
		
		/**
		 * Created By Bhagya on June 02nd, 2020
		 * @param user
		 * 
		 * Function for to check the user email existance
		 */


		function checkUserEmailExistance(user){
			var result=false;
			var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			$.ajax({
				url:"/idlemonitor/user/checkuserexistance.do",
				type:'GET',
				async:false,
				data:{
					user:user
				},
				success:function(data){
					if(data==="success"){
						if(user!="" && !emailFilter.test(user)){
							$('#contactEmail_error').html('* Company Email Address is not Valid');
						}else{
							isUserEmailExist=false;		
							$('#contactEmail_error').html('');
						}
					}		
					else{
						isUserEmailExist=true;		
						$('#contactEmail_error').html('*Company Email Already Exist');
					}							
				},
			complete:{
				
			}
			});		
			$.ajaxSetup({async: true});
			
		}
		
		
		
		/**
		 * Created By Bhagya On june 02nd, 2020
		 * Function for to validate the provider registration form
		 */
		function validateEditAdminRegistration(){
			var result=false;
			var a,b,c,d,e,f=true;
			var nameFilter=/^(?=.*[a-z])/; //matches only letters
			var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
				
			var companyName=$('#companyName').val();
			var contactEmail=$('#contactEmail').val();
			var contactNumber=$('#contactNumber').val();
			var companyAddress=$('#companyAddress').val();
			var username=$('#username').val();
			var password=$('#password').val();
			
			
			var currentCompanyName=$('#current_company_name').val();
			var currentContactEmail=$('#current_contact_email').val();
			var currentUsername=$('#current_username').val();
			var companyCode=$('#companyCode').val();
			var currentCompanyCode=$('#current_company_code').val();
			
			if(companyName=="" || companyName==null){
				a=false;
				$('#companyName_error').html('*Required');
			}
		
			else{
				a=true;
				$('#companyName_error').html('');
				if(companyName!=currentCompanyName){
				checkCompanyNameExistance(companyName);
				}
			}
			if(contactEmail=="" || contactEmail==null){
				b=false;
				$('#contactEmail_error').html('*Required');
			}
			else if(contactEmail!="" && !emailFilter.test(contactEmail)){
				b=false;
				$('#contactEmail_error').html('* Company Email Address is not Valid');
			}
			
			else{
				b=true;
				$('#contactEmail').html('');
				if(contactEmail!=currentContactEmail){
				checkUserEmailExistance(contactEmail);
				}
			}
			if(contactNumber=="" || contactNumber==null){
				c=false;
				$('#contactNumber_error').html('*Required');
			}
			else{
				c=true;
				$('#contactNumber_error').html('');
			}
			
			if(companyAddress=="" || companyAddress==null){
				d=false;
				$('#companyAddress_error').html('*Required');
			}
			else{
				d=true;
				$('#companyAddress_error').html('');
				
			}

			if(username=="" || username==null){
				e=false;
				$('#username_error').html('*Required');
			}
			else if(!nameFilter.test(username)){
				e=false;
				$('#username_error').html('*Should have atleast one Alphabet');
			}
			else{
				e=true;
				$('#username_error').html('');
				if(username!=currentUsername){
				checkUserNameExistance(username);
				}
			}
			if(companyCode=="" || companyCode==null){
				f=false;
				$('#companyCode_error').html('*Required');
			}
		
			else{
				f=true;
				$('#companyCode_error').html('');
				if(companyCode!=currentCompanyCode){
				checkCompanyCodeExistance(companyCode);
				}
			}					
			
			if(a==true && b==true && c==true && d==true && e==true && f==true ){
				result=true;
			}
			else{
				result=false;
			}		
		
			 if(isUserNameExist==true || isUserEmailExist==true || isCompanyNameExist==true || isCompanyCodeExist==true ){
				 result=false;
			 }
								 
			/*if(isUserEmailExist==true){
				result=false;
			}*/
			
			return result;
			
		}
		
		
		/**
		 * Created By bHagya On June 05nd, 2020
		 * @param user
		 * 
		 * Function for to check the Company Existance by companyname
		 */
		function checkCompanyNameExistance(companyName){
			var a=false;
			$.ajax({
				url:"/idlemonitor/user/checkcompanyexistance.do",
				type:'GET',
				async:false,
				data:{
					companyName:companyName
				},
				success:function(data){
					if(data==="success"){
						isCompanyNameExist=false;	
						$('#companyName_error').html('');
					}		
					else{
						isCompanyNameExist=true;		
						$('#companyName_error').html('*Company Name Already Exist');
					}	
										
				},
			complete:{
			}
			});		
			$.ajaxSetup({async: true});
		}
		
		/**
		 * Created By bHagya On July 24th, 2020
		 * @param user
		 * 
		 * Function for to check the Company Existance by companycode
		 */
		function checkCompanyCodeExistance(companyCode){
			var a=false;
			$.ajax({
				url:"/idlemonitor/user/checkcompanycodeexistance.do",
				type:'GET',
				async:false,
				data:{
					companyCode:companyCode
				},
				success:function(data){
					if(data==="success"){
						isCompanyCodeExist=false;	
						$('#companyCode_error').html('');
					}		
					else{
						isCompanyCodeExist=true;		
						$('#companyCode_error').html('*Company Code Already Exist');
					}	
										
				},
			complete:{
			}
			});		
			$.ajaxSetup({async: true});
		}
		
		