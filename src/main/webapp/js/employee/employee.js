    var isemployeeNumberExist=false;
	var isEmployeeEmailExist=false;


function deleteScreenshots(){
    	
		var userId=$('#userId').val();
		var period=$('#period').val();
		var selectedDate=$('#selectedDate').val();
		    /*var someObj = {};
		    someObj.selectedScreenshots = [];
		   
		    $("input:checkbox").each(function() {
		        if ($(this).is(":checked")) {
		            someObj.selectedScreenshots.push($(this).attr("id"));
		        } 
		    });*/
			
		    var selectedScreenshots = [];
		   
		    $("input:checkbox").each(function() {
		        if ($(this).is(":checked")) {
		            selectedScreenshots.push($(this).attr("id"));
		        } 
				
		    });
		
		if(confirm("Are you Sure to Delete User")==true){
			$.ajax({
			    url: "/idlemonitor/employee/deletescreenshots.do",
			    type: 'GET',
			    data:
			    	{
			    		userId:userId,
						period:period,
						selectedDate:selectedDate,
						dataIds:selectedScreenshots.join(",")
			    	},
			   	success: function(data) {
			        alert("Screenshots Deleted Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Deleting Screenshots");
			    }
			});
		}
		    //window.location="${pageContext.request.contextPath}/employee/deletescreenshots.do?userId="+userId+"&period="+period+"&selectedDate="+selectedDate+"&dataIds="+someObj.selectedScreenshots 
		    //window.open(url,"_self");
		}

		/**
		 * Created By bHagya On August 12th, 2020 
		 * Function for to delete the screenshots
		 */
		/*function deleteScreenshots(){
			var userId=$('#userId').val();
    		var period=$('#period').val();
    		var selectedDate=$('#selectedDate').val();
			var someObj = {};
    		    someObj.selectedScreenshots = [];
    		    someObj.fruitsDenied = [];

    		    $("input:checkbox").each(function() {
    		        if ($(this).is(":checked")) {
    		            someObj.selectedScreenshots.push($(this).attr("id"));
    		        } 
    		    });
			$.ajax({
				url:"/idlemonitor/employee/deletescreenshots.do",
				type:'POST',
				async:false,
				data:{
					dataIds:someObj.selectedScreenshots
				},
				success:function(data){
					if(data==="success"){
						alert(" Screenshot Images Deleted Successfully");
						window.location.reload();
						
					}		
					else{
						alert(" Something went wrong while deleting screenshots");
					}	
										
				},
			complete:{
			}
			});		
			$.ajaxSetup({async: true});
		}*/
		
		
	function changeStatus(sel,id){
		var status=sel.value;
		if(status=="active"){
			$.ajax({
			    url: "/idlemonitor/employee/changeuserstatus.do",
			    type: 'GET',
			    data:
			    	{
			    		userId:id,
			    		isActive:true
			    	},
			   	success: function(data) {
			        alert("User Activated Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Activating User");
			    }
			});
		}
		else if(status=="inactive"){
			$.ajax({
			    url: "/idlemonitor/employee/changeuserstatus.do",
			    type: 'GET',
			    data:
			    	{
				    	userId:id,
				    	isActive:false
			    	},
			   	success: function(data) {
			   		alert("User Inactivated Successfully");
			   		window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Inactivating User");
			    }
			});
		}
		
	}
	/**
	Created By Bhagya on October 20th, 2020
	Function for to delete the employee user
	 */
	function deleteEmployeeUser(sel,id){
		var status=sel.value;
		if(confirm("Are you Sure to Delete User")==true){
			$.ajax({
			    url: "/idlemonitor/employee/deleteemployeeuser.do",
			    type: 'GET',
			    data:
			    	{
			    		userId:id
			    	},
			   	success: function(data) {
			        alert("User Deleted Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Deleting User");
			    }
			});
		}
	}
	
	/**
	* Created By Harshitha 
	* Function for to edit the employee user
	* Function for to validate the provider registration form
	**/
	
		
	function validateEditEmployeeRegistration(){
			var result=false;
			var a,b,c=true;
			var nameFilter=/^(?=.*[a-z])/; //matches only letters
			var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
				
			var employeeName=$('#employeeName').val();
			var employeeNumber=$('#employeeNumber').val();
			var emailId=$('#emailId').val();
			
			var currentemployeeNumber=$('#current_employee_number').val();
			var currentContactEmail=$('#current_contact_email').val();
			
			
			if(employeeName=="" || employeeName==null){
				a=false;
				$('#employeeName_error').html('*Required');
			}
		
			else{
				a=true;
				$('#employeeName_error').html('');
				
			}
			
			if(employeeNumber=="" || employeeNumber==null){
				b=false;
				$('#employeeNumber_error').html('*Required');
			S}
			else{
				b=true;
				if(employeeNumber!=currentemployeeNumber){
				checkEmployeeNumberExistance(employeeNumber);
			}
			}
			
			if(emailId=="" || emailId==null){
				c=false;
				$('#emailId_error').html('*Required');
			}
			else if(emailId!="" && !emailFilter.test(emailId)){
				c=false;
				$('#emailId_error').html('*Employee Contact Email Address is not Valid');
			}
			
			else{
				c=true;
				if(emailId!=currentContactEmail){
				checkEmployeeEmailExistance(emailId);
				}
			}
			
			
			
			if(a==true && b==true && c==true){
				result=true;
			}
			else{
				result=false;
			}		
		
			 if(isemployeeNumberExist==true || isEmployeeEmailExist==true ){
				 result=false;
			 }
								 
			
			return result;
			
		}
		
		/**
		 * Created By Harshitha on March 03rd, 2021
		 * @param user
		 * 
		 * Function for to check the Employee Number Existance
		 */
		function checkEmployeeNumberExistance(employeeNumber){
			$.ajax({
				url:"/idlemonitor/user/checkemployeenumberexistance.do",
				type:'GET',
				async:false,
				data:{
					employeeNumber:employeeNumber
				},
				success:function(data){
					if(data==="success"){
						isemployeeNumberExist=false;	
						$('#employeeNumber_error').html('');
					}		
					else{
						isemployeeNumberExist=true;		
						$('#employeeNumber_error').html('*Employee Number Already Exist');
					}	
										
				},
			complete:{
			}
			});		
			$.ajaxSetup({async: true});
		}
		
		
		/**
		 * Created By Harshitha on March 02nd, 2021
		 * @param user
		 * 
		 * Function for to check the Employee Email Existance
		 */


		function checkEmployeeEmailExistance(emailId){
			$.ajax({
				url:"/idlemonitor/user/checkemployeeemailexistance.do",
				type:'GET',
				async:false,
				data:{
					emailId:emailId
				},
				success:function(data){
					if(data==="success"){
						
							isEmployeeEmailExist=false;		
							$('#emailId_error').html('');
						
					}		
					else{
						isEmployeeEmailExist=true;		
						$('#emailId_error').html('*Employee Email Already Exist');
					}							
				},
			complete:{
				
			}
			});		
			$.ajaxSetup({async: true});
			
		}
		
		
		/**
		 * Created By Harshitha on March 28th, 2021
		 * 
		 * 
		 * Function for to validate from date and to date
		 */
		 
		 function validateFromDateAndToDate()
		 {
		 /* var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;*/
    
	        var a,b=true;
            /*var dateFilter = new RegExp("/^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/");*/
            
            var fromDate=$('#fromDate').val();
            var toDate=$('#toDate').val();
             
            if(fromDate=="" || fromDate==null){
				a=false;
				$('#fromDate_error').html('*Required');
			}
			/*else if(fromDate!="" && !dateFilter.test(fromDate)){
				a=false;
				$('#fromDate_error').html('*Given date is not Valid');
			}*/
			
			else{
				a=true;
				
			}
			
			if(toDate=="" || toDate==null ){
				b=false;
				$('#toDate_error').html('*Required');
			}
			/*else if(toDate!="" && !dateFilter.test(toDate)){
				b=false;
				$('#toDate_error').html('*Given date is not Valid');
			}*/
			
			
			else{
				b=true;
			}
			
			if(a==true && b==true){
				return true;
			}
			else{
				return false;
			}						 
			
		 }