/**
	function Save or update project
	Created By Bhagya on April 26th, 2021
 */

      function saveorUpdateProject(projectId){
		  var projectName =$("#project_name_"+projectId).val();
		  if(projectName=="" || projectName==null){
		 /* alert("project name cannot be empty");*/
    	   	$('#projectName_error_'+projectId).html('*Required');
    	   	return false;
    	  }
    	  else{
	    	  var clientName=$("#client_name_"+projectId).val();
	
    	 $.ajax({
				url:"/idlemonitor/project/saveorupdateproject.do",
				type:'GET',
				async:false,
				data:{
					projectId:projectId,
					projectName:projectName,
					clientName:clientName
				},
				success:function(data){
					if(data==="success"){
						if(projectId==0){
							alert(" Project Added Successfully ");
							
						}
						else{
							alert(" Project Updated Successfully");
						}
						
					}		
										
				},
			complete:{
			}
			});		
			$.ajaxSetup({async: true});
      }
      }
    
	/** 
	Created By Bhagya on April 27th, 2021
	function for to change the active/inactive status
	 */
	function changeStatus(sel,id){
		var status=sel.value;
		if(status=="active"){
			$.ajax({
			    url: "/idlemonitor/project/changeprojectstatus.do",
			    type: 'GET',
			    data:
			    	{
			    		projectId:id,
			    		isActive:true
			    	},
			   	success: function(data) {
			        alert("Project Activated Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Activating Project");
			    }
			});
		}
		else if(status=="inactive"){
			$.ajax({
			    url: "/idlemonitor/project/changeprojectstatus.do",
			    type: 'GET',
			    data:
			    	{
				    	projectId:id,
				    	isActive:false
			    	},
			   	success: function(data) {
			   		alert("Project Inactivated Successfully");
			   		window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Inactivating Project");
			    }
			});
		}
		
	}
	/**
	Created By Bhagya on April 27th, 2021
	Function for to delete the Project
	 */
	function deleteProject(sel,id){
		var status=sel.value;
		if(confirm("Are you Sure to Delete Project")==true){
			$.ajax({
			    url: "/idlemonitor/project/deleteproject.do",
			    type: 'GET',
			    data:
			    	{
			    		projectId:id
			    	},
			   	success: function(data) {
			        alert("Project Deleted Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Deleting Project");
			    }
			});
		}
	}
	

   /**
	function Save or update task
	Created By Harshitha on May 04, 2021
 */

      function saveorUpdateTask(taskId){
      
    	  var taskName =$("#task_name_"+taskId).val();
    	  if(taskName=="" || taskName==null){
    	   	$('#taskName_error_'+taskId).html('*Required');
    	   	return false;
    	  }
    	  else{
	    	  var projectId = $("#projectId").val();
		
	    	 $.ajax({
					url:"/idlemonitor/project/saveorupdatetask.do",
					type:'GET',
					async:false,
					data:{
						taskId:taskId,
						taskName:taskName,
						projectId:projectId
					},
					success:function(data){
						if(data==="success"){
						
							if(taskId==0){
								alert(" Task Added Successfully ");
							}
							else{
								alert(" Task Updated Successfully");
							}
							window.open('/idlemonitor/project/viewtasks.do?projectId='+projectId);
						}		
											
					},
				complete:{
				}
				});		
				$.ajaxSetup({async: true});
			}
      }
      
  
  /** 
	Created By Harshitha on May 05, 2021
	function to change the active/inactive status of task
	 */
	function changeStatus(sel,id){
		var status=sel.value;
		if(status=="active"){
			$.ajax({
			    url: "/idlemonitor/project/changetaskstatus.do",
			    type: 'GET',
			    data:
			    	{
			    		taskId:id,
			    		isActive:true
			    	},
			   	success: function(data) {
			        alert("task Activated Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Activating task");
			    }
			});
		}
		else if(status=="inactive"){
			$.ajax({
			    url: "/idlemonitor/project/changetaskstatus.do",
			    type: 'GET',
			    data:
			    	{
				    	taskId:id,
				    	isActive:false
			    	},
			   	success: function(data) {
			   		alert("task Inactivated Successfully");
			   		window.location.reload();
			    },
			    error:function(data,status,er) {
			    	alert("Error While Inactivating task");
			    }
			});
		}
		
	}
	
	
	/**
	Created By Harshitha on May 05, 2021
	Function for to delete the task
	 */
	function deleteTask(sel,id){
		var status=sel.value;
		if(confirm("Are you Sure to Delete Task")==true){
			$.ajax({
			    url: "/idlemonitor/project/deletetask.do",
			    type: 'GET',
			    data:
			    	{
			    		taskId:id
			    	},
			   	success: function(data) {
			        alert("task Deleted Succesfully");
			        window.location.reload();
			    },
			    error:function(data,status,er) {
			        alert("Error While Deleting task");
			    }
			});
		}
		
	}  
	
	