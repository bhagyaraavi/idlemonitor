
// Idle time configuration Js
   
      function saveorUpdateConfiguration(){
    	  var minutes =$("#idle_minutes").val();
    	 $.ajax({
				url:"/idlemonitor/user/idletimeconfiguration.do",
				type:'GET',
				async:false,
				data:{
					idleMinutes:minutes
				},
				success:function(data){
					if(data==="success"){
						$("#idle_minutes").val(minutes);
						alert("Updated Suceessfully");
					}		
					else{
						
					}	
										
				},
			complete:{
			}
			});		
			$.ajaxSetup({async: true});
      }
      
      
      
      function getConfigData(){
    	 
     	 $.ajax({
 				url:"/idlemonitor/user/getidletimeconfiguration.do",
 				type:'GET',
 				async:false,
 				data:{
 					
 				},
 				success:function(data){
 						$("#idle_minutes").val(data);
 						var idleMinutes =$("#idle_minutes").val();
 				    	
 				    	if(null==idleMinutes||idleMinutes<5){
 				    		//Added, set initial value.
 				        	$("#idle_minutes").val(5);
 				           $("#idle_minutes_label").text(0);
 				        
 				         
 				    	}
 				    	
 				          $("#slider").slider({
 				              animate: true,
 				              value:idleMinutes,
 				              min: 5,
 				              max: 60,
 				              step: 1,
 				              slide: function(event, ui) {
 				                  update(1,ui.value); //changed
 				              }
 				          });


 				        
 				          update();
 									
 				},
 			complete:{
 			}
 			});		
 			$.ajaxSetup({async: true});
       }
      

    //changed. now with parameter
      function update(slider,val) {
        //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
        var $idle_minutes = slider == 1?val:$("#idle_minutes").val();
       
         $( "#idle_minutes" ).val($idle_minutes);
         $( "#idle_minutes_label" ).text($idle_minutes);
         $('#slider a').html('<label> '+$idle_minutes+' </label>');
      
      }
      
   // End of idle Time Configuration JS