
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Idle Monitor : Dashboard</title>
    <%@ include file="/jsp/common/common.jsp"%>
     <script type="text/javascript">
    $(document).ready(function(){
		$('a').removeClass('active');
		$('#dashboard').addClass('active');
    });
    </script>
</head>
<body>
<!--===============================================
                      NAVBAR
===============================================-->


   <%@ include file="/jsp/common/applicationHeader.jsp"%>


    <section class="side-bar-warp">
    	<!--  LEFT SIDE PANEL JSP FILE  -->
        <%@ include file="/jsp/common/leftSidePanel.jsp"%>
        <!--  LEFT SIDE PANEL JSP FILE  -->
        <div class="dashboard-stats">
            <div class="col-md-12">
                <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <section class="panel panel-box">
                                <div class="panel-left panel-icon bg-success">
                                    <i class="fa fa-dollar text-large stat-icon success-text"></i>
                                </div>
                                <div class="panel-right panel-icon bg-reverse">
                                    <p class="size-h1 no-margin countdown_first">32</p>
                                    <p class="text-muted no-margin text"><span>No. of Projects Assigned</span></p>
                                </div>
                            </section>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <section class="panel panel-box">
                                <div class="panel-left panel-icon bg-danger">
                                    <i class="fa fa-shopping-cart text-large stat-icon danger-text"></i>
                                </div>
                                <div class="panel-right panel-icon bg-reverse">
                                    <p class="size-h1 no-margin countdown_second">25</p>
                                    <p class="text-muted no-margin text"><span>No. of Projects Completed</span></p>
                                </div>
                            </section>
                            
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <section class="panel panel-box">
                                <div class="panel-left panel-icon bg-lovender">
                                    <i class="fa fa-rocket text-large stat-icon lovender-text"></i>
                                </div>
                                <div class="panel-right panel-icon bg-reverse">
                                    <p class="size-h1 no-margin countdown_third">04</p>
                                    <p class="text-muted no-margin text"><span>No. of Projects in Process</span></p>
                                </div>
                            </section>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <section class="panel panel-box">
                                <div class="panel-left panel-icon bg-info">
                                    <i class="fa fa-users text-large stat-icon info-text"></i>
                                </div>
                                <div class="panel-right panel-icon bg-reverse">
                                    <p class="size-h1 no-margin countdown_fourth">03</p>
                                    <p class="text-muted no-margin text"><span>No. of Projects Pending</span></p>
                                </div>
                            </section>
                        </div>    
                    </div> 
        </div>
    </div>
    </section>


<script type="text/javascript">
    

$(document).ready(function () {
    $(document).on('click', '.cta', function () {
        $(this).toggleClass('active')
    })
});


$(document).ready(function(){
    $(".hamburger").click(function(){
        $('.sidebar-menu').removeClass("flowHide");  
        $(".sidebar-menu").toggleClass("small-side-bar");
        $('.nav-link-name').toggleClass('name-hide');        
    });
});




 $(document).ready(function () {    
      $(".nav-link").hover(function () {           
          $('.sidebar-menu').removeClass("flowHide");  
          $(this).addClass('tax-active');
              
      }, function () {
          $('.sidebar-menu')
             .addClass("flowHide");
          $(this).removeClass('tax-active');
             
      });    
  });
</script>
<script type="text/javascript">
     $(function() {
   

       function countUp(count) {
        var div_by = 100,
            speed = Math.round(count / div_by),
            $display = $('.countdown_first'),
            run_count = 1,
            int_speed = 24;
        var int = setInterval(function () {
            if (run_count < div_by) {
                $display.text(speed * run_count);
                run_count++;
            } else if (parseInt($display.text()) < count) {
                var curr_count = parseInt($display.text()) + 1;
                $display.text(curr_count);
            } else {
                clearInterval(int);
            }
        }, int_speed);
    }
    countUp(6791);

function countUp2(count) {
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.countdown_second'),
        run_count = 1,
        int_speed = 24;
    var int = setInterval(function () {
        if (run_count < div_by) {
            $display.text(speed * run_count);
            run_count++;
        } else if (parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}
countUp2(555);

function countUp3(count) {
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.countdown_fourth'),
        run_count = 1,
        int_speed = 24;
    var int = setInterval(function () {
        if (run_count < div_by) {
            $display.text(speed * run_count);
            run_count++;
        } else if (parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}
countUp3(999);
           
           });

         $(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
    
</script>
<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>