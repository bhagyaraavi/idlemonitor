<!DOCTYPE html>
<html lang="en">
<head>
	<title>Idle Monitor : Register</title>
	
<!--  COMMON JSP FILE  -->
	<%@ include file="/jsp/common/common.jsp"%>
	 <script type="text/javascript" src="${pageContext.request.contextPath}/js/admin/admin.js"></script>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="${pageContext.request.contextPath}/images/logo_1.png" alt="IMG">
				</div>
		<form:form method="post" id="admin_registration" name="adminRegistration" class="login100-form validate-form" action="${pageContext.request.contextPath}/user/adminregistration.do" commandName="adminUserDto"  autocomplete="off" onsubmit="return validateAdminRegistration();"
			enctype="multipart/form-data">
				<!-- <form class="login100-form validate-form"> -->
					<span class="login100-form-title">
						New User Registration
					</span>
					<div class="wrap-input100">
					<form:input class="input1001" type="text" name="" placeholder="Company Code" path="companyDto.companyCode" id="companyCode"/>
						<span id="companyCode_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-cube" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100">
					<form:input class="input1001" type="text" name="" placeholder="Company Name" path="companyDto.companyName" id="companyName"/>
						<span id="companyName_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-cube" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100" >
						<form:input class="input1001" type="text" name="" placeholder="Contact Email" path="contactEmail" id="contactEmail"/>
						<span id="contactEmail_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100 ">
						<form:input class="input1001" type="text" name="" placeholder="Contact Number" path="contactNumber" id="contactNumber"/>
						<span id="contactNumber_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100">
						<form:input class="input1001" type="text" name="" placeholder="Company Address" path="companyAddress" id="companyAddress"/>
						<span id="companyAddress_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-location-arrow" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100">
						<form:input class="input1001" type="text" name="" placeholder="Username" path="username" id="username"/>
						<span id="username_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100">
						<form:input class="input1001" type="password" name="pass" placeholder="Password" path="password" id="password" />
						<span id="password_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Sign Up
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Already have an account?
						</span>
						<a class="txt2" href="${pageContext.request.contextPath}/login.do">
							Sign In <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>

					
				</form:form>
			</div>
		</div>
	</div>
	
	
</body>
</html>