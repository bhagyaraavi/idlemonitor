<!DOCTYPE html>
<html lang="en">
<head>
	<title>Idle Monitor : Registration Success</title>
	<%@ include file="/jsp/common/common.jsp"%>
</head>
<body>
	
	<%@ include file="/jsp/common/header.jsp"%>
    <section class="m_contenr">
        <div class="container">
            <div class="row justify-content-md-center">
            <div class="col col-lg-8">
                <img src="${pageContext.request.contextPath}/images/register.png" alt="">
                    <h2>Registration Confirmation</h2>
                    <p>${message}. Please, check your spam folder if you do not receive an email that includes an account confirmation link in 5 minutes</p>
                    <a href="${pageContext.request.contextPath}" style="text-decoration: underline;color:#01e;">Click Here to goto home page</a>
                </div>
            </div>
        </div>
    </section>
    <%@ include file="/jsp/common/footer.jsp"%>
</body>
</html>