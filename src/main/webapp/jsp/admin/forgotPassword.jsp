<!DOCTYPE html>
<html lang="en">
<head>
	<title>Idle Monitor : Forgot password</title>
	<!--  COMMON JSP FILE  -->
	<%@ include file="/jsp/common/common.jsp"%>
	 
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt mn_top" data-tilt>
					<img src="${pageContext.request.contextPath}/images/forgot.png" alt="IMG">
				</div>
			<div class="left">
				<c:if test="${ not empty status}">
				     <div id="login_head_text" class="status">
							${status}
					 </div> 
		  		</c:if>
				<c:if test="${empty status && not empty error}">
					<div id="login_head_text" class="status" style="color:#FF0000;">
						${error}
					</div>
		  		</c:if>	

				<form class="login100-form" action="${pageContext.request.contextPath}/user/forgotpassword.do" method = "POST" >
					<span class="login100-form-title">
						Forgot Your Password?
					</span>
					<p class="text-center">Enter your email address and we will send you instructions to reset your password</p><br/>
					
					<div class="wrap-input100">
						<input class="input1001" type="text" name="emailId" placeholder="Email Address">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope-open-o" aria-hidden="true"></i>
						</span>
					</div>

					
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Continue
						</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>