<!DOCTYPE html>
<html lang="en">
<head>
    <title>Idle Monitor : Login</title>
   		<%@ include file="/jsp/common/common.jsp"%>
</head>
<body>
    
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt" data-tilt>
                    <img src="${pageContext.request.contextPath}/images/logo_1.png" alt="IMG">
                </div>

 

                <form class="login100-form validate-form" action="<c:url value= '/login' />"  method='POST'>
                    
                    <c:if test="${ not empty status}">
                    	<div class="text-center p-t-136 success_color">
                        <p>${status}</p>
                        </div>
                    </c:if>
                    <c:if test="${empty status && not empty error}">
                    	<div class="text-center p-t-136 success_color">
                        <p>${error}</p>
                        </div>
                    </c:if>
                    <span class="login100-form-title">
                        Login
                    </span>
                    
                    <!-- <div class="wrap-input100">
						<select  class="input1001">
							<option>Company Name</option>
						</select>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-cube" aria-hidden="true"></i>
						</span>
					</div> -->
					<div class="wrap-input100">
						<input class="input1001" type="text" name="username" placeholder="Username">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100">
						<input class="input1001" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2" href="${pageContext.request.contextPath}/user/forgotpassword.do">
							Username / Password?
						</a>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="${pageContext.request.contextPath}/user/adminregistration.do">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
				</div>
			</div>
		</div>
    
</body>
</html>