<!-- Created By bhagya On may 16th,2017 
	Page For Error-->
<!DOCTYPE html>

<html>
<head>

	<title>${title}</title>
	<%@ include file="/jsp/common/common.jsp"%>
</head>

<body>
<!--Header-->

	<%-- <%@ include file="/jsp/common/header.jsp"%> --%>
	
  <!--//Header-->
  <div class="mn_register">
  	<div class="register-box">
  		<div class="register mn_reg">
        	<div class="register-box">
          		<div class="col-md-6">
            		<div class="mn_add">
              			<h4>Registration Status</h4>
            		</div>
            	</div>
          		<div class="col-md-12">
            		<form class="form-horizontal" autocomplete="off" action="" method="POST" >
          		
	          		
				
	            	<div class="error_status">
				         ${message}
				    </div>
        			
        			<div style="font-size: 16px;">
						 <a href="${pageContext.request.contextPath}" style="text-decoration: underline;color:#01e;">	Click here to goto Home </a>
					 </div>
					 
					</form>
          		</div>
          <div class="clearfix"></div>
  		  </div>
  		</div>
  		<!--FOOTER-->
	     <%-- <%@ include file="/jsp/common/homePageFooter.jsp"%> --%>
  	</div>
  </div>

</body>
</html>