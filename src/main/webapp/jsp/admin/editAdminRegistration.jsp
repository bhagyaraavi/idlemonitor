<!DOCTYPE html>
<html lang="en">
<head>
	<title>Idle Monitor : Edit Profile</title>
	 <%@ include file="/jsp/common/common.jsp"%>
	 <script type="text/javascript" src="${pageContext.request.contextPath}/js/admin/admin.js"></script>
</head>
<body>
	 <%@ include file="/jsp/common/applicationHeader.jsp"%>
    <section class="side-bar-warp">
        <%@ include file="/jsp/common/leftSidePanel.jsp"%>
        <div class="dashboard-stats">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                    	<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="${pageContext.request.contextPath}/images/edit.png" alt="IMG">
				</div>
				<form:form method="post" id="edit_admin_registration" name="editAdminRegistration" class="login100-form validate-form" action="${pageContext.request.contextPath}/user/editadminregistration.do" commandName="adminUserDto"  autocomplete="off" onsubmit="return validateEditAdminRegistration();"
				enctype="multipart/form-data">
				<input type="hidden"  name="adminUserId" value="${adminUserDto.adminUserId}" />
				<input type="hidden"   value="${adminUserDto.companyDto.companyName}" id="current_company_name"/>
				<input type="hidden"   value="${adminUserDto.companyDto.companyCode}" id="current_company_code"/>
			       <input type="hidden"   value="${adminUserDto.username}" id="current_username"/>
			        <input type="hidden"  value="${adminUserDto.contactEmail}" id="current_contact_email"/>
					<span class="login100-form-title">
						Edit User Registration
					</span>
					<div class="wrap-input100">
					<form:input class="input1001" type="text" name="" placeholder="Company Code" path="companyDto.companyCode" id="companyCode" value="${adminUserDto.companyDto.companyCode}" readonly="true"/>
						<span id="companyCode_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-cube" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100">
					<form:input class="input1001" type="text" name="" placeholder="Company Name" path="companyDto.companyName" id="companyName" value="${adminUserDto.companyDto.companyName}" readonly="true"/>
						<span id="companyName_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-cube" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100" >
						<form:input class="input1001" type="text" name="" placeholder="Contact Email" path="contactEmail" id="contactEmail" value="${adminUserDto.contactEmail}"/>
						<span id="contactEmail_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100 ">
						<form:input class="input1001" type="text" name="" placeholder="Contact Number" path="contactNumber" id="contactNumber" value="${adminUserDto.contactNumber}"/>
						<span id="contactNumber_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100">
						<form:input class="input1001" type="text" name="" placeholder="Company Address" path="companyAddress" id="companyAddress" value="${adminUserDto.companyAddress}"/>
						<span id="companyAddress_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-location-arrow" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100">
						<form:input class="input1001" type="text" name="" placeholder="Username" path="username" id="username" value="${adminUserDto.username}"/>
						<span id="username_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Submit
						</button>
					</div>

					

					
				</form:form>
			</div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<script type="text/javascript">
    

$(document).ready(function () {
    $(document).on('click', '.cta', function () {
        $(this).toggleClass('active')
    })
});


$(document).ready(function(){
    $(".hamburger").click(function(){
        $('.sidebar-menu').removeClass("flowHide");  
        $(".sidebar-menu").toggleClass("small-side-bar");
        $('.nav-link-name').toggleClass('name-hide');        
    });
});




 $(document).ready(function () {    
      $(".nav-link").hover(function () {           
          $('.sidebar-menu').removeClass("flowHide");  
          $(this).addClass('tax-active');
              
      }, function () {
          $('.sidebar-menu')
             .addClass("flowHide");
          $(this).removeClass('tax-active');
             
      });    
  });
</script>
<script type="text/javascript">
         $(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
    
</script>
<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>