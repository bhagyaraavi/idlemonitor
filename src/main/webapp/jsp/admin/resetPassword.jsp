<!DOCTYPE html>
<html lang="en">
<head>
	<title>Idle Monitor : Reset Password</title>
	<meta charset="UTF-8">
<!--  COMMON JSP FILE  -->
	<%@ include file="/jsp/common/common.jsp"%>
	 
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt " data-tilt>
					<img src="${pageContext.request.contextPath}/images/forgot.png" alt="IMG">
				</div>
				<div class="left">
  
					<c:if test="${ not empty status}">
					     <div id="login_head_text" class="status">
								${status}
						 </div>
					  </c:if>
					<c:if test="${empty status && not empty error}">
					<div id="login_head_text" class="status" style="color:#FF0000;">
						${error}
					</div>
				  </c:if>
				<form class="login100-form " action="${pageContext.request.contextPath}/user/resetpassword.do" method = "POST" >
					<span class="login100-form-title">
						Reset Your Password
					</span>
					<!-- <p class="text-center">Check your email for the password reset code. Then click the link or enter the reset code below to reset your password</p><br/>
					 -->
					<%-- <div class="wrap-input100 >
						<input class="input1001" type="password" name="" placeholder=" Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-code" aria-hidden="true"></i>
						</span>
					</div> --%>

					
					<div class="wrap-input100 validate-input" >
						<input class="input1001" type="password" name="password" placeholder="New Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" >
						<input class="input1001" type="password" name="confirmPassword" placeholder="Re-enter New Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<input type="hidden" name="userId" value="${userId}" />
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Reset Password
						</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
	

</body>
</html>