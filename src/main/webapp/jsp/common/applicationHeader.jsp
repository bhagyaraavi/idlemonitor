 
 <nav class="navbar navbar-expand-sm bg-dark navbar-dark mn_menu">

        <!-- Brand/logo -->
        <a class="navbar-brand" href="#">
    <img src="${pageContext.request.contextPath}/images/logo.png" alt="logo" style="width:180px;">
  </a>
        <div class="hamburger">
            <div class="cta">
                <div class="toggle-btn type14"></div>
            </div>
        </div>

        <!-- Links -->
         <div class="navbar-nav ml-auto navbar-right">
            <button onclick="getConfigData()" data-toggle="modal" data-target="#squarespaceModal" class="center-block"><i class="fa fa-cog"></i></button>
                <div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <center><span class="modal-title">Configuration</span></center>
                            <button type="button" class="close" data-dismiss="modal">�</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="price-box">
                                <form class="form-horizontal form-pricing" role="form">
                              <%--   <form method="post" id="idletime_configuration" name="idleTimeConfiguration" class="form-horizontal form-pricing" action="${pageContext.request.contextPath}/user/idletimeconfiguration.do"   autocomplete="off"  > --%>
                                	
                                  <div class="price-slider">
                                        <h5>Idle Time Configuration</h5>
                                        <p>set the idle time for employees or users for to save the power</p>
                                    <h4 class="great">Minutes </h4>
                                    <div class="col-sm-12">
                                      <div id="slider"></div>
                                    </div>
                                  </div>
                                  <div class="price-form">
                                    <div class="form-group">
                                      <div class="col-sm-12">
                                        <input type="hidden" id="idle_minutes" name="idleMinutes"class="form-control">
                                        <p class="price lead" id="idle_minutes_label"></p><span class="price lead"></span> <h6>Minutes </h6>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="price-form1 text-center">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary" onclick="saveorUpdateConfiguration()">Update</button>
                                  </div>
                              </div>
                                </form>
                            </div>
                        </div>
                      </div>
                    </div>
              </div>
            <ul>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user"></i> <b class="caret"></b></a>
                <ul  class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/user/editadminregistration.do?adminUserId=${sessionScope.userId}">Edit Registration</a></li>
                    <li><a href="${pageContext.request.contextPath}/logoutsuccess.do">Logout</a></li>
                </ul>
            </li>
        </ul>
        </div>
        
    </nav>