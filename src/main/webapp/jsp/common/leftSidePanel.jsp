<div class="sidebar-menu full-side-bar flowHide">
            <nav class="">
                <ul class="navbar-nav">
                    <li  class="nav-item ">
                        <a class="nav-link active" id="dashboard" href="${pageContext.request.contextPath}/user/admindashboard.do">
                        <span class="sidebar-icon"><i class="fa fa-dashboard"></i></span>
                        <span class="nav-link-name tax-show">Dashboard</span>
                        </a>
                    </li>
                    <li  class="nav-item">
                        <a class="nav-link" id="employees" href="${pageContext.request.contextPath}/employee/viewemployees.do">
                        <span class="sidebar-icon"><i class="fa fa-users"></i></span>
                        <span class="nav-link-name tax-show">Employees</span>
                        </a>
                    </li>
                    
                     <li class="nav-item">
                        <a class="nav-link" id="screenshots" href="${pageContext.request.contextPath}/employee/viewscreenshots.do?userId=0">
                        <span class="sidebar-icon"><i class="fa fa-camera"></i></span>
                        <span class="nav-link-name tax-show">Screenshots</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="work_hours" href="${pageContext.request.contextPath}/employee/viewworkhours.do">
                        <span class="sidebar-icon"><i class="fa fa-clock-o"></i></span>
                        <span class="nav-link-name tax-show">Work Hours</span>
                        </a>
                    </li>
                     <li class="nav-item">
                        <a class="nav-link" id="projects" href="${pageContext.request.contextPath}/project/viewprojects.do">
                        <span class="sidebar-icon"><i class="fa fa-tasks"></i></span>
                        <span class="nav-link-name tax-show">Projects/Tasks</span>
                        </a>
                    </li>
                     <!-- Created by Harshitha on 24th,June 2021 for weekly work Hours Report -->
                    <li class="nav-item">
                        <a class="nav-link" id="work_hours" href="${pageContext.request.contextPath}/employee/weeklyworkedhoursreport.do">
                        <span class="sidebar-icon"><i class="fa fa-clock-o"></i></span>
                        <span class="nav-link-name tax-show">Weekly Hours</span>
                        </a>
                    </li>
                    <li  class="nav-item">
                        <a class="nav-link" id="reports" href="#">
                        <span class="sidebar-icon"><i class="fa fa-file"></i></span>
                        <span class="nav-link-name tax-show">Reports</span>
                        </a>
                    </li>
                   
                </ul>
            </nav>
        </div>