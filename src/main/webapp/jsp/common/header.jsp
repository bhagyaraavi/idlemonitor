<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark">
        <div class="container">
            <a class="navbar-brand  col-md-5" href="index.html">
                <div class="logo_new"><img src="${pageContext.request.contextPath}/images/logo.png" alt=""></img></div>
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars mfa-white"></span>
            </button>
            <div class="col-md-7">
                <div class="collapse navbar-collapse  right" id="navbarResponsive">
                    <div class="form-inline my-2 my-lg-0">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-link">
                                <div class="btn-group show-on-hover">
        				          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
        				            Products <span class="caret"></span>
        				          </button>
        				          <ul class="dropdown-menu" role="menu">
        				            <li><a href="#">Idle Timer <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry</span></a></li>
        				            <li class="divider"></li>
                                    <li><a href="#">Monitor Tool <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry</span></a></li>
        				          </ul>
        				        </div>
                            </li>
                            <c:if test="${empty sessionScope.username}" >
                            <li class="nav-link">
                                <a class="mn_top_1" href="${pageContext.request.contextPath}/login.do" >SIGN IN</a>
                            </li>
                            </c:if>
                            <!-- 
                            <li class="nav-link">
                                <a class="btn btn-primary btn-block btn-register" href="#">Register</a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>