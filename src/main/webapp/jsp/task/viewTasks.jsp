<!--
 Created by Harshitha on 30th April 2021
 JSP page to View Tasks 
 -->


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Idle Monitor : Tasks</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataTables.bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pagination.css">
    <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/vendor/jquery/dataTables.bootstrap4.min.js"></script>
<script src="${pageContext.request.contextPath}/js/project/project.js" type="text/javascript"></script>

   <%@ include file="/jsp/common/common.jsp"%>
   <script>
   $(document).ready(function()
		   {
		$('a').removeClass('active');
		$('#projects').addClass('active');
		var sortBy=$('#sortBy').val();
		var sortDirection=$('#sortDirection').val();
		if(sortDirection=='true')
		{
			jQuery("#task_name,#task_id,#creation_date,#status").removeClass('sorting').addClass('sorting_asc');
			
		}
		else{
			jQuery("#task_name,#task_id,#creation_date,#status").removeClass('sorting').addClass('sorting_desc');
		}
		
});
   </script>
</head>
<body>
<!--===============================================
                      NAVBAR
===============================================-->

   <%@ include file="/jsp/common/applicationHeader.jsp"%>

    <section class="side-bar-warp">
         <%@ include file="/jsp/common/leftSidePanel.jsp"%>
        
        <div class="dashboard-stats">
            <div class="col-md-12">
            	 <div class="row">
                    <ul class="breadcrumb">
                        <li class="completed"><a href="${pageContext.request.contextPath}/project/viewprojects.do">Projects</a></li>
                        <li class="active"><a href="javascript:void(0);">Task Management</a></li>
                    </ul>
                    </div>
                    <!--
                   for tittle change
                    -->
                  <%--   <div class="header"><h6>${title}</h6> 
                     <p><a id="period" class="mn_active" href="$getprojectName{pageContext.request.contextPath}/project/viewProject.do?period"></a>
               </p>
                </div> --%>
               
              
                
                
                
                <form:form commandName="displayListBean" method="POST" class="form-horizontal" action="${pageContext.request.contextPath}/project/viewtasks.do" role="form">
								<form:hidden path="sortBy" value="${displayListBean.sortBy}" id="sortBy" />
								<form:hidden path="searchBy" />
								<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection" />
								<form:hidden path="pagerDto.range" />
								<form:hidden path="pagerDto.pageNo" />
								<form:hidden path="pagerDto.projectId" value="${projectId}"/>

								<c:set var="range" value="${displayListBean.pagerDto.range}" />
								<input type="hidden" id="projectId"   value="${projectId}"/>
								<!-- SELECT  PAGE RANGE -->
								
							<div class="row">
									<div class="col-md-3">
										<div class="dataTables_length" id="dataTableId_length" 	style="margin-left: 10px;">
											<label style="font-weight: 900;">Show 
											<select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
													<option value="10" ${range=='10' ? 'selected' : '' }>10</option>
													<option value="25" ${range=='25' ? 'selected' : '' }>25</option>
													<option value="50" ${range=='50' ? 'selected' : '' }>50</option>
													<option value="100" ${range=='100' ? 'selected' : '' }>100</option>
											</select> entries
											</label>
										</div>
									</div>
									
								
									
									<!-- END OF SELECT PAGE RANGE -->
									<!-- SEARCH DIVISION -->
									<div class="col-md-9">
									
										<div class="row">
											<div class="col-md-1 no_pad">
											<label>Search By:</label>
											</div>
											<div class="col-md-3 no_pad_left">
												<input type="text" id="searchText" name="searchText" placeholder="Search for tasks "	value="">
											</div>
											
									<div class="col-md-8 no_pad_left">
									
											<div class="no_pad_left pull-right">												
        										<a href="" class="btn btn-default mn_proj btn-rounded mb-2" data-toggle="modal" data-target="#modalAddTaskForm">Add task</a>
											<!--  ADD Task Popup -->
											<div class="modal fade" id="modalAddTaskForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									          <div class="modal-dialog" role="document">
									            <div class="modal-content mn_model_cont">
									              <div class="modal-header text-center">
									                <h4 class="modal-title w-100 font-weight-bold">Add Task</h4>
									                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									                  <span aria-hidden="true">&times;</span>
									                </button>
								              	</div>
													<div class="modal-body" id="task_popup_0">
																	              
														<form class="login100-form validate-form" role="form">				              
															
															<div class="wrap-input100">
															<input id="task_name_0" name="taskName" placeholder="Task Name" type="text" class="input1001" value="">
																<span id="taskName_error_0" class="error_msg"></span>
																<span class="focus-input100"></span>
																
															</div>
															
															 
															<div class="container-login100-form-btn">
																<button class="login100-form-btn" onclick="return saveorUpdateTask(0);">
																	ADD
																</button>
															</div>
															
														</form>
																	              
													 		</div>
														</div>
													</div>
												</div>
												<!--  END Of Add Task  -->
											</div>
											
											<div class="no_pad_left pull-left">
												<button class="btn mn_none1" type="button" value="Search"
													onclick="searchResults();">Search</button>
											</div>
											<div class="no_pad_right pull-left">
												<button class="btn mn_none" type="button"
													onclick="window.location='${pageContext.request.contextPath}/project/viewtasks.do?projectId=${projectId}'">Reset</button>
											</div>
									</div>
										</div>
									</div>
								</div>
								<!-- END OF Search divison -->

								
							<div class="clearfix"></div>
								<div class="col-md-12 nopad">
								<div class="dataTables_wrapper table-responsive">
								<c:choose>
           							<c:when test="${empty message }">
											<table id="dataTableId" class="display dataTable table"	role="grid" aria-describedby="dataTableId_info"	style="width: 100%;">
												<thead>
													<tr>
														<th class="sorting" id="task_name" onclick="sortResults('taskName')">Task Name</th>
													<th class="sorting" id="project_name" onclick="sortResults('projectName')">Project Name</th> 	
													 <th class="sorting" id="task_creation_date" onclick="sortResults('taskCreationDate')">Task Creation Date</th>
														<th  id="status" onclick="sortResults('status')">Status</th>
														<th id="edit_task">Edit Task</th>
														<th id="delete_task">Delete Task</th>
												</tr>
												</thead>
												<tbody>
													
													<c:forEach var="taskDto" items="${taskDtos}">
														<tr>
															
														<td id="task_${taskDto.taskId}">${taskDto.taskName}</td>
														<!-- Check -->	
														<td id="project_${taskDto.projectDto.projectId}">${taskDto.projectDto.projectName}</td> 
														
															<fmt:formatDate	value='${taskDto.taskCreationDate}'	pattern='MM/dd/yyyy hh:mm:ss' var="taskCreationDate" />
															<td id="task_creation_date_${taskDto.taskId}">${taskCreationDate}</td>
                											<td >
                												<input type="hidden" id="accountStatus${taskDto.taskId}" value="${taskDto.isActive}">
                												<c:choose>
                													<c:when test="${taskDto.isActive==true}">
                														<c:set var="taskStatus" value="active"></c:set>
                													</c:when>
                													<c:otherwise>
                														<c:set var="taskStatus" value="inactive"></c:set>
                													</c:otherwise>
                												</c:choose>
						                      					
						                      					<input type="hidden" id="taskStatus${taskDto.taskId}" value="${taskStatus}">
																<select id="status_${taskDto.taskId}"  onchange="changeStatus(this,${taskDto.taskId});">
											                    <option value="active" ${taskStatus=='active' ? 'selected' : '' }>Active</option>
											                    <option value="inactive" ${taskStatus=='inactive' ? 'selected' : '' }>Inactive</option>
											                   
											                   </select>
											                 </td>
															
											                 
											                 <td>
											                
											                 <a href=""  class="btn btn-default btn-rounded mb-2"data-toggle="modal" data-target="#modalEditTaskForm${taskDto.taskId}"><i class="fa fa-edit" aria-hidden="true"></i></a>
											                 
											                 <!-- Edit Project popup -->	
											                 
											<div class="modal fade" id="modalEditTaskForm${taskDto.taskId}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									          <div class="modal-dialog" role="document">
									            <div class="modal-content mn_model_cont">
									              <div class="modal-header text-center">
									                <h4 class="modal-title w-100 font-weight-bold">Edit Task</h4>
									                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									                  <span aria-hidden="true">&times;</span>
									                </button>
									              	</div>
													<div class="modal-body" id="task_popup_${taskDto.taskId}">
												<form class="login100-form validate-form" role="form">				              
															<div class="wrap-input100">
															<input id="task_name_${taskDto.taskId}"  placeholder="Task Name" type="text" class="input1001" value="${taskDto.taskName}">
																<span id="taskName_error_${taskDto.taskId}" class="error_msg"></span>
																<span class="focus-input100"></span>
																
															</div>
															
													 	<%-- <div class="wrap-input100">
																<input id="project_name_${taskDto.projectDto.projectName}"  placeholder="Project Name" type="text" class="input1001" value="${taskDto.projectDto.projectName}">
																<span id="project_error_${taskDto.projectDto.projectName}" class="error_msg"></span>
																<span class="focus-input100"></span>
																
															</div> --%>   	
															
														
															<div class="container-login100-form-btn">
																
																       
																<button class="login100-form-btn" onclick="return saveorUpdateTask(${taskDto.taskId})">
																	EDIT
																</button>
																 
																
                                                                                                    																
                                                               	
                                                              							
															</div>
															
														</form>
																	              
													 		</div>
														</div>
													</div>
												</div>
											               
											               </td>  
															<td id="delete_task_${taskDto.taskId}"><a style="font-size:20px;position:relative;top:-5px;" onclick="deleteTask(this,${taskDto.taskId});"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												
												</td>
												</tr>
													</c:forEach>
													
													
												</tbody>
											</table>
										
										<div class="clearfix"></div>
											
											<c:set var="first" value="0" />
											<c:set var="end"
												value="${displayListBean.pagerDto.pagesNeeded }" />
											<c:set var="page" value="${displayListBean.pagerDto.pageNo}" />
											<c:set var="total"
												value="${displayListBean.pagerDto.totalItems}" />
											<c:set var="firstResult"
												value="${displayListBean.pagerDto.firstResult}" />
										<c:set var="lastResult"
												value="${displayListBean.pagerDto.lastResult}" />
											<div class="dataTables_info" id="dataTableId_info"
												role="status" aria-live="polite">Showing
												${firstResult} to ${lastResult} of ${total} entries</div>
											<div class="dataTables_paginate paging_simple_numbers"
												id="dataTableId_paginate">

												<%@ include file="/jsp/common/pager.jsp"%>

											</div>
										</c:when>
									
									<c:otherwise>
										<h6 class="no_msg">${message}</h6>
									</c:otherwise>
									
									
									</c:choose>
									</div>
								</div>
							</form:form>
            
        </div>
    </div>
    </section>


<script type="text/javascript">
    

$(document).ready(function () {
    $(document).on('click', '.cta', function () {
       $(this).toggleClass('active')
    })
});


$(document).ready(function(){
    $(".hamburger").click(function(){
        $('.sidebar-menu').removeClass("flowHide");  
        $(".sidebar-menu").toggleClass("small-side-bar");
        $('.nav-link-name').toggleClass('name-hide');        
    });
});




 $(document).ready(function () {    
      $(".nav-link").hover(function () {           
          $('.sidebar-menu').removeClass("flowHide");  
          $(this).addClass('tax-active');
              
      }, function () {
          $('.sidebar-menu')
             .addClass("flowHide");
          $(this).removeClass('tax-active');
             
      });    
  });
</script>


<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>