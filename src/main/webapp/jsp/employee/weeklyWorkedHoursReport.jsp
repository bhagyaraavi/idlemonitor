<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
   <title>Idle Monitor : Weekly Work Hours</title>

   <%@ include file="/jsp/common/common.jsp"%>
   
<!--     <style type="text/css">
    .right{
      box-shadow: 0px 0px 5px #ddd;
      padding: 15px
    }
    .input-group{
    	display:block
    }
    .input-group input{
    	width:100% !important;
    	float:left;
    }
    .mn_middle_content{
      text-align: center;
      width:100%
    }
    .right label{
      width: 100%;
      text-align: left;
    }
    .input-group .btn{
      text-align: center;display: table;
    margin: 30px auto 0;
    }

    .input-group{
      position: relative;
    }
    .input-group i{
          margin: 0;
          position: absolute;
    right: 10px;
    top: 37px;

          z-index:  999;
    }
  </style> -->
   
</head>
<body>
<!--===============================================
                      NAVBAR
===============================================-->


     <%@ include file="/jsp/common/applicationHeader.jsp"%>


    <section class="side-bar-warp">
       <%@ include file="/jsp/common/leftSidePanel.jsp"%>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/employee/employee.js"></script>
       
        <div class="dashboard-stats">
            <div class="col-md-12">
                 <div class="row">
                    <ul class="breadcrumb">
                        <li class="completed"><a href="javascript:void(0);">Idle Monitor</a></li>
                        <li class="active"><a href="javascript:void(0);">Weekly WorkHours</a></li>
                    </ul>
                  


 <div class="main-content mn_middle_content input-color">
    <div class="container mt-4">
      <div class="row align-items-center justify-content-center">
      
      <form method="post" id="view_weekly_work_hours" name="viewWeeklyWorkHours" class="validate-form" action="${pageContext.request.contextPath}/employee/viewweeklyworkedhours.do"   autocomplete="off" onsubmit="return validateFromDateAndToDate();">
				<c:if test="${not empty message}">
				<h2>${message}</h2>
				</c:if>
				
        <div class="col-md-12 ">
          <h2 class="mb-3">Weekly Reports</h2>
          <div class="right">
          <div class="row"><!-- 
            <h4>Select Weekly report</h4> -->
            <div class="col-md-6">
            <div class="input-group no-border input-lg ">
              <label>Select From</label>
              <input type="text" name="fromDate" class="form-control" id="fromDate"  placeholder="Select Date..." readonly='true' >
               <span id="fromDate_error" class="error_msg"></span>
               <i class="fa fa-calendar-o" aria-hidden="true"></i>
            </div>
            </div>
            <div class="col-md-6">
            <div class="input-group no-border input-lg ">
              <label>Select To</label>
              <input type="text" name="toDate" class="form-control" id="toDate" placeholder="Select Date..." readonly='true'>
              <span id="toDate_error" class="error_msg"></span>
              <i class="fa fa-calendar-o" aria-hidden="true"></i>
            </div>
            </div>
            <div class="input-group">
              <button class="btn btn-success">Generate Report</button>
            </div>
          </div>
        </div>
      </div>
      </form>
      
    </div>
  </div>
  </div>
  
  
  </div>
  </div>
  </div>
 

<script>
  $( function() {
    $( "#fromDate" ).datepicker({
    		onSelect: function(selected) {
    			var endDate=$("#fromDate").datepicker('getDate','+7D');
    			endDate.setDate(endDate.getDate()+7);
                $("#toDate").datepicker("option","minDate", selected)
                $("#toDate").datepicker("option","maxDate",endDate)
              }
    }); 
   
  } );
  $( function() {
    $( "#toDate" ).datepicker({
    	onSelect: function(selected) {
        $("#fromDate").datepicker("option","maxDate", selected)
    	}
    });
   
  } );
  

  $(document).ready(function () {
      $(document).on('click', '.cta', function () {
         $(this).toggleClass('active')
      })
  });


  $(document).ready(function(){
      $(".hamburger").click(function(){
          $('.sidebar-menu').removeClass("flowHide");  
          $(".sidebar-menu").toggleClass("small-side-bar");
          $('.nav-link-name').toggleClass('name-hide');        
      });
  });




   $(document).ready(function () {    
        $(".nav-link").hover(function () {           
            $('.sidebar-menu').removeClass("flowHide");  
            $(this).addClass('tax-active');
                
        }, function () {
            $('.sidebar-menu')
               .addClass("flowHide");
            $(this).removeClass('tax-active');
               
        });    
    });
  </script>
  
   </section>
<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>