<!DOCTYPE html>
<html lang="en">
<head>
    <title>Idle Monitor : Employee Idle Timings</title>
    <%@ include file="/jsp/common/common.jsp"%>
    <script type="text/javascript">
    $(document).ready(function(){
		var period=$('#period').val();
		$('a').removeClass('mn_active');
		$('#period'+period).addClass('mn_active');
    });
    </script>
</head>
<body>
<!--===============================================
                      NAVBAR
===============================================-->


   <%@ include file="/jsp/common/applicationHeader.jsp"%>


    <section class="side-bar-warp">
       <%@ include file="/jsp/common/leftSidePanel.jsp"%>
        
        <div class="dashboard-stats">
            <div class="col-md-12">

                <div class="row">
                    <ul class="breadcrumb">
                        <li class="completed"><a href="${pageContext.request.contextPath}/employee/viewworkhours.do">Work Hours</a></li>
                        <li class="active"><a href="javascript:void(0);">${employeeName} Work Time Details</a></li>
                    </ul>
                     <input type="hidden" name="period" id="period" value="${period}" ></input>
                    <div class="header"><h6>${title}</h6> 
                    <p><a id="period0" class="mn_active" href="${pageContext.request.contextPath}/employee/employeeidletimings.do?employeeUserId=${employeeUserId}&period=0">Yesterday</a>
                    <a id="period6" href="${pageContext.request.contextPath}/employee/employeeidletimings.do?employeeUserId=${employeeUserId}&period=6">7 Days</a> 
                    <a id="period13" href="${pageContext.request.contextPath}/employee/employeeidletimings.do?employeeUserId=${employeeUserId}&period=13">14 Days</a> 
                    <a id="period31" href="${pageContext.request.contextPath}/employee/employeeidletimings.do?employeeUserId=${employeeUserId}&period=31">One month</a> 
                    <input type="text" id="datepicker" name="date" value="${selectedDate}" class="form-control" onselect="dateSelection();"   placeholder="Date Selection">
                    </p></div>
                    
                    <input type="hidden"  id="employeeUserId" value="${employeeUserId}" />
                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                <div class="tile">
                        <!-- <div class="employee_details employee_details_1">

                            <div class="dates mn_dates">
                            <div class="start">
                                    <strong>Work Date</strong>
                                    <span></span>
                                </div>
                                <div >
                                    <strong>Login Date&Time</strong>
                                    <span></span>	
                                </div>
                                <div class="ends">
                                    <strong>Idle Date&Time</strong>
                                </div>
                                <div >
                                    <strong>Logout Date&Time</strong>
                                </div>
                            </div>

                        </div> -->
                        <%-- <div class="employee_details employee_details_2">
                        
                        	<c:forEach var="employeeUserDetailsDto" items="${employeeUserDetailsDtos}">
                            <div class="dates"> --%>
                            <%-- <div class="start">
                            		<fmt:formatDate	value='${employeeUserDetailsDto.workDate}'	pattern='MM/dd/yyyy' var="employeeWorkDate" />
                                   ${employeeWorkDate}
                                    <span></span>
                                </div>
                                <div >
                               		<fmt:formatDate	value='${employeeUserDetailsDto.loginDateTime}'	pattern='MM/dd/yyyy hh:mm:ss' var="employeeLoginDateTime" />
                                   ${employeeLoginDateTime}
                                    <span></span>
                                </div>
                                <div class="ends">
                               	 	<fmt:formatDate	value='${employeeUserDetailsDto.idleDateTime}'	pattern='MM/dd/yyyy hh:mm:ss' var="employeeIdleDateTime" />
                                    ${employeeIdleDateTime}
                                </div>
                                <div >
                                	<fmt:formatDate	value='${employeeUserDetailsDto.logoutDateTime}'	pattern='MM/dd/yyyy hh:mm:ss' var="employeeLogoutDateTime" />
                                    ${employeeLogoutDateTime}
                                </div> --%>
                                <table style="width: 100%;" class="mn_table_employee">
												<thead>
													<tr>
														<th>Work Date</th>
														<th>Login Time</th>
														<th>Idle Time</th>
														<th>Logout Time</th>
														
													</tr>
												</thead>
												<tbody>
													<c:choose>
                        								<c:when test="${empty message}">
													<c:forEach var="employeeUserDetailsDto" items="${employeeUserDetailsDtos}">
														<tr>
															<fmt:formatDate	value='${employeeUserDetailsDto.workDate}'	pattern='MM/dd/yyyy' var="employeeWorkDate" />
															<td >${employeeWorkDate}</td>
															<fmt:formatDate	value='${employeeUserDetailsDto.loginDateTime}'	pattern='hh:mm:ss a' var="employeeLoginDateTime" />
															<td >${employeeLoginDateTime}</td>
															<fmt:formatDate	value='${employeeUserDetailsDto.idleDateTime}'	pattern='hh:mm:ss a' var="employeeIdleDateTime" />
															<td >${employeeIdleDateTime}</td>
															<fmt:formatDate	value='${employeeUserDetailsDto.logoutDateTime}'	pattern='hh:mm:ss a' var="employeeLogoutDateTime" />
															<td > ${employeeLogoutDateTime}</td>
                										
														</tr>
														</c:forEach>
														</c:when>
							                        	<c:otherwise>
															<tr>
																<td colspan="4"><h6 class="no_msg">${message}</h6></td>
															</tr>
														
								                        </c:otherwise>
								                        </c:choose>
												</tbody>
												 
											</table>
                           
                         
                     </div>
               

            </div>
            
                    <div class="col-md-6">
                        <div class="Utilization">
                        <h2>Idle Time Statistics</h2>
                        <div id="container"></div>
                    </div>
                    </div>
                </div>
                 </div> 
                <div class="row">
                    
            
                </div>
            </div>
       
    </section>


   


<script type="text/javascript">
    

$(document).ready(function () {
	//$("#datepicker").datepicker({ dateFormat:"MM/dd/yyyy"});
    $(document).on('click', '.cta', function () {
        $(this).toggleClass('active')
    })
});


$(document).ready(function(){
    $(".hamburger").click(function(){
        $('.sidebar-menu').removeClass("flowHide");  
        $(".sidebar-menu").toggleClass("small-side-bar");
        $('.nav-link-name').toggleClass('name-hide');        
    });
});
$('#datepicker').change(function() { 
	var date=$('#datepicker').val();
	var employeeUserId=$('#employeeUserId').val();
	var url = "${pageContext.request.contextPath}/employee/employeeidletimings.do?employeeUserId="+employeeUserId+"&selectedDate="+date;
	window.open(url,"_self");
});

 $(document).ready(function () {    
      $(".nav-link").hover(function () {           
          $('.sidebar-menu').removeClass("flowHide");  
          $(this).addClass('tax-active');
              
      }, function () {
          $('.sidebar-menu')
             .addClass("flowHide");
          $(this).removeClass('tax-active');
             
      });    
  });
</script>
<script type="text/javascript">
         $(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
    
</script>
<script type="text/javascript">
    $( function() {
        $( "#datepicker" ).datepicker({
        	dateFormat: 'mm-dd-yy',
            numberOfMonths: 2,
            maxDate: "+0",
        });
    } );
   
   $(document).ready(function () {   
	  /*  Highcharts.chart('container', {
		   credits: {
	    	    enabled: false //for removing the watermark
	    	  },
			 chart: {
	                type: 'bar'
	            },
	            title: {
	                text: ''
	            },
	            xAxis: {
	                categories: ['10m:15s', '15m:25s', '23m:10s']
	            },
	            yAxis: {
	                title: {
	                    text: 'Fruit eaten'
	                }
	            },
	            series: [{
	                name: 'Jane',
	                data: [1, 0, 4]
	            }, {
	                name: 'John',
	                data: [5, 7, 3]
	            }]
	        }); */
	   
	   Highcharts.chart('container', {
		   chart: {
		     type: 'column'
		   },
		   title: {
		     text: 'Column chart with negative values'
		   },
		   xAxis: {
		     type: 'datetime'
		   },
		   yAxis: {
		     type: 'datetime',
		     dateTimeLabelFormats: { //force all formats to be hour:minute:second
		       second: '%H:%M',
		       minute: '%H:%M',
		       hour: '%H:%M',
		       day: '%H:%M',
		       week: '%H:%M',
		       month: '%H:%M',
		       year: '%H:%M'
		     }
		   },
		   tooltip: {
		     pointFormat: "<span style='color:{point.color}'>\u25CF</span> {series.name}: <b>{point.label}</b><br/>"
		   },
		   credits: {
		     enabled: false
		   },
		   series: [{
		     name: 'Total',
		     data: [1, 2, 4]
		   }, {
		     name: 'Out of total',
		     data: [5, 7, 3]
		   }]
		 });
	 });
	 
</script>
<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html> 

