<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Idle Monitor : View Weekly Work Hours</title>
<%@ include file="/jsp/common/common.jsp"%>
<!-- <style type="text/css">
    .right{
      box-shadow: 0px 0px 5px #ddd;
      padding: 15px;
      width:100%;
      float:left
    }
    </style> -->

<!-- <style type="text/css">
    .mn_proj, .btn-unique{nb 
    
      background: #0e1237e;
      border:solid 2px #0e1237;
      color: #fff;
      margin: 0 0 0 10px;
      border-radius: 25px;
      padding: 5px 20px 7px
    }
    .mn_proj:hover, .btn-unique:hover{
      color: #0e1237;
      border:solid 2px #0e1237;
      background: none
    }
    .modal-title{
      color: #bf8a62
    }
    .form-control{
      border-radius: 20px;
    }
    .mn_tas{
      background: #bf8a62;
      border:solid 2px #bf8a62;
      color: #fff;
      border-radius: 25px;
      padding: 5px 20px 7px
    }
    .mn_tas:hover{
      color: #bf8a62;
      border:solid 2px #bf8a62;
      background: none
    }
    .avatar img {
    width: 25px;
    border-radius: 50%;
    }
    .shadow span{
      font-weight: normal;
    } 
    .media-body span{
      font-weight: 700;
    }
    .right{
      float: right;
    }
    .right p{
          display: inline;
          margin: 0 10px 0 0
    }
    .main-content{
    	width:100%;
    	float:left;
    	padding:0 15px
    }
    .card{
    	padding:0
    }
  </style> -->

</head>
<body>

<%@ include file="/jsp/common/applicationHeader.jsp"%>

<section class="side-bar-warp">
       <%@ include file="/jsp/common/leftSidePanel.jsp"%>
       
        <div class="dashboard-stats">
            <div class="col-md-12">
                 <div class="row">
                    <ul class="breadcrumb">
                        <li class="completed"><a href="javascript:void(0);">Idle Monitor</a></li>
                        <li class="active"><a href="javascript:void(0);">Weekly WorkHours</a></li>
                    </ul>
                  


  <div class="main-content">
    <div class=" mt-4">
      <div class="row">
        <div class="col-12">
      <h2 class="mb-3">Weekly Worked Hours</h2>
    </div>
        <div class="col-12">
          
      <div class="right">

     
      <div class="row">

        <div class="col">
          <div class="card shadow">
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                   
                    <th scope="col" id="employee_name">Employee Name</th>
                   
                    <th scope="col">${titleDto.day1Title}<br/></th>
                    <th scope="col">${titleDto.day2Title}<br/></th>
                    <th scope="col">${titleDto.day3Title}<br/></th>
                    <th scope="col">${titleDto.day4Title}<br/></th>
                    <th scope="col">${titleDto.day5Title}<br/></th>
                    <th scope="col">${titleDto.day6Title}<br/></th>
                    <th scope="col">${titleDto.day7Title}<br/></th>
                    <th scope="col">Total</th>
                   
                  </tr>
                </thead>
                <tbody>
                <c:forEach var="weeklyWorkHoursDto" items="${weeklyWorkHoursDtos}">
                  <tr>
                    <td scope="row">
                      <div class="media align-items-center">
                       <!--  <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="images/logo.png">
                        </a> -->
                        <div class="media-body">
                          <span class="mb-0 text-sm" id="employee_name_ ${weeklyWorkHoursDto.userDto.userId}">${weeklyWorkHoursDto.userDto.employeeName}
                          </span>
                        </div>
                      </div>
                    </td>
                    <td id="day1Hours_ ${weeklyWorkHoursDto.userDto.userId}">
                   ${weeklyWorkHoursDto.day1Hours}
                    </td>
                    <td id="day2Hours_ ${weeklyWorkHoursDto.userDto.userId}">
                     ${weeklyWorkHoursDto.day2Hours}
                    </td>
                    <td id="day3Hours_ ${weeklyWorkHoursDto.userDto.userId}">
                      ${weeklyWorkHoursDto.day3Hours}
                    </td>
                    <td id="day4Hours_ ${weeklyWorkHoursDto.userDto.userId}">
                     ${weeklyWorkHoursDto.day4Hours}
                    </td>
                    <td id="day5Hours_ ${weeklyWorkHoursDto.userDto.userId}">
                     ${weeklyWorkHoursDto.day5Hours}
                    </td>
                    <td id="day6Hours_ ${weeklyWorkHoursDto.userDto.userId}">
                     ${weeklyWorkHoursDto.day6Hours}
                    </td>
                    <td id="day7Hours_ ${weeklyWorkHoursDto.userDto.userId}">
                      ${weeklyWorkHoursDto.day7Hours}
                    </td>
                    <td id="totalHours_ ${weeklyWorkHoursDto.userDto.userId}">
                      <strong>${weeklyWorkHoursDto.totalHours}</strong>
                    </td>
                  </tr>
                  </c:forEach>
                </tbody>
              </table>
            </div>
            <div class="card-footer py-1">
              <nav aria-label="...">
                <ul class="pagination justify-content-end mb-0">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">
                      <i class="fa fa-angle-left"></i>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#">
                      <i class="fa fa-angle-right"></i>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <!-- Dark table -->
      
    </div>
  </div>
  
  </div>
  </div>
  </div>
  
  </div>
  </div>
  </div>

<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  $( function() {
    $( "#datepicker1" ).datepicker();
  } );
  

  $(document).ready(function () {
      $(document).on('click', '.cta', function () {
         $(this).toggleClass('active')
      })
  });


  $(document).ready(function(){
      $(".hamburger").click(function(){
          $('.sidebar-menu').removeClass("flowHide");  
          $(".sidebar-menu").toggleClass("small-side-bar");
          $('.nav-link-name').toggleClass('name-hide');        
      });
  });




   $(document).ready(function () {    
        $(".nav-link").hover(function () {           
            $('.sidebar-menu').removeClass("flowHide");  
            $(this).addClass('tax-active');
                
        }, function () {
            $('.sidebar-menu')
               .addClass("flowHide");
            $(this).removeClass('tax-active');
               
        });    
    });
  </script>
  
   </section>
  
<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>