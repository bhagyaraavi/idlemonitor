
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Idle Monitor : Work Hours</title>
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataTables.bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pagination.css">
    <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/vendor/jquery/dataTables.bootstrap4.min.js"></script>
<script src="${pageContext.request.contextPath}/js/employee/employee.js"></script>
   <%@ include file="/jsp/common/common.jsp"%>
   
 <script>
   $(document).ready(function(){
		$('a').removeClass('active');
		$('#work_hours').addClass('active');
		var period=$('#period').val();
		$('a').removeClass('mn_active');
		$('#period'+period).addClass('mn_active');
		var sortBy=$('#sortBy').val();
		var sortDirection=$('#sortDirection').val();
		if(sortDirection=='true'){
			jQuery("#employee_name,#employee_number,#work_date,#work_hours").removeClass('sorting').addClass('sorting_asc');
			
		}
		else{
			jQuery("#employee_name,#employee_number,#work_date,#work_hours").removeClass('sorting').addClass('sorting_desc');
		}
		
});
   </script>
   <script type="text/javascript">
    $( function() {
        $( "#datepicker" ).datepicker({
            numberOfMonths: 2,
            maxDate: "+0",
        });
        $('#datepicker').change(function() { 
			var date=$('#datepicker').val();
			var url = "${pageContext.request.contextPath}/employee/viewworkhours.do?selectedDate="+date;
			window.open(url,"_self");
		});
    });
   
</script>

</head>
<body>
<!--===============================================
                      NAVBAR
===============================================-->


     <%@ include file="/jsp/common/applicationHeader.jsp"%>


    <section class="side-bar-warp">
       <%@ include file="/jsp/common/leftSidePanel.jsp"%>
       
        <div class="dashboard-stats">
            <div class="col-md-12">
                 <div class="row">
                    <ul class="breadcrumb">
                        <li class="completed"><a href="javascript:void(0);">Idle Monitor</a></li>
                        <li class="active"><a href="javascript:void(0);">WorkHours</a></li>
                    </ul>
                    <div class="header"><h6>${title}</h6> 
                    <input type="hidden" name="period" id="period" value="${period}" ></input>
                     <p><a id="period0" class="mn_active" href="${pageContext.request.contextPath}/employee/viewworkhours.do?period=0">Yesterday</a>
                    <a id="period6" href="${pageContext.request.contextPath}/employee/viewworkhours.do?period=6">7 Days</a> 
                    <a id="period13" href="${pageContext.request.contextPath}/employee/viewworkhours.do?period=13">14 Days</a> 
                    <a id="period31" href="${pageContext.request.contextPath}/employee/viewworkhours.do?period=31">One month</a> 
                    <input type="text" id="datepicker" name="date" value="${selectedDate}" class="form-control" onselect="dateSelection();"   placeholder="Date Selection">
                    </p>
                    </div>
                </div>
                <form:form commandName="displayListBean" method="POST" class="form-horizontal" action="${pageContext.request.contextPath}/employee/viewworkhours.do" role="form">
								<form:hidden path="sortBy" value="${displayListBean.sortBy}" id="sortBy" />
								<form:hidden path="searchBy" />
								<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection" />
								<form:hidden path="pagerDto.range" />
								<form:hidden path="pagerDto.pageNo" />
								<form:hidden path="period" value="${period}"/>
								<form:hidden path="selectedDate" value="${selectedDate}"/>

								<c:set var="range" value="${displayListBean.pagerDto.range}" />
								<!-- SELECT  PAGE RANGE -->
								
								<div class="row">
									<div class="col-md-6">
										<div class="dataTables_length" id="dataTableId_length" 	style="margin-left: 10px;">
											<label style="font-weight: 900;">Show 
											<select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
													<option value="10" ${range=='10' ? 'selected' : '' }>10</option>
													<option value="25" ${range=='25' ? 'selected' : '' }>25</option>
													<option value="50" ${range=='50' ? 'selected' : '' }>50</option>
													<option value="100" ${range=='100' ? 'selected' : '' }>100</option>
											</select> entries
											</label>
										</div>
									</div>
									<!-- END OF SELECT PAGE RANGE -->
									<!-- SEARCH DIVISION -->
									<%-- <div class="col-md-6">
										<div class="row">
											<div class="col-md-2 no_pad_left">
											<label>Search By:</label>
											</div>
											<div class="col-md-5 no_pad_left">
												<input type="text" id="searchText" name="searchText" placeholder="Search for employees "	value="${displayListBean.searchBy}">
											</div>
											
									<div class="col-md-5 no_pad_left">
									
											<div class="col-md-6 no_pad_left pull-right">
												<button class="btn mn_none" type="button"
													onclick="window.location='${pageContext.request.contextPath}/employee/viewworkhours.do'">Reset</button>
											</div>
											<div class="col-md-6 no_pad_left pull-right">
												<button class="btn mn_none1" type="button" value="Search"
													onclick="searchResults();">Search</button>
											</div>
									</div>
										</div>
									</div> --%>
								</div>
								<!-- END OF Search divison -->

								
							<div class="clearfix"></div>
								<div class="col-md-12 nopad">
								<div class="dataTables_wrapper table-responsive">
								<c:choose>
           							<c:when test="${empty message}">
											<table id="dataTableId" class="display dataTable table"	role="grid" aria-describedby="dataTableId_info"	style="width: 100%;">
												<thead>
													<tr>
														<th>Employee Name</th>
														<th>Employee Number</th>
														<th class="sorting" id="work_date" onclick="sortResults('workDate')">Work Date</th>
														<th class="sorting" id="work_hours" onclick="sortResults('workHours')">Work Hours</th>
														<th id="work_details">Work Timings</th>
														
														
													</tr>
												</thead>
												<tbody>
													
													<c:forEach var="userCompletedWorkHoursDto" items="${userCompletedWorkHoursDtos}">
														<tr>
															<td id="employee_name_ ${userCompletedWorkHoursDto.dataId}">${userCompletedWorkHoursDto.userDto.employeeName}</td>
															<td id="employee_number_${userCompletedWorkHoursDto.dataId}">${userCompletedWorkHoursDto.userDto.employeeNumber}</td>
															
															<fmt:formatDate	value='${userCompletedWorkHoursDto.workDate}'	pattern='MM/dd/yyyy' var="workDate" />
															<td id="work_date_${userCompletedWorkHoursDto.dataId}">${workDate}</td>
															<td id="work_hours_${userCompletedWorkHoursDto.dataId}">${userCompletedWorkHoursDto.workHours}</td>
                											
															<td id="work_details_${userCompletedWorkHoursDto.dataId}"><a href="${pageContext.request.contextPath}/employee/employeeidletimings.do?employeeUserId=${userCompletedWorkHoursDto.userDto.userId}&period=0">Work Timings</a></td>
															
														</tr>
													</c:forEach>
												</tbody>
											</table>
										
										<div class="clearfix"></div>
										
											<c:set var="first" value="0" />
											<c:set var="end"
												value="${displayListBean.pagerDto.pagesNeeded }" />
											<c:set var="page" value="${displayListBean.pagerDto.pageNo}" />
											<c:set var="total"
												value="${displayListBean.pagerDto.totalItems}" />
											<c:set var="firstResult"
												value="${displayListBean.pagerDto.firstResult}" />
											<c:set var="lastResult"
												value="${displayListBean.pagerDto.lastResult}" />
											<div class="dataTables_info" id="dataTableId_info"
												role="status" aria-live="polite">Showing
												${firstResult} to ${lastResult} of ${total} entries</div>
											<div class="dataTables_paginate paging_simple_numbers"
												id="dataTableId_paginate">

												<%@ include file="/jsp/common/pager.jsp"%>

											</div>
										</c:when>
									
									<c:otherwise>
										<h6 class="no_msg">${message}</h6>
									</c:otherwise>
									
									</c:choose>
									</div>
								</div>
								
							</form:form>


        </div>
        </div>
  
    </section>




<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>