 <!--  
	 Created By Harshitha on 15th Febraury 2021
     Page to edit/update the Employee Details
	-->

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Idle Monitor : Edit Profile</title>
	 <%@ include file="/jsp/common/common.jsp"%>
	 <script type="text/javascript" src="${pageContext.request.contextPath}/js/employee/employee.js"></script>
</head>
<body>
	
	 <%@ include file="/jsp/common/applicationHeader.jsp"%>
    <section class="side-bar-warp">
        <%@ include file="/jsp/common/leftSidePanel.jsp"%>
        <div class="dashboard-stats">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                    	<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="${pageContext.request.contextPath}/images/edit.png" alt="IMG">
				</div>
				<form:form method="post" id="edit_employee_registration" name="editemployeeRegistration" class="login100-form validate-form" action="${pageContext.request.contextPath}/user/editemployeeregistration.do" commandName="userDto"  autocomplete="off" onsubmit="return validateEditEmployeeRegistration();"
				enctype="multipart/form-data">
				<input type="hidden" id="userId" name="userId" value="${userId}" />
				<input type="hidden" id="current_employee_number" name="empNum" value="${userDto.employeeNumber}" />
				<input type="hidden" id="current_contact_email" name="email" value="${userDto.emailId}" />
				

					<span class="login100-form-title">
						Edit Employee Registration
					</span>
					<div class="wrap-input100">
					<form:input class="input1001" type="text" name="" placeholder="Employee Name" path="employeeName" id="employeeName" value="${employeeName}"/>
						<span id="employeeName_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-cube" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100">
					<form:input class="input1001" type="text" name="" placeholder="Employee Number" path="employeeNumber" id="employeeNumber" value="${employeeNumber}" />
						<span id="employeeNumber_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-cube" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100" >
						<form:input class="input1001" type="text" name="" placeholder="Contact Email" path="emailId" id="emailId" value="${emailId}"/>
						<span id="emailId_error" class="error_msg"></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>
				
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Update
						</button>
					</div>

					
				</form:form>
			</div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<script type="text/javascript">
    

$(document).ready(function () {
    $(document).on('click', '.cta', function () {
        $(this).toggleClass('active')
    })
});


$(document).ready(function(){
    $(".hamburger").click(function(){
        $('.sidebar-menu').removeClass("flowHide");  
        $(".sidebar-menu").toggleClass("small-side-bar");
        $('.nav-link-name').toggleClass('name-hide');        
    });
});




 $(document).ready(function () {    
      $(".nav-link").hover(function () {           
          $('.sidebar-menu').removeClass("flowHide");  
          $(this).addClass('tax-active');
              
      }, function () {/''
          $('.sidebar-menu')
             .addClass("flowHide");
          $(this).removeClass('tax-active');
             
      });    
  });
</script>
<script type="text/javascript">
         $(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
    
</script>
<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>