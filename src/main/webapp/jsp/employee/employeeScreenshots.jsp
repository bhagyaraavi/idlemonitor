
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Idle Monitor : Screenshot</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataTables.bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pagination.css">
    <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.dataTables.min.js"></script>
	<script src="${pageContext.request.contextPath}/vendor/jquery/dataTables.bootstrap4.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/employee/employee.js"></script>
    <%@ include file="/jsp/common/common.jsp"%>
    <script type="text/javascript">
    $(document).ready(function(){
		$('a').removeClass('active');
		$('#screenshots').addClass('active');
		var period=$('#period').val();
		$('a').removeClass('mn_active');
		$('#period'+period).addClass('mn_active');
    });
    $( function() {
        $( "#datepicker" ).datepicker({
        	dateFormat: 'mm-dd-yy',
            numberOfMonths: 2,
            maxDate: "+0",
        });
    } );
    
    function getScreenshotsByDate(){
    	
    	var date=$('#datepicker').val();
    	var userId=$('#userId').val();
    	
    	var url = "${pageContext.request.contextPath}/employee/viewscreenshots.do?userId="+userId+"&selectedDate="+date;
    	window.open(url,"_self");
    }
   
    	function getscreenshotsByUser(userId){
    		
    		var period=$('#period').val();
    		var userSelected=userId;
    		window.location= "${pageContext.request.contextPath}/employee/viewscreenshots.do?userId="+userSelected+"&period="+period
    		
    	}
    	
    	
    </script>
</head>
<body>
<!--===============================================
                      NAVBAR
===============================================-->


   <%@ include file="/jsp/common/applicationHeader.jsp"%>
   
		<section class="side-bar-warp">
    	<%@ include file="/jsp/common/leftSidePanel.jsp"%>
        
        <div class="dashboard-stats">
            <div class="col-md-12">

                <div class="row">
                    <ul class="breadcrumb">
                        <li class="completed"><a href="javascript:void(0);">Idle Monitor</a></li>
                        <li class="active"><a href="javascript:void(0);">Screenshot</a></li>
                    </ul>
                    
                    <div class="header">
                    
	                    <h6>${title} </h6>
	                    <%-- <c:if test="${empty message}"> --%>
	                    <select class="form-control mn_screens" id="userSelected" onchange="getscreenshotsByUser(this.value)">
	                    	<option>Select User</option>
	                    	<c:forEach var="userDto" items="${userDtos}">
              					<c:set var = "selectedUser" value = "${selectedUser}" />
              					<option value="${userDto.userId}" ${userDto.employeeName == selectedUser ? 'selected="selected"' : '' }>${userDto.employeeName}</option>
              				</c:forEach>
	                    </select>
	                   <%--  </c:if> --%>
	                    <input type="hidden" name="period" id="period" value="${period}" ></input>
                      <input type="hidden"  name="userId" id="userId" value="${userId}" />
                       <input type="hidden"  name="selectedDate" id="selectedDate" value="${selectedDate}" />
	                     
	                    <p><a id="period0" class="mn_active" href="${pageContext.request.contextPath}/employee/viewscreenshots.do?userId=${userId}&period=0">Today</a>
	                    <a id="period6" href="${pageContext.request.contextPath}/employee/viewscreenshots.do?userId=${userId}&period=6">7 Days</a> 
	                    <a id="period13" href="${pageContext.request.contextPath}/employee/viewscreenshots.do?userId=${userId}&period=13">14 Days</a> 
	                    <a id="period31" href="${pageContext.request.contextPath}/employee/viewscreenshots.do?userId=${userId}&period=31">One month</a> 
	                    <input type="text" id="datepicker" name="date" value="${selectedDate}" class="form-control" placeholder="Date Selection" onchange="getScreenshotsByDate()">
	                    </p>
                    </div>
                   </div>
                    
                    <form:form commandName="displayListBean" method="POST" class="form-horizontal" action="${pageContext.request.contextPath}/employee/viewscreenshots.do?userId=${userId}&period=${period}&selectedDate=${selectedDate}" role="form">
								<form:hidden path="pagerDto.range" />
								<form:hidden path="pagerDto.pageNo" />
								<form:hidden path="sortBy" value="${displayListBean.sortBy}" id="sortBy" />
								<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection" />
								<c:set var="range" value="${displayListBean.pagerDto.range}" />
								<!-- SELECT  PAGE RANGE -->
								<div class="row">
									<div class="col-md-6">
									<div class="dataTables_length" id="dataTableId_length">
										<label style="font-weight: 900;">Show 
										<select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
												<option value="10" ${range=='10' ? 'selected' : '' }>10</option>
												<option value="25" ${range=='25' ? 'selected' : '' }>25</option>
												<option value="50" ${range=='50' ? 'selected' : '' }>50</option>
												<option value="100" ${range=='100' ? 'selected' : '' }>100</option>
										</select> entries
										</label>
									</div>
				                   </div>
									<!-- END OF SELECT PAGE RANGE -->
									
									<div class="col-md-6 pull-right">
				                   <button onclick="deleteScreenshots()">Delete Screenshots</button>
				                   	</div>
				                   </div>
                        <div class="Utilization mn_scre">
                        <form method="get"> 
                         <c:choose>
                            	<c:when test="${empty message }">
                            <div class="row">
                           
                            		<c:forEach var="screenCaptureDto" items="${screenCaptureDtos}">
                            		  <div class="col-md-2">
		                                <div class="mn_scr">
		                                  <%--   <label class="btn mn_btn"><img src="${screenCaptureDto.base64Image}" alt="..." class="img-thumbnail img-check"> --%>
		                                  <label class="btn mn_btn"><a class="thumbnail img-thumbnail img-check" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="images/sc_1.jpg" data-target="#image-gallery${screenCaptureDto.dataId}">
                                        	<img class="img-thumbnail img-check" src="${screenCaptureDto.base64Image}"></a>
                                        	<%-- <img class="img-thumbnail img-check" src="${pageContext.request.contextPath}/images/sc_1.jpg"></a> --%>
		                                    <input type="checkbox" name="image${screenCaptureDto.dataId}" id="${screenCaptureDto.dataId}" value="${screenCaptureDto.dataId}" class="hidden" autocomplete="off"></label>
		                                    <div class="mn_details">
		                                        <div class="mn_name"><img src="${pageContext.request.contextPath}/images/user.png" alt=""> <p>${screenCaptureDto.userDto.employeeName}</p></div>
		                                        <fmt:formatDate	value='${screenCaptureDto.captureDateTime}'	pattern='MM/dd/yyyy hh:mm:ss a' var="screenshotCreationDate" />
		                                        <div class="mn_date">${screenshotCreationDate}</div>
		                                    </div>
		                                </div>
                            		</div>
		                               <div class="modal fade" id="image-gallery${screenCaptureDto.dataId}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <div class="modal-dialog" >
			                                    <div class="modal-content" >
			                                        <div class="modal-header">
			                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button>
			                                            <h4 class="modal-title" id="image-gallery-title"></h4>
			                                            <div class="mn_left" >
			                                                    <h6>${screenCaptureDto.userDto.employeeName}</h6>
			                                                    <p>(${screenshotCreationDate})</p> 
			                                                </div>
			                                        </div>
			                                        <div class="modal-body" >
			                                            <img src="${screenCaptureDto.base64Image}" class="img-responsive">
			                                            <%-- <img  src="${pageContext.request.contextPath}/images/sc_1.jpg" class="img-responsive"> --%>
			                                        </div>
			                                    </div>
			                                  </div>
                               				</div>
                            	</c:forEach>
                            	
                         
                        </div>
                           </c:when>
                            	<c:otherwise>
                            		<h6 class="no_msg">${message}</h6>
                            	</c:otherwise>
                            </c:choose>  
                            </form>
                        </div>

                        <div class="clearfix"></div>
							
                            	<c:if test="${empty message}">
							<c:set var="first" value="0" />
							<c:set var="end"
								value="${displayListBean.pagerDto.pagesNeeded }" />
							<c:set var="page" value="${displayListBean.pagerDto.pageNo}" />
							<c:set var="total"
								value="${displayListBean.pagerDto.totalItems}" />
							<c:set var="firstResult"
								value="${displayListBean.pagerDto.firstResult}" />
							<c:set var="lastResult"
								value="${displayListBean.pagerDto.lastResult}" />
							<div class="dataTables_info" id="dataTableId_info"
								role="status" aria-live="polite">Showing
								${firstResult} to ${lastResult} of ${total} entries</div>
							<div class="dataTables_paginate paging_simple_numbers"
								id="dataTableId_paginate">

								<%@ include file="/jsp/common/pager.jsp"%>
						
						</div>
						</c:if>
						
                    </form:form>
                </div>
            </div>
       
    </section>


<!-- Resources -->

<script type="text/javascript">
    

$(document).ready(function () {
    $(document).on('click', '.cta', function () {
        $(this).toggleClass('active')
    })
});


$(document).ready(function(){
    $(".hamburger").click(function(){
        $('.sidebar-menu').removeClass("flowHide");  
        $(".sidebar-menu").toggleClass("small-side-bar");
        $('.nav-link-name').toggleClass('name-hide');        
    });
});




 $(document).ready(function () {    
      $(".nav-link").hover(function () {           
          $('.sidebar-menu').removeClass("flowHide");  
          $(this).addClass('tax-active');
              
      }, function () {
          $('.sidebar-menu')
             .addClass("flowHide");
          $(this).removeClass('tax-active');
             
      });    
  });
</script>
<script type="text/javascript">
         $(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
    
</script>
</body>
</html>