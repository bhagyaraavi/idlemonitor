<!DOCTYPE html>
<html lang="en">
<head>
    <title>Idle Monitor : Employees</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataTables.bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pagination.css">
    <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/vendor/jquery/dataTables.bootstrap4.min.js"></script>
<script src="${pageContext.request.contextPath}/js/employee/employee.js"></script>
   <%@ include file="/jsp/common/common.jsp"%>
   <script>
   $(document).ready(function()
		   {
		$('a').removeClass('active');
		$('#employees').addClass('active');
		var sortBy=$('#sortBy').val();
		var sortDirection=$('#sortDirection').val();
		if(sortDirection=='true')
		{
			jQuery("#employee_name,#employee_number,#creation_date,#email_id,#status").removeClass('sorting').addClass('sorting_asc');
			
		}
		else{
			jQuery("#employee_name,#employee_number,#creation_date,#email_id,#status").removeClass('sorting').addClass('sorting_desc');
		}
		
});
   </script>
</head>
<body>
<!--===============================================
                      NAVBAR
===============================================-->

   <%@ include file="/jsp/common/applicationHeader.jsp"%>

    <section class="side-bar-warp">
         <%@ include file="/jsp/common/leftSidePanel.jsp"%>
        
        <div class="dashboard-stats">
            <div class="col-md-12">
            	 <div class="row">
                    <ul class="breadcrumb">
                        <li class="completed"><a href="javascript:void(0);">Idle Monitor</a></li>
                        <li class="active"><a href="javascript:void(0);">Employee</a></li>
                    </ul>
                    
                </div>
                <form:form commandName="displayListBean" method="POST" class="form-horizontal" action="${pageContext.request.contextPath}/employee/viewemployees.do" role="form">
								<form:hidden path="sortBy" value="${displayListBean.sortBy}" id="sortBy" />
								<form:hidden path="searchBy" />
								<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection" />
								<form:hidden path="pagerDto.range" />
								<form:hidden path="pagerDto.pageNo" />

								<c:set var="range" value="${displayListBean.pagerDto.range}" />
								<!-- SELECT  PAGE RANGE -->
								
								<div class="row">
									<div class="col-md-6">
										<div class="dataTables_length" id="dataTableId_length" 	style="margin-left: 10px;">
											<label style="font-weight: 900;">Show 
											<select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
													<option value="10" ${range=='10' ? 'selected' : '' }>10</option>
													<option value="25" ${range=='25' ? 'selected' : '' }>25</option>
													<option value="50" ${range=='50' ? 'selected' : '' }>50</option>
													<option value="100" ${range=='100' ? 'selected' : '' }>100</option>
											</select> entries
											</label>
										</div>
									</div>
									<!-- END OF SELECT PAGE RANGE -->
									<!-- SEARCH DIVISION -->
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-2 no_pad_left">
											<label>Search By:</label>
											</div>
											<div class="col-md-5 no_pad_left">
												<input type="text" id="searchText" name="searchText" placeholder="Search for employees "	value="${displayListBean.searchBy}">
											</div>
											
									<div class="col-md-5 no_pad_left">
									
											<div class="col-md-6 no_pad_left pull-right">
												<button class="btn mn_none" type="button"
													onclick="window.location='${pageContext.request.contextPath}/employee/viewemployees.do'">Reset</button>
											</div>
											<div class="col-md-6 no_pad_left pull-right">
												<button class="btn mn_none1" type="button" value="Search"
													onclick="searchResults();">Search</button>
											</div>
									</div>
										</div>
									</div>
								</div>
								<!-- END OF Search divison -->

								
							<div class="clearfix"></div> .
								<div class="col-md-12 nopad">
								<div class="dataTables_wrapper table-responsive">
								<c:choose>
           							<c:when test="${empty message}">
											<table id="dataTableId" class="display dataTable table"	role="grid" aria-describedby="dataTableId_info"	style="width: 100%;">
												<thead>
													<tr>
														<th class="sorting" id="employee_name" onclick="sortResults('employeeName')">Employee Name</th>
														<th class="sorting" id="employee_number" onclick="sortResults('employeeNumber')">Employee Number</th>
														<th class="sorting" id="email_id" onclick="sortResults('emailId')">Email</th>
														<th class="sorting" id="employee_creation_date" onclick="sortResults('creationDate')">Employee Creation Date</th>
														<th class="sorting" id="status" onclick="sortResults('status')">Status</th>
														<th id="edit_user">Edit User</th>
														<th id="delete_user">Delete User</th>
														
													</tr>
												</thead>
												<tbody>
													
													<c:forEach var="employeeDto" items="${employeeDtos}">
														<tr>
															<td id="employee_name_ ${employeeDto.userId}">${employeeDto.employeeName}</td>
															<td id="employee_number_${employeeDto.userId}">${employeeDto.employeeNumber}</td>
															<td id="email_id_${employeeDto.userId}">${employeeDto.emailId}</td>
															<fmt:formatDate	value='${employeeDto.creationDate}'	pattern='MM/dd/yyyy hh:mm:ss' var="employeeCreationDate" />
															<td id="employee_creation_date_${employeeDto.userId}">${employeeCreationDate}</td>
                											<td >
                												<input type="hidden" id="accountStatus${employeeDto.userId}" value="${employeeDto.isActive}">
                												<c:choose>
                													<c:when test="${employeeDto.isActive==true}">
                														<c:set var="userStatus" value="active"></c:set>
                													</c:when>
                													<c:otherwise>
                														<c:set var="userStatus" value="inactive"></c:set>
                													</c:otherwise>
                												</c:choose>
						                      					
						                      					<input type="hidden" id="userStatus${employeeDto.userId}" value="${userStatus}">
																<select id="status_${employeeDto.userId}"  onchange="changeStatus(this,${employeeDto.userId});">
											                    <option value="active" ${userStatus=='active' ? 'selected' : '' }>Active</option>
											                    <option value="inactive" ${userStatus=='inactive' ? 'selected' : '' }>Inactive</option>
											                   
											                   </select>
											                 </td>

											                 
											                 <td>
											                 <%--  <a style="font-size:20px;position:relative;top:-5px;" onclick="	(this,${employeeDto.userId});"><i class="fa fa-edit" aria-hidden="true"></i></a></td> --%>
											               <a  href="${pageContext.request.contextPath}/user/editemployeeregistration.do?userId=${employeeDto.userId}" ><i class="fa fa-edit" aria-hidden="true"></i></a></td>  
															<td id="delete_user_${employeeDto.userId}"><a style="font-size:20px;position:relative;top:-5px;" onclick="deleteEmployeeUser(this,${employeeDto.userId});"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										
										<div class="clearfix"></div>
											
											<c:set var="first" value="0" />
											<c:set var="end"
												value="${displayListBean.pagerDto.pagesNeeded }" />
											<c:set var="page" value="${displayListBean.pagerDto.pageNo}" />
											<c:set var="total"
												value="${displayListBean.pagerDto.totalItems}" />
											<c:set var="firstResult"
												value="${displayListBean.pagerDto.firstResult}" />
										<c:set var="lastResult"
												value="${displayListBean.pagerDto.lastResult}" />
											<div class="dataTables_info" id="dataTableId_info"
												role="status" aria-live="polite">Showing
												${firstResult} to ${lastResult} of ${total} entries</div>
											<div class="dataTables_paginate paging_simple_numbers"
												id="dataTableId_paginate">

												<%@ include file="/jsp/common/pager.jsp"%>

											</div>
										</c:when>
									
									<c:otherwise>
										<h6 class="no_msg">${message}</h6>
									</c:otherwise>
									
									</c:choose>
									</div>
								</div>
							</form:form>
            
        </div>
    </div>
    </section>


<script type="text/javascript">
    

$(document).ready(function () {
    $(document).on('click', '.cta', function () {
       $(this).toggleClass('active')
    })
});


$(document).ready(function(){
    $(".hamburger").click(function(){
        $('.sidebar-menu').removeClass("flowHide");  
        $(".sidebar-menu").toggleClass("small-side-bar");
        $('.nav-link-name').toggleClass('name-hide');        
    });
});




 $(document).ready(function () {    
      $(".nav-link").hover(function () {           
          $('.sidebar-menu').removeClass("flowHide");  
          $(this).addClass('tax-active');
              
      }, function () {
          $('.sidebar-menu')
             .addClass("flowHide");
          $(this).removeClass('tax-active');
             
      });    
  });
</script>


<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>