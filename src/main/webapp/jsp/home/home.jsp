<!DOCTYPE html>
<html lang="en">
<head>
	<title>Idle Monitor : Home</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	 <nav class="navbar fixed-top navbar-expand-lg navbar-dark">
        <div class="container">
            <a class="navbar-brand  col-md-5" href="index.html">
                <div class="logo_new "><img src="images/logo.png" alt=""></div>
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars mfa-white"></span>
            </button>

         

            <div class="col-md-7">
            <div class="collapse navbar-collapse float-right" id="navbarResponsive">
                <div class="form-inline my-2 my-lg-0">
                    
                <ul class="navbar-nav ml-auto">
                    <li class="nav-link">
                        <div class="btn-group show-on-hover">
				          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				            Products <span class="caret"></span>
				          </button>
				          <ul class="dropdown-menu" role="menu">
				            <li><a href="#">Idle Timer <span>Learn exactly how you spend your time plus save your power when you are in idle state</span></a></li>
				            <li class="divider"></li>
                            <li><a href="#">Monitor Tool <span>Monitor your employee work status</span></a></li>
				          </ul>
				        </div>
                    </li>
                    <li class="nav-link">
                        <a class="mn_top_1" href="${pageContext.request.contextPath}/login.do" >SIGN IN</a>
                    </li>
                    <li class="nav-link">
                        <a class="btn btn-primary btn-block btn-register" href="${pageContext.request.contextPath}/user/adminregistration.do">Register</a>
                    </li>

                </ul>
                </div>
            </div>
        </div>
        </div>
    </nav>

    
    <header class="masthead text-white ">
        <div class="overlay"></div>
        <div class="container slider-top-text">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="my-heading">Take back control of your time</h3>
                    <p class="myp-slider">Millions of smart people and businesses trust Idle Monitor award-winning tools to help them become more focused, productive, and motivated.</p>
                    <a class="btn btn-primary btn-join" href="#">Find the product that's right for you</a>

                </div>
                <div class="col-md-12 text-center mt-5">
                    <div class="scroll-down">
                        <a class="btn btn-default btn-scroll floating-arrow" href="#gobottom" id="bottom"><i class="fa fa-angle-down"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="testimonials text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto wow fadeInUp">
                    <h3 class="text-center font-weight-bold"><span class="bg-main">VALUES</span> </h3>
                    <p class=" text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3 mt-4 wow bounceInUp" data-wow-duration="1.4s">
                    <div class="card">
                        <img class="card-img-top" src="images/financials.png" alt="">
                        <div class="card-block">

                            <h4 class="card-title text-center">FINANCIALS</h4>
                        </div>
                        <div class="card-footer text-center">
                            <small>Lorem Ipsum is simply dummy text of the printing and typesetting</small>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 mt-4 wow bounceInUp" data-wow-duration="1.4s">
                    <div class="card">
                        <img class="card-img-top" src="images/security.png" alt="">
                        <div class="card-block">

                            <h4 class="card-title text-center">SECURITY</h4>

                        </div>
                        <div class="card-footer text-center">
                            <small>Lorem Ipsum is simply dummy text of the printing and typesetting</small>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 mt-4 wow bounceInUp" data-wow-duration="1.4s">
                    <div class="card">
                        <img class="card-img-top" src="images/efficiency.png" alt="">
 						<div class="card-block">
                            <h4 class="card-title text-center">EFFICIENCY</h4>

                        </div>
                        <div class="card-footer text-center">
                            <small>Lorem Ipsum is simply dummy text of the printing and typesetting</small>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 mt-4 wow bounceInUp" data-wow-duration="1.4s">
                    <div class="card">
                        <img class="card-img-top" src="images/insight.png" alt="">
                        <div class="card-block">
                            <h4 class="card-title text-center">INSIGHTS</h4>

                        </div>
                        <div class="card-footer text-center">
                            <small>Lorem Ipsum is simply dummy text of the printing and typesetting</small>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonials mybg-events">

        <div class="container">
            
            <div class="row">
                <div class="col-md-12 wow fadeInUp">
                    <h3 class="title-heading text-center">TIME TRACKING IN NUMBERS</h3>
                    <p class="myp text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <div class="row wow dashboard-stats slideInLeft" data-wow-duration="3s">
                        <div class="col-md-3 col-sm-6">
                            <section class="panel panel-box">
                                <div class="panel-left panel-icon bg-success">
                                    <i class="fa fa-birthday-cake text-large stat-icon success-text"></i>
                                </div>
                                <div class="panel-right panel-icon bg-reverse">
                                    <p class="size-h1 no-margin countdown_first">12</p>
                                    <p class="text-muted no-margin text"><span>years in business</span></p>
                                </div>
                            </section>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <section class="panel panel-box">
                                <div class="panel-left panel-icon bg-danger">
                                    <i class="fa fa-users text-large stat-icon"></i>
                                </div>
                                <div class="panel-right panel-icon bg-reverse">
                                    <p class="size-h1 no-margin countdown_second">2 million<span class="size-h3">+</span></p>
                                    <p class="text-muted no-margin text"><span>users</span></p>
                                </div>
                            </section>
                            
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <section class="panel panel-box">
                                <div class="panel-left panel-icon bg-lovender">
                                    <i class="fa fa-wifi text-large stat-icon"></i>
                                </div>
                                <div class="panel-right panel-icon bg-reverse">
                                    <p class="size-h1 no-margin countdown_third">100k<span class="size-h3">+</span></p>
                                    <p class="text-muted no-margin text"><span>monthly blog readers</span></p>
                                </div>
                            </section>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <section class="panel panel-box">
                                <div class="panel-left panel-icon bg-info">
                                    <i class="fa fa-clock-o text-large stat-icon"></i>
                                </div>
                                <div class="panel-right panel-icon bg-reverse">
                                    <p class="size-h1 no-margin countdown_fourth">3 billion</p>
                                    <p class="text-muted no-margin text"><span>hours logged</span></p>
                                </div>
                            </section>
                        </div>    
                    </div> 
    </section>


   
    <footer class="footer bg-dark">
        <div class="container">
            <div class="row">
               
                <div class="col-lg-6 text-center text-lg-left my-auto  wow zoomIn">
                    <ul class="list-inline mb-2">
                        <li class="list-inline-item">
                            <a href="#">About</a>
                        </li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item">
                            <a href="#">Contact</a>
                        </li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item">
                            <a href="#">Terms of Use</a>
                        </li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item">
                            <a href="#">Privacy Policy</a>
                        </li>
                    </ul>
                    <p class="text-muted small mb-4 mb-lg-0">© Idle Monitor 2020. All Rights Reserved.</p>
                </div>
                <div class="col-lg-6 text-center text-lg-right my-auto  wow zoomIn">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item mr-3">
                            <a href="#">
                                <i class="fa fa-facebook fa-2x fa-fw"></i>
                            </a>
                        </li>
                        <li class="list-inline-item mr-3">
                            <a href="#">
                                <i class="fa fa-twitter fa-2x fa-fw"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-instagram fa-2x fa-fw"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
	

	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->


<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
     <script>
              new WOW().init();
              </script>
    <script>
        $(window).scroll( function(){

 
          var topWindow = $(window).scrollTop();
          var topWindow = topWindow * 1.5;
          var windowHeight = $(window).height();
          var position = topWindow / windowHeight;
          position = 1 - position;
        
          $('#bottom').css('opacity', position);
        
        });

        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("main").style.display = "0";
            document.body.style.backgroundColor = "white";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginRight= "0";
            document.body.style.backgroundColor = "white";
        }

 
     $(window).on("scroll", function() {
            if ($(this).scrollTop() > 10) {
                $("nav.navbar").addClass("mybg-dark");
                $("nav.navbar").addClass("navbar-shrink");
              

            } else {
                $("nav.navbar").removeClass("mybg-dark");
                $("nav.navbar").removeClass("navbar-shrink");
               
            }
            
      

        });
        
        $(function() {
  $('#bottom').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});


</script>
<script type="text/javascript">
	 $(function() {
   

	   function countUp(count) {
		var div_by = 100,
			speed = Math.round(count / div_by),
			$display = $('.countdown_first'),
			run_count = 1,
			int_speed = 24;
		var int = setInterval(function () {
			if (run_count < div_by) {
				$display.text(speed * run_count);
				run_count++;
			} else if (parseInt($display.text()) < count) {
				var curr_count = parseInt($display.text()) + 1;
				$display.text(curr_count);
			} else {
				clearInterval(int);
			}
		}, int_speed);
	}
	countUp(12);

function countUp2(count) {
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.countdown_second'),
        run_count = 1,
        int_speed = 24;
    var int = setInterval(function () {
        if (run_count < div_by) {
            $display.text(speed * run_count);
            run_count++;
        } else if (parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}
countUp2(2);

function countUp3(count) {
    var div_by = 100,
        speed = Math.round(count / div_by),
        $display = $('.countdown_fourth'),
        run_count = 1,
        int_speed = 24;
    var int = setInterval(function () {
        if (run_count < div_by) {
            $display.text(speed * run_count);
            run_count++;
        } else if (parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}
countUp3(3 billion);
		   
		   });
</script>
</body>
</html>