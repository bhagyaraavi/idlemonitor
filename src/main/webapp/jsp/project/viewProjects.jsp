<!DOCTYPE html>
<html lang="en">
<head>
    <title>Idle Monitor : Projects</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataTables.bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pagination.css">
    <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/vendor/jquery/dataTables.bootstrap4.min.js"></script>
<script src="${pageContext.request.contextPath}/js/project/project.js" type="text/javascript"></script>

   <%@ include file="/jsp/common/common.jsp"%>
   <script>
   $(document).ready(function()
		   {
		$('a').removeClass('active');
		$('#projects').addClass('active');
		var sortBy=$('#sortBy').val();
		var sortDirection=$('#sortDirection').val();
		if(sortDirection=='true')
		{
			jQuery("#employee_name,#employee_number,#creation_date,#email_id,#status").removeClass('sorting').addClass('sorting_asc');
			
		}
		else{
			jQuery("#employee_name,#employee_number,#creation_date,#email_id,#status").removeClass('sorting').addClass('sorting_desc');
		}
		
});
   </script>
</head>
<body>
<!--===============================================
                      NAVBAR
===============================================-->

   <%@ include file="/jsp/common/applicationHeader.jsp"%>

    <section class="side-bar-warp">
         <%@ include file="/jsp/common/leftSidePanel.jsp"%>
        
        <div class="dashboard-stats">
            <div class="col-md-12">
            	 <div class="row">
                    <ul class="breadcrumb">
                        <li class="completed"><a href="javascript:void(0);">Idle Monitor</a></li>
                        <li class="active"><a href="javascript:void(0);">Project Management</a></li>
                    </ul>
                    
                </div>
                <form:form commandName="displayListBean" method="POST" class="form-horizontal" action="${pageContext.request.contextPath}/project/viewprojects.do" role="form">
								<form:hidden path="sortBy" value="${displayListBean.sortBy}" id="sortBy" />
								<form:hidden path="searchBy" />
								<form:hidden path="sortDirection" value="${displayListBean.sortDirection }" id="sortDirection" />
								<form:hidden path="pagerDto.range" />
								<form:hidden path="pagerDto.pageNo" />

								<c:set var="range" value="${displayListBean.pagerDto.range}" />
								<!-- SELECT  PAGE RANGE -->
								
								 <div class="row">
									<div class="col-md-3">
										<div class="dataTables_length" id="dataTableId_length" 	style="margin-left: 10px;">
											<label style="font-weight: 900;">Show 
											<select name="dataTableId_length" id="selectrange" aria-controls="dataTableId" class="">
													<option value="10" ${range=='10' ? 'selected' : '' }>10</option>
													<option value="25" ${range=='25' ? 'selected' : '' }>25</option>
													<option value="50" ${range=='50' ? 'selected' : '' }>50</option>
													<option value="100" ${range=='100' ? 'selected' : '' }>100</option>
											</select> entries
											</label>
										</div>
									</div>  
									
									
									<!-- END OF SELECT PAGE RANGE -->
									<!-- SEARCH DIVISION -->
									<div class="col-md-9">
									
										<div class="row">
											<div class="col-md-1 no_pad">
											<label>Search By:</label>
											</div>
											<div class="col-md-3 no_pad_left">
												<input type="text" id="searchText" name="searchText" placeholder="Search for projects "	value="">
											</div>
											
									<div class="col-md-8 no_pad_left">
									
											<div class="no_pad_left pull-right">												
        										<a href="" class="btn btn-default mn_proj btn-rounded mb-2" data-toggle="modal" data-target="#modalAddProjectForm">Add project</a>
											<!--  ADD Project Popup -->
											<div class="modal fade" id="modalAddProjectForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									          <div class="modal-dialog" role="document">
									            <div class="modal-content mn_model_cont">
									              <div class="modal-header text-center">
									                <h4 class="modal-title w-100 font-weight-bold">Add Project</h4>
									                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									                  <span aria-hidden="true">&times;</span>
									                </button>
								              	</div>
													<div class="modal-body" id="project_popup_0">
																	              
														<form class="login100-form validate-form" role="form">				              
															<input type="hidden" name="projectId" id="project_id" value="">
															<div class="wrap-input100">
															<input id="project_name_0" name="projectName" placeholder="Project Name" type="text" class="input1001" value="">
																<span id="projectName_error_0" class="error_msg"></span>
																<span class="focus-input100"></span>
																
															</div>
															<div class="wrap-input100">
																<input id="client_name_0" name="clientName" placeholder="Client Name" type="text" class="input1001" value="">
																<span id="client_error" class="error_msg"></span>
																<span class="focus-input100"></span>
																
															</div>
															
															<div class="container-login100-form-btn">
																<button class="login100-form-btn" onclick=" return saveorUpdateProject(0)">
																	ADD
																</button>
															</div>
															
														</form>
																	              
													 		</div>
														</div>
													</div>
												</div>
												<!--  END Of Add Project  -->
											</div>
											
											<div class="no_pad_left pull-left">
												<button class="btn mn_none1" type="button" value="Search"
													onclick="searchResults();">Search</button>
											</div>
											<div class="no_pad_right pull-left">
												<button class="btn mn_none" type="button"
													onclick="window.location='${pageContext.request.contextPath}/project/viewprojects.do'">Reset</button>
											</div>
									</div>
										</div>
									</div>
								</div>
								<!-- END OF Search divison -->

								
							<div class="clearfix"></div> .
								<div class="col-md-12 nopad">
								<div class="dataTables_wrapper table-responsive">
								<c:choose>
           							<c:when test="${empty message }">
											<table id="dataTableId" class="display dataTable table"	role="grid" aria-describedby="dataTableId_info"	style="width: 100%;">
												<thead>
													<tr>
														<th class="sorting" id="project_name" onclick="sortResults('projectName')">Project Name</th>
														<th class="sorting" id="client_name" onclick="sortResults('clientName')">Client Name</th>
														<th class="sorting" id="project_creation_date" onclick="sortResults('projectCreationDate')">Project Creation Date</th>
														<th  id="status">Status</th>
														<th  id="viewTasks" >Tasks</th>
														<th id="edit_project">Edit Project</th>
														<th id="delete_project">Delete Project</th>
														
											
													</tr>
												</thead>
												<tbody>
													
													<c:forEach var="projectDto" items="${projectDtos}">
														<tr>
															<td id="project_${projectDto.projectId}">${projectDto.projectName}</td>
															<td id="client_${projectDto.projectId}">${projectDto.clientName}</td>
															<fmt:formatDate	value='${projectDto.projectCreationDate}'	pattern='MM/dd/yyyy hh:mm:ss' var="projectCreationDate" />
															<td id="project_creation_date_${projectDto.projectId}">${projectCreationDate}</td>
                											<td >
                												<input type="hidden" id="accountStatus${projectDto.projectId}" value="${projectDto.isActive}">
                												<c:choose>
                													<c:when test="${projectDto.isActive==true}">
                														<c:set var="projectStatus" value="active"></c:set>
                													</c:when>
                													<c:otherwise>
                														<c:set var="projectStatus" value="inactive"></c:set>
                													</c:otherwise>
                												</c:choose>
						                      					
						                      					<input type="hidden" id="projectStatus${projectDto.projectId}" value="${projectStatus}">
																<select id="status_${projectDto.projectId}"  onchange="changeStatus(this,${projectDto.projectId});">
											                    <option value="active" ${projectStatus=='active' ? 'selected' : '' }>Active</option>
											                    <option value="inactive" ${projectStatus=='inactive' ? 'selected' : '' }>Inactive</option>
											                   
											                   </select>
											                 </td>
															<td><a href="${pageContext.request.contextPath}/project/viewtasks.do?projectId=${projectDto.projectId}">View Tasks</a></td>
											                 
											                 <td>
											                
											                 <a href=""  class="btn btn-default btn-rounded mb-2"data-toggle="modal" data-target="#modalEditProjectForm${projectDto.projectId}"><i class="fa fa-edit" aria-hidden="true"></i></a>
											                 
											                 <!-- Edit Project popup -->	
											                 
											<div class="modal fade" id="modalEditProjectForm${projectDto.projectId}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									          <div class="modal-dialog" role="document">
									            <div class="modal-content mn_model_cont">
									              <div class="modal-header text-center">
									                <h4 class="modal-title w-100 font-weight-bold">Edit Project</h4>
									                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									                  <span aria-hidden="true">&times;</span>
									                </button>
									              	</div>
													<div class="modal-body" id="project_popup_${projectDto.projectId}">
																	              
														<form class="login100-form validate-form" role="form">				              
															
															<div class="wrap-input100">
															<input id="project_name_${projectDto.projectId}"  placeholder="Project Name" type="text" class="input1001" value="${projectDto.projectName}">
																<span id="projectName_error_${projectDto.projectId}" class="error_msg"></span>
																<span class="focus-input100"></span>
																
															</div>
															<div class="wrap-input100">
																<input id="client_name_${projectDto.projectId}"  placeholder="Client Name" type="text" class="input1001" value="${projectDto.clientName}">
																<span id="client_error_${projectDto.projectId}" class="error_msg"></span>
																<span class="focus-input100"></span>
																
															</div>
															
															<div class="container-login100-form-btn">
																<button class="login100-form-btn" onclick="return saveorUpdateProject(${projectDto.projectId})">
																	EDIT
																</button>
															</div>
															
														</form>
																	              
													 		</div>
														</div>
													</div>
												</div>
											               
											               </td>  
															<td id="delete_project_${projectDto.projectId}"><a style="font-size:20px;position:relative;top:-5px;" onclick="deleteProject(this,${projectDto.projectId});"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												
												</td>
												</tr>
													</c:forEach>
													
													
												</tbody>
											</table>
										
										<div class="clearfix"></div>
											
											<c:set var="first" value="0" />
											<c:set var="end"
												value="${displayListBean.pagerDto.pagesNeeded }" />
											<c:set var="page" value="${displayListBean.pagerDto.pageNo}" />
											<c:set var="total"
												value="${displayListBean.pagerDto.totalItems}" />
											<c:set var="firstResult"
												value="${displayListBean.pagerDto.firstResult}" />
										<c:set var="lastResult"
												value="${displayListBean.pagerDto.lastResult}" />
											<div class="dataTables_info" id="dataTableId_info"
												role="status" aria-live="polite">Showing
												${firstResult} to ${lastResult} of ${total} entries</div>
											<div class="dataTables_paginate paging_simple_numbers"
												id="dataTableId_paginate">

												<%@ include file="/jsp/common/pager.jsp"%>

											</div>
										</c:when>
									
									<c:otherwise>
										<h6 class="no_msg">${message}</h6>
									</c:otherwise>
									
									
									</c:choose>
									</div>
								</div>
							</form:form>
            
        </div>
    </div>
    </section>


<script type="text/javascript">
    

$(document).ready(function () {
    $(document).on('click', '.cta', function () {
       $(this).toggleClass('active')
    })
});


$(document).ready(function(){
    $(".hamburger").click(function(){
        $('.sidebar-menu').removeClass("flowHide");  
        $(".sidebar-menu").toggleClass("small-side-bar");
        $('.nav-link-name').toggleClass('name-hide');        
    });
});




 $(document).ready(function () {    
      $(".nav-link").hover(function () {           
          $('.sidebar-menu').removeClass("flowHide");  
          $(this).addClass('tax-active');
              
      }, function () {
          $('.sidebar-menu')
             .addClass("flowHide");
          $(this).removeClass('tax-active');
             
      });    
  });
</script>


<%@ include file="/jsp/common/applicationFooter.jsp"%>
</body>
</html>