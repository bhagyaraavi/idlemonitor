<!-- Created By bhagya On may 16th,2017 
	Page For Error-->
<!DOCTYPE html>

<html>
<meta HTTP-EQUIV="Pragma" content="no-cache">
<meta HTTP-EQUIV="Expires" content="-1" >
<head>

	<title>Error</title>
	<%@ include file="/jsp/common/common.jsp"%>
</head>

<body>
<!--Header-->

	 <%@ include file="/jsp/common/header.jsp"%> 
	
  <!--//Header-->
 
  		 <section class="m_contenr">
        <div class="container">
            <div class="row justify-content-md-center">
            <div class="col col-lg-8">
                    <h2>Error</h2>
                    <p> Something went Wrong</p>
                    <a href="${pageContext.request.contextPath}" style="text-decoration: underline;color:#01e;">Click Here to goto home page</a>
                </div>
            </div>
        </div>
    </section>
  		<!--FOOTER-->
	     <%@ include file="/jsp/common/footer.jsp"%>
  	 

</body>
</html>