package com.idlemonitor.frontend.user.service;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.CompanyNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.EmployeeEmailIdNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.MailNotSentException;
import com.idlemonitor.common.exceptions.PasswordNotMatchException;
import com.idlemonitor.common.exceptions.UserEmailExistanceException;
import com.idlemonitor.common.exceptions.UserLoginsNotFoundException;
import com.idlemonitor.common.exceptions.UserLoginsNotSavedOrUpdatedException;
import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.backend.employee.dao.EmployeeDao;
import com.idlemonitor.backend.user.dao.UserDao;
import com.idlemonitor.backend.user.dao.UserDaoImpl;
import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.backend.user.model.IdleTimeConfiguration;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserCompletedWorkHours;
import com.idlemonitor.backend.user.model.UserLogins;
import com.idlemonitor.backend.user.model.UserRole;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
import com.idlemonitor.backend.user.model.UserWorkHours;

import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotSavedOrUpdatedException;
import com.idlemonitor.frontend.admin.dto.CompanyDto;
import com.idlemonitor.common.exceptions.EmployeeNumberNotFoundException;
import com.idlemonitor.frontend.user.dto.AdminUserDto;
import com.idlemonitor.frontend.user.dto.IdleTimeConfigurationDto;
import com.idlemonitor.frontend.user.dto.UserDto;
import com.idlemonitor.frontend.user.dto.UserLoginsDto;
import com.idlemonitor.frontend.user.dto.WeeklyWorkHoursDto;
import com.idlemonitor.frontend.utility.EmailSender;
import com.idlemonitor.frontend.utility.amazonS3.AmazonS3Service;
import java.text.ParseException;
/**
 * Created By Bhagya
 * @author user
 * Implementation class for User services
 */
@Transactional
@Service("userService")
public class UserServiceImpl implements UserService{
	
	private static Logger log = Logger.getLogger(UserServiceImpl.class);

	@Resource(name = "userDao")
	private UserDao userDao;
	
	@Resource(name = "employeeDao")
	private EmployeeDao employeeDao;
	
	@Resource(name = "emailSender")
	private EmailSender emailSender;
	
	@Resource(name="amazonS3Service")
	private AmazonS3Service amazonS3Service;
	
	@Autowired
	private org.springframework.security.crypto.password.PasswordEncoder bCryptEncoder;
	
	private String userScreenCaptureImagePath;
	
	public String getUserScreenCaptureImagePath() {
		return userScreenCaptureImagePath;
	}
	public void setUserScreenCaptureImagePath(String userScreenCaptureImagePath) {
		this.userScreenCaptureImagePath = userScreenCaptureImagePath;
	}
	/**
	 * Created by bhagya on may 28th, 2020
	 * Service for get the user based on username or email
	 */
	public AdminUserDto getUserByUsernameOrEmailId(String emailId) {
		
		log.info("UserServiceImpl-> getUserByUsernameOrEmailId();");
		try {
			
			AdminUser user = this.userDao.getUserforAuthentication(emailId);
			if (user != null) {
				 AdminUserDto userDto = AdminUserDto.populateAdminUserDto(user);
				return userDto;
			} else {
				return null;
			}
		} 
		catch(UserNotFoundException e){
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service " + e.toString());
			return null;
		}
	}
	/**
	 * Created By Bhagya on may 28th, 2020
	 * @param adminUserDto
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Service for to save or update admin user details
	 */
	public Integer saveOrUpdateAdminUser(AdminUserDto adminUserDto) throws UserNotSavedOrUpdatedException{
		log.info(" Inside UserServiceImpl -> saveOrUpdateAdminUser ");
		Boolean newUser=false;
		AdminUser adminUser=null;
		Company company=null;
		try{
		adminUser=this.userDao.getAdminUserByAdminUserId(adminUserDto.getAdminUserId());
		adminUser.setAccountModifiedDate(new Date());
		company=adminUser.getCompany();
		}
		catch(UserNotFoundException e){
			newUser=true;
			adminUser=new AdminUser();
			company=new Company();
			adminUser.setUserRole(UserRole.ROLE_IDLE_TIMER);
			adminUser.setAccountCreationDate(new Date());
			adminUser.setIsActive(false);
			adminUser.setAccountActivationToken(this.generateToken());
			 Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				cal.add(Calendar.DATE, 1);
				Date validity = cal.getTime();
				adminUser.setActivationTokenExpiryDate(validity);
		}
		company.setCompanyName(adminUserDto.getCompanyDto().getCompanyName());
		company.setCompanyCode(adminUserDto.getCompanyDto().getCompanyCode());
		try {
			this.userDao.saveOrUpdateCompany(company);
		} catch (CompanyNotSavedOrUpdatedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		adminUser.setCompany(company);
		adminUser.setContactEmail(adminUserDto.getContactEmail());
		adminUser.setContactNumber(adminUserDto.getContactNumber());
		adminUser.setCompanyAddress(adminUserDto.getCompanyAddress());
		adminUser.setUsername(adminUserDto.getUsername());
		if(null!=adminUserDto.getPassword()){
		String encryptedPassword = bCryptEncoder.encode(adminUserDto.getPassword());
		adminUser.setPassword(encryptedPassword);
		}
		Integer savedUserId=this.userDao.saveOrUpdateAdminUser(adminUser);
		
		if(null!=savedUserId && newUser==true){
			AdminUserDto adminuserDto=AdminUserDto.populateAdminUserDto(adminUser);
			try{
				this.emailSender.sendConfirmationMail(adminuserDto);
				} catch (MailNotSentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
		}
		return savedUserId;
	}
	
	/**
	 * Created By Bhagya On May 28th, 2020
	 * 
	 * @param email
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for to send the password reset email to user
	 * @throws MailNotSentException
	 */
	public Integer sendPasswordResetEmail(String email)throws UserNotFoundException, UserNotSavedOrUpdatedException,MailNotSentException {
		log.info("UserServiceImpl-> sendPasswordResetEmail()");
		AdminUser adminUser = this.userDao.getUserforAuthentication(email);
		String passwordToken = this.generateToken();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		Date validity = cal.getTime();
		adminUser.setPasswordTokenExpiryDate(validity);
		adminUser.setPasswordToken(passwordToken);
		this.userDao.saveOrUpdateAdminUser(adminUser);
		AdminUserDto eznrollUserDto = AdminUserDto.populateAdminUserDto(adminUser);
		this.emailSender.sendForgotPasswordMail(eznrollUserDto);
		return 1;
	}
	
	/**
	 * Created By Bhagya On May 28th, 2020 This Method return random UUID
	 * java.util.UUID class represents an immutable universally unique
	 * identifier (UUID)
	 * 
	 */
	public String generateToken() {
		log.info("UserServiceImpl-> generatePasswordToken()");
		String uuid = UUID.randomUUID().toString();
		String token = uuid.toString().replaceAll("-", "").toUpperCase();
		return token;
	}
	
	/**
	 * Created By Bhagya On June 02nd,2017
	 * 
	 * @param token
	 * @return
	 * @throws UserNotFoundException
	 * 
	 *             Method For Getting the UserId By Password Reset TOken
	 */
	public Integer getUserIdByPasswordResetToken(String token)throws UserNotFoundException {
		log.info("UserServiceImpl-> getUserIdByPasswordResetToken()");
		AdminUser adminUser = this.userDao.getAdminUserByPasswordResetToken(token);
		return adminUser.getAdminUserId();
	}
	
	/**
	 * Created By Bhagya On June 02nd, 2017
	 * 
	 * @param userId
	 * @param password
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 *             Method for hadling the process of password reset
	 */
	public Integer handlePasswordReset(Integer userId, String password)throws UserNotFoundException, UserNotSavedOrUpdatedException {
		log.info("UserServiceImpl-> handlePasswordReset()");
		Integer result = 0;
		AdminUser adminUser = this.userDao.getAdminUserByAdminUserId(userId);
		Date passwordTokenExpiryDate = adminUser.getPasswordTokenExpiryDate();
		if (passwordTokenExpiryDate.after(new Date())) {
			String encryptedPassword = bCryptEncoder.encode(password);
			adminUser.setPassword(encryptedPassword);
			adminUser.setPasswordToken(null);
			adminUser.setPasswordTokenExpiryDate(null);
			result = this.userDao.saveOrUpdateAdminUser(adminUser);
		}
		return result;
	}
	
	/**
	 * Created By bhagya on june 02nd, 2020
	 * 
	 * Method to Activate User
	 * 1. Gets User based on Account TOken..
	 * 2. Updates isApproved(true)
	 * 3. Delete Account Token..
	 * 4. Update User.
	 * 5. Send Response Back
	 */
	public Integer activateAdminUserFromAccountToken(String token)throws Exception{
		log.info("UserServiceImpl-> activateAdminUserFromAccountToken()");
		Integer result = 0;
		AdminUser adminUser=this.userDao.getAdminUserByAccountToken(token);
		Date activationTokenExpiryDate = adminUser.getActivationTokenExpiryDate();
		if (activationTokenExpiryDate.after(new Date())) {
			adminUser.setIsActive(true);
			adminUser.setAccountActivationToken(null);
			adminUser.setActivationTokenExpiryDate(null);
		 result=this.userDao.saveOrUpdateAdminUser(adminUser);
		}
		
		return result;
	}
	/**
	 * 
	 * @param adminUserId
	 * @return
	 * @throws UserNotFoundException
	 */
	public AdminUserDto getAdminUserByAdminUserId(Integer adminUserId)throws UserNotFoundException {
		
		log.info("UserServiceImpl-> getAdminUserByAdminUserId();");
		try {
			
			AdminUser user = this.userDao.getAdminUserByAdminUserId(adminUserId);
			if (user != null) {
				 AdminUserDto userDto = AdminUserDto.populateAdminUserDto(user);
				return userDto;
			} else {
				return null;
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service " + e.toString());
			return null;
		}
	}
	/**
	 * Created by bhagya on may 28th, 2020
	 * Service for get the company based on Company name
	 */
	public CompanyDto getCompanyByCompanyName(String companyName) {
		
		log.info("UserServiceImpl-> getCompanyByCompanyName();");
		try {
			
			Company company=this.userDao.getCompanybyCompanyName(companyName);
			if (company != null) {
				 CompanyDto companyDto = CompanyDto.populateCompanyDto(company);
				return companyDto;
			} else {
				return null;
			}
		} 
		catch(CompanyNotFoundException e){
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service " + e.toString());
			return null;
		}
	}
	/**
	 * Created By Bhagya on June 22nd, 2020
	 * @param idleTimeConfigurationDto
	 * @return
	 * 
	 * Service for to save or update Idle time configuration
	 * @throws IdleTimeConfigurationNotSavedOrUpdatedException 
	 */
	
	public Integer saveOrUpdateIdleTimeConfiguration(IdleTimeConfigurationDto idleTimeConfigurationDto) throws IdleTimeConfigurationNotSavedOrUpdatedException{
		log.info("UserServiceImpl -> saveOrUpdateIdleTimeConfiguration()");
		IdleTimeConfiguration idleTimeConfiguration=null;
		AdminUser adminUser=null;
		
		
		try {
			adminUser = this.userDao.getAdminUserByAdminUserId(idleTimeConfigurationDto.getAdminUserDto().getAdminUserId());
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			idleTimeConfiguration=this.userDao.getIdleTimeConfigurationByAdminUser(adminUser);
		}
		catch(IdleTimeConfigurationNotFoundException e){
			idleTimeConfiguration=new IdleTimeConfiguration();
		}
		idleTimeConfiguration.setAdminUser(adminUser);
		idleTimeConfiguration.setIdleMinutes(idleTimeConfigurationDto.getIdleMinutes());
		Integer savedResult=this.userDao.saveOrUpdateIdleTimeConfiguration(idleTimeConfiguration);
		return savedResult;
	}
	/**
	 * created By Bhagya on june24th 2020
	 * @param adminUserId
	 * @return
	 * 
	 * Service for to get the idle time configuration by admin userId
	 */
	public IdleTimeConfigurationDto getIdleTimeConfigurationByAdminUserId(Integer adminUserId){
		log.info("UserServiceImpl -> getIdleTimeConfigurationByAdminUserId ");
		IdleTimeConfigurationDto idleTimeConfigurationDto=null;
		try{
			
			AdminUser adminUser=this.userDao.getAdminUserByAdminUserId(adminUserId);
			IdleTimeConfiguration idleTimeConfiguration=this.userDao.getIdleTimeConfigurationByAdminUser(adminUser);
			idleTimeConfigurationDto=IdleTimeConfigurationDto.populateIdleTimeConfigurationDto(idleTimeConfiguration);
		}
		catch(UserNotFoundException e){
			//e.printStackTrace();
			log.info("Admin User Not Found");
		}
		catch(IdleTimeConfigurationNotFoundException e){
			//e.printStackTrace();
			log.info("Idle time configuration Not found for admin user "+adminUserId);
		}
		return idleTimeConfigurationDto;
	}
	/**
	 * Created by bhagya on june 29th, 2020
	 * @param userDto
	 * @return
	 * @throws CompanyNotFoundException
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * service for to save or update user info
	 * @throws UserNotFoundException 
	 * @throws UserEmailExistanceException 
	 */
	
	public Integer saveOrUpdateUser(UserDto userDto) throws CompanyNotFoundException,UserNotSavedOrUpdatedException,UserEmailExistanceException{
		log.info(" Inside UserServiceImpl -> saveOrUpdateUser ");
		User user=null;
		Boolean newUser=false;
		try{
			if(null!=userDto.getUserId()){
			user=this.userDao.getUserByUserId(userDto.getUserId());
			user.setAccountUpdatedDate(Timestamp.valueOf(LocalDateTime.now()));
			
			}
		}
		catch(UserNotFoundException e){
			try {
				this.userDao.getClientUserByEmail(userDto.getEmailId());
				throw new UserEmailExistanceException();
			}
			 catch(UserNotFoundException e1) {
				 
			 }
			newUser=true;
			user=new User();
			user.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
			user.setStatus("UNKOWN");
			user.setStatusUpdatedDate(Timestamp.valueOf(LocalDateTime.now()));
			user.setIsActive(false);
			user.setIsAppRunning(false);
			user.setAccountActivationToken(this.generateToken());
			 Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				cal.add(Calendar.DATE, 1);
				Date validity = cal.getTime();
				user.setActivationTokenExpiryDate(validity);
				
				//Company company=this.userDao.getCompanybyCompanyName(userDto.getCompanyDto().getCompanyName());
				Company company=this.userDao.getCompanybyCompanyCode(userDto.getCompanyDto().getCompanyCode());
				user.setCompany(company);
				user.setPassword(userDto.getPassword());
			
		}
		
		user.setEmailId(userDto.getEmailId());
		user.setUsername(userDto.getUsername());
		user.setEmployeeName(userDto.getEmployeeName());
		user.setEmployeeNumber(userDto.getEmployeeNumber());
		Integer savedUserId=this.userDao.saveOrUpdateUserInfo(user);
		
		if(null!=savedUserId && newUser==true){
			
			try{
				this.emailSender.sendRegistartionConfirmationMailforClientUser(user);
				} catch (MailNotSentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.info(""); // need to update the log info
				}
		}
		
		return savedUserId;
	}
	/**
	 * Created By Bhagya On June 29th, 2020
	 * @param username
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to get the userid by client username
	 */
	public Integer getUserIdByClientusername(String username)throws UserNotFoundException {
		log.info("UserServiceImpl-> getUserIdByClientUsername()");
		User clientUser = this.userDao.getClientUserByUserName(username);
		return clientUser.getUserId();
	}
	/**
	 * Created by bhagya on june 30th, 2020
	 * @param username
	 * @param encryptedPassword
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to authenticateClientUserLogin 
	 * @throws CompanyNotFoundException 
	 */
	public Integer authenticateClientUserLogin(String username,String password,String companyCode) throws UserNotFoundException, CompanyNotFoundException{
		log.info("UserServiceimpl -> authenticateclientUserLogin()");
		Company company=this.userDao.getCompanybyCompanyCode(companyCode);
		User clientUser=this.userDao.getClientUserByUserNameAndCompany(username, company);
		Boolean isAuthenticated = BCrypt.checkpw(password, clientUser.getPassword());
		Integer userId = 0;
		
		if(isAuthenticated){
			
			userId = clientUser.getUserId();
			
		}
		
		
		return userId;
	}
	/**
	 * Created by bhagya on july 01st, 2020
	 * @param userLoginsDto
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserLoginsNotSavedOrUpdatedException
	 * 
	 * Service for to save user Logins info
	 */
	public Integer saveUserLoginsInfo(UserLoginsDto userLoginsDto) throws UserNotFoundException, UserLoginsNotSavedOrUpdatedException{
		log.info("UserServiceImpl -> saveUserLoginsInfo");
		UserLogins userLogins=new UserLogins();
		userLogins.setLoginDateTime(Timestamp.valueOf(LocalDateTime.now()));
		User user=this.userDao.getClientUserByUserId(userLoginsDto.getUserDto().getUserId());
		userLogins.setUser(user);
		userLogins.setWorkDate(getCurrentDate());
		userLogins.setDescription(userLoginsDto.getDescription());
		Integer savedResult=this.userDao.saveOrUpdateUserLoginsInfo(userLogins);
		return savedResult;
		
	}
	/**
	 * Created By Bhagya on july 01st, 2020
	 * @param userLoginsDto
	 * @return
	 * @throws UserLoginsNotFoundException
	 * @throws UserLoginsNotSavedOrUpdatedException
	 * @throws UserNotFoundException
	 * 
	 * Service for to save user Idle timings info
	 */
	public Integer saveUserIdleTimingsInfo(UserLoginsDto userLoginsDto) throws UserLoginsNotFoundException, UserLoginsNotSavedOrUpdatedException, UserNotFoundException{
		log.info("UserServiceImpl -> saveUserIdleTimingsInfo");
		User user=this.userDao.getClientUserByUserId(userLoginsDto.getUserDto().getUserId());
		UserLogins userLogins=this.userDao.getUserLoginsInfoByUserIdAndDataId(user, userLoginsDto.getDataId());
		userLogins.setIdleDateTime(Timestamp.valueOf(LocalDateTime.now()));
		userLogins.setDescription(userLoginsDto.getDescription());
		Integer savedResult=this.userDao.saveOrUpdateUserLoginsInfo(userLogins);
		return savedResult;
		
	}
	/**
	 * Created By Bhagya On July 01st, 2020
	 * @param userLoginsDto
	 * @return
	 * @throws UserLoginsNotFoundException
	 * @throws UserLoginsNotSavedOrUpdatedException
	 * @throws UserNotFoundException
	 * Service for to update user shutdown info
	 */
	public Integer saveUserShutdownTimingsInfo(UserLoginsDto userLoginsDto) throws UserLoginsNotFoundException, UserLoginsNotSavedOrUpdatedException, UserNotFoundException{
		log.info("UserServiceImpl -> saveUserShutdownTimingsInfo");
		User user=this.userDao.getClientUserByUserId(userLoginsDto.getUserDto().getUserId());
		user.setIsAppRunning(false);
		try {
			this.userDao.saveOrUpdateUserInfo(user);
		} catch (UserNotSavedOrUpdatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UserLogins userLogins=this.userDao.getUserLoginsInfoByUserIdAndWorkDate(user, getCurrentDate());
		userLogins.setShutdownDateTime(Timestamp.valueOf(LocalDateTime.now()));
		userLogins.setDescription(userLoginsDto.getDescription());
		Integer savedResult=this.userDao.saveOrUpdateUserLoginsInfo(userLogins);
		return savedResult;
		
	}
	/**
	 * Created by Bhagya on july 01st, 2020
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * Service for to get user by userid
	 */
	public UserDto getUserByUserId(Integer userId) throws UserNotFoundException
	{
		log.info("UserServiceImpl -> getUserByUserId");
		User user =this.userDao.getClientUserByUserId(userId);
		UserDto userDto=UserDto.populateUserDto(user);
		return userDto;
	}
	/**
	 * Created By Bhagya on july 01st,2020
	 * @param userId
	 * @return
	 * 
	 * Service for to get Idle time configuration by clientuserId
	 */
	public IdleTimeConfigurationDto getIdleTimeConfigurationByClientUserId(Integer userId){
		log.info("UserServiceImpl -> getIdleTimeConfigurationByClientUserId ");
		IdleTimeConfigurationDto idleTimeConfigurationDto=null;
		try{
			User user=this.userDao.getClientUserByUserId(userId);
			AdminUser adminUser=this.userDao.getAdminUserByCompany(user.getCompany());
			IdleTimeConfiguration idleTimeConfiguration=this.userDao.getIdleTimeConfigurationByAdminUser(adminUser);
			idleTimeConfigurationDto=IdleTimeConfigurationDto.populateIdleTimeConfigurationDto(idleTimeConfiguration);
			if(null==idleTimeConfigurationDto.getIdleMinutes()&&idleTimeConfigurationDto.getIdleMinutes().length()==0){
				idleTimeConfigurationDto.setIdleMinutes("5"); // Default time				
			}
		}
		catch(UserNotFoundException e){
			//e.printStackTrace();
			log.info("Admin User Not Found");
		}
		catch(IdleTimeConfigurationNotFoundException e){
			//e.printStackTrace();
			log.info("Idle time configuration Not found for  user ");
		}
		return idleTimeConfigurationDto;
	}
	
	/**
	 * Created by Bhagya On July 01st, 2020
	 * Service for to save user screen capture images
	 */
	public Integer saveUserScreenCaptureImage(Integer userId, MultipartFile captureImage) throws Exception {
		log.info("inside UserServiceImpl -> saveUserScreenCaptureImage() ");
		User user=this.userDao.getClientUserByUserId(userId);
		String s3Result="";
		Integer savedResult=0;
		System.out.println(captureImage.getContentType() + " " + captureImage.getName()
				+ " " + captureImage.getSize());
		String fileType = captureImage.getOriginalFilename().substring(
				captureImage.getOriginalFilename().lastIndexOf(".") + 1);
		Map<String,Object> resultMap=amazonS3Service.saveFileToS3Bucket(userScreenCaptureImagePath, userId, fileType, captureImage,null);
		if(!resultMap.isEmpty()){
			s3Result=(String) resultMap.get("message");
			if(s3Result.equalsIgnoreCase("success")){
				String fileName=(String) resultMap.get("fileName");
				UserScreenCaptures userScreenCaptures=new UserScreenCaptures();
				userScreenCaptures.setCaptureDateTime(Timestamp.valueOf(LocalDateTime.now()));
				userScreenCaptures.setCaptureScreenImage(fileName);
				userScreenCaptures.setUser(user);
				userScreenCaptures.setCompany(user.getCompany());
				savedResult = this.userDao.saveOrUpdateUserScreenCapturesInfo(userScreenCaptures);
			}
		}
		return savedResult;
	}
	/**
	 * Created By Bhagya On July 01st, 2020
	 * @param userId
	 * @param status
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * @throws UserNotFoundException
	 * 
	 * Service for to update the client user status
	 */
	public Integer updateClientUserStatus(Integer userId,String status) throws UserNotSavedOrUpdatedException, UserNotFoundException{
		log.info("Inside UserServiceImpl -> updateClientUserStatus()");
		User user=this.userDao.getClientUserByUserId(userId);
		user.setStatus(status);
		user.setStatusUpdatedDate(Timestamp.valueOf(LocalDateTime.now()));
		Integer updatedResult=this.userDao.saveOrUpdateUserInfo(user);
		return updatedResult;
	}
	
	/**
	 * java.util.date to java.sql.date conversion
	 * @return
	 */
	private static java.sql.Date getCurrentDate() {
	    java.util.Date today = new java.util.Date();
	    return new java.sql.Date(today.getTime());
	}
	
	/**
	 * Created by Harshitha on 12th May, 2021
	 * java.util.date to java.sql.date conversion
	 * @return
	 */
	private static java.sql.Date getFromDate() {
	    java.util.Date today = new java.util.Date();
	    return new java.sql.Date(today.getTime());
	}
	private static java.sql.Date getToDate() {
	    java.util.Date today = new java.util.Date();
	    return new java.sql.Date(today.getTime());
	}
	
	/**
	 * Created By Bhagya On July 01st, 2020
	 * @param userId
	 * @param workHours
	 * @return
	 * 
	 * @throws UserNotFoundException
	 * @throws UserWorkHoursNotSavedOrUpdatedException
	 * 
	 * Service for to save or update user work hours
	 */
	public Integer saveUserWorkHours(Integer userId,String workHours) throws UserNotFoundException, UserWorkHoursNotSavedOrUpdatedException{
		log.info(" Inside UserServiceImpl -> saveUserWorkHours ");
		UserWorkHours userWorkHours=new UserWorkHours();
		User user=this.userDao.getClientUserByUserId(userId);
		userWorkHours.setUser(user);
		userWorkHours.setWorkHours(workHours);
		userWorkHours.setWorkDate(getCurrentDate());
		Integer savedDataId=this.userDao.saveOrUpdateUserWorkHours(userWorkHours);
		return savedDataId;
	}
	
	/**Created By Bhagya On 13th August 2020
	 * 
	 * @param userId
	 * @param workHours
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserWorkHoursNotSavedOrUpdatedException
	 * 
	 * Service for to save or update user Work Hours
	 * 
	 * Modified by bhagya on march 18th, 2021
	 * Checking the condition of TimeSavedInDB and TimeComingFromRequest
	 * 
	 */
	
	
	  public Integer saveOrUpdateUserWorkHours(Integer userId,String workHours,Boolean isAppClosed)throws UserNotFoundException, UserWorkHoursNotSavedOrUpdatedException{
	  log.info(" Inside UserServiceImpl -> saveOrUpdateUserWorkHours ");
	  User user=this.userDao.getClientUserByUserId(userId); 
	  UserWorkHours userWorkHours=null;
	   
	  	try {
		  userWorkHours=this.userDao.getUserWorkHoursByUserAndWorkDateAndIsAppClosed(user,getCurrentDate());
		  SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		  Date t1 = null;
		  Date t2 = null;
		  try {
		   t1=dateFormat.parse(userWorkHours.getWorkHours());
		   t2 = dateFormat.parse(workHours);
		  }
		  catch(Exception e) {
			  
		  }
		 
		  if(t1.before(t2)) {
			  userWorkHours.setWorkHours(workHours);
		  }
		 
	  	}
	  	catch(UserWorkHoursNotFoundException e) {
		  userWorkHours=new UserWorkHours();
		  userWorkHours.setUser(user); 
		  userWorkHours.setWorkDate(getCurrentDate());
		  userWorkHours.setWorkHours(workHours); 
		}
	  userWorkHours.setIsAppClosed(isAppClosed);
	  Integer savedDataId=this.userDao.saveOrUpdateUserWorkHours(userWorkHours);
	  return savedDataId; 
	  }
	 
	/**
	 * Created By Bhagya on july 03rd, 2020
	 * @param userId
	 * @return
	 * @throws UserWorkHoursNotFoundException
	 * @throws UserNotFoundException
	 * 
	 * Service for to get the time worked by user based on current date
	 * 
	 * Modified By bHagya on AUgust 14th, 2020
	 * Commented the method, becuase we are updating the work hours from the schedueler every 10 minutes.
	 *  So, in this method we don't calculate the work hours. 
	 *  Removed the work hours calculation and modified the service
	 */
	
	
	  public Map<String,Object> getTimeWorkedByUserBasedOnCurrentDate(Integer userId) throws UserWorkHoursNotFoundException, UserNotFoundException{
	  log.info("Inside UserServiceimpl -> getTimeWorkedByUserBasedOnCurrentDate() "); 
	  User user=this.userDao.getClientUserByUserId(userId); 
	  Map<String,Object> map=new HashMap<String,Object>();  
	  ArrayList<UserWorkHours> userWorkHours=this.userDao.getListOfUserWorkHoursByWorkDateAndUser(user,getCurrentDate());
	  Integer hours = 0; 
	  Integer minutes = 0; 
	  Integer seconds = 0;
	  
	  for(UserWorkHours userWorkHour: userWorkHours) { 
		  hours +=Integer.parseInt(userWorkHour.getWorkHours().split(":")[0]);
		  minutes +=Integer.parseInt(userWorkHour.getWorkHours().split(":")[1]); 
		  seconds +=Integer.parseInt(userWorkHour.getWorkHours().split(":")[2]);
		  // need to apply divide/60 formula instead of calculating seconds and minutes in for loop
		  if(seconds>=60){
			  seconds=seconds-60; 
			  minutes=minutes+1; 
		 } 
		  if(minutes>=60){
			  minutes=minutes-60; 
			  hours=hours+1; 
		 } 
	} 
	  String displayHours=String.valueOf(hours);
	  String displayMinutes=String.valueOf(minutes);
	  String displaySeconds=String.valueOf(seconds); 
	  // need to handle all these if conditions in hh:mm:ss date format instead of maintaining 3 if loops
	  	if(hours<10){
	  		displayHours="0"+displayHours; 
	  	} 
	  	if(minutes<10){
	  		displayMinutes="0"+displayMinutes;
	  	} 
	  	if(seconds<10){
	  		displaySeconds="0"+displaySeconds;
	  	} 
	  String timeWorked=displayHours+":"+displayMinutes+":"+displaySeconds;
	  SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
	  map.put("date", dateFormat.format(getCurrentDate()));
	  map.put("timeWorked",timeWorked);
	  return map;
	  }
	 /**
	  * Created By Bhagya On October 21st, 2020
	  * @param userId
	  * @param period
	  * @param selectedDate
	  * @return
	  * @throws UserWorkHoursNotFoundException
	  * @throws UserNotFoundException
	  * @throws ParseException
	  * 
	  * Service for to get the employee work hours based on date criteria
	  */
	  public Map<String,Object> getEmployeeWorkHoursBasedOnYesterdayDate(User user,Date yesterdayDate) throws UserWorkHoursNotFoundException, UserNotFoundException, ParseException{
		  log.info("Inside UserServiceimpl -> getEmployeeWorkHoursBasedOnYesterdayDate() "); 
		 
		  Map<String,Object> map=new HashMap<String,Object>();  
		   
		  	ArrayList<UserWorkHours> userWorkHours=this.userDao.getListOfUserWorkHoursByWorkDateAndUser(user, yesterdayDate);
		  	System.out.println(" work hours size "+userWorkHours.size());
			  Integer hours = 0; 
			  Integer minutes = 0; 
			  Integer seconds = 0;
		  
		  for(UserWorkHours userWorkHour: userWorkHours) { 
			  hours +=Integer.parseInt(userWorkHour.getWorkHours().split(":")[0]);
			  minutes +=Integer.parseInt(userWorkHour.getWorkHours().split(":")[1]); 
			  seconds +=Integer.parseInt(userWorkHour.getWorkHours().split(":")[2]);
			  // need to apply divide/60 formula instead of calculating seconds and minutes in for loop
			  if(seconds>=60){
				  seconds=seconds-60; 
				  minutes=minutes+1; 
			 } 
			  if(minutes>=60){
				  minutes=minutes-60; 
				  hours=hours+1; 
			 } 
		} 
		  String displayHours=String.valueOf(hours);
		  String displayMinutes=String.valueOf(minutes);
		  String displaySeconds=String.valueOf(seconds); 
		  // need to handle all these if conditions in hh:mm:ss date format instead of maintaining 3 if loops
		  	if(hours<10){
		  		displayHours="0"+displayHours; 
		  	} 
		  	if(minutes<10){
		  		displayMinutes="0"+displayMinutes;
		  	} 
		  	if(seconds<10){
		  		displaySeconds="0"+displaySeconds;
		  	} 
		  String timeWorked=displayHours+":"+displayMinutes+":"+displaySeconds;
		  SimpleDateFormat dateFormat=new SimpleDateFormat("MM/dd/yyyy");
		  map.put("date", dateFormat.format(yesterdayDate));
		  map.put("timeWorked",timeWorked);
		  return map;
		  }
		 
	 
	/*
	 * public Map<String,Object> getTimeWorkedByUserBasedOnCurrentDate(Integer
	 * userId) throws UserWorkHoursNotFoundException, UserNotFoundException{
	 * log.info("Inside UserServiceimpl -> getTimeWorkedByUserBasedOnCurrentDate() "
	 * ); User user=this.userDao.getClientUserByUserId(userId); Map<String,Object>
	 * map=new HashMap<String,Object>(); UserWorkHours
	 * userWorkHours=this.userDao.getUserWorkHoursByUserAndworkDate(user,
	 * getCurrentDate()); SimpleDateFormat dateFormat=new
	 * SimpleDateFormat("MM/dd/yyyy"); map.put("date",
	 * dateFormat.format(getCurrentDate())); map.put("timeWorked",
	 * userWorkHours.getWorkHours()); return map; }
	 */
	/**
	 * Created by bhagya on july 24th, 2020
	 * Service for get the company based on Company code
	 */
	public CompanyDto getCompanyByCompanyCode(String companyCode) {
		
		log.info("UserServiceImpl-> getCompanyByCompanyCode();");
		try {
			
			Company company=this.userDao.getCompanybyCompanyCode(companyCode);
			if (company != null) {
				 CompanyDto companyDto = CompanyDto.populateCompanyDto(company);
				return companyDto;
			} else {
				return null;
			}
		} 
		catch(CompanyNotFoundException e){
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service - get company by company code " + e.toString());
			return null;
		}
	}
	
	/**
	 * Created by Bhagya on July 25, 201520
	 *  Method to init Forgot Pasword
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * 
	 *             Steps : 1. GET Client User by ID 2. Generate unique code 3.
	 *             Save it to user 4. Send a mail containing code.. 5. Send
	 *             response to mobile app.
	 * 
	 */
	public Integer initForgotPasswordforClient(String email) throws Exception {
		log.info("UserServiceImpl -> initForgotPasswordforClient() ");
		User user=this.userDao.getClientUserByEmail(email);
		user.setPasswordToken(RandomStringUtils.randomNumeric(4));
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 1);
		user.setPasswordTokenExpiryDate(cal.getTime());
		Integer updatedResult = this.userDao.saveOrUpdateUserInfo(user);
		// sending forgot password mail.
		this.emailSender.sendForgotPasswordMailforClientUser(user);
		return updatedResult;
	}

	/**
	 * Created by Bhagya on July 25, 2020
	 * 
	 * Method to reset password for client user
	 * 
	 * @param email
	 * @param token
	 * @throws Exception
	 */
	public void resetClientUserPassword(String email, String token,String newPassword) throws Exception {
		log.info("UserServiceImpl -> resetClientUserPassword()");
		User user=this.userDao.getClientUserByEmail(email);
		if (token.equals(user.getPasswordToken())) {
			user.setPasswordToken("");
			user.setPasswordTokenExpiryDate(null);
			user.setPassword(this.bCryptEncoder.encode(newPassword));
			this.userDao.saveOrUpdateUserInfo(user);
		} else {
			throw new PasswordNotMatchException();
		}
	}
	
	/**
	 * Created By bhagya on july 28th, 2020
	 * 
	 * Method to Activate User
	 * 1. Gets User based on Account TOken..
	 * 2. Updates isApproved(true)
	 * 3. Delete Account Token..
	 * 4. Update User.
	 * 5. Send Response Back
	 */
	public Integer activateClientUserFromAccountToken(String token)throws Exception{
		log.info("UserServiceImpl-> activateClientUserFromAccountToken()");
		Integer result = 0;
		User user=this.userDao.getClientUserByAccountToken(token);
		Date activationTokenExpiryDate = user.getActivationTokenExpiryDate();
		if (activationTokenExpiryDate.after(new Date())) {
			user.setIsActive(true);
			user.setAccountActivationToken(null);
			user.setActivationTokenExpiryDate(null);
		 result=this.userDao.saveOrUpdateUserInfo(user);
		}
		
		return result;
	}
	/**
	 * Created By Bhagya on August 14th, 2020
	 * @param userLoginsDto
	 * @return
	 * @throws UserLoginsNotFoundException
	 * @throws UserLoginsNotSavedOrUpdatedException
	 * @throws UserNotFoundException
	 * 
	 * Service for to save the user logout timings info
	 */
	public Integer saveUserLogoutTimingsInfo(UserLoginsDto userLoginsDto) throws UserLoginsNotFoundException, UserLoginsNotSavedOrUpdatedException, UserNotFoundException{
		log.info("UserServiceImpl -> saveUserLogoutTimingsInfo");
		User user=this.userDao.getClientUserByUserId(userLoginsDto.getUserDto().getUserId());
		user.setIsAppRunning(false);
		try {
			this.userDao.saveOrUpdateUserInfo(user);
		} catch (UserNotSavedOrUpdatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UserLogins userLogins=this.userDao.getUserLoginsInfoByUserIdAndWorkDate(user, getCurrentDate());
		userLogins.setLogoutDateTime(Timestamp.valueOf(LocalDateTime.now()));
		userLogins.setDescription(userLoginsDto.getDescription());
		Integer savedResult=this.userDao.saveOrUpdateUserLoginsInfo(userLogins);
		return savedResult;
		
	}
	
	/**
	 * Created By Bhagya on August 25th, 2020
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * @throws UserWorkHoursNotSavedOrUpdatedException
	 * 
	 * Service for to update user work hours is app closed flag value.
	 * @throws UserWorkHoursNotFoundException 
	 */
	
	public Integer UpdateUserWorkHoursIsAppClosedFlag(Integer userId)throws UserNotFoundException, UserWorkHoursNotSavedOrUpdatedException, UserWorkHoursNotFoundException{
		  log.info(" Inside UserServiceImpl -> UpdateUserWorkHoursIsAppClosedFlag ");
		  User user=this.userDao.getClientUserByUserId(userId); 
		  UserWorkHours userWorkHours=this.userDao.getUserWorkHoursByUserAndWorkDateAndIsAppClosed(user,getCurrentDate());
			  userWorkHours.setIsAppClosed(true);
			  Integer savedDataId=this.userDao.saveOrUpdateUserWorkHours(userWorkHours);
		
		  return savedDataId; 
		  }
	 /**
	  * Created by Bhagya on March 10th 2021
	  * @param userId
	  * @param captureImages
	  * @return
	  * @throws Exception
	  * 
	  * Service for to save the multiple screenshots which was captured offline
	  */
	public Integer saveUserScreenCaptureMultipleImages(List<MultipartFile> captureImages) throws Exception {
		log.info("inside UserServiceImpl -> saveUserScreenCaptureMutlipleImages() ");
		
		String s3Result="";
		Integer savedResult=0;
		for(MultipartFile captureImage:captureImages) {
			String originalFileName=captureImage.getOriginalFilename();
			Integer indexvalue=originalFileName.indexOf("_");
			Integer userId=Integer.parseInt(originalFileName.substring(0,indexvalue));
			User user=this.userDao.getClientUserByUserId(userId);
			String fileType = captureImage.getOriginalFilename().substring(
					captureImage.getOriginalFilename().lastIndexOf(".") + 1);
			System.out.println(" file type "+fileType);
			Map<String,Object> resultMap=amazonS3Service.saveFileToS3Bucket(userScreenCaptureImagePath, userId, fileType, captureImage,originalFileName);
			if(!resultMap.isEmpty()){
				s3Result=(String) resultMap.get("message");
				if(s3Result.equalsIgnoreCase("success")){
					String fileName=(String) resultMap.get("fileName");
					UserScreenCaptures userScreenCaptures=new UserScreenCaptures();
					//userScreenCaptures.setCaptureDateTime(Timestamp.valueOf(LocalDateTime.now()));
					String timestamp=originalFileName.substring(indexvalue+1,originalFileName.indexOf(".p")).replace("#", ":");
					userScreenCaptures.setCaptureDateTime(Timestamp.valueOf(timestamp));
					
					userScreenCaptures.setCaptureScreenImage(fileName);
					userScreenCaptures.setUser(user);
					userScreenCaptures.setCompany(user.getCompany());
					savedResult = this.userDao.saveOrUpdateUserScreenCapturesInfo(userScreenCaptures);
				}
			}
		}
		return savedResult;
	}
	/**
	 * Created by Harshitha on March 3rd, 2021
	 * Service to get the employee number
	 */
	public UserDto getEmployeeNumber(String employeeNumber) throws EmployeeNumberNotFoundException{
		
		log.info("UserServiceImpl-> getEmployeeNumber();");
		try {
			
			User user=this.userDao.getEmployeeNumber(employeeNumber);
			if (user != null) {
				UserDto userDto = UserDto.populateUserDto(user);
				return userDto;
			} else {
				return null;
			}
		} 
		catch(EmployeeNumberNotFoundException e){
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service - get employee number " + e.toString());
			return null;
		}
	}
	
	/**
	 * Created by Harshitha on March 4th, 2021
	 * Service to get the employee email id
	 */
	public UserDto getEmployeeEmailId(String emailId) throws EmployeeEmailIdNotFoundException{
		
		log.info("UserServiceImpl-> getEmployeeEmailId();");
		try {
			
			User user=this.userDao.getEmployeeEmailId(emailId);
			if (user != null) {
				UserDto userDto = UserDto.populateUserDto(user);
				return userDto;
			} else {
				return null;
			}
		} 
		catch(EmployeeEmailIdNotFoundException e){
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("error in user Service - get employee email id " + e.toString());
			return null;
		}
	
	}

	/**
	 * Created by Harshitha on May 14th, 2021
	  * Service to get the Weekly work hours 
	 */
	 public ArrayList<WeeklyWorkHoursDto> getWeeklyWorkHours(String fromDate,String toDate) throws  UserNotFoundException{
		  log.info("Inside UserServiceimpl -> getWeeklyWorkHours() "); 
		  System.out.println("inside weekly WH service layer" + fromDate + toDate);
		  ArrayList<WeeklyWorkHoursDto> weeklyWorkHoursDtos = new ArrayList<WeeklyWorkHoursDto>();
		  try {
		  ArrayList<User> users=this.employeeDao.getListOfUsers();
		  System.out.println("userSize" + users.size());
		  for(User user:users) {
			  
			  WeeklyWorkHoursDto weeklyWorkHoursDto = new WeeklyWorkHoursDto();
			  
			  try {
				  
			  ArrayList<UserCompletedWorkHours> userCompletedWorkHours=this.userDao.getListOfUserWorkHoursByFromDateToDateAndUser(user,sqlDateFormat(fromDate),sqlDateFormat(toDate));
			  System.out.println("completedWorkHourSize" + userCompletedWorkHours.size() + "userId" + user.getUserId());
			  Integer hours = 0; 
			  Integer minutes = 0; 
			  Integer seconds = 0;
			  Integer count = 1;
			 
			  for(UserCompletedWorkHours userCompletedWorkHours1:userCompletedWorkHours ) { 
				 System.out.println(" Count Value "+count);
				if(count==1)
				{
					 System.out.println("inside if block- 1st tittle");
					weeklyWorkHoursDto.setDay1Hours(userCompletedWorkHours1.getWorkHours());
					 System.out.println("inside 1st tittle dto");
					weeklyWorkHoursDto.setDay1Title(userCompletedWorkHours1.getWorkDate().toString());
				}
				if(count==2)
				{
			 System.out.println("inside if block- 2nd tittle");
					weeklyWorkHoursDto.setDay2Hours(userCompletedWorkHours1.getWorkHours());
					 System.out.println("inside 2nd tittle data dto");
					weeklyWorkHoursDto.setDay2Title(userCompletedWorkHours1.getWorkDate().toString());
				}
				if(count==3)
				{
					System.out.println("inside if block- 3rd tittle");
					weeklyWorkHoursDto.setDay3Hours(userCompletedWorkHours1.getWorkHours());
					 System.out.println("inside 3rd tittle data dto");
					weeklyWorkHoursDto.setDay3Title(userCompletedWorkHours1.getWorkDate().toString());
				}
				if(count==4)
				{
					weeklyWorkHoursDto.setDay4Hours(userCompletedWorkHours1.getWorkHours());
					weeklyWorkHoursDto.setDay4Title(userCompletedWorkHours1.getWorkDate().toString());
				}
				if(count==5)
					
					
				{
					weeklyWorkHoursDto.setDay5Hours(userCompletedWorkHours1.getWorkHours());
					weeklyWorkHoursDto.setDay5Title(userCompletedWorkHours1.getWorkDate().toString());
				}
				if(count==6)
				{
					weeklyWorkHoursDto.setDay6Hours(userCompletedWorkHours1.getWorkHours());
					weeklyWorkHoursDto.setDay6Title(userCompletedWorkHours1.getWorkDate().toString());
				}
				if(count==7)
				{
					weeklyWorkHoursDto.setDay7Hours(userCompletedWorkHours1.getWorkHours());
					weeklyWorkHoursDto.setDay7Title(userCompletedWorkHours1.getWorkDate().toString());
				}
				
				count=count+1;
				
				  hours +=Integer.parseInt(userCompletedWorkHours1.getWorkHours().split(":")[0]);
				  minutes +=Integer.parseInt(userCompletedWorkHours1.getWorkHours().split(":")[1]); 
				  seconds +=Integer.parseInt(userCompletedWorkHours1.getWorkHours().split(":")[2]);
				  // need to apply divide/60 formula instead of calculating seconds and minutes in for loop
				  if(seconds>=60){
					  seconds=seconds-60; 
					  minutes=minutes+1; 
				 } 
				  if(minutes>=60){
					  minutes=minutes-60; 
					  hours=hours+1; 
				 } 
			} // for loop
			  
			  String displayHours=String.valueOf(hours);
			  String displayMinutes=String.valueOf(minutes);
			  String displaySeconds=String.valueOf(seconds); 
			  
			  	if(hours<10){
			  		displayHours="0"+displayHours; 
			  	} 
			  	if(minutes<10){
			  		displayMinutes="0"+displayMinutes;
			  	} 
			  	if(seconds<10){
			  		displaySeconds="0"+displaySeconds;
			  	} 
			  String totalHours=displayHours+":"+displayMinutes+":"+displaySeconds;
			  System.out.println("Total Hours" +totalHours);
              weeklyWorkHoursDto.setTotalHours(totalHours);	 
              weeklyWorkHoursDto.setUserDto(UserDto.populateUserDto(user));
              weeklyWorkHoursDtos.add(weeklyWorkHoursDto);
              System.out.println("end of try block");
			  }// try- User Completed Work Hours
			  catch(ParseException e) {
				  System.out.println("Inside Parse Exception ");
			  }
			  catch(UserWorkHoursNotFoundException e) {
				  
			  }
			  
		  } //for loop
		  }	//try - user & Weekly Work Hours
		  catch(UserNotFoundException e) {
			  
		  }
		  return weeklyWorkHoursDtos;
		  }
	 
	 
	 
	 public static Date sqlDateFormat(String date) throws ParseException{
		 
	        SimpleDateFormat monthDateFormat = new SimpleDateFormat("MM/dd/yyyy");
	        SimpleDateFormat yearMonthFormat = new SimpleDateFormat("yyyy-MM-dd");
	        Date mmDDFormat=monthDateFormat.parse(date);
	        date=yearMonthFormat.format(mmDDFormat); 
	        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	        Date yearMMDD=sqlDateFormat.parse(date);
	        
	        return yearMMDD;
	    }
	 /**
	  * Created By Bhagya On October 21st, 2021
	  * Service for to update user client app running status and send the login confirmation mail
	  * 
	  * 1. By default, IsAppRunning field is False
	  * 2. Once after user login authentication setting the ISAPPRUNNING field value as True.
	  */
	 public Integer updateUserApppRunningStatusAndLoginConfirmationMail(Integer userId) throws UserNotFoundException {
		 User user=this.userDao.getClientUserByUserId(userId);
		 user.setIsAppRunning(true);
		 Integer userNo=0;
		 try {
			userNo=this.userDao.saveOrUpdateUserInfo(user);
		} catch (UserNotSavedOrUpdatedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 UserDto userDto=UserDto.populateUserDto(user);
			
			  try { 
				  this.emailSender.sendIdleMonitorLoginEmail(userDto); 
				  } 
			  catch(MailNotSentException e) { 
				  log.info("UserServiceimpl -> authenticateclientUserLogin() -> MailNotSentException()");
				  e.printStackTrace();
				  }
			 
		 return userNo;
	 }
	}

