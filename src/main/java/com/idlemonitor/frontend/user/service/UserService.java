package com.idlemonitor.frontend.user.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeEmailIdNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeNumberNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.MailNotSentException;
import com.idlemonitor.common.exceptions.UserEmailExistanceException;
import com.idlemonitor.common.exceptions.UserLoginsNotFoundException;
import com.idlemonitor.common.exceptions.UserLoginsNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotSavedOrUpdatedException;
import com.idlemonitor.frontend.admin.dto.CompanyDto;
import com.idlemonitor.frontend.user.dto.AdminUserDto;
import com.idlemonitor.frontend.user.dto.IdleTimeConfigurationDto;
import com.idlemonitor.frontend.user.dto.UserDto;
import com.idlemonitor.frontend.user.dto.UserLoginsDto;
import com.idlemonitor.frontend.user.dto.WeeklyWorkHoursDto;
/**
 * Created By Bhagya
 * @author user
 * Interface class for USer Services
 */
public interface UserService{
	public AdminUserDto getUserByUsernameOrEmailId(String emailId);
	public Integer saveOrUpdateAdminUser(AdminUserDto adminUserDto) throws UserNotSavedOrUpdatedException;
	public Integer sendPasswordResetEmail(String email)throws UserNotFoundException, UserNotSavedOrUpdatedException,MailNotSentException;
	public Integer getUserIdByPasswordResetToken(String token)throws UserNotFoundException;
	public Integer handlePasswordReset(Integer userId, String password)throws UserNotFoundException, UserNotSavedOrUpdatedException;
	public Integer activateAdminUserFromAccountToken(String token)throws Exception;
	public AdminUserDto getAdminUserByAdminUserId(Integer adminUserId)throws UserNotFoundException;
	public CompanyDto getCompanyByCompanyName(String companyName);
	public Integer saveOrUpdateIdleTimeConfiguration(IdleTimeConfigurationDto idleTimeConfigurationDto) throws IdleTimeConfigurationNotSavedOrUpdatedException;
	public IdleTimeConfigurationDto getIdleTimeConfigurationByAdminUserId(Integer adminUserId);
	public Integer saveOrUpdateUser(UserDto userDto) throws CompanyNotFoundException,UserNotSavedOrUpdatedException, UserEmailExistanceException;
	public Integer getUserIdByClientusername(String username)throws UserNotFoundException;
	public Integer authenticateClientUserLogin(String username,String encryptedPassword,String companyCode) throws UserNotFoundException,CompanyNotFoundException;
	public Integer saveUserLoginsInfo(UserLoginsDto userLoginsDto) throws UserNotFoundException, UserLoginsNotSavedOrUpdatedException;
	public Integer saveUserIdleTimingsInfo(UserLoginsDto userLoginsDto) throws UserLoginsNotFoundException, UserLoginsNotSavedOrUpdatedException, UserNotFoundException;
	public Integer saveUserShutdownTimingsInfo(UserLoginsDto userLoginsDto) throws UserLoginsNotFoundException, UserLoginsNotSavedOrUpdatedException, UserNotFoundException;
	public UserDto getUserByUserId(Integer userId) throws UserNotFoundException;
	public IdleTimeConfigurationDto getIdleTimeConfigurationByClientUserId(Integer userId);
	public Integer saveUserScreenCaptureImage(Integer userId, MultipartFile captureImage) throws Exception;
	public Integer updateClientUserStatus(Integer userId,String status) throws UserNotSavedOrUpdatedException, UserNotFoundException;
	public Integer saveUserWorkHours(Integer userId,String workHours) throws UserNotFoundException, UserWorkHoursNotSavedOrUpdatedException;
	public Map<String,Object> getTimeWorkedByUserBasedOnCurrentDate(Integer userId) throws UserWorkHoursNotFoundException, UserNotFoundException;
	public CompanyDto getCompanyByCompanyCode(String companyCode);
	public Integer initForgotPasswordforClient(String email) throws Exception;
	public void resetClientUserPassword(String email, String token,String newPassword) throws Exception;
	public Integer activateClientUserFromAccountToken(String token)throws Exception;
	public Integer saveOrUpdateUserWorkHours(Integer userId,String workHours,Boolean isAppClosed) throws UserNotFoundException, UserWorkHoursNotSavedOrUpdatedException;
	public Integer saveUserLogoutTimingsInfo(UserLoginsDto userLoginsDto) throws UserLoginsNotFoundException, UserLoginsNotSavedOrUpdatedException, UserNotFoundException;
	public Integer UpdateUserWorkHoursIsAppClosedFlag(Integer userId)throws UserNotFoundException, UserWorkHoursNotSavedOrUpdatedException, UserWorkHoursNotFoundException;
	public Map<String,Object> getEmployeeWorkHoursBasedOnYesterdayDate(User user,Date yesterdayDate) throws UserWorkHoursNotFoundException, UserNotFoundException, ParseException;
	public Integer saveUserScreenCaptureMultipleImages(List<MultipartFile> captureImages) throws Exception;
	/**
	 * Created By Harshitha
	 * Interface class for User Services for the employee number validation
	 */
	public UserDto getEmployeeNumber(String employeeNumber) throws EmployeeNumberNotFoundException;
	
	/*
	 * Created By Harshitha
	 * Interface class for User Services for the employee email id validation
	 */
	public UserDto getEmployeeEmailId(String emailId) throws EmployeeEmailIdNotFoundException;
	/*
	 * Created By Harshitha
	 * Interface class for User's Weekly Worked Hours 
	 */
	 public ArrayList<WeeklyWorkHoursDto> getWeeklyWorkHours(String fromDate,String toDate) throws  UserNotFoundException;
	 public Integer updateUserApppRunningStatusAndLoginConfirmationMail(Integer userId) throws UserNotFoundException ;
}
