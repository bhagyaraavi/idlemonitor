package com.idlemonitor.frontend.user.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
import com.idlemonitor.common.utility.JsonParser;
import com.idlemonitor.frontend.admin.dto.CompanyDto;
import com.idlemonitor.frontend.user.dto.AdminUserDto;
import com.idlemonitor.frontend.user.dto.IdleTimeConfigurationDto;
import com.idlemonitor.frontend.user.dto.UserCompletedWorkHoursDto;
import com.idlemonitor.frontend.user.dto.UserDto;
import com.idlemonitor.frontend.user.dto.WeeklyWorkHoursDto;
import com.idlemonitor.frontend.user.service.UserService;
import com.idlemonitor.frontend.utility.dto.DisplayListBeanDto;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

import com.idlemonitor.common.exceptions.EmployeeNumberNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeEmailIdNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeNotFoundException;
/**
 * Created By Bhagya
 * @author user
 * 
 * Controller class managing the web or admin users
 *
 */
@Controller("userManagementController")
@RequestMapping("/user")
public class UserManagementController{
	
	private static Logger log=Logger.getLogger(UserManagementController.class);
	
	@Resource(name="userService")
	private UserService userService;
	
	/**
	 * Created By Bhagya On May 28th, 2020
	 * @param map
	 * @param providerDto
	 * @return
	 * 
	 * Service for to initiate the Admin user registartion
	 */
	@RequestMapping(value="/adminregistration.do",method=RequestMethod.GET)
	public String initAdminUserRegistration(Map<String,Object> map,@ModelAttribute("adminUserDto") AdminUserDto adminUserDto){
		log.info( " UserManagementController -> initAdminUserRegistration()");

		try{
			
			return "admin/adminRegistration";
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating admin registration  "+e.getMessage());
			map.put("message", "Error while initiating admin registration ");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On May 28th, 2020
	 * @param map
	 * @param request
	 * @param response
	 * @param adminUserDto
	 * @param redAttribs
	 * @return
	 * 
	 * Service for to save or update Admin registration details
	 */
	
	@RequestMapping(value="/adminregistration.do",method=RequestMethod.POST)
	public String adminRegistration(Map<String,Object> map,HttpServletRequest request,HttpServletResponse response,@ModelAttribute("adminUserDto") AdminUserDto adminUserDto,RedirectAttributes redAttribs){
		log.info(" UserManagementController -> adminRegistration()");
		try{
			Integer savedResult=this.userService.saveOrUpdateAdminUser(adminUserDto);
			
			 if(savedResult>0){
					return "redirect:/user/registersuccess.do?email="+adminUserDto.getContactEmail();
					
			    }
			    else {
					throw new Exception();
			    }
		
		}
		catch(UserNotSavedOrUpdatedException e){
			map.put("message", "User Not Saved Or Updated Exception");
			return "error";
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while saving admin user registration information  "+e.getMessage());
			map.put("message", "Error while saving admin user registration information");
			return "error";
		}
	}
	
	/**
	 * Created By Bhagya On May 28th, 2020
	 * @param username
	 * @return
	 * 
	 * Method for to check the user Existance based on either username or email
	 */
	@RequestMapping(value="/checkuserexistance.do", method=RequestMethod.GET)
	@ResponseBody
	public String checkUsernameOrEmailExistance(@RequestParam("user")String user){
		log.info("UserManagementController -> checkUsernameOrEmailExistance()");
		try{
			
			AdminUserDto adminUserDto=this.userService.getUserByUsernameOrEmailId(user);
			if(null==adminUserDto){
				throw new UserNotFoundException();
			}
			else{
				return "fail";
			}
		}
		catch(UserNotFoundException e)
		{
			return "success";
		}
		catch(Exception e){
			log.error(" Error while check use existance "+e.getMessage());
			e.printStackTrace();
			return "fail";
		}
		 
	}
	/**
	 * Created By Bhagya On May 28th, 2020
	 * @return
	 * Method for to initiate the forgot password
	 */
	
	@RequestMapping(value="/forgotpassword.do", method=RequestMethod.GET)
	public String initForgotPassword(Map<String,Object> map){
		log.info("UserManagementController -> initForgotPassword()");
		try{
			/*
			 * String msg= ""; map.put("error", msg);
			 */
			return "admin/forgotPassword";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiate forgot password "+e.getMessage());
			map.put("message", "Error while initiate forgot password");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya On May 28th, 2020
	 * @return
	 * 
	 * Method for to perform the forgot password  functionality
	 */
	@RequestMapping(value="/forgotpassword.do", method=RequestMethod.POST)
	public String performForgotPassword(@RequestParam("emailId")String emailId,Map<String,Object> map){
		log.info("UserManagementController -> performForgotPassword()");
		try{	
			if(!emailId.equals("")){
				String msg="";
				/*
				 * AdminUserDto
				 * adminUserDto=this.userService.getUserByUsernameOrEmailId(emailId);
				 */
				Integer successMail=this.userService.sendPasswordResetEmail(emailId);
				if(successMail==1) {
					msg= "Password reset link has been sent to your registered email, please check your mail.";
					map.put("status", msg);
					return "admin/login";
				}
				else {
					throw new Exception();
				}
				
			}
			else{
				String msg= "Please enter the email id.";
				map.put("error", msg);
				return "admin/forgotPassword";
			}
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String msg= "Email not registered, please Sign Up.";
			map.put("status", msg);
			return "login";
		}
		catch(Exception e){
			log.error(" Error while performing forgot Password "+e.getMessage());
			String msg= "Error while performing forgot password";
			map.put("message",msg);
			e.printStackTrace();
			return "error";
		}
		
	}
	
	/**
	 * Created By bhagya On May 28th, 2020
	 * @param token
	 * @param map
	 * @return
	 * 
	 * Method for initiate the Reset Password
	 */
	@RequestMapping(value="/resetpassword.do",method=RequestMethod.GET)
	public String initResetPassword(@RequestParam("token") String token,Map<String,Object> map){
		log.info("UserManagementController -> initResetPassword()");
		try{
			Integer userId = this.userService.getUserIdByPasswordResetToken(token);
			map.put("userId", userId);
			/*
			 * String msg= ""; map.put("error", msg);
			 */
			return "admin/resetPassword";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiate reset Password "+e.getMessage());
			map.put("errorMessage","This link is not valid any more");
			return "login";
		}
	}
	
	/**
	 * Created By Bhagya on May 28th, 2020
	 * @param userId
	 * @param password
	 * @param confirmPassword
	 * @param map
	 * @return
	 * Method For Saving the reset Password
	 */
	@RequestMapping(value="/resetpassword.do",method=RequestMethod.POST)
	public String performResetPassword(@RequestParam("userId") Integer userId,@RequestParam("password") String password,
			@RequestParam("confirmPassword") String confirmPassword,Map<String,Object> map){
		log.info("UserManagementController ->performResetPassword()");
		try{
			map.put("userId", userId);
			if(!password.equals("") && !confirmPassword.equals("")){
				if(password.length()<8){
					
					map.put("error", "Password must be more than 8 characters");
					return "admin/resetPassword";
				}
				if(password.equals(confirmPassword)){
					this.userService.handlePasswordReset(userId, password);
					map.put("status", "Password Reset Successfully");
					return "admin/login";
					}
				else{
					map.put("error", "Confirmation Password doesn't match Password, Please enter again");
					return "admin/resetPassword";
					}	
				}
			else{
					
				if(password.equals("") && confirmPassword.equals("")){
						map.put("error", "Enter all the values");
				}
				return "admin/resetPassword";	
			}
		}
		catch(UserNotFoundException e){
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(UserNotSavedOrUpdatedException e){
			String error="User Not Saved Or Updated";
			map.put("message",error);
			return "error";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while reset Password "+e.getMessage());
			String error="Problem While Reset Password, Please Try Again Later";
			map.put("message",error);
			return "error";
		}
		
	}
	
	/**
	 * Created By Bhagya on May 28th, 2020
	 * @param map
	 * @param email
	 * @return
	 * 
	 * Method for to send registration confirmation message
	 */
	@RequestMapping(value="/registersuccess.do",method=RequestMethod.GET)
	public String registrationSuccess(Map<String,Object> map, @RequestParam("email") String email){
		log.info(" UserManagementController -> registrationSuccess()");
		try{
				 	String message="Registered Successfully, A Mail has been sent to your email: "+email+", Please click on the activation link to activate your account ";
					String title="Successfully Registered to Idle Monitor, Please check your mail to activate the account";
					map.put("title", title);
					map.put("message", message);
					return "admin/registerSuccess";
		    
		}
					
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while sending registration success information  "+e.getMessage());
			map.put("message", "Error while sending registration success information");
			return "error";
		}
	}
	
	/**
	 * Created By Bhagya On May 28th, 2020
	 * Activating User Based on Account Token
	 */
	@RequestMapping(method=RequestMethod.GET,value="/activateuser.do")
	public String activateAdminUserAccount(@RequestParam("token") String accountToken,@RequestParam("uname") String username,Map<String, Object> map,RedirectAttributes redAttribs){
		log.info("inside UserManagementController -> activateAdminUserAccount()");
		try{	
			Integer activationResult=this.userService.activateAdminUserFromAccountToken(accountToken);
			AdminUserDto adminUserDto=this.userService.getUserByUsernameOrEmailId(username);
			
			if(activationResult>0){
				return "redirect:/login.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(UserNotFoundException e){
			String message="No User found, User may be activated already";
			String title="User Activation";
			map.put("title", title);
			log.error("No User found, User may be activated already"+e.getMessage()+" "+e.toString());
			map.put("message", message);
			
			return "admin/registrationSuccess";
		}
		catch(Exception e){
			String message="Error while activating user";
			e.printStackTrace();
			log.error(message+" "+e.getMessage());
			map.put("message", message);
			return "error";
		}
	}
	
	/**
	 * Created By Bhagya on May 28th, 2020
	 * @param locationId
	 * @param map
	 * @return
	 * 
	 * Method for to initiate the edit provider Location
	 */
	@RequestMapping(value="editadminregistration.do",method=RequestMethod.GET)
	public String initEditAdminRegistration(@RequestParam("adminUserId") Integer adminUserId,Map<String,Object> map,@ModelAttribute("adminUserDto") AdminUserDto userDto){
		log.info("UserManagementController -> initEditAdminRegistration() ");
		try{
			
			AdminUserDto adminUserDto=this.userService.getAdminUserByAdminUserId(adminUserId);
			map.put("adminUserDto", adminUserDto);
			return "admin/editAdminRegistration";
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while edit registration  "+e.getMessage());
			String error="Error while edit registration";
			map.put("message",error);
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya on May 28th, 2020
	 * @param providerLocationDto
	 * @param redAttribs
	 * @param map
	 * @return
	 * 
	 * Method for to update the provider Location
	 */
	
	@RequestMapping(value="editadminregistration.do",method=RequestMethod.POST)
	public String editAdminRegistration(@ModelAttribute("adminUserDto") AdminUserDto adminUserDto,RedirectAttributes redAttribs,Map<String,Object> map){
		log.info(" UserManagementController -> editAdminRegistration()");
		try{
			/*Authentication auth=SecurityContextHolder.getContext().getAuthentication();
			String username=auth.getName();*/
			Integer updatedResult=this.userService.saveOrUpdateAdminUser(adminUserDto);
			if(updatedResult>0){
				redAttribs.addFlashAttribute("status","Registered User Updated Successfully");
				return "redirect:/employee/viewemployees.do";
			}
			else{
				throw new Exception();
			}
		}
		catch(UserNotFoundException e){
			e.printStackTrace();
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while updating user details  "+e.getMessage());
			String error="Error while updating user details";
			map.put("message",error);
			return "error";
		}
	}
	
	/**
	 * Created By Bhagya On june 05th, 2020
	 * @param username
	 * @return
	 * 
	 * Method for to check the Company Existance based on company name
	 */
	@RequestMapping(value="/checkcompanyexistance.do", method=RequestMethod.GET)
	@ResponseBody
	public String checkCompanyExistance(@RequestParam("companyName")String companyName){
		log.info("UserManagementController -> checkCompanyExistance()");
		try{
			
			CompanyDto companyDto=this.userService.getCompanyByCompanyName(companyName);
			if(null==companyDto){
				throw new CompanyNotFoundException();
			}
			else{
				return "fail";
			}
		}
		catch(CompanyNotFoundException e)
		{
			return "success";
		}
		catch(Exception e){
			log.error(" Error while check company existance"+e.getMessage());
			e.printStackTrace();
			return "fail";
		}
		 
	}
	/**
	 * Created By Bhagya On May 28th, 2020
	 * @param map
	 * @param providerDto
	 * @return
	 * 
	 * Service for to initiate the Admin user registartion
	 */
	@RequestMapping(value="/admindashboard.do",method=RequestMethod.GET)
	public String initAdminUserDashboard(Map<String,Object> map){
		log.info( " UserManagementController -> initAdminUserDashboard()");

		try{
			
			return "admin/dashboard";
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while initiating admin Dashboard "+e.getMessage());
			map.put("message", "Error while initiating admin Dashboard ");
			return "error";
		}
		
	}
	/**
	 * Created By Bhagya on june 22nd, 2020
	 * @param request
	 * @param map
	 * @param idleTimeConfigurationDto
	 * @return
	 * 
	 * Service for to save or update Idletimeconfiguration
	 */
	@RequestMapping(value="/idletimeconfiguration.do", method=RequestMethod.GET)
	@ResponseBody
	public String saveOrUpdateIdleTimeConfiguration(HttpServletRequest request,Map<String,Object> map,@RequestParam("idleMinutes")String idleMinutes){
		log.info( " UserManagementController -> saveOrUpdateIdleTimeConfiguration()");
		try{
			HttpSession session=request.getSession();
			Integer adminUserId=(Integer) session.getAttribute("userId");
			AdminUserDto adminUserDto=this.userService.getAdminUserByAdminUserId(adminUserId);
			IdleTimeConfigurationDto idleTimeConfigurationDto=new IdleTimeConfigurationDto();
			idleTimeConfigurationDto.setAdminUserDto(adminUserDto);
			idleTimeConfigurationDto.setIdleMinutes(idleMinutes);
			Integer result=this.userService.saveOrUpdateIdleTimeConfiguration(idleTimeConfigurationDto);
			if(result>0){
				return "success";
			}
			else{
				throw new IdleTimeConfigurationNotSavedOrUpdatedException();
			}
			
		}
		catch(IdleTimeConfigurationNotSavedOrUpdatedException e){
			String error="IdleTimeConfiguration Not Saved Or Updated Exception";
			map.put("message",error);
			return "fail";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while saving or updating idletimeconfiguration "+e.getMessage());
			map.put("message", "Error while saving or updating idletimeconfiguration ");
			return "fail";
		}
		
	}
	/**
	 * Created By Bhagya on June 24th, 2020
	 * @param request
	 * @param map
	 * @return
	 * 
	 * Ajax service for to get the idle time configuration data
	 */
	@RequestMapping(value="/getidletimeconfiguration.do", method=RequestMethod.GET)
	@ResponseBody
	public String getIdleTimeConfiguration(HttpServletRequest request,Map<String,Object> map){
		log.info( " UserManagementController -> getIdleTimeConfiguration()");
		String idleMinutes="";
		try{
			HttpSession session=request.getSession();
			Integer adminUserId=(Integer) session.getAttribute("userId");
			IdleTimeConfigurationDto idleTimeConfigurationDto=this.userService.getIdleTimeConfigurationByAdminUserId(adminUserId);
			if(null!=idleTimeConfigurationDto){
				idleMinutes= idleTimeConfigurationDto.getIdleMinutes();
			}
					
			
			return idleMinutes;
			
			
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while getting idletimeconfiguration "+e.getMessage());
			map.put("message", "Error while saving or updating getting  idletimeconfiguration ");
			return "fail";
		}
		
	}
	
	/**
	 * Created By Bhagya On june 05th, 2020
	 * @param username
	 * @return
	 * 
	 * Method for to check the Company Existance based on company name
	 */
	@RequestMapping(value="/checkcompanycodeexistance.do", method=RequestMethod.GET)
	@ResponseBody
	public String checkCompanyCodeExistance(@RequestParam("companyCode")String companyCode){
		log.info("UserManagementController -> checkCompanyCodeExistance()");
		try{
			
			
			CompanyDto companyDto=this.userService.getCompanyByCompanyCode(companyCode);
			if(null==companyDto){
				throw new CompanyNotFoundException();
			}
			else{
				return "fail";
			}
		}
		catch(CompanyNotFoundException e)
		{
			return "success";
		}
		catch(Exception e){
			log.error(" Error while check company code existance"+e.getMessage());
			e.printStackTrace();
			return "fail";
		}
		 
	}
	
	  /**
	 * Created By Harshitha on 15th Febraury 2021
     * Method to edit/update the Employee Details
	 */
	
		@RequestMapping(value="editemployeeregistration.do",method=RequestMethod.GET)
		public String initEditEmployeeRegistration(@RequestParam("userId") Integer userId,Map<String,Object> map,@ModelAttribute("UserDto") UserDto userDto){
			log.info("UserManagementController -> initEditEmployeeRegistration() ");
			try{
				
				UserDto userDataDto =this.userService.getUserByUserId(userId);		
				map.put("userDto", userDataDto);
				return "employee/editEmployeeRegistration";
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				String error="User Not Found";
				map.put("message",error);
				return "error";
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("Error while edit registration  "+e.getMessage());
				String error="Error while edit registration";
				map.put("message",error);
				return "error";
			}
			
		}
		
		  /**
		 * Created By Harshitha on 15th Febraury 2021
	     * Method to edit/update the Employee Details
		 */
		
		@RequestMapping(value="editemployeeregistration.do",method=RequestMethod.POST)
		public String editEmployeeRegistration(@ModelAttribute("userDto") UserDto userDto,RedirectAttributes redAttribs,Map<String,Object> map){
			log.info(" UserManagementController -> editEmployeeRegistration()");
			try{
				/*Authentication auth=SecurityContextHolder.getContext().getAuthentication();
				String username=auth.getName();*/
				Integer updatedResult=this.userService.saveOrUpdateUser(userDto);
				if(updatedResult>0){
					redAttribs.addFlashAttribute("status","Registered User Updated Successfully");
					return "redirect:/employee/viewemployees.do";
				}
				else{
					throw new Exception();
				}
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				String error="User Not Found";
				map.put("message",error);
				return "error";
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("Error while updating user details  "+e.getMessage());
				String error="Error while updating user details";
				map.put("message",error);
				return "error";
			}
		}
		
		/**
		 * Created By Harshitha On March 1st , 2021
		 * Method for to check the Employee Existance based on Employee Number
		 */
		@RequestMapping(value="/checkemployeenumberexistance.do", method=RequestMethod.GET)
		@ResponseBody
		public String checkEmployeeNumberExistance(@RequestParam("employeeNumber")String employeeNumber){
			log.info("UserManagementController -> checkEmployeeNumberExistance()");
			try{
				UserDto userDto=this.userService.getEmployeeNumber(employeeNumber);
				if(null==userDto){
					throw new EmployeeNumberNotFoundException();
				}
				else{
					return "fail";
				}
			}
			catch(EmployeeNumberNotFoundException e)
			{
				return "success";
			}
			catch(Exception e){
				log.error(" Error while checking Employee Number"+e.getMessage());
				e.printStackTrace();
				return "fail";
			}
			 
		}
		
		/**
		 * Created By Harshitha On March 04th, 2021
		 * @param username
		 * @return
		 * 
		 * Method for to check the Employee Email Existance 
		 */
		@RequestMapping(value="/checkemployeeemailexistance.do", method=RequestMethod.GET)
		@ResponseBody
		public String checkEmployeeEmailExistance(@RequestParam("emailId")String emailId){
			log.info("UserManagementController -> checkEmployeeEmailExistance()");
			try{
				
				
				UserDto userDto=this.userService.getEmployeeEmailId(emailId);
				if(null==userDto){
					throw new EmployeeEmailIdNotFoundException();
				}
				else{
					return "fail";
				}
			}
			catch(EmployeeEmailIdNotFoundException e)
			{
				return "success";
			}
			catch(Exception e){
				log.error(" Error while check employee emailId existance"+e.getMessage());
				e.printStackTrace();
				return "fail";
			}
			 
		}
		
}
