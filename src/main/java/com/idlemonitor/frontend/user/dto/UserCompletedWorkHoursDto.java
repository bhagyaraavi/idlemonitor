package com.idlemonitor.frontend.user.dto;

import java.util.Date;


import com.idlemonitor.backend.user.model.UserCompletedWorkHours;
import com.idlemonitor.frontend.admin.dto.CompanyDto;

/**
 * 
 * @author Bhagya
 * Dto class for User Completed WorkHours 
 */
public class UserCompletedWorkHoursDto{
	private Integer dataId;
	private UserDto userDto;
	private Date workDate;
	private String workHours;
	private Integer totalUsers;
	private CompanyDto companyDto;
	
	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(java.util.Date date) {
		this.workDate = date;
	}
	public String getWorkHours() {
		return workHours;
	}
	public void setWorkHours(String workHours) {
		this.workHours = workHours;
	}
	public Integer getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}
	
	public CompanyDto getCompanyDto() {
		return companyDto;
	}
	public void setCompanyDto(CompanyDto companyDto) {
		this.companyDto = companyDto;
	}
public static UserCompletedWorkHoursDto populateUserCompletedWorkHoursDto(UserCompletedWorkHours userCompletedWorkHours){
		UserCompletedWorkHoursDto userCompletedWorkHoursDto=new UserCompletedWorkHoursDto();
		userCompletedWorkHoursDto.setDataId(userCompletedWorkHours.getDataId());
		userCompletedWorkHoursDto.setUserDto(UserDto.populateUserDto(userCompletedWorkHours.getUser()));
		userCompletedWorkHoursDto.setWorkDate(userCompletedWorkHours.getWorkDate());
		userCompletedWorkHoursDto.setWorkHours(userCompletedWorkHours.getWorkHours());
		userCompletedWorkHoursDto.setCompanyDto(CompanyDto.populateCompanyDto(userCompletedWorkHours.getCompany()));
		return userCompletedWorkHoursDto;
	}
	
}