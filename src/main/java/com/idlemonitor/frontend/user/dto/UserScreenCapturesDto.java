package com.idlemonitor.frontend.user.dto;

import java.sql.Timestamp;

import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
import com.idlemonitor.frontend.admin.dto.CompanyDto;
/**
 * Created By Bhagya
 * @author user
 * Dto class for User Screen Captures Dto
 */
public class UserScreenCapturesDto{
	private Integer dataId;
	private UserDto userDto;
	private Timestamp captureDateTime;
	private String captureScreenImage;
	private Integer totalCaptures;
	private String base64Image;
	private CompanyDto companyDto;
	
	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public Timestamp getCaptureDateTime() {
		return captureDateTime;
	}
	public void setCaptureDateTime(Timestamp captureDateTime) {
		this.captureDateTime = captureDateTime;
	}
	public String getCaptureScreenImage() {
		return captureScreenImage;
	}
	public void setCaptureScreenImage(String captureScreenImage) {
		this.captureScreenImage = captureScreenImage;
	}
	
	
	public Integer getTotalCaptures() {
		return totalCaptures;
	}
	public void setTotalCaptures(Integer totalCaptures) {
		this.totalCaptures = totalCaptures;
	}
	
	public String getBase64Image() {
		return base64Image;
	}
	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}
	
	public CompanyDto getCompanyDto() {
		return companyDto;
	}
	public void setCompanyDto(CompanyDto companyDto) {
		this.companyDto = companyDto;
	}
	public static UserScreenCapturesDto populateUserScreenCapturesDto(UserScreenCaptures userScreenCaptures){
		UserScreenCapturesDto userScreenCapturesDto=new UserScreenCapturesDto();
		userScreenCapturesDto.setDataId(userScreenCaptures.getDataId());
		userScreenCapturesDto.setUserDto(UserDto.populateUserDto(userScreenCaptures.getUser()));
		userScreenCapturesDto.setCaptureDateTime(userScreenCaptures.getCaptureDateTime());
		userScreenCapturesDto.setCaptureScreenImage(userScreenCaptures.getCaptureScreenImage());
		userScreenCapturesDto.setCompanyDto(CompanyDto.populateCompanyDto(userScreenCaptures.getCompany()));
		return userScreenCapturesDto;
		
	}
}