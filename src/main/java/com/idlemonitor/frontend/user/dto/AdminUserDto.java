package com.idlemonitor.frontend.user.dto;

import java.util.Date;

import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.backend.user.model.UserRole;
import com.idlemonitor.frontend.admin.dto.CompanyDto;
/**
 * Created By Bhagya
 * @author user
 * 
 * Dto class for Admin User
 *
 */
public class AdminUserDto{
	private Integer adminUserId;
	private CompanyDto companyDto;
	private String contactEmail;
	private String contactNumber;
	private String companyAddress;
	private String username;
	private String password;
	private UserRole userRole;
	private String passwordToken;
	private Date passwordTokenExpiryDate;
	private Boolean isActive;
	private String accountActivationToken;
	private Date activationTokenExpiryDate;
	private Date accountModifiedDate;
	private Date accountCreationDate;
	
	public Integer getAdminUserId() {
		return adminUserId;
	}
	public void setAdminUserId(Integer adminUserId) {
		this.adminUserId = adminUserId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	
	
	public CompanyDto getCompanyDto() {
		return companyDto;
	}
	public void setCompanyDto(CompanyDto companyDto) {
		this.companyDto = companyDto;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	
	public String getPasswordToken() {
		return passwordToken;
	}
	public void setPasswordToken(String passwordToken) {
		this.passwordToken = passwordToken;
	}
	public Date getPasswordTokenExpiryDate() {
		return passwordTokenExpiryDate;
	}
	public void setPasswordTokenExpiryDate(Date passwordTokenExpiryDate) {
		this.passwordTokenExpiryDate = passwordTokenExpiryDate;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getAccountActivationToken() {
		return accountActivationToken;
	}
	public void setAccountActivationToken(String accountActivationToken) {
		this.accountActivationToken = accountActivationToken;
	}
	public Date getActivationTokenExpiryDate() {
		return activationTokenExpiryDate;
	}
	public void setActivationTokenExpiryDate(Date activationTokenExpiryDate) {
		this.activationTokenExpiryDate = activationTokenExpiryDate;
	}
	
	public Date getAccountModifiedDate() {
		return accountModifiedDate;
	}
	public void setAccountModifiedDate(Date accountModifiedDate) {
		this.accountModifiedDate = accountModifiedDate;
	}
	public Date getAccountCreationDate() {
		return accountCreationDate;
	}
	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}
	public static AdminUserDto populateAdminUserDto(AdminUser adminUser){
		AdminUserDto adminUserDto=new AdminUserDto();
		adminUserDto.setAdminUserId(adminUser.getAdminUserId());
		adminUserDto.setCompanyDto(CompanyDto.populateCompanyDto(adminUser.getCompany()));
		adminUserDto.setContactEmail(adminUser.getContactEmail());
		adminUserDto.setContactNumber(adminUser.getContactNumber());
		adminUserDto.setCompanyAddress(adminUser.getCompanyAddress());
		adminUserDto.setUsername(adminUser.getUsername());
		adminUserDto.setPassword(adminUser.getPassword());
		adminUserDto.setUserRole(adminUser.getUserRole());
		adminUserDto.setPasswordToken(adminUser.getPasswordToken());
		if(null!=adminUser.getPasswordTokenExpiryDate())
			adminUserDto.setPasswordTokenExpiryDate(adminUser.getPasswordTokenExpiryDate());
		adminUserDto.setIsActive(adminUser.getIsActive());
		adminUserDto.setAccountActivationToken(adminUser.getAccountActivationToken());
		adminUserDto.setActivationTokenExpiryDate(adminUser.getActivationTokenExpiryDate());
		adminUserDto.setAccountCreationDate(adminUser.getAccountCreationDate());
		adminUserDto.setAccountModifiedDate(adminUser.getAccountModifiedDate());
		return adminUserDto;
	}
}