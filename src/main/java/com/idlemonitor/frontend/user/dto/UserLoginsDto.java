package com.idlemonitor.frontend.user.dto;

import java.sql.Timestamp;
import java.util.Date;

import com.idlemonitor.backend.user.model.UserLogins;
/**
 * Created By BHagya
 * @author user
 * Dto class for User/employee Logins Dto
 */
public class UserLoginsDto{
	
	private Integer dataId;
	private UserDto userDto;
	private Timestamp loginDateTime;
	private Timestamp idleDateTime;
	private Timestamp shutdownDateTime;
	private Timestamp logoutDateTime;
	private Date workDate;
	private String description;
	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public Timestamp getLoginDateTime() {
		return loginDateTime;
	}
	public void setLoginDateTime(Timestamp loginDateTime) {
		this.loginDateTime = loginDateTime;
	}
	public Timestamp getIdleDateTime() {
		return idleDateTime;
	}
	public void setIdleDateTime(Timestamp idleDateTime) {
		this.idleDateTime = idleDateTime;
	}
	public Timestamp getShutdownDateTime() {
		return shutdownDateTime;
	}
	public void setShutdownDateTime(Timestamp shutdownDateTime) {
		this.shutdownDateTime = shutdownDateTime;
	}
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Timestamp getLogoutDateTime() {
		return logoutDateTime;
	}
	public void setLogoutDateTime(Timestamp logoutDateTime) {
		this.logoutDateTime = logoutDateTime;
	}
	public static UserLoginsDto populateUserLoginsDto(UserLogins userLogins){
		UserLoginsDto userLoginsDto=new UserLoginsDto();
		userLoginsDto.setDataId(userLogins.getDataId());
		userLoginsDto.setUserDto(UserDto.populateUserDto(userLogins.getUser()));
		if(null!=userLogins.getLoginDateTime()){
			userLoginsDto.setLoginDateTime(userLogins.getLoginDateTime());
		}
		if(null!=userLogins.getIdleDateTime()){
			userLoginsDto.setIdleDateTime(userLogins.getIdleDateTime());
		}
		if(null!=userLogins.getShutdownDateTime()){
			userLoginsDto.setShutdownDateTime(userLogins.getShutdownDateTime());
		}
		if(null!=userLogins.getLogoutDateTime()){
			userLoginsDto.setLogoutDateTime(userLogins.getLogoutDateTime());
		}
		userLoginsDto.setWorkDate(userLogins.getWorkDate());
		if(null!=userLogins.getDescription()){
			userLoginsDto.setDescription(userLogins.getDescription());
		}
		return userLoginsDto;
	}
}