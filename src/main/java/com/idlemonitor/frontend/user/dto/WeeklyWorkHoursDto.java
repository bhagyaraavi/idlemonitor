package com.idlemonitor.frontend.user.dto;


/**
 * Created By Harshitha on 13th May, 2021
 * Dto class for User's Weekly Work Hours 
 */
public class WeeklyWorkHoursDto{
	private UserDto userDto;
	private String day1Hours;
	private String day2Hours;
	private String day3Hours;
	private String day4Hours;
	private String day5Hours;
	private String day6Hours;
	private String day7Hours;
	private String totalHours;
	private String day1Title;
	private String day2Title;
	private String day3Title;
	private String day4Title;
	private String day5Title;
	private String day6Title;
	private String day7Title;
	
	
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}

	public String getDay1Hours() {
		return day1Hours;
	}
	public void setDay1Hours(String day1Hours) {
		this.day1Hours = day1Hours;
	}

	public String getDay2Hours() {
		return day2Hours;
	}
	public void setDay2Hours(String day2Hours) {
		this.day2Hours = day2Hours;
	}

	public String getDay3Hours() {
		return day3Hours;
	}
	public void setDay3Hours(String day3Hours) {
		this.day3Hours = day3Hours;
	}

	public String getDay4Hours() {
		return day4Hours;
	}
	public void setDay4Hours(String day4Hours) {
		this.day4Hours = day4Hours;
	}

	public String getDay5Hours() {
		return day5Hours;
	}
	public void setDay5Hours(String day5Hours) {
		this.day5Hours = day5Hours;
	}

	public String getDay6Hours() {
		return day6Hours;
	}
	public void setDay6Hours(String day6Hours) {
		this.day6Hours = day6Hours;
	}

	public String getDay7Hours() {
		return day7Hours;
	}
	public void setDay7Hours(String day7Hours) {
		this.day7Hours = day7Hours;
	}

	public String getTotalHours() {
		return totalHours;
	}
	public void setTotalHours(String totalHours) {
		this.totalHours = totalHours;
	}
	public String getDay1Title() {
		return day1Title;
	}
	public void setDay1Title(String day1Title) {
		this.day1Title = day1Title;
	}
	public String getDay2Title() {
		return day2Title;
	}
	public void setDay2Title(String day2Title) {
		this.day2Title = day2Title;
	}
	public String getDay3Title() {
		return day3Title;
	}
	public void setDay3Title(String day3Title) {
		this.day3Title = day3Title;
	}
	public String getDay4Title() {
		return day4Title;
	}
	public void setDay4Title(String day4Title) {
		this.day4Title = day4Title;
	}
	public String getDay5Title() {
		return day5Title;
	}
	public void setDay5Title(String day5Title) {
		this.day5Title = day5Title;
	}
	public String getDay6Title() {
		return day6Title;
	}
	public void setDay6Title(String day6Title) {
		this.day6Title = day6Title;
	}
	public String getDay7Title() {
		return day7Title;
	}
	public void setDay7Title(String day7Title) {
		this.day7Title = day7Title;
	}
	
}

