package com.idlemonitor.frontend.user.dto;

import java.sql.Timestamp;
import java.util.Date;

import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.frontend.admin.dto.CompanyDto;
/**
 * Created By BHagya
 * @author user
 * Dto class for USer /employee
 */
public class UserDto{
	private Integer userId;
	private String username;
	private String password;
	private CompanyDto companyDto;
	private String employeeNumber;
	private String employeeName;
	private String emailId;
	private String status;
	private Timestamp statusUpdatedDate;
	private Timestamp creationDate;
	private Integer totalUsers;
	private String passwordToken;
	private Date passwordTokenExpiryDate;
	private Boolean isActive;
	private String accountActivationToken;
	private Date activationTokenExpiryDate;
	// added the workedHours by bhagya on august 14th, 2020 - to generate the daily report of employees
	private String workedHours;
	private Boolean isCompletedHours;
	private Timestamp accountUpdatedDate;
	// Created by Harshitha on 30 Aug 2021 for multi device login issue
		private Boolean isAppRunning;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public CompanyDto getCompanyDto() {
		return companyDto;
	}
	public void setCompanyDto(CompanyDto companyDto) {
		this.companyDto = companyDto;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Timestamp getStatusUpdatedDate() {
		return statusUpdatedDate;
	}
	public void setStatusUpdatedDate(Timestamp statusUpdatedDate) {
		this.statusUpdatedDate = statusUpdatedDate;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	public Integer getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}
	
	public String getPasswordToken() {
		return passwordToken;
	}
	public void setPasswordToken(String passwordToken) {
		this.passwordToken = passwordToken;
	}
	public Date getPasswordTokenExpiryDate() {
		return passwordTokenExpiryDate;
	}
	public void setPasswordTokenExpiryDate(Date passwordTokenExpiryDate) {
		this.passwordTokenExpiryDate = passwordTokenExpiryDate;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getAccountActivationToken() {
		return accountActivationToken;
	}
	public void setAccountActivationToken(String accountActivationToken) {
		this.accountActivationToken = accountActivationToken;
	}
	public Date getActivationTokenExpiryDate() {
		return activationTokenExpiryDate;
	}
	public void setActivationTokenExpiryDate(Date activationTokenExpiryDate) {
		this.activationTokenExpiryDate = activationTokenExpiryDate;
	}
	
	public String getWorkedHours() {
		return workedHours;
	}
	public void setWorkedHours(String workedHours) {
		this.workedHours = workedHours;
	}
	
	public Boolean getIsCompletedHours() {
		return isCompletedHours;
	}
	public void setIsCompletedHours(Boolean isCompletedHours) {
		this.isCompletedHours = isCompletedHours;
	}
	
	/**
	  * Created By Harshitha on Febraury 22nd, 2021
	  * user's Updated Account Date 
	  */
	public Timestamp getAccountUpdatedDate() {
		return accountUpdatedDate;
	}
	public void setAccountUpdatedDate(Timestamp accountUpdatedDate) {
		this.accountUpdatedDate = accountUpdatedDate;
	}
	/*
	 * Created by Harshitha on 30 Aug 2021 for multi device login issue
	 */
	public Boolean getIsAppRunning() {
		return isAppRunning;
	}
	public void setIsAppRunning(Boolean isAppRunning) {
		this.isAppRunning = isAppRunning;
	}
	public static UserDto populateUserDto(User user){
		UserDto userDto=new UserDto();
		userDto.setUserId(user.getUserId());
		userDto.setUsername(user.getUsername());
		userDto.setPassword(user.getPassword());
		userDto.setCompanyDto(CompanyDto.populateCompanyDto(user.getCompany()));
		userDto.setEmailId(user.getEmailId());
		userDto.setEmployeeNumber(user.getEmployeeNumber());
		userDto.setEmployeeName(user.getEmployeeName());
		userDto.setStatus(user.getStatus());
		userDto.setStatusUpdatedDate(user.getStatusUpdatedDate());
		userDto.setCreationDate(user.getCreationDate());
		userDto.setPasswordToken(user.getPasswordToken());
		if(null!=user.getPasswordTokenExpiryDate()){
			userDto.setPasswordTokenExpiryDate(user.getPasswordTokenExpiryDate());
		}
		userDto.setIsActive(user.getIsActive());
		userDto.setAccountActivationToken(user.getAccountActivationToken());
		if(null!=user.getActivationTokenExpiryDate()) {
			userDto.setActivationTokenExpiryDate(user.getActivationTokenExpiryDate());
		}
		
		/**
		  * Created By Harshitha on Febraury 22nd, 2021
		  * Service for to get the user's Updated Account Date based on user
		  */
		if(null!=user.getAccountUpdatedDate())
		{
			userDto.setAccountUpdatedDate(user.getAccountUpdatedDate());
		}
		/*
		 * Created by Harshitha on 30 Aug 2021 for multi device login issue
		 */
		userDto.setIsAppRunning(user.getIsAppRunning());
		return userDto;
	}

}