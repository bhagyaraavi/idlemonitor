package com.idlemonitor.frontend.user.dto;
import com.idlemonitor.backend.user.model.IdleTimeConfiguration;
/**
 * Created By Bhagya
 * Dto class for Idle time configuration Dto
 * @author user
 *
 */
public class IdleTimeConfigurationDto{
	private Integer configId;
	private AdminUserDto adminUserDto;
	private String idleMinutes;
	public Integer getConfigId() {
		return configId;
	}
	public void setConfigId(Integer configId) {
		this.configId = configId;
	}
	
	public AdminUserDto getAdminUserDto() {
		return adminUserDto;
	}
	public void setAdminUserDto(AdminUserDto adminUserDto) {
		this.adminUserDto = adminUserDto;
	}
	public String getIdleMinutes() {
		return idleMinutes;
	}
	public void setIdleMinutes(String idleMinutes) {
		this.idleMinutes = idleMinutes;
	}
	
	public static IdleTimeConfigurationDto populateIdleTimeConfigurationDto(IdleTimeConfiguration idleTimeConfiguration){
		IdleTimeConfigurationDto idleTimeConfigurationDto=new IdleTimeConfigurationDto();
		idleTimeConfigurationDto.setConfigId(idleTimeConfiguration.getConfigId());
		idleTimeConfigurationDto.setAdminUserDto(AdminUserDto.populateAdminUserDto(idleTimeConfiguration.getAdminUser()));
		idleTimeConfigurationDto.setIdleMinutes(idleTimeConfiguration.getIdleMinutes());
		return idleTimeConfigurationDto;
	}
	
}
