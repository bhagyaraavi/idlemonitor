package com.idlemonitor.frontend.user.dto;

import java.util.Date;

import com.idlemonitor.backend.user.model.UserWorkHours;
/**
 * Created By bHagya
 * @author user
 * Dto class for User Work Hours 
 */
public class UserWorkHoursDto{
	private Integer dataId;
	private UserDto userDto;
	private Date workDate;
	private String workHours;
	private Boolean isAppClosed;
	
	public Integer getDataId() {
		return dataId;
	}
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	public String getWorkHours() {
		return workHours;
	}
	public void setWorkHours(String workHours) {
		this.workHours = workHours;
	}
	
	public Boolean getIsAppClosed() {
		return isAppClosed;
	}
	public void setIsAppClosed(Boolean isAppClosed) {
		this.isAppClosed = isAppClosed;
	}
	public static UserWorkHoursDto populateUserWorkHoursDto(UserWorkHours userWorkHours){
		
		UserWorkHoursDto userWorkHoursDto=new UserWorkHoursDto();
		userWorkHoursDto.setDataId(userWorkHours.getDataId());
		userWorkHoursDto.setUserDto(UserDto.populateUserDto(userWorkHours.getUser()));
		userWorkHoursDto.setWorkDate(userWorkHours.getWorkDate());
		userWorkHoursDto.setWorkHours(userWorkHours.getWorkHours());
		userWorkHoursDto.setIsAppClosed(userWorkHours.getIsAppClosed());
		return userWorkHoursDto;
	}
}

