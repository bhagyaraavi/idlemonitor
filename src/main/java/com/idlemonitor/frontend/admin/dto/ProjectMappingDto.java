package com.idlemonitor.frontend.admin.dto;

import com.idlemonitor.backend.admin.model.ProjectMapping;
import com.idlemonitor.frontend.user.dto.UserDto;
/**
 * Created on APril 04th, 2020
 * @author Bhagya
 * Dto class for Project mapping
 * By using this dto we are using users for projects
 */
public class ProjectMappingDto{
	private Integer mappingId;
	private ProjectDto projectDto;
	private UserDto userDto;
	
	public Integer getMappingId() {
		return mappingId;
	}
	public void setMappingId(Integer mappingId) {
		this.mappingId = mappingId;
	}
	public ProjectDto getProjectDto() {
		return projectDto;
	}
	public void setProjectDto(ProjectDto projectDto) {
		this.projectDto = projectDto;
	}
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	
	public static ProjectMappingDto populateProjectMappingDto(ProjectMapping projectMapping) {
		ProjectMappingDto projectMappingDto=new ProjectMappingDto();
		projectMappingDto.setMappingId(projectMapping.getMappingId());
		projectMappingDto.setProjectDto(ProjectDto.populateProjectDto(projectMapping.getProject()));
		projectMappingDto.setUserDto(UserDto.populateUserDto(projectMapping.getUser()));
		return projectMappingDto;
	}
}