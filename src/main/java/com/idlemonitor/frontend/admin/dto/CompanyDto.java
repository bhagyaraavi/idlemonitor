package com.idlemonitor.frontend.admin.dto;

import com.idlemonitor.backend.admin.model.Company;
/**
 * CReated By Bhagya
 * @author user
 * Dto class for company
 */
public class CompanyDto{
	
	private Integer companyId;
	private String companyName;
	private String companyCode;
	private String adminEmailIds;
	
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	
	public String getAdminEmailIds() {
		return adminEmailIds;
	}
	public void setAdminEmailIds(String adminEmailIds) {
		this.adminEmailIds = adminEmailIds;
	}
	public static CompanyDto populateCompanyDto(Company company){
		CompanyDto companyDto=new CompanyDto();
		companyDto.setCompanyId(company.getCompanyId());
		companyDto.setCompanyName(company.getCompanyName());
		companyDto.setCompanyCode(company.getCompanyCode());
		companyDto.setAdminEmailIds(company.getAdminEmailIds());
		return companyDto;
	}
}