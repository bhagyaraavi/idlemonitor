package com.idlemonitor.frontend.admin.dto;

import java.util.Date;

import com.idlemonitor.backend.admin.model.Task;

/**
 * Created on August 04th, 2020
 * @author Bhagya
 * Dto class for task
 */
public class TaskDto{
	private Integer taskId;
	private String taskName;
	private ProjectDto projectDto;
	private Date taskCreationDate;
	private Date taskModificationDate;
	private Boolean isActive;
	private Integer totalTasks;
	
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public ProjectDto getProjectDto() {
		return projectDto;
	}
	public void setProjectDto(ProjectDto projectDto) {
		this.projectDto = projectDto;
	}
	public Date getTaskCreationDate() {
		return taskCreationDate;
	}
	public void setTaskCreationDate(Date taskCreationDate) {
		this.taskCreationDate = taskCreationDate;
	}
	public Date getTaskModificationDate() {
		return taskModificationDate;
	}
	public void setTaskModificationDate(Date taskModificationDate) {
		this.taskModificationDate = taskModificationDate;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	
	
	public Integer getTotalTasks() {
		return totalTasks;
	}
	public void setTotalTasks(Integer totalTasks) {
		this.totalTasks = totalTasks;
	}
	public static TaskDto populateTaskDto(Task task) {
		TaskDto taskDto=new TaskDto();
		taskDto.setTaskId(task.getTaskId());
		taskDto.setTaskName(task.getTaskName());
		taskDto.setTaskCreationDate(task.getTaskCreationDate());
		if(null!=task.getTaskModificationDate())
			taskDto.setTaskModificationDate(task.getTaskModificationDate());
		taskDto.setProjectDto(ProjectDto.populateProjectDto(task.getProject()));
		taskDto.setIsActive(task.getIsActive());
		return taskDto;
	}
	
	
}