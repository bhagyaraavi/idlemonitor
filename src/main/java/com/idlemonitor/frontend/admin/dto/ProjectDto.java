package com.idlemonitor.frontend.admin.dto;

import java.util.Date;

import com.idlemonitor.backend.admin.model.Project;
import com.idlemonitor.frontend.user.dto.AdminUserDto;
/**
 * 
 * @author Bhagya
 * Created On August 03rd, 2020
 * Dto class for ProjectDto
 */

public class ProjectDto{
	
	private Integer projectId;
	private String projectName;
	private String clientName;
	private CompanyDto companyDto;
	private AdminUserDto adminUserDto;
	private Date projectCreationDate;
	private Date projectModificationDate;
	private Boolean isActive;
	private Integer totalProjects;
	
	
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public CompanyDto getCompanyDto() {
		return companyDto;
	}
	public void setCompanyDto(CompanyDto companyDto) {
		this.companyDto = companyDto;
	}
	public AdminUserDto getAdminUserDto() {
		return adminUserDto;
	}
	public void setAdminUserDto(AdminUserDto adminUserDto) {
		this.adminUserDto = adminUserDto;
	}
	public Date getProjectCreationDate() {
		return projectCreationDate;
	}
	public void setProjectCreationDate(Date projectCreationDate) {
		this.projectCreationDate = projectCreationDate;
	}
	public Date getProjectModificationDate() {
		return projectModificationDate;
	}
	public void setProjectModificationDate(Date projectModificationDate) {
		this.projectModificationDate = projectModificationDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	public Integer getTotalProjects() {
		return totalProjects;
	}
	public void setTotalProjects(Integer totalProjects) {
		this.totalProjects = totalProjects;
	}
	public static ProjectDto populateProjectDto(Project project) {
		ProjectDto projectDto=new ProjectDto();
		projectDto.setProjectId(project.getProjectId());
		projectDto.setProjectName(project.getProjectName());
		projectDto.setClientName(project.getClientName());
		projectDto.setCompanyDto(CompanyDto.populateCompanyDto(project.getCompany()));
		projectDto.setAdminUserDto(AdminUserDto.populateAdminUserDto(project.getAdminUser()));
		projectDto.setProjectCreationDate(project.getProjectCreationDate());
		if(null!=project.getProjectModificationDate())
			projectDto.setProjectModificationDate(project.getProjectModificationDate());
		projectDto.setIsActive(project.getIsActive());
		return projectDto;
	}
	
}