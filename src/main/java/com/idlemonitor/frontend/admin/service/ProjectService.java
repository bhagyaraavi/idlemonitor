package com.idlemonitor.frontend.admin.service;

import java.util.ArrayList;

import com.idlemonitor.backend.admin.model.Task;
import com.idlemonitor.common.exceptions.ProjectNotFoundException;
import com.idlemonitor.common.exceptions.ProjectNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.TaskNotFoundException;
import com.idlemonitor.common.exceptions.TaskNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.frontend.admin.dto.ProjectDto;
import com.idlemonitor.frontend.admin.dto.TaskDto;
/**
 * Created On AUgust 03rd, 2020
 * @author Bhagya
 * Interface class for project services
 */
public interface ProjectService{
	public Integer saveOrUpdateProject(ProjectDto projectDto,Integer adminUserId) throws UserNotFoundException, ProjectNotSavedOrUpdatedException;
	public Integer saveOrUpdateTask(TaskDto taskDto,Integer projectId) throws ProjectNotFoundException, TaskNotSavedOrUpdatedException;
	public ArrayList<ProjectDto> getProjectsByAdminUser(String adminUsername,Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws  UserNotFoundException, ProjectNotFoundException;
	public Integer changeProjectActiveStatus(Integer projectId,Boolean isActive) throws ProjectNotFoundException, ProjectNotSavedOrUpdatedException;
	public Integer deleteProject(Integer projectId);
	/**
	 * Created On April 28th, 2021
	 * @author Harshitha
	 * Interface for Task View service
	 */
	public ArrayList<TaskDto> getTasksByProjectId(Integer projectId,Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws  ProjectNotFoundException, TaskNotFoundException;
	/**
	 * Created On May 3rd, 2021
	 * @author Harshitha
	 * Interface for Task Status service
	 */
	public Integer changeTaskActiveStatus(Integer taskId,Boolean isActive) throws TaskNotFoundException, TaskNotSavedOrUpdatedException;
	/**
	 * Created On May 3rd, 2021
	 * @author Harshitha
	 * Interface for deletion of task
	 * @param taskId 
	 */
	public Integer deleteTask(Integer taskId);
}
