package com.idlemonitor.frontend.admin.service;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.idlemonitor.backend.admin.dao.ProjectDao;
import com.idlemonitor.backend.admin.model.Project;
import com.idlemonitor.backend.admin.model.Task;
import com.idlemonitor.backend.employee.dao.EmployeeDao;
import com.idlemonitor.backend.user.dao.UserDao;
import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.common.exceptions.EmployeeNotFoundException;
import com.idlemonitor.common.exceptions.ProjectNotFoundException;
import com.idlemonitor.common.exceptions.ProjectNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.TaskNotFoundException;
import com.idlemonitor.common.exceptions.TaskNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.frontend.admin.dto.ProjectDto;
import com.idlemonitor.frontend.admin.dto.TaskDto;
/**
 * Created On AUgust 03rd, 2020
 * @author Bhagya
 * 
 * Service Implementation class for Project
 */
@Transactional
@Service("projectService")
public class ProjectServiceImpl implements ProjectService{
	
	private static Logger log = Logger.getLogger(ProjectServiceImpl.class);
	
	@Resource(name = "projectDao")
	private ProjectDao projectDao;
	
	@Resource(name = "userDao")
	private UserDao userDao;

	private Object projectId;
	
	/**
	 * Created By Bhagya on August 04th, 2020
	 * @param projectDto
	 * @return
	 * @throws ProjectNotSavedOrUpdatedException 
	 * @throws Exception
	 * Service for to save or update project
	 */
	public Integer saveOrUpdateProject(ProjectDto projectDto,Integer adminUserId) throws UserNotFoundException, ProjectNotSavedOrUpdatedException {
		log.info(" ProjectServiceImpl -> saveOrUpdateProject() ");
		Project project=null;
		Integer savedResult=0;
		
			try{
				project=this.projectDao.getProjectByProjectId(projectDto.getProjectId());
				project.setProjectModificationDate(new Date());
			}
			catch(ProjectNotFoundException e) {
				project=new Project();
				project.setProjectCreationDate(new Date());
				project.setIsActive(true);
				AdminUser adminUser=this.userDao.getAdminUserByAdminUserId(adminUserId);
				project.setAdminUser(adminUser);
				project.setCompany(adminUser.getCompany());
			}
			
			project.setProjectName(projectDto.getProjectName());
			project.setClientName(projectDto.getClientName());
			savedResult=this.projectDao.saveOrUpdateProject(project);
			return savedResult;
				
		
	}
	/**
	 * Created By BHagya on AUgust 04th, 2020
	 * @param taskDto
	 * @return
	 * @throws ProjectNotFoundException
	 * @throws TaskNotSavedOrUpdatedException
	 * 
	 * Service for to save or update task
	 */
	public Integer saveOrUpdateTask(TaskDto taskDto, Integer projectId) throws ProjectNotFoundException, TaskNotSavedOrUpdatedException {
		log.info("ProjectServiceImpl -> saveOrUpdateTask() ");
		Task task =null;
		Integer savedResult=0;
		try {
			task=this.projectDao.getTaskByTaskId(taskDto.getTaskId());
			task.setTaskModificationDate(new Date());
		}
		catch(TaskNotFoundException e) {
			task=new Task();
			task.setTaskCreationDate(new Date());
			task.setIsActive(true);
		}
		task.setTaskName(taskDto.getTaskName());
		Project project=this.projectDao.getProjectByProjectId(projectId);
		task.setProject(project);
		savedResult=this.projectDao.saveOrUpdateTask(task);
		return savedResult;
	}
	
	/**
	 * Created By Bhagya on april 27th, 2021
	 * @param adminUserId
	 * @param pageNo
	 * @param pageSize
	 * @param searchBy
	 * @param sortBy
	 * @param ascending
	 * @return
	 * @throws UserNotFoundException
	 * @throws ProjectNotFoundException
	 * 
	 * Service for to get the projects for to display it in admin web application
	 */
	public ArrayList<ProjectDto> getProjectsByAdminUser(String adminUsername,Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws  UserNotFoundException, ProjectNotFoundException{
    	log.info(" ProjectServiceImpl -> getProjectsByAdminUser() ");
  	  ArrayList<ProjectDto> projectDtos=new ArrayList<ProjectDto>();
  	  AdminUser adminUser=this.userDao.getUserforAuthentication(adminUsername);
	  	  ArrayList<Project> projects=this.projectDao.getProjectsListByAdminUser(adminUser, pageNo, pageSize, searchBy, sortBy, ascending);
	  	  for(Project project:projects){ 
	  		  ProjectDto projectDto=ProjectDto.populateProjectDto(project);
	  		projectDtos.add(projectDto);
	  	  }
	  	projectDtos.get(0).setTotalProjects( projects.get(0).getTotalProjects()); 
	  	  return projectDtos;
    }
	/**
	 * Created By Bhagya on april 27th, 2021
	 * @param projectId
	 * @param isActive
	 * @return
	 * @throws ProjectNotFoundException
	 * @throws ProjectNotSavedOrUpdatedException
	 * 
	 * Service for to change the project status
	 */
	public Integer changeProjectActiveStatus(Integer projectId,Boolean isActive) throws ProjectNotFoundException, ProjectNotSavedOrUpdatedException{
    	log.info("ProjectServiceImpl -> changeProjectActiveStatus");	    	
    	Project project=this.projectDao.getProjectByProjectId(projectId);
    	project.setIsActive(isActive);
    	Integer updatedResult=this.projectDao.saveOrUpdateProject(project);
    	return updatedResult;
    }
	/**
	 * Created By Bhagya on APril 27th, 2021
	 * @param projectId
	 * @return
	 * @throws ProjectNotFoundException
	 * @throws TaskNotFoundException
	 * 
	 * Service for to delete the project
	 * 1. delete the tasks under the project
	 * 2. delete the project
	 */
	public Integer deleteProject(Integer projectId){
		log.info("ProjectServiceImpl ->  deleteProject");
		Integer result=0;
		try {
			Project project=this.projectDao.getProjectByProjectId(projectId);
				try {
				ArrayList<Task> tasks=this.projectDao.getListOfTasksByProject(project);
				this.projectDao.deleteTasks(tasks);
				
				}
				catch(TaskNotFoundException e) {
					
				}
			 result=this.projectDao.deleteProject(project);
		}
		catch(ProjectNotFoundException e) {
			
		}
		return result;
	}
	/**
	 * Created By Harshitha on April 28th, 2021
	 * @param projectId
	 * @param pageNo
	 * @param pageSize
	 * @param searchBy
	 * @param sortBy
	 * @param ascending
	 * @return
	 * @throws ProjectNotFoundException
	 * @throws TaskNotFoundException
	 * 
	 * Service for to get the tasks for to display it in admin web application
	 */
	public ArrayList<TaskDto> getTasksByProjectId(Integer projectId,Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws  ProjectNotFoundException, TaskNotFoundException{
    	log.info(" ProjectServiceImpl -> getTasksByProjectId() ");
  	  ArrayList<TaskDto> taskDtos=new ArrayList<TaskDto>();
  	  
  	  Project project=this.projectDao.getProjectByProjectId(projectId);
	  	  ArrayList<Task> tasks=this.projectDao.getTasksListByProjectforAdmin(project, pageNo, pageSize, searchBy, sortBy, ascending);
	  	  for(Task task:tasks){ 
	  		TaskDto taskDto=TaskDto.populateTaskDto(task);
	  		taskDtos.add(taskDto);
	  	  }
	  	taskDtos.get(0).setTotalTasks( tasks.get(0).getTotalTasks()); 
	  	  return taskDtos;
    }
	
	/**
	 * Created By Harshitha on may 3rd, 2021
	 * @param taskId
	 * @param isActive
	 * @return
	 * @throws TaskNotFoundException
	 * @throws TaskNotSavedOrUpdatedException
	 * 
	 * Service for to change the Task status
	 */
	public Integer changeTaskActiveStatus(Integer taskId,Boolean isActive) throws TaskNotFoundException, TaskNotSavedOrUpdatedException{
    	log.info("ProjectServiceImpl -> changeTaskActiveStatus");	    	
    	Task task=this.projectDao.getTaskByTaskId(taskId);
    	task.setIsActive(isActive);
    	Integer updatedResult=this.projectDao.saveOrUpdateTask(task);
    	return updatedResult;
    }
	
	/**
	 * Created By Harshitha on May 3rd, 2021
	 * Service for to delete the task
	 */
	public Integer deleteTask(Integer taskId){
		log.info("ProjectServiceImpl ->  deleteTask");
		Integer result=0;
		try {
			Task task=this.projectDao.getTaskByTaskId(taskId);
			ArrayList<Task> tasks = new ArrayList<Task>();
			tasks.add(task);
			 result=this.projectDao.deleteTasks(tasks);
		}
		catch(TaskNotFoundException e) {
			
		}
		return result;
	}
	
}
