package com.idlemonitor.frontend.admin.controller;

import java.util.ArrayList;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.idlemonitor.common.exceptions.EmployeeNotFoundException;
import com.idlemonitor.common.exceptions.ProjectNotFoundException;
import com.idlemonitor.common.exceptions.ProjectNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.TaskNotFoundException;
import com.idlemonitor.common.exceptions.TaskNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.frontend.admin.dto.ProjectDto;
import com.idlemonitor.frontend.admin.dto.TaskDto;
import com.idlemonitor.frontend.admin.service.ProjectService;
import com.idlemonitor.frontend.utility.dto.DisplayListBeanDto;

@Controller("projectController")
@RequestMapping("/project")
public class ProjectController{
	
	private static Logger log=Logger.getLogger(ProjectController.class);
	
	@Resource(name="projectService")
	private ProjectService projectService;
	
	
	/**
	 * Created By Bhagya on APril 27th, 2021
	 * @param map
	 * @param listBeanDto
	 * @return
	 * 
	 * Service for to view projects by admin
	 */
	
	@RequestMapping(value="/viewprojects.do")
	public String viewProjectsByCompany(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto) {
		log.info(" ProjectController -> viewProjectsByCompany()");
		Integer totalResults;
		try{
			
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();
			String adminUsername=auth.getName();
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("projectId");
			}
			
			ArrayList<ProjectDto> projectDtos=this.projectService.getProjectsByAdminUser(adminUsername,listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSearchBy(),listBeanDto.getSortBy(),listBeanDto.getSortDirection());
			totalResults=projectDtos.get(0).getTotalProjects();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("projectDtos", projectDtos);
			map.put("projectsSize",projectDtos.get(0).getTotalProjects());
			return "project/viewProjects";
		}
		catch(UserNotFoundException e){
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(ProjectNotFoundException e){
			String error="No Projects Found";
			map.put("message",error);
			return "project/viewProjects";
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while getting projects  "+e.getMessage());
			String error="Error while getting projects";
			map.put("message",error);
			return "error";
		}
		
		
	}
	
	/**
	 * Created By Bhagya on APril 26th,2021
	 * @param request
	 * @param map
	 * @param projectId
	 * @param projectName
	 * @param clientName
	 * @return
	 * 
	 * Service for to save or update project
	 */
	@RequestMapping(value="/saveorupdateproject.do", method=RequestMethod.GET)
	@ResponseBody
	public String saveOrUpdateProject(HttpServletRequest request,Map<String,Object> map,@RequestParam("projectId")Integer projectId,@RequestParam("projectName")String projectName,@RequestParam("clientName")String clientName){
		log.info( " UserManagementController -> saveOrUpdateProject()");
		
		try{
			HttpSession session=request.getSession();
			Integer adminUserId=(Integer) session.getAttribute("userId");
			ProjectDto projectDto=new ProjectDto();
			projectDto.setProjectId(projectId);
			projectDto.setProjectName(projectName);
			projectDto.setClientName(clientName);
			Integer savedResult=this.projectService.saveOrUpdateProject(projectDto,adminUserId);
			if(savedResult>0){
				return "success";
			}
			else{
				throw new ProjectNotSavedOrUpdatedException();
			}
			
		}
		catch(ProjectNotSavedOrUpdatedException e){
			String error="Project Not Saved Or Updated Exception";
			map.put("message",error);
			return "fail";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error while saving or updating project "+e.getMessage());
			map.put("message", "Error while saving or updating project ");
			return "fail";
		}
		
	}
	/***
	 * Created by bhagya on april 27th, 2021
	 * @param projectId
	 * @param isActive
	 * @return
	 * 
	 * Service for to change the project status 
	 */
	 @ResponseBody
		@RequestMapping("/changeprojectstatus.do")
		public String changeProjectStatus(@RequestParam("projectId")Integer projectId,@RequestParam("isActive") Boolean isActive){
			log.info(" ProjectController -> changeProjectStatus()");
			try{
				Integer savedResult=this.projectService.changeProjectActiveStatus(projectId, isActive);
				return "SUCCESS";
			}
			catch(ProjectNotFoundException e){
				log.error(" User Not Found For Changing the user Status");
				return "ERROR";
			}
			catch(ProjectNotSavedOrUpdatedException e){
				log.error("User Status Is Not Saved Or Updated");
				return "ERROR";
			}
			catch(Exception e){
				e.printStackTrace();
				return "ERROR";
			}
			
		}
	
	 
	 /**
	   * Created By Bhagya on april 27th, 2021
	   * @param projectId
	   * @return
	   * 
	   * Service for to delete the project
	   */
	  @ResponseBody
		@RequestMapping("/deleteproject.do")
		public String deleteProject(@RequestParam("projectId")Integer projectId){
			log.info(" ProjectController -> deleteProject()");
			try{
				
				Integer deletedResult=this.projectService.deleteProject(projectId);
				
				if(deletedResult>0) {
					return "SUCCESS";	
				}
				else {
					return "ERROR";
				}
			}
			catch(Exception e){
				e.printStackTrace();
				return "ERROR";
			}
			
		}
	  
	  /**
		 * Created By Harshitha on April 28th, 2021
		 * @param map
		 * @param listBeanDto
		 * @return
		 * 
		 * Service for to view Task by admin
		 */
		
		@RequestMapping(value="/viewtasks.do")
		public String viewTasksByProject(Map<String,Object> map,@RequestParam(value="projectId",required=false)Integer projectId,@ModelAttribute(value="displayListBean") DisplayListBeanDto listBeanDto) {
			log.info(" ProjectController -> viewTasksByProject()");
			Integer totalResults;
			try{
				if(projectId==null) {
					projectId=listBeanDto.getPagerDto().getProjectId();
				}
				if(null==listBeanDto.getSortBy()){
					listBeanDto.setSortBy("taskId");
				}
				
				ArrayList<TaskDto> taskDtos=this.projectService.getTasksByProjectId(projectId,listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
						listBeanDto.getSearchBy(),listBeanDto.getSortBy(),listBeanDto.getSortDirection());
				totalResults=taskDtos.get(0).getTotalTasks();
				listBeanDto.getPagerDto().setTotalItems(totalResults);
				
				map.put("projectId", projectId);
				map.put("taskDtos", taskDtos);
				map.put("tasksSize",taskDtos.get(0).getTotalTasks());
				return "task/viewTasks";
			}
			catch(ProjectNotFoundException e){
				String error="Project Not Found";
				map.put("message",error);
				map.put("projectId", projectId);
				return "error";
			}
			catch(TaskNotFoundException e){
				String error="No Tasks Found";
				map.put("projectId", projectId);
				map.put("message",error);
				return "task/viewTasks";
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("Error while getting tasks  "+e.getMessage());
				String error="Error while getting tasks";
				map.put("message",error);
				map.put("projectId", projectId);
				return "error";
			}
			
			
		}
		
		/**
		 * Created By Harshitha on May 03rd,2021
		 * Service for to save or update Task
		 */
		@RequestMapping(value="/saveorupdatetask.do", method=RequestMethod.GET)
		@ResponseBody
		public String saveOrUpdateTask(Map<String,Object> map,@RequestParam("projectId")Integer projectId,@RequestParam("taskId")Integer taskId,@RequestParam("taskName")String taskName){
			log.info( " ProjectController -> saveOrUpdateTask()");
			
			try{
				
				TaskDto taskDto=new TaskDto();
				taskDto.setTaskId(taskId);
				taskDto.setTaskName(taskName);
				Integer savedResult=this.projectService.saveOrUpdateTask(taskDto,projectId);
				if(savedResult>0){
					return "success";
				}
				else{
					throw new TaskNotSavedOrUpdatedException();
				}
				
				
			}
			catch(TaskNotSavedOrUpdatedException e){
				String error="Task Not Saved Or Updated Exception";
				map.put("message",error);
				return "fail";
			}
			catch(Exception e){
				e.printStackTrace();
				log.error(" Error while saving or updating task "+e.getMessage());
				map.put("message", "Error while saving or updating task  ");
				return "fail";
			}
			
		}
		
		/***
		 * Created by Harshitha on may 3rd, 2021
		 * Service for to change the task status 
		 */
		 @ResponseBody
			@RequestMapping("/changetaskstatus.do")
			public String changeTaskStatus(@RequestParam("taskId")Integer taskId,@RequestParam("isActive") Boolean isActive){
				log.info(" ProjectController -> changeTaskStatus()");
				try{
					Integer savedResult=this.projectService.changeTaskActiveStatus(taskId, isActive);
					return "SUCCESS";
				}
				catch(TaskNotFoundException e){
					log.error(" Project Not Found For Changing the Task Status");
					return "ERROR";
				}
				catch(TaskNotSavedOrUpdatedException e){
					log.error("Task Status Is Not Saved Or Updated");
					return "ERROR";
				}
				catch(Exception e){
					e.printStackTrace();
					return "ERROR";
				}
				
			}
		 
		 /**
		   * Created By Harshitha on May 3rd, 2021
		   * Service for to delete the task
		   */
		  @ResponseBody
			@RequestMapping("/deletetask.do")
			public String deleteTask(@RequestParam("taskId")Integer taskId){
				log.info(" ProjectController -> deleteTask()");
				try{
					
					Integer deletedResult=this.projectService.deleteTask(taskId);
					
					if(deletedResult>0) {
						return "SUCCESS";	
					}
					else {
						return "ERROR";
					}
				}
				catch(Exception e){
					e.printStackTrace();
					return "ERROR";
				}
				
			}
}