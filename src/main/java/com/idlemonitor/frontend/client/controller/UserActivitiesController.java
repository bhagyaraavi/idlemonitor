package com.idlemonitor.frontend.client.controller;

import java.util.List;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import com.idlemonitor.common.exceptions.MailNotSentException;
import com.idlemonitor.common.exceptions.PasswordNotMatchException;
import com.idlemonitor.common.exceptions.UserLoginsNotFoundException;
import com.idlemonitor.common.exceptions.UserLoginsNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotSavedOrUpdatedException;
import com.idlemonitor.common.utility.JsonParser;
import com.idlemonitor.frontend.user.dto.IdleTimeConfigurationDto;
import com.idlemonitor.frontend.user.dto.UserDto;
import com.idlemonitor.frontend.user.dto.UserLoginsDto;
import com.idlemonitor.frontend.user.service.UserService;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;
import twitter4j.org.json.JSONArray;
/**
 * Created By Bhagya
 * @author user
 * controller class for user activities - rets services for client app
 */
@RestController
@RequestMapping("/clientactivity")
public class UserActivitiesController{
	
	 private static Logger log=Logger.getLogger(UserActivitiesController.class);	
	 
	 	@Resource(name = "userService")
		private UserService userService;
	 	
	/**
	  * Created By Bhagya On June 29th, 2020
	  * @param username
	  * @param password
	  * @param companyName
	  * @param emailId
	  * @param userId
	  * @return
	  * @throws JSONException
	  * 
	  * Service for to save or update clientuser logins info
	  */
	 
	 	@RequestMapping(value="/saveorupdateuserloginsinfo",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String saveOrUpdateUserLoginsInfo(HttpServletRequest request,HttpServletResponse response)	throws JSONException{
			log.info("inside Client -> UserActivitiesController -> saveOrUpdateUserLoginsInfo()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				UserDto userDto=new UserDto();
				
				userDto.setUserId(inputjson.getInt("userId"));
				UserLoginsDto userLoginsDto=new UserLoginsDto();
				userLoginsDto.setUserDto(userDto);
				userLoginsDto.setDescription(inputjson.getString("description"));
				userLoginsDto.setDataId(inputjson.getInt("dataId"));
				Integer savedDataId=0;
				if(userLoginsDto.getDescription().equalsIgnoreCase("login")){
					savedDataId=this.userService.saveUserLoginsInfo(userLoginsDto);
				}
				else if(userLoginsDto.getDescription().equalsIgnoreCase("idle")){
					savedDataId=this.userService.saveUserIdleTimingsInfo(userLoginsDto);
				}
				else if(userLoginsDto.getDescription().equalsIgnoreCase("shutdown")){
					savedDataId=this.userService.saveUserShutdownTimingsInfo(userLoginsDto);
				}
				else if(userLoginsDto.getDescription().equalsIgnoreCase("logout")){
					savedDataId=this.userService.saveUserLogoutTimingsInfo(userLoginsDto);
				}
				json.accumulate("status", "success");
				json.accumulate("dataId", savedDataId);
				
			}
			
			
			catch(UserNotFoundException e){
				e.printStackTrace();
				log.error(e.getMessage());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Found");
				
			}
			catch(UserLoginsNotFoundException e){
				e.printStackTrace();
				log.error(e.getMessage());
				json.accumulate("status", "fail");
				json.accumulate("error", "UserLogins Not Found");
				
			}
			catch(UserLoginsNotSavedOrUpdatedException e){
				e.printStackTrace();
				log.error(e.getMessage());
				json.accumulate("status", "fail");
				json.accumulate("error", "UserLogins Not Saved or Updated");
				
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("Fail to save or update User Logins Info"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			return json.toString();
		}
	 	/**
	 	 * Created By Bhagya
	 	 * Service for get idle minute configuration data
	 	 * 
	 	 * */
	 	
	 	
	 	@RequestMapping(value="/getidleminutes",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String getIdleMinutes(HttpServletRequest request,HttpServletResponse response)	throws JSONException{
			log.info("inside Client -> UserActivitiesController -> getIdleMinutes()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				Integer userId=inputjson.getInt("userId");
				IdleTimeConfigurationDto idleTimeConfigurationDto=this.userService.getIdleTimeConfigurationByClientUserId(userId);
				if(null!=idleTimeConfigurationDto){
				json.accumulate("status", "success");
				json.accumulate("idleMinutes", idleTimeConfigurationDto.getIdleMinutes());
				}
				
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("error while getting idle minutes"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			return json.toString();
		}
	
	 	/**
	  * Created By BHagya
	  * Service for to save the screenshots of employee/user
	  */
	 	
	 	@RequestMapping("/savescreencaptureimage")
		public String saveUserScreenCaptureImage(HttpServletRequest request,HttpServletResponse response)throws JSONException{
			log.info("inside UserActivitiesController -> saveUserScreenCaptureImage() ");
			System.out.println("INSIDE CONTROLLER ");
			JSONObject json=new JSONObject();
			try{
			
				MultipartRequest multiRequest=(MultipartRequest) request;			
				//JSONObject inputJson=JsonParser.getJson(request, response);
				MultipartFile imageFile=multiRequest.getFile("files");
				//System.out.println(imageFile.getOriginalFilename());
				//System.out.println("REQ "+request.getParameter("json")+" "+request.getParameter("userId"));
				 JSONObject json1=new JSONObject(request.getParameter("json"));
				// System.out.println("request : "+json1.toString());
				Integer userId=json1.getInt("userId");
				Integer updatedResult=this.userService.saveUserScreenCaptureImage(userId, imageFile);
				if(updatedResult>0){
					json.accumulate("STATUS", "success");
					json.accumulate("resultId", updatedResult);
				}
				else{
					throw new Exception();
				}
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("Error While Saving Screen Capture Image"+e.toString());
				json.accumulate("ERROR", "Error While Saving Screen Capture Image");
			}		
			return json.toString();
		}
		/**
		 * Created By Bhagya 
		 * Service for to update the client user login status
		 * */
	 	@RequestMapping(value="/updatestatus.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String updateClientUserStatus(HttpServletRequest request,HttpServletResponse response)	throws JSONException{
			log.info("inside Client -> UserActivitiesController -> updateClientUserStatus()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				Integer userId=inputjson.getInt("userId");
				String status=inputjson.getString("status");
				
				
				Integer updatedResult=this.userService.updateClientUserStatus(userId, status);
				if(updatedResult>0){
				json.accumulate("status", "success");
				json.accumulate("resultId", updatedResult);
				
				
				}
				
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				log.error("user Not found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Found");
				
			}
			catch(UserNotSavedOrUpdatedException e){
				e.printStackTrace();
				log.error("User Not saved or Updated"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Client User Not saved or updated");
				
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("error while updating client user status"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			return json.toString();
		}
	 	/**
	 	 * Created By Bhagya
	 	 * Service for to save user work hours
	 	 * 
	 	 * */
	 	@RequestMapping(value="/saveuserworkhours.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String saveOrUpdateUserWorkHours(HttpServletRequest request,HttpServletResponse response)	throws JSONException{
			log.info("inside Client -> UserActivitiesController -> saveOrUpdateUserWorkHours()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				Integer userId=inputjson.getInt("userId");
				String workHours=inputjson.getString("workHours");
				Integer savedResult=this.userService.saveUserWorkHours(userId, workHours);
				if(savedResult>0){
				json.accumulate("status", "success");
				json.accumulate("resultId", savedResult);
				}
				
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				log.error("user Not found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Found");
				
			}
			catch(UserWorkHoursNotSavedOrUpdatedException e){
				e.printStackTrace();
				log.error("User Work hours Not saved or Updated"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Client User Work Hours Not saved or updated");
				
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("error while Saving or updating user work hours"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			return json.toString();
		}
	 
	 	/**
	 	 * Created By bHagya
	 	 * Service for to get the worked hours by user
	 	 */
	 	@RequestMapping(value="/gettimeworkedbyuser",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String getTimeWorkedByUserOnCurrentDate(HttpServletRequest request,HttpServletResponse response)	throws JSONException{
			log.info("inside Client -> UserActivitiesController -> getTimeWorkedByUserOnCurrentDate()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				Integer userId=inputjson.getInt("userId");
				Map<String,Object> resultMap=this.userService.getTimeWorkedByUserBasedOnCurrentDate(userId);
				
				json.accumulate("status", "success");
				json.accumulate("date", resultMap.get("date"));
				json.accumulate("timeWorked", resultMap.get("timeWorked"));
				
				
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				log.error("user Not found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Found");
				
			}
			catch(UserWorkHoursNotFoundException e){
				e.printStackTrace();
				log.error("User Work hours Not Found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Client User Work Hours Not Found");
				
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("error while Saving or updating user work hours"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			return json.toString();
		}
	 
	 	/**
		 * Created by Bhagya on july 25, 2020
		 * 
		 *  Method to initiate Forgot Password process.
		 *  
		 *  
		 * @param request
		 * @param response
		 * @return
		 * @throws JSONException
		 */
		@RequestMapping(value="/forgotpassword.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String initForgotPassword(HttpServletRequest request,HttpServletResponse response)throws JSONException{
			log.info("UserActivitiesController-> initForgotPassword() ");
			JSONObject json=new JSONObject();
			try{
				JSONObject inputJson=JsonParser.getJson(request, response);
				String email=inputJson.getString("email");
				
				Integer result=this.userService.initForgotPasswordforClient(email);
				if(result>0){
					json.accumulate("email", email);
					json.accumulate("status", "success");
					json.accumulate("successMessage", " : Token has been sent to your email :"+email+" Please check your email ");
				}
				else{
					throw new Exception();
				}
			}
			catch(UserNotFoundException e){
				log.error(" NO User Found with given Email "+e.toString());
				json.accumulate("error", "No User Exists with Entered Email");
				json.accumulate("status", "fail");
			}
			catch(MailNotSentException e){
				e.printStackTrace();
				log.error("Error While Sending Password Token to mail "+e.toString());
				json.accumulate("error", "Error While Sending Password Token through Mail");
				json.accumulate("status", "fail");
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("Error While Sending Password Token Mail "+e.toString());
				json.accumulate("error", "Error While Initiating Forgot Password");
				json.accumulate("status", "fail");	
			}		
			return json.toString();
		}
		
		
		
		/**
		 * Created by Bhagya on July 25, 2020
		 * 
		 * Method to resetPassword...
		 * 
		 * @param request
		 * @param response
		 * @return
		 * @throws Exception
		 */
		@RequestMapping(value="/resetpassword.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String resetPassword(HttpServletRequest request,HttpServletResponse response)throws Exception{
			log.info("UserActivitiesController-> resetPassword()");
			JSONObject json=new JSONObject();
			try{
				JSONObject inputJson=JsonParser.getJson(request, response);
				String email=inputJson.getString("email");
				String token=inputJson.getString("token");
				String password=inputJson.getString("password");
				this.userService.resetClientUserPassword(email, token, password);
				json.accumulate("status", "success");
				json.accumulate("successMessage", "Password has been Reset Successfully");
			}
			catch(PasswordNotMatchException e){
				e.printStackTrace();
				log.error("Password Token does not match");
				json.accumulate("error","Password Token Does Not Match, Please Enter Again");
				json.accumulate("status", "fail");
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("Error While Resetting Client User Password");
				json.accumulate("error", "Error While Resetting Password");
				json.accumulate("status", "fail");
			}
			
			
			return json.toString();
		}
		
		/**
	 	 * Created By Bhagya on AUgust 13th, 2020
	 	 * Service for to save or update user work hours
	 	 * 
	 	 * */
	 	@RequestMapping(value="/saveorupdateuserworkhours.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String saveorUpdateUserWorkHours(HttpServletRequest request,HttpServletResponse response) throws JSONException{
			log.info("inside Client -> UserActivitiesController -> saveorUpdateUserWorkHours()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				Integer userId=inputjson.getInt("userId");
				String workHours=inputjson.getString("workHours");
				Boolean isAppClosed=inputjson.getBoolean("isAppClosed");
				Integer savedResult=this.userService.saveOrUpdateUserWorkHours(userId, workHours,isAppClosed);
				if(savedResult>0){
				json.accumulate("status", "success");
				json.accumulate("resultId", savedResult);
				}
				
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				log.error("user Not found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Found");
				
			}
			catch(UserWorkHoursNotSavedOrUpdatedException e){
				e.printStackTrace();
				log.error("User Work hours Not saved or Updated"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Client User Work Hours Not saved or updated");
				
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("error while Saving or updating user work hours"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			return json.toString();
		}
	 	
	 	/**
	 	 * Created By Bhagya on August 25th, 2020
	 	 * @param request
	 	 * @param response
	 	 * @return
	 	 * @throws JSONException
	 	 * 
	 	 * Service for to update the user work hours flag
	 	 */
		@RequestMapping(value="/updateuserworkhoursflag.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String UpdateUserWorkHoursFlag(HttpServletRequest request,HttpServletResponse response) throws JSONException{
			log.info("inside Client -> UserActivitiesController -> UpdateUserWorkHoursFlag()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				Integer userId=inputjson.getInt("userId");
				Integer savedResult=this.userService.UpdateUserWorkHoursIsAppClosedFlag(userId);
				if(savedResult>0){
				json.accumulate("status", "success");
				json.accumulate("resultId", savedResult);
				}
				
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				log.error("user Not found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Found");
				
			}
			catch(UserWorkHoursNotFoundException e) {
				e.printStackTrace();
				log.error("user work hours Not found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User work hours Not Found");
			}
			catch(UserWorkHoursNotSavedOrUpdatedException e){
				e.printStackTrace();
				log.error("User Work hours Not saved or Updated"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Client User Work Hours Not saved or updated");
				
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("error while updating user work hours flag value"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			
			return json.toString();
		}
		/**
		 * Created By BHagya on march 10th, 2021
		 * @param request
		 * @param response
		 * @return
		 * @throws JSONException
		 * 
		 * Service for to save multiple user screen captures which was captured in user offline condition
		 */
	 	
		@RequestMapping("/savescreencapturemultipleimages.do")
		public String saveUserScreenCaptureMultipleImages(HttpServletRequest request,HttpServletResponse response)throws JSONException{
			log.info("inside UserActivitiesController -> saveUserScreenCaptureImage() ");
			
			JSONObject json=new JSONObject();
			try{
			
				MultipartRequest multiRequest=(MultipartRequest) request;			
				List<MultipartFile> imageFiles=multiRequest.getFiles("files");
				Integer updatedResult=this.userService.saveUserScreenCaptureMultipleImages(imageFiles);
				if(updatedResult>0){
					json.accumulate("STATUS", "success");
					json.accumulate("resultId", updatedResult);
				}
				else{
					throw new Exception();
				}
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("Error While Saving Screen Capture Image"+e.toString());
				json.accumulate("ERROR", "Error While Saving Screen Capture Image");
			}		
			return json.toString();
		}
		
		/**
	 	 * Created By Harshitha on 20th September 2021
	 	 * Service for to get the user details
	 	 */
	 	@RequestMapping(value="/getuserdetails.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String getUserDetails(HttpServletRequest request,HttpServletResponse response)	throws JSONException{
			log.info("inside Client -> UserActivitiesController -> getUserDetails()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				Integer userId=inputjson.getInt("userId");
				UserDto userDto=this.userService.getUserByUserId(userId);
				
				json.accumulate("status", "success");
				json.accumulate("isAppRunning", userDto.getIsAppRunning());
				
				
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				log.error("user Not found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Found");
				
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("error while getting user information"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			System.out.println(" User Details JSON Response "+json.toString());
			return json.toString();
		}
	 /**
	  * Created By Bhagya on october 21st, 2021
	  * @param request
	  * @param response
	  * @return
	  * @throws JSONException
	  * 
	  * Service for update the Application running status and send the login confirmation mail to user
	  * 
	  */
	 	@RequestMapping(value="/updateapprunningstatus.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
	 	public String updateAppRunningStatusAndSendLoginEmail(HttpServletRequest request,HttpServletResponse response) throws JSONException {
	 		JSONObject json=new JSONObject();
	 		try {
	 			JSONObject inputjson=JsonParser.getJson(request, response);
				Integer userId=inputjson.getInt("userId");
				Integer userNo=this.userService.updateUserApppRunningStatusAndLoginConfirmationMail(userId);
				json.accumulate("status", "success");
	 		}
	 		catch(UserNotFoundException e) {
	 			e.printStackTrace();
				log.error("user Not found"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Found");
				
	 		}
	 		catch(Exception e) {
	 			e.printStackTrace();
	 			log.error("error while getting user information"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
	 		}
	 		return json.toString();
	 	}
	 
	 	
}