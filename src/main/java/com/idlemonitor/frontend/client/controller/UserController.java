package com.idlemonitor.frontend.client.controller;

import javax.annotation.Resource;


import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.MailNotSentException;
import com.idlemonitor.common.exceptions.UserEmailExistanceException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.utility.JsonParser;
import com.idlemonitor.frontend.admin.dto.CompanyDto;
import com.idlemonitor.frontend.user.dto.UserDto;
import com.idlemonitor.frontend.user.service.UserService;
import com.idlemonitor.frontend.user.service.UserServiceImpl;

import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;




/**
 * Created by Bhagya on June 29, 2020
 * 
 * Method to Client USer Controller..
 * 
 * @author KNS-ACCONTS
 *
 */


@RestController
@RequestMapping("/clientuser")
public class UserController {
	
	 private static Logger log=Logger.getLogger(UserController.class);	
	 
	 	@Resource(name = "userService")
		private UserService userService;
	 	
	/**
	  * Created By Bhagya On June 29th, 2020
	  * @param username
	  * @param password
	  * @param companyName
	  * @param emailId
	  * @param userId
	  * @return
	  * @throws JSONException
	  * 
	  * Service for to save or update clientuser
	  */
	 
	 	@RequestMapping(value="/saveorupdateclientuser",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String saveOrUpdateUser(HttpServletRequest request,HttpServletResponse response)	throws JSONException{
			log.info("inside Client -> Usercontroller -> saveOrUpdateUser()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response); // handle the json parser log information when json request was null or fail
				UserDto userDto=new UserDto();
				
				userDto.setUserId(inputjson.getInt("userId"));
				userDto.setUsername(inputjson.getString("username"));
				userDto.setPassword(inputjson.getString("password"));
				userDto.setEmailId(inputjson.getString("emailId"));
				CompanyDto companyDto=new CompanyDto();
				companyDto.setCompanyName(inputjson.getString("companyName")); // need to remove company name because we are maintaining company code
				companyDto.setCompanyCode(inputjson.getString("companyCode"));
				userDto.setCompanyDto(companyDto);
				userDto.setEmployeeName(inputjson.getString("employeeName"));
				userDto.setEmployeeNumber(inputjson.getString("employeeNumber"));
				Integer savedUserId=this.userService.saveOrUpdateUser(userDto);
				json.accumulate("status", "success");
				json.accumulate("userId", savedUserId);
				json.accumulate("successMessage", "Token has been sent to your email :"+userDto.getEmailId()+" Please check your email ");
				
			}
			
			catch(UserNotSavedOrUpdatedException e){
				e.printStackTrace();
				log.error("UserNotSavedOrUpdatedException"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "User Not Saved or Updated");
				
			}
			catch(CompanyNotFoundException e){
				e.printStackTrace();
				log.error("No company exist with the given company code");
				json.accumulate("status", "fail");
				json.accumulate("error", "No company exist with the given company code");
			
			}
			catch(UserEmailExistanceException e) {
				e.printStackTrace();
				log.error("User Email Already Exist");
				json.accumulate("status", "fail");
				json.accumulate("error", "User Email Already Exist");
			}
			catch(Exception e){
				e.printStackTrace();
				log.error("Fail to save or update User"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			return json.toString();
		}
	 
	 	/**
	 	 * Created By Bhagya on june 29th, 2020
	 	 * @param username
	 	 * @return
	 	 * @throws JSONException
	 	 * 
	 	 * Service for to check the username already exist or not
	 	 */
	 	@RequestMapping(value="/checkusername",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String checkUsername(HttpServletRequest request,HttpServletResponse response) throws JSONException{
			log.info("Client - USerController -> checkUsername()");
			
			JSONObject json=new JSONObject();
			Integer userId=0;
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				String username=inputjson.getString("username");
				 userId=this.userService.getUserIdByClientusername(username);
				 json.accumulate("status", "success");
				 json.accumulate("userId", userId);
				
				
			}
			catch(UserNotFoundException e){
				json.accumulate("status", "fail");
				json.accumulate("userId", userId);
			
			}
			catch(Exception e){
				e.printStackTrace();
				json.accumulate("status", "fail");
				json.accumulate("userId", userId);
				
			}
			
			return json.toString();
	 	}
	 
	 /**
		 * Created by Bhagya on june 30th, 2020
		 * Method to authenticate User..
		 * @param username
		 * @param password
		 * @return
		 * @throws JSONException 
		 */
		@RequestMapping(value="/authenticateclientuser",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String authenticateUser(HttpServletRequest request,HttpServletResponse response) throws JSONException{
			log.info("Client - USerController -> authenticateclientuser()");
			JSONObject json=new JSONObject();
			
			try{
				JSONObject inputjson=JsonParser.getJson(request, response);
				String username=inputjson.getString("username");
				String password=inputjson.getString("password");
				String companyCode=inputjson.getString("companyCode");
				if(null==companyCode || companyCode.trim().length()<1){
					json.accumulate("status", "fail");
					json.accumulate("error", "Company Code Required");
					json.accumulate("userId", 0);
					
				}
				else if(null==username || username.trim().length()<1){
					json.accumulate("status", "fail");
					json.accumulate("error", "Username Required");
					json.accumulate("userId", 0);
					
				}
				else if(null==password || password.trim().length()<1){
					json.accumulate("status", "fail");
					json.accumulate("error", "Password Required");
					json.accumulate("userId", 0);
					
				}
				else{
					
					Integer userId=this.userService.authenticateClientUserLogin(username, password,companyCode);
					
					if(userId!=0){
						json.accumulate("status", "success");
						json.accumulate("userId",userId);
						
					}
					else{
						throw new AuthenticationException();
					}
				}
			}
			catch(AuthenticationException e){
				e.printStackTrace();
				log.error("Fail to Authenticate User"+e.toString());
				json.accumulate("status", "Authentication Failure");
				json.accumulate("error", "Username or password does not match");
				
			}
			catch(CompanyNotFoundException e){
				e.printStackTrace();
				log.error("Fail to Authenticate User"+e.toString());
				json.accumulate("status", "Authentication Failure");
				json.accumulate("error", "No company exist with the given company code");
				
			}
			catch(UserNotFoundException e){
				e.printStackTrace();
				log.error("Fail to Authenticate User"+e.toString());
				json.accumulate("status", "Authentication Failure");
				json.accumulate("error", "No user exist with the given name");
				
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("Fail to Authenticate User"+e.toString());
				json.accumulate("status", "fail");
				json.accumulate("error", "Unexpected error occured");
				
			}
			return json.toString();
		}
	 
		/**
		 * Created By Bhagya on july 28th, 2020
		 * @param request
		 * @param response
		 * @return
		 * @throws JSONException
		 * 
		 * Service for to confirm the user registration by token value
		 */
		
		@RequestMapping(value="/registrationconfirmation.do",method=RequestMethod.POST,produces={"application/json; charset=UTF-8","*/*;charset=UTF-8"})
		public String userRegistrationConfirmation(HttpServletRequest request,HttpServletResponse response)throws JSONException{
			log.info("UserActivitiesController-> userRegistrationConfirmation() ");
			JSONObject json=new JSONObject();
			try{
				JSONObject inputJson=JsonParser.getJson(request, response);
				String token=inputJson.getString("token");
				
				Integer result=this.userService.activateClientUserFromAccountToken(token);
				if(result>0){
					
					json.accumulate("status", "success");
					json.accumulate("successMessage", "Verified your email successfully! ");
				}
				else{
					throw new Exception();
				}
			}
			catch(UserNotFoundException e){
				log.error(" NO User Found with given Token "+e.toString());
				json.accumulate("error", "No User Exists with the given Verification Token"); // i need to modify the messages
				json.accumulate("status", "fail");
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("Error While verifying registration confirmation Token "+e.toString());
				json.accumulate("error", "Error While verifying registration confirmation Token");
				json.accumulate("status", "fail");	
			}		
			return json.toString();
		}

}
