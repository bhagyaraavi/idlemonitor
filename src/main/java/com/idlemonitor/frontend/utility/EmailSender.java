package com.idlemonitor.frontend.utility;

import java.util.ArrayList;

import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.common.exceptions.MailNotSentException;
import com.idlemonitor.frontend.user.dto.AdminUserDto;
import com.idlemonitor.frontend.user.dto.UserDto;

/**
 * Created By bhagya on may 28th, 2020
 * @author KNS-ACCONTS
 * 
 * Interface for email Sender
 *
 */
public interface EmailSender{
	
	public void sendForgotPasswordMail(final AdminUserDto userDto) throws MailNotSentException;
	public void sendConfirmationMail( final AdminUserDto adminUserDto)throws MailNotSentException;
	public void sendForgotPasswordMailforClientUser(final User user)throws MailNotSentException;
	public void sendRegistartionConfirmationMailforClientUser(final User user)throws MailNotSentException;
	public void sendDailyReportOfEmployeeWorkedHoursToAdmin(ArrayList<UserDto> userDtos,String adminMailId)throws MailNotSentException;
	public void sendIdleMonitorLoginEmail( final UserDto userDto)throws MailNotSentException;

}