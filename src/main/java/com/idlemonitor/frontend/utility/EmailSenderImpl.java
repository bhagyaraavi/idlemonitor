package com.idlemonitor.frontend.utility;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.common.exceptions.MailNotSentException;
import com.idlemonitor.frontend.user.dto.AdminUserDto;
import com.idlemonitor.frontend.user.dto.UserDto;

/**
 * Created By Bhagya On May 28th, 2020
 * @author KNS-ACCONTS
 * Class for email Sender Impl
 */
public class EmailSenderImpl implements EmailSender {
	
private static Logger log=Logger.getLogger(EmailSenderImpl.class);
	
	/**
	 * handles sending mail. Autowired using the bean defined in name mailSender
	 */
	@Autowired
	private JavaMailSender mailSender;

	/**
	 * 
	 * holds the server url (used to build the forgot password link in mail body)
	 * 
	 */
	private String serverHost;
	/**
	 * Template engine. Autowired using the bean defined in the name velocityEngine
	 */
	@Autowired
	private VelocityEngine velocityEngine;

	
	public String getServerHost() {
		return serverHost;
	}


	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}
	
	
	private String imagesPath;
	
		
	public String getImagesPath() {
		return imagesPath;
	}


	public void setImagesPath(String imagesPath) {
		this.imagesPath = imagesPath;
	}
	
	
	
	
	/**
	 * Created By bhagya on april 02nd, 2020
	 * Method for send mail to users for forgot mail
	 * @param userDto
	 */
	public void sendForgotPasswordMail(final AdminUserDto userDto) throws MailNotSentException{
		log.info("inside  sendForgotPasswordMail()");
		
		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
				message.setFrom("no-reply@idlemonitor.com");
				message.setTo(userDto.getContactEmail());
				message.setSubject("Idle Monitor Password Reset Link");
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("email", userDto.getContactEmail());
				map.put("name", userDto.getUsername());
				map.put("passwordToken", userDto.getPasswordToken());
				map.put("user", userDto);
				map.put("serverURL", serverHost);
				//System.out.println(map.toString());
				String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/idlemonitor/templates/forgotPassword.vm","UTF-8", map);
				message.setText(content,true);
        		//System.out.println(content);
				
				
				
				
				  FileSystemResource res = new FileSystemResource(new File( imagesPath+
				  "/logo.png")); message.addInline("logo", res);
				  
				  FileSystemResource res2=new FileSystemResource(new
				  File(imagesPath+"/facebook.png")); message.addInline("facebook", res2);
				  
				  FileSystemResource res3=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("twitter", res3);
				  
				  FileSystemResource res4=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("passwordreset", res4);
				 
				 
				 
				 
				 
			}
		};		
		this.mailSender.send(preparator);
	}
	
	
	/**
	 * Created By Bhagya on april 05th, 2020
	 * 
	 * Registration Confirmation Email
	 *
	 */
	public void sendConfirmationMail( final AdminUserDto adminUserDto)throws MailNotSentException{ 
		log.info("inside  EmailSenderImpl -> sendConfirmationMail()");
		
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
		 public void prepare(MimeMessage mimeMessage) throws Exception {            	
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
                message.setFrom("no-reply@idlemonitor.com");
                message.setTo(adminUserDto.getContactEmail());             
                message.setSubject("Idle Monitor Registration Confirmation");                
                Map<String, Object> map=new HashMap<String, Object>();
                map.put("email", adminUserDto.getContactEmail());
                if(adminUserDto.getUsername()!=null && adminUserDto.getUsername().trim().length()>0){
					map.put("name", adminUserDto.getUsername());
				}
                System.out.println("account activation token "+adminUserDto.getAccountActivationToken());
        		map.put("user", adminUserDto);
        		map.put("serverURL", serverHost);
        		
        		String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/idlemonitor/templates/confirmRegistration.vm","UTF-8", map);
        		
				message.setText(content,true);
				
        	
				
				
				
				
				  FileSystemResource res = new FileSystemResource(new File( imagesPath+
				  "/logo.png")); message.addInline("logo", res);
				  
				  FileSystemResource res2=new FileSystemResource(new
				  File(imagesPath+"/facebook.png")); message.addInline("facebook", res2);
				  
				  FileSystemResource res3=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("twitter", res3);
				  
				  FileSystemResource res4=new FileSystemResource(new
				  File(imagesPath+"/verify.png")); message.addInline("verify", res4);
				 
				 
				 
				 
				        
            }
        };
        this.mailSender.send(preparator);		
	}
	
	/**
	 * 
	 *  Created by Bhagya  on July 25, 2020
	 *  
	 *  Method to send ForgotPassword Code for Client User..
	 *  
	 * 
	 * 
	 * @param User
	 * @throws MailNotSentException
	 */
	public void sendForgotPasswordMailforClientUser(final User user)throws MailNotSentException{
		log.info("EmailSenderImpl-> sendForgotPasswordMailforClientUser() ");
		MimeMessagePreparator preparator=new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
				message.setFrom("no-reply@idlemonitor.com");
				message.setTo(user.getEmailId());
				message.setSubject("Forgot Password: IdleMonitor");
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("email", user.getEmailId());
				map.put("name", user.getUsername()); // need to use either first name and last name instead of username
				map.put("passwordToken", user.getPasswordToken());
				map.put("serverURL", serverHost);
				
				String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/idlemonitor/templates/forgotPasswordClientUser.vm","UTF-8", map);
				message.setText(content,true);
				
				
				
				
				
				  FileSystemResource res = new FileSystemResource(new File(imagesPath+
				  "/logo.png")); message.addInline("logo", res);
				  
				  FileSystemResource res2=new FileSystemResource(new
				  File(imagesPath+"/facebook.png")); message.addInline("facebook", res2);
				  
				  FileSystemResource res3=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("twitter", res3);
				 
				 
				 
				  
				 
			}
		};
		this.mailSender.send(preparator);
	}
	/**
	 * Created By Bhagya on july 28th, 2020
	 * @param user
	 * @throws MailNotSentException
	 * 
	 * Email template for verify the cient user registartion email
	 */
	public void sendRegistartionConfirmationMailforClientUser(final User user)throws MailNotSentException{
		log.info("EmailSenderImpl-> sendRegistartionConfirmationMailforClientUser() ");
		MimeMessagePreparator preparator=new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
				message.setFrom("no-reply@idlemonitor.com");
				message.setTo(user.getEmailId());
				message.setSubject("Registration Confirmation: IdleMonitor");
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("email", user.getEmailId());
				map.put("name", user.getUsername()); // need to modify employee name instead of username
				map.put("verifyToken", user.getAccountActivationToken());
				map.put("serverURL", serverHost);
				
				String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/idlemonitor/templates/emailVerifyClientUser.vm","UTF-8", map);
				message.setText(content,true);
				
				
				
				
				  FileSystemResource res = new FileSystemResource(new File(imagesPath+
				  "/logo.png")); message.addInline("logo", res);
				  
				  FileSystemResource res2=new FileSystemResource(new
				  File(imagesPath+"/facebook.png")); message.addInline("facebook", res2);
				  
				  FileSystemResource res3=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("twitter", res3);
				 
				 
				 
				  
				 
			}
		};
		this.mailSender.send(preparator);
	}
	
	
	/**
	 * Created By Bhagya on August 14th, 2020
	 * @param user
	 * @throws MailNotSentException
	 * 
	 * Email template for send the employees worked hours to the admin
	 */
	public void sendDailyReportOfEmployeeWorkedHoursToAdmin(ArrayList<UserDto> userDtos,String adminMailId)throws MailNotSentException{
		log.info("EmailSenderImpl-> sendRegistartionConfirmationMailforClientUser() ");
		MimeMessagePreparator preparator=new MimeMessagePreparator() {
			
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true);
				message.setFrom("no-reply@idlemonitor.com");
				message.setTo(adminMailId);
				message.setSubject("Employees Worked Hours Report: IdleMonitor");
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("users",userDtos);
				map.put("serverURL", serverHost);
				
				String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/idlemonitor/templates/employeesDailyStatusReport.vm","UTF-8", map);
				message.setText(content,true);
				
				
				
				
				
				  FileSystemResource res = new FileSystemResource(new File(imagesPath+
				  "/logo.png")); message.addInline("logo", res);
				  
				  FileSystemResource res2=new FileSystemResource(new
				  File(imagesPath+"/facebook.png")); message.addInline("facebook", res2);
				  
				  FileSystemResource res3=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("twitter", res3);
				 
				 
				 
				  
				
			}
		};
		this.mailSender.send(preparator);
	}
	
	/**
	 * Created By Bhagya on nov 16th, 2020
	 * 
	 * Idle monitor client app started or login confirmation email
	 *
	 */
	public void sendIdleMonitorLoginEmail( final UserDto userDto)throws MailNotSentException{ 
		log.info("inside  EmailSenderImpl -> sendIdleMonitorLoginEmail()");
		
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
		 public void prepare(MimeMessage mimeMessage) throws Exception {            	
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
                message.setFrom("no-reply@idlemonitor.com");
                message.setTo(userDto.getEmailId());             
                message.setSubject("Idle Monitor Started");                
                Map<String, Object> map=new HashMap<String, Object>();
                map.put("email", userDto.getEmailId());
				map.put("name", userDto.getEmployeeName());
        		map.put("user", userDto);
        		map.put("serverURL", serverHost);
        		
        		String content = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/idlemonitor/templates/clientAppLoginConfirmation.vm","UTF-8", map);
        		
				message.setText(content,true);
				
				
				
				
				
				  FileSystemResource res = new FileSystemResource(new File( imagesPath+
				  "/logo.png")); message.addInline("logo", res);
				  
				  FileSystemResource res2=new FileSystemResource(new
				  File(imagesPath+"/facebook.png")); message.addInline("facebook", res2);
				  
				  FileSystemResource res3=new FileSystemResource(new
				  File(imagesPath+"/twitter.png")); message.addInline("twitter", res3);
				  
				  FileSystemResource res4=new FileSystemResource(new
				  File(imagesPath+"/verify.png")); message.addInline("verify", res4);
				 
				 
				 
				        
            }
        };
        this.mailSender.send(preparator);		
	}
	
}