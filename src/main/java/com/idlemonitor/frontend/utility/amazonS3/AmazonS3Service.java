package com.idlemonitor.frontend.utility.amazonS3;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
/**
 * Created By bHagya
 * @author user
 * Services for to handle the functionalities with amazons3
 */
public interface AmazonS3Service{
	public void getBucketsList();
	public void createFolder(String foldername);
	public Map<String,Object> saveFileToS3Bucket(String userScreenCaptureImagePath,Integer userId, String fileType, MultipartFile file,String fileName);
	public String getS3ImageAsBase64String(String screenshotsPath,Integer userId,String filename);
	public Integer deleteMultipleObjectsInS3MultipleFolder(String screenshotsPath,ArrayList<User> users);
	public Boolean deleteMultipleObjectsInS3Folder(String screenshotsPath,ArrayList<UserScreenCaptures> userScreenCaptures);
	
}