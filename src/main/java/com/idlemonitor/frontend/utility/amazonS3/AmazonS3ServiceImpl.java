package com.idlemonitor.frontend.utility.amazonS3;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
/**
 * Created By bHagya
 * @author user
 * Services for to handle the amazon s3 functionalities
 */
@Service("amazonS3Service")
public class AmazonS3ServiceImpl implements AmazonS3Service{
	
	private static Logger log = Logger.getLogger(AmazonS3ServiceImpl.class);
	
	private  String AWS_KEY;

    private  String AWS_SECRET;

    private  String BUCKET;

    private String FOLDER_SUFFIX = "/";
    
    

    public String getAWS_KEY() {
		return AWS_KEY;
	}
	public void setAWS_KEY(String aWS_KEY) {
		AWS_KEY = aWS_KEY;
	}
	public String getAWS_SECRET() {
		return AWS_SECRET;
	}
	public void setAWS_SECRET(String aWS_SECRET) {
		AWS_SECRET = aWS_SECRET;
	}
	public String getBUCKET() {
		return BUCKET;
	}
	public void setBUCKET(String bUCKET) {
		BUCKET = bUCKET;
	}
	public String getFOLDER_SUFFIX() {
		return FOLDER_SUFFIX;
	}
	public void setFOLDER_SUFFIX(String fOLDER_SUFFIX) {
		FOLDER_SUFFIX = fOLDER_SUFFIX;
	}
	/**
	 * Created By BHagya
	 * Service for to get the list of buckets
	 */
	public void getBucketsList(){
		log.info("AmazonS3ServiceImpl -> getBucketsList");
		AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
        List<Bucket> buckets = s3client.listBuckets();
        System.out.println("Your Amazon S3 buckets:");
        for (Bucket b : buckets) {
            System.out.println("* " + b.getName() +" location ");
        }
    }
	/**
	 * Created By Bhagya
	 * Service for to create the folder inside s3 bucket
	 */
	public void createFolder(String foldername){
		log.info("AmazonS3ServiceImpl ->createFolder()");
		/*
		 * AWSCredentials credentials = new BasicAWSCredentials(AWS_KEY,AWS_SECRET);
		 * AmazonS3 s3client = new AmazonS3Client(credentials);
		 */
		AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
		 // TODO validate foldername

        // Create metadata for your folder & set content-length to 0

		ObjectMetadata metadata = new ObjectMetadata();

       metadata.setContentLength(0);

       // Create empty content

       InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

        // Create a PutObjectRequest passing the foldername suffixed by /

       PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKET, foldername + FOLDER_SUFFIX,emptyContent, metadata);

       // Send request to S3 to create folder
      
       s3client.putObject(putObjectRequest);
	}
	/**
	 * Created By bhagya on july 09th, 2020
	 * @param userScreenCaptureImagePath
	 * @param userId
	 * @param fileType
	 * @param file
	 * @return
	 * 
	 * Service for to save the fileor image to s3 bucket
	 */
	public Map<String,Object> saveFileToS3Bucket(String userScreenCaptureImagePath,Integer userId, String fileType, MultipartFile file,String fileName){
		log.info(" AmazonS3Example -> saveFileToS3Bucket()");
		System.out.println(" Inside savefile controller class");
		Map<String,Object> map= new HashMap<String,Object>();
		try{
			 AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
			
	    	this.createFolder(userScreenCaptureImagePath+userId.toString());
	    	if(fileName==null) {
	    	 fileName=new Date().getTime() + "." + fileType;
	    	}
	    	System.out.println(" file name "+fileName);
			String keyName = userScreenCaptureImagePath+userId+ "/"+fileName;
			
			byte[] contentBytes = null;
			/*
			 * Obtain the Content length of the Input stream for S3 header
			 */
			try {
			    InputStream is = file.getInputStream();
			    contentBytes = IOUtils.toByteArray(is);
			} catch (IOException e) {
				log.info("AmazonS3Example -> saveFileToS3Bucket() ->Failed while reading bytes from %s"+e.getMessage());
			    System.err.printf("Failed while reading bytes from %s", e.getMessage());
			} 

			Long contentLength = Long.valueOf(contentBytes.length);

			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(contentLength);

			 
			InputStream is=file.getInputStream();
			
			s3client.putObject(new PutObjectRequest(BUCKET, keyName, is,metadata)); // need to fetch s3 request status.
			map.put("message", "success");
			map.put("fileName", fileName);
			
			return map;			
			
		}
		catch (AmazonServiceException ase) {
           
            log.error("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
           log.error("Error Message:    " + ase.getMessage());
           log.error("HTTP Status Code: " + ase.getStatusCode());
           log.error("AWS Error Code:   " + ase.getErrorCode());
           log.error("Error Type:       " + ase.getErrorType());
           log.error("Request ID:       " + ase.getRequestId());
           
           System.out.println("Caught an AmazonServiceException, which " +
           		"means your request made it " +
                   "to Amazon S3, but was rejected with an error response" +
                   " for some reason.");
           System.out.println("Error Message:    " + ase.getMessage());
           System.out.println("HTTP Status Code: " + ase.getStatusCode());
           System.out.println("AWS Error Code:   " + ase.getErrorCode());
           System.out.println("Error Type:       " + ase.getErrorType());
           System.out.println("Request ID:       " + ase.getRequestId());
           
           map.put("message", "error");
            
            return map;
        } catch (AmazonClientException ace) {
        	  log.error("Caught an AmazonClientException, which " +
              		"means the client encountered " +
                      "an internal error while trying to " +
                      "communicate with S3, " +
                      "such as not being able to access the network.");
              log.error("Error Message: " + ace.getMessage());
            System.out.println("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
          
            map.put("message", "error");
            
            return map;
        }
		catch(Exception e){
			e.printStackTrace();
			log.error(" Error While Uploading File to S3 for bucket "+BUCKET + e.getMessage() );
			map.put("message", "error");
            
            return map;
		}
		
	}
	
	/**
	 * Created By BHagya
	 * Service for to get the s3image as base64 string
	 */
	
	
	public String getS3ImageAsBase64String(String screenshotsPath,Integer userId,String filename){
		log.info(" AmazonS3serviceImpl -> getS3ImageAsBase64String()");
		String base64="";
		try{	
			AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
		
    	String keyName =screenshotsPath+userId+"/"+filename;
		GetObjectRequest request = new GetObjectRequest(BUCKET,keyName);
		S3Object s3object = s3client.getObject(request);
		S3ObjectInputStream objectContent=s3object.getObjectContent();
		BufferedImage imgBuf = ImageIO.read(objectContent);
		base64=this.encodeBase64URL(imgBuf);
		}
		catch(Exception e){
			log.info(" Exception while gets3ImageAsBase64String ");
		}
		
		return base64;
		
	}
	
	/**
	 * Created By Bhagya
	 * @param imgBuf
	 * @return
	 * @throws IOException
	 * 
	 * Service for to encode the base64 url
	 */
	public String encodeBase64URL(BufferedImage imgBuf) throws IOException {
		log.info("AmazonS3ServiceImpl -> encodeBase64URL");
	    String base64;

	    if (imgBuf == null) {
	        base64 = null;
	    } else {
	        Base64 encoder = new Base64();
	        ByteArrayOutputStream out = new ByteArrayOutputStream();

	        ImageIO.write(imgBuf, "PNG", out);

	        byte[] bytes = out.toByteArray();
	        base64 = "data:image/png;base64,"+ new String(encoder.encode(bytes), "UTF-8");
	    }

	    return base64;
	}
	/**
	 * Created by bhagya on july 18th, 2020
	 * @param screenshotsPath
	 * @param userId
	 * @return
	 * 
	 * Service for to delete the multiple objects in s3 folder
	 */
	public Integer deleteMultipleObjectsInS3MultipleFolder(String screenshotsPath,ArrayList<User> users){
		log.info("Inside AmazonS3ServiceImpl -> deleteMultipleObjectsInS3MultipleFolder() ");
		Integer result=0;
		try{
			AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
		
    	for(User user:users){
		    String key = screenshotsPath+user.getUserId()+"/";   
		    ObjectListing objects = s3client.listObjects(BUCKET, key);
		    System.out.println(" key "+key);
		        for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) 
		            {
		        	System.out.println("objectSummary "+objectSummary.getKey());
		            s3client.deleteObject(BUCKET, objectSummary.getKey());
		            }
    		}
    	result=1;
		}
		catch(Exception e){
			e.printStackTrace();
			log.info("Exception at delete multiple objects in s3 mutliple folder "+e.getMessage());
		}
		
		return result;
	        
	}
	/**
	 * Created By Bhagya
	 * Service for to delete multiple objects in s3 folder
	 */
	public Boolean deleteMultipleObjectsInS3Folder(String screenshotsPath,ArrayList<UserScreenCaptures> userScreenCaptures){
		log.info("Inside AmazonS3ServiceImpl -> deleteMultipleObjectsInS3Folder() ");
		Boolean result=false;
		try{
			AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
			
    	for(UserScreenCaptures userScreenCapture:userScreenCaptures){
		    String key = screenshotsPath+userScreenCapture.getUser().getUserId()+"/"+userScreenCapture.getCaptureScreenImage(); 
		    s3client.deleteObject(BUCKET, key); // need to check the delete object return type
    		}
    	result=true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.info("Exception at delete multiple objects in s3 folder "+e.getMessage());
		}
		
		return result;
	        
	}
}