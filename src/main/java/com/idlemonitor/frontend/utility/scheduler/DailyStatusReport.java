package com.idlemonitor.frontend.utility.scheduler;

import org.apache.log4j.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.idlemonitor.frontend.employee.service.EmployeeService;



/**
 * Created by bhagya on July 18th, 2020
 * @author Home
 * Class for to delete the user screenshots by implementing the quartz listener job functionality
 */


@EnableScheduling
@Service("dailyStatusReport")
public class DailyStatusReport{
	private static Logger log = Logger.getLogger(DailyStatusReport.class);
	@Autowired
	private EmployeeService employeeService;
	
	 @Scheduled(cron="0 30 22 ? * MON-FRI") // every day 10:30pm  
	    public void dailyStatusReportMethod()
	    {
		 log.info("Inside Scheduler of daily status report ");
	             System.out.println("INSIDE Quartz Scheduler of daily status report");
		         employeeService.sendDailyStatusReportEmailToAdmin();
		  log.info("Completed the service for Scheduler of daily status report");
	    }
}
