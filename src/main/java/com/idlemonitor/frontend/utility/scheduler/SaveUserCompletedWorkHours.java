package com.idlemonitor.frontend.utility.scheduler;

import org.apache.log4j.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.idlemonitor.frontend.employee.service.EmployeeService;



/**
 * Created by bhagya on october 22nd, 2020
 * @author Home
 * Class for to save User Completed WorkHours by implementing the quartz listener job functionality
 */


@EnableScheduling
@Service("saveUserCompletedWorkHours")
public class SaveUserCompletedWorkHours{
	private static Logger log = Logger.getLogger(SaveUserCompletedWorkHours.class);
	@Autowired
	private EmployeeService employeeService;
	
	 @Scheduled(cron="0 0 05 ? * ?") // every day 5 am  0 0 05 ? * ?
	    public void saveUserCompletedWorkHours()
	    {
		 log.info("Inside Scheduler of saveUserCompletedWorkHours ");
	        System.out.println("INSIDE Quartz Scheduler of saveUserCompletedWorkHours");
		    employeeService.saveUsersCompletedWorkedHours();
		  log.info("Completed the service for Scheduler of saveUserCompletedWorkHours");
	    }
}
