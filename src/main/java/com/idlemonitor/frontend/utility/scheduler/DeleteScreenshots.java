package com.idlemonitor.frontend.utility.scheduler;

import org.apache.log4j.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.idlemonitor.frontend.employee.service.EmployeeService;



/**
 * Created by bhagya on July 18th, 2020
 * @author Home
 * Class for to delete the user screenshots by implementing the quartz listener job functionality
 */


@EnableScheduling
@Service("deleteScreenshots")
public class DeleteScreenshots{
	private static Logger log = Logger.getLogger(DeleteScreenshots.class);
	@Autowired
	private EmployeeService employeeService;
	
	 @Scheduled(cron="0 0 04 05 * ?") // every month 5th day 04am morning
	    public void deleteScreenshotsMethod()
	    {
		 log.info("Inside Scheduler of delete screenshots ");
	             System.out.println("INSIDE Quartz Scheduler of delete screenshots");
		         employeeService.deleteMultipleScreenshotsByCronJob();
		  log.info("Completed the service for Scheduler of delete screenshots");
	    }
}
