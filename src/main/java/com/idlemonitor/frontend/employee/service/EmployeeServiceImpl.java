package com.idlemonitor.frontend.employee.service;

import java.text.ParseException;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.lang.*;
import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.backend.admin.model.ProjectMapping;
import com.idlemonitor.backend.employee.dao.EmployeeDao;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserCompletedWorkHours;
import com.idlemonitor.backend.user.model.UserLogins;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
import com.idlemonitor.backend.user.model.UserWorkHours;
import com.idlemonitor.backend.user.dao.UserDao;
import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeNotFoundException;
import com.idlemonitor.common.exceptions.MailNotSentException;
import com.idlemonitor.common.exceptions.ProjectMappingNotFoundException;
import com.idlemonitor.common.exceptions.UserCompletedWorkHoursNotFoundException;
import com.idlemonitor.common.exceptions.UserCompletedWorkHoursNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserLoginsNotFoundException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserScreenCapturesNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
import com.idlemonitor.frontend.employee.dto.EmployeeIdleTimeDto;
import com.idlemonitor.frontend.user.dto.UserLoginsDto;
import com.idlemonitor.frontend.user.dto.UserScreenCapturesDto;
import com.idlemonitor.frontend.user.service.UserService;
import com.idlemonitor.frontend.utility.EmailSender;
import com.idlemonitor.frontend.utility.amazonS3.AmazonS3Service;
import com.idlemonitor.frontend.user.dto.UserCompletedWorkHoursDto;
import com.idlemonitor.frontend.user.dto.UserDto;

/**
 * Created By Bhagya
 * @author user
 * 
 * Implementation class for Employee Service
 *
 */
@Transactional
@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService{
	
	private static Logger log = Logger.getLogger(EmployeeServiceImpl.class);
	
	@Resource(name = "employeeDao")
	private EmployeeDao employeeDao;
	
	@Resource(name = "userDao")
	private UserDao userDao;
	
	@Resource(name = "emailSender")
	private EmailSender emailSender;
	
	@Resource(name="amazonS3Service")
	private AmazonS3Service amazonS3Service;
	
	@Resource(name = "userService")
	private UserService userService;
	
	private String userScreenCaptureImagePath;
	
	public String getUserScreenCaptureImagePath() {
		return userScreenCaptureImagePath;
	}
	public void setUserScreenCaptureImagePath(String userScreenCaptureImagePath) {
		this.userScreenCaptureImagePath = userScreenCaptureImagePath;
	}
	
	/**
	   * Created By Bhagya On july 12th, 2017
	   * @param username
	   * @return
	   * @throws ProviderNotFoundException
	   * @throws UserNotFoundException
	   * @throws ProviderLocationNotFoundException
	   * 
	   * Method for to get provider Locations By username
	   */
	    public ArrayList<UserDto> getEmployeeUsersByAdminUsername(String username,Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws  UserNotFoundException, EmployeeNotFoundException{
	    	log.info(" EmployeeServiceImpl -> getEmployeeUsersByAdminUsername() ");
	  	  ArrayList<UserDto> employeeUserDtos=new ArrayList<UserDto>();
	  	  AdminUser adminUser=this.userDao.getUserforAuthentication(username);
	  	  ArrayList<User> employeeUsers=this.employeeDao.getUsersByCompanyId(adminUser.getCompany(), pageNo, pageSize, searchBy, sortBy, ascending);
	  	  for(User employeeUser:employeeUsers){ // need to modify the List method instead of for loop.
	  		  UserDto employeeUserDto=UserDto.populateUserDto(employeeUser);
	  		  employeeUserDtos.add(employeeUserDto);
	  	  }
	  	  employeeUserDtos.get(0).setTotalUsers( employeeUsers.get(0).getTotalUsers());
	  	  return employeeUserDtos;
	    }
	    /**
	     * Created by bhagya on June 09th,2020
	     * @param employeeUserId
	     * @return
	     * @throws EmployeeNotFoundException
	     * 
	     * Service for to get the employee user logins by employee userId
	     * @throws ParseException 
	     */
	    public ArrayList<UserLoginsDto> getEmployeeUserLoginsByEmployeeUserId(Integer employeeUserId,Integer period,String selectedDate) throws EmployeeNotFoundException, ParseException{
	    	log.info("EmployeeServiceImpl -> getEmployeeUserLoginsByEmployeeUserId");
	    	ArrayList<UserLoginsDto> employeeUserDetailsDtos=new ArrayList<UserLoginsDto>();
	    	User employeeUser=this.employeeDao.getEmployeeUserByUserId(employeeUserId); 
	    	SimpleDateFormat monthDateFormat = new SimpleDateFormat("MM-dd-yyyy");
	    	Date today=new Date();
	    	if(null==period || period==0){ // for to show the yesterday data
	    		period=1;
	    	}
	    	if(null==selectedDate || selectedDate.length()<=0){		
		    	String todayDateFormat = monthDateFormat.format(today);
		    	selectedDate=todayDateFormat;
	    	}
	    	Date fromDate=null;
	    	Date toDate=null;
	    	
	    	if(null!=selectedDate && selectedDate.length()>0){
		    	Calendar cal = Calendar.getInstance();
				Date newFormatDate=monthDateFormat.parse(selectedDate);
				cal.setTime(newFormatDate);
				cal.add(Calendar.DATE, -(period));
				Date dateBeforeDays = cal.getTime();
				fromDate=dateBeforeDays;
				toDate=today;
	    	}
			//fromDate=monthDateFormat.format(dateBeforeDays);
	    	
	    	ArrayList<UserLogins> employeeUserLogins=this.employeeDao.getEmployeeUserLoginsByUserId(employeeUser, fromDate, toDate);
	    	for(UserLogins employeeUserDetails:employeeUserLogins){
	    		UserLoginsDto employeeUserDetailsDto=UserLoginsDto.populateUserLoginsDto(employeeUserDetails);
	    		employeeUserDetailsDtos.add(employeeUserDetailsDto);
	    	} 	
	    	return employeeUserDetailsDtos;
	    }
	    /**
	     * Created By Bhagya ON June 10th, 2020
	     * @param employeeUserId
	     * @return
	     * @throws EmployeeNotFoundException
	     * 
	     * Service for to get the employee userdetails By employee UserId
	     */
	    
	    public UserDto getEmployeeUserDetailsByEmployeeUserId(Integer employeeUserId) throws EmployeeNotFoundException{
	    	log.info("EmployeeServiceImpl -> getEmployeeUserDetailsByEmployeeUserId");	    	
	    	User employeeUser=this.employeeDao.getEmployeeUserByUserId(employeeUserId);
	    	UserDto employeeUserDto=UserDto.populateUserDto(employeeUser);
	    	return employeeUserDto;
	    }
	/**
	 * Created By Bhagya
	 * @param employeeUserLogins
	 * Service for to clculate idle time on each day
	 */
	    public void calculateIdleTimeOnEachDay(ArrayList<UserLogins> employeeUserLogins){
	    	for(UserLogins employeeUserDetails:employeeUserLogins){
	    		SimpleDateFormat dateformat=new SimpleDateFormat("MM/dd/yyyy");
	    		
	    		String loginDate=dateformat.format(employeeUserDetails.getLoginDateTime());
	    		String idleDate=dateformat.format(employeeUserDetails.getIdleDateTime());
	    		EmployeeIdleTimeDto employeeIdleTimeDto = new EmployeeIdleTimeDto();
	    		Integer oldhours=0;
	    		if(loginDate.equals(idleDate)){
	    			employeeIdleTimeDto.setIdleDate(idleDate);
	    			long diff = employeeUserDetails.getIdleDateTime().getTime() - employeeUserDetails.getLoginDateTime().getTime();
	    			long diffSeconds = diff / 1000 % 60;
	    			long diffMinutes = diff / (60 * 1000) % 60;
	    			long diffHours = diff / (60 * 60 * 1000) % 24;
	    			//System.out.println(" seconds "+diffSeconds +" minutes "+diffMinutes +" hours "+diffHours);
	    			int idleHours=(int)diffHours;
	    			
	    			employeeIdleTimeDto.setIdleHours(oldhours+idleHours);
	    			
	    			 oldhours=idleHours;
	    		}
	    	}
	    	
	    	
	    }
	    
	    /**
	     * Created By Bhagya On july 07th, 2020
	     * @param userId
	     * @param pageNo
	     * @param pageSize
	     * @param ascending
	     * @return
	     * @throws UserScreenCapturesNotFoundException
	     * @throws UserNotFoundException
	     * 
	     * Service for to fetch the screenshots of users
	     * @throws ParseException 
	     */
	    public ArrayList<UserScreenCapturesDto> getScreenshotsOfUser(String loggedInAdminUsername,Integer userId,Integer pageNo, Integer pageSize,Integer period,String selectedDate) throws UserScreenCapturesNotFoundException, UserNotFoundException, ParseException {
	    log.info(" EmployeeServiceImpl -> getScreenshotsOfUser() ");
	  	  ArrayList<UserScreenCapturesDto> screenCaptureDtos=new ArrayList<UserScreenCapturesDto>();
	  	  User user=null;	  	  
	  	  if(null!=userId && userId!=0){
	  	   user=this.userDao.getUserByUserId(userId);
	  	  }
		  	SimpleDateFormat monthDateFormat = new SimpleDateFormat("MM-dd-yyyy");
	    	Date today=new Date();
	    	if(null==period){
	    		period=1;
	    	}
	    	if(null==selectedDate || selectedDate.length()<=0){	
		    	String todayDateFormat = monthDateFormat.format(today);
		    	selectedDate=todayDateFormat;
	    	}
	    	Date fromDate=null;
	    	Date toDate=null;
	    	
	    	if(null!=selectedDate && selectedDate.length()>0){
		    	Calendar cal = Calendar.getInstance();
				Date newFormatDate=monthDateFormat.parse(selectedDate);
				cal.setTime(newFormatDate);
				cal.add(Calendar.DATE, -(period));
				Date dateBeforeDays = cal.getTime();
				fromDate=dateBeforeDays;
				toDate=today;
	    	}
			//fromDate=monthDateFormat.format(dateBeforeDays);
	    	//System.out.println(" fromDate "+fromDate +" toDate "+toDate);
	    	AdminUser adminUser=this.userDao.getUserforAuthentication(loggedInAdminUsername);
	  	  ArrayList<UserScreenCaptures> screenCaptures=this.employeeDao.getScreenshotsOfUsers(adminUser.getCompany(),user, pageNo, pageSize,fromDate,toDate);
	  	  for(UserScreenCaptures screenCapture:screenCaptures){
	  		  UserScreenCapturesDto userScreenCapturesDto=UserScreenCapturesDto.populateUserScreenCapturesDto(screenCapture);
	  		  String base64ImageURL=amazonS3Service.getS3ImageAsBase64String(userScreenCaptureImagePath, screenCapture.getUser().getUserId(), screenCapture.getCaptureScreenImage());
	  		  userScreenCapturesDto.setBase64Image(base64ImageURL);
	  		  screenCaptureDtos.add(userScreenCapturesDto);
	  	  }
	  	  screenCaptureDtos.get(0).setTotalCaptures(screenCaptures.get(0).getTotalCaptures());
	  	  return screenCaptureDtos;
	    }
	    /**
	     * created By Bhagya on July 08th, 2020
	     * @return
	     * @throws UserNotFoundException
	     * 
	     * Service for to get the list of users or employees
	     */
	    public ArrayList<UserDto> getListOfUsers() throws UserNotFoundException{
	    	log.info("EmployeeServiceimpl -> getListOfUsers()");
	    	ArrayList<User> users=this.employeeDao.getListOfUsers();
	    	ArrayList<UserDto> userDtos=new ArrayList<UserDto>();
	    	for(User user:users){
	    		UserDto userDto=UserDto.populateUserDto(user);
	    		userDtos.add(userDto);	
	    	}
	    	return userDtos;
	    }
	    /**
	     * Created by bhagya on july 08th, 2020
	     * @param dataId
	     * @return
	     * @throws Exception
	     * 
	     * Service for to delete the employee user screen capture images
	     */
	    public Integer deleteEmployeeUserScreenCaptureImages(ArrayList<Integer> dataIds) throws Exception{
	    	log.info("EmployeeServiceImpl -> deleteEmployeeUserScreenCaptureImages()");
	    	System.out.println(" Employee service  delete screen shots "+dataIds);
	    	Boolean result=false; 
	    	ArrayList<UserScreenCaptures> userScreenCaptures=this.employeeDao.getUserScreenCapturesByDataIds(dataIds);
	    	Integer deletedResult=this.employeeDao.deleteUserScreenCaptures(userScreenCaptures);
	    	 result=this.amazonS3Service.deleteMultipleObjectsInS3Folder(getUserScreenCaptureImagePath(), userScreenCaptures);
	    	if(result==true&&deletedResult>0){
	    		
	    		return deletedResult;
	    	}
	    	else{
	    		return 0;
	    	}
	    }
	    /**
	     * 
	     * Created By Bhagya on july 18th, 2020
	     * @return
	     * 
	     * Service for to delete the multipleScrrenshots By Cron Job
	     */ 
	   public Integer deleteMultipleScreenshotsByCronJob(){
		   log.info("EmployeeServiceImpl -> deleteMultipleScreenshotsByCronJob()");
		   Integer deletedResult=0;
		   try{
			  ArrayList<User> users=employeeDao.getListOfUsers();
			  ArrayList<UserScreenCaptures> userScreenCaptures=this.employeeDao.getUserScreenCapturesByUsers(users);
			  if(userScreenCaptures.size()>0){ 
				//deletedResult=this.amazonS3Service.deleteMultipleObjectsInS3MultipleFolder(userScreenCaptureImagePath, users); commented this code based on userId folder
				Boolean s3Result= this.amazonS3Service.deleteMultipleObjectsInS3Folder(userScreenCaptureImagePath, userScreenCaptures); // this service will delete based on userId/image name 
				 deletedResult=this.employeeDao.deleteUserScreenCaptures(userScreenCaptures);
				 
			  }
			  
			  
		   }
		   catch(Exception e){
			   e.printStackTrace();
			   log.info(" Exception while deleting userscreen captures by cron job "+e.getMessage());
		   }
		   return deletedResult;
	   }
	   /**
	    * Created By Bhagya on August 14th, 2020
	    * @param company
	    * @return
	    * @throws EmployeeNotFoundException
	    * 
	    * Service for to get users with worked hours by current date
	 * @throws UserNotFoundException 
	    */
	  public ArrayList<UserDto> getUsersWithWorkedHoursByCurrentDate(Company company) throws EmployeeNotFoundException, UserNotFoundException{
		  log.info("EmployeeServiceImpl -> getUsersWithWorkedHoursByCurrentDate()");
		  ArrayList<UserDto> userDtos=new ArrayList<UserDto>();
		  ArrayList<User> users=this.employeeDao.getUsersByCompanyId(company, null, null, null, null, null);
		  for(User user:users) {
			  UserDto userDto=UserDto.populateUserDto(user);
			  try {
				  Map<String,Object> workedHoursMap=this.userService.getTimeWorkedByUserBasedOnCurrentDate(user.getUserId()); 
				  String workedHours=workedHoursMap.get("timeWorked").toString();
				  userDto.setWorkedHours(workedHours);
				  Integer workHours=Integer.parseInt(workedHours.substring(0, 2));
				  if(workHours>=8) {
					  userDto.setIsCompletedHours(true);
				  }
				  else {
					  userDto.setIsCompletedHours(false);
				  }
			  }
			  catch(UserWorkHoursNotFoundException e) {
				  userDto.setWorkedHours("00:00:00");
				  userDto.setIsCompletedHours(false);
			  }
			  userDtos.add(userDto);
		  }
		  return userDtos;
		  
	  }
	  
	  /**
		 * java.util.date to java.sql.date conversion
		 * @return
		 */
		private static java.sql.Date getCurrentDate() {
		    java.util.Date today = new java.util.Date();
		    return new java.sql.Date(today.getTime());
		}
		/**
		 * Created By Bhagya on AUgust 14th, 2020
		 * Service for to send an email to companies admins , regarding there employees daily work hours report
		 */
		
		public void sendDailyStatusReportEmailToAdmin() {
			try {
				ArrayList<Company> companies=this.employeeDao.getListOfCompanies();
				for(Company company: companies) {
					try {
						ArrayList<UserDto> userDtos=this.getUsersWithWorkedHoursByCurrentDate(company);
						String[] adminEmailids=company.getAdminEmailIds().split(",");
						for (String adminEmailId : adminEmailids) {
							 try {
								this.emailSender.sendDailyReportOfEmployeeWorkedHoursToAdmin(userDtos, adminEmailId);
							} 
							 catch (MailNotSentException e) {
								log.info(" Error while sending an daily worked hours email to admin : "+adminEmailId);
								e.printStackTrace();
							}
						}
					}
					catch(UserNotFoundException e) {
						log.info(" No User Found ");
					}
					catch(EmployeeNotFoundException e) {
						log.info(" No Employees Found for company "+company.getCompanyName());
					}
					
					
				}
			}
			catch(CompanyNotFoundException e) {
				log.info(" No Companies Found to send a daily status report to admin");
			}
		}
		/**
		 * Created By Bhagya On October 19th, 2020
		 * @param employeeUserId
		 * @param isActive
		 * @return
		 * @throws EmployeeNotFoundException
		 * @throws UserNotSavedOrUpdatedException
		 * 
		 * Service for update the status of user account
		 */
		
		public Integer changeEmployeeUserAccountStatus(Integer employeeUserId,Boolean isActive) throws EmployeeNotFoundException, UserNotSavedOrUpdatedException{
	    	log.info("EmployeeServiceImpl -> changeEmployeeUserAccountStatus");	    	
	    	User employeeUser=this.employeeDao.getEmployeeUserByUserId(employeeUserId);
	    	employeeUser.setIsActive(isActive);
	    	Integer updatedResult=this.userDao.saveOrUpdateUserInfo(employeeUser);
	    	return updatedResult;
	    }
		/**
		 * Created By Bhagya on october 20th, 2020
		 * @param employeeUserId
		 * @return
		 * 
		 * Service for to delete the employeeUser.. following steps we are doing
		 * 1. delete the user work hours
		 * 2. delete the user logins
		 * 3. delete the user screen captures
		 * 4. delete the project mappings
		 * 5. delete the employee user
		 */
		
		public Integer deleteEmployeeUser(Integer employeeUserId) {
			log.info("EmployeeServiceImpl -> deleteEmployeeUser()");
			Integer deletedResult=0;
			User employeeUser=null;
			try {
				employeeUser=this.employeeDao.getEmployeeUserByUserId(employeeUserId);
				try {
					ArrayList<UserWorkHours> userWorkHours=this.userDao.getListOfUserWorkHoursByWorkDateAndUser(employeeUser, null);
					this.employeeDao.deleteUserWorkHours(userWorkHours);
				}
				catch(UserWorkHoursNotFoundException e) {
					log.info("EmployeeServiceImpl -> deleteEmployeeUser() -> UserWorkHoursNotFoundException -> "+e.getMessage());
				}
				try {
					ArrayList<UserCompletedWorkHours> userCompletedWorkHours=this.userDao.getListOfUserCompletedWorkHoursByUser(employeeUser);
					this.employeeDao.deleteUserCompletedWorkHours(userCompletedWorkHours);
				}
				catch(UserCompletedWorkHoursNotFoundException e) {
					log.info("EmployeeServiceImpl -> deleteEmployeeUser() -> UserCompletedWorkHoursNotFoundException -> "+e.getMessage());
				}
				try {
					ArrayList<UserLogins> userLogins=this.userDao.getListOfUserLoginsInfoByUser(employeeUser);
					this.employeeDao.deleteUserLogins(userLogins);
				 }
				catch(UserLoginsNotFoundException e) {
					log.info("EmployeeServiceImpl -> deleteEmployeeUser() -> UserLoginsNotFoundException -> "+e.getMessage());
				}
				this.deleteMultipleScreenshotsOfUser(employeeUser);
				try {
					ArrayList<ProjectMapping> projectMappings=this.employeeDao.getListOfProjectMappingsOfUser(employeeUser);
					this.employeeDao.deleteProjectMappingsOfUser(projectMappings);
				}
				catch(ProjectMappingNotFoundException e) {
					log.info("EmployeeServiceImpl -> deleteEmployeeUser() -> ProjectMappingNotFoundException -> "+e.getMessage());
				}	
				deletedResult=this.employeeDao.deleteEmployeeUser(employeeUser);	
			}
			catch(EmployeeNotFoundException e) {
				log.info("EmployeeServiceImpl -> deleteEmployeeUser() -> EmployeeNotFoundException -> "+e.getMessage());
			}
			
			return deletedResult;
		}
		
		/**
	     * 
	     * Created By Bhagya on october 20, 2020
	     * @return
	     * 
	     * Service for to delete the multipleScrrenshots Of User
	     */ 
	   public Integer deleteMultipleScreenshotsOfUser(User employeeUser){
		   log.info("EmployeeServiceImpl ->deleteMultipleScreenshotsOfUser()");
		   Integer deletedResult=0;
		   try{
			  ArrayList<User> users=new ArrayList<User>();
			  users.add(employeeUser);
			  ArrayList<UserScreenCaptures> userScreenCaptures=this.employeeDao.getUserScreenCapturesByUser(employeeUser);
			  if(userScreenCaptures.size()>0){
				  this.employeeDao.deleteUserScreenCaptures(userScreenCaptures);
			  }
			  deletedResult=this.amazonS3Service.deleteMultipleObjectsInS3MultipleFolder(userScreenCaptureImagePath, users);
			  
		   }
		   catch(Exception e){
			   e.printStackTrace();
			   log.info(" Exception while deleting userscreen captures by cron job "+e.getMessage());
		   }
		   return deletedResult;
	   }
	   /**
	    * Created By Bhagya on october 22nd, 2020
	    * @param company
	    * @throws EmployeeNotFoundException
	    * @throws UserNotFoundException
	    * @throws ParseException
	    * 
	    * Service for to save user completed work hours
	    */
	   
	   public void saveUsersCompletedWorkedHours(){
		 log.info("EmployeeServiceImpl -> saveUsersCompletedWorkedHours()");
	    try {
	      ArrayList<Company> companies=this.employeeDao.getListOfCompanies();
		  for(Company company: companies) {
			  ArrayList<User> users=new ArrayList<User>();
				  users = this.employeeDao.getUsersByCompanyId(company, null, null, null, null, null);
				  for(User user:users) {
					  UserCompletedWorkHours userCompletedWorkHours=new UserCompletedWorkHours();
					  
					  try {
						  Calendar cal = Calendar.getInstance();
						   cal.add(Calendar.DATE, -(1));
						   Date yesterdayDate = cal.getTime();
						   userCompletedWorkHours.setCompany(company);
						  userCompletedWorkHours.setUser(user);
						  userCompletedWorkHours.setWorkDate(yesterdayDate);
							
							 Map<String, Object> workedHoursMap = this.userService.getEmployeeWorkHoursBasedOnYesterdayDate(user,yesterdayDate);
							 String workedHours=workedHoursMap.get("timeWorked").toString();
							 userCompletedWorkHours.setWorkHours(workedHours);
						} 
						catch (UserNotFoundException e) {
							 log.info("EmployeeServiceImpl -> saveUsersCompletedWorkedHours() -> UserNotFoundException -> "+e.getMessage());
							e.printStackTrace();
						} 
						catch (ParseException e) {
							 log.info("EmployeeServiceImpl -> saveUsersCompletedWorkedHours() -> ParseException -> "+e.getMessage());
							 e.printStackTrace();
						}
					 
						catch(UserWorkHoursNotFoundException e) {
						  userCompletedWorkHours.setWorkHours("00:00:00");
					  
						} 
					  catch(Exception e) {
						  log.info("EmployeeServiceImpl -> saveUsersCompletedWorkedHours() -> Inside first try ->Exception -> "+e.getMessage());
						  e.printStackTrace();
					  }
					  
					  try {
						  this.userDao.saveOrUpdateUserCompletedWorkHours(userCompletedWorkHours);
						  
					  }
					  catch (UserCompletedWorkHoursNotSavedOrUpdatedException e) {
						 log.info("EmployeeServiceImpl -> UserCompletedWorkHoursNotSavedOrUpdatedException -> "+e.getMessage());
						 e.printStackTrace();
					}
					  catch(Exception e) {
						  log.info("EmployeeServiceImpl -> saveUsersCompletedWorkedHours() -> Inside second try ->Exception -> "+e.getMessage());
						  e.printStackTrace();
					  }
				  }
			  } 
			 
		  }// for
		    catch (CompanyNotFoundException e1) {
		    	log.info("EmployeeServiceImpl -> saveUsersCompletedWorkedHours() -> CompanyNotFoundException -> "+e1.getMessage());
		    	e1.printStackTrace();
			}
		     catch (EmployeeNotFoundException e1) {
				log.info("EmployeeServiceImpl -> saveUsersCompletedWorkedHours() -> EmployeeNotFoundException -> "+e1.getMessage());
				e1.printStackTrace();
			 } 
		    catch(Exception e) {
		    	log.info("EmployeeServiceImpl -> saveUsersCompletedWorkedHours() -> Main try -> Exception -> "+e.getMessage());
		    	e.printStackTrace();
		    }
	  } //method
	   /**
	    * Created By Bhagya on october 22nd, 2020
	    * @param username
	    * @param pageNo
	    * @param pageSize
	    * @param searchBy
	    * @param sortBy
	    * @param ascending
	    * @return
	    * @throws UserNotFoundException
	    * @throws EmployeeNotFoundException
	    * 
	    * Service for to get the list of employees work hours based on admin  
	 * @throws ParseException 
	    */
	   public ArrayList<UserCompletedWorkHoursDto> getEmployeesWorkHoursByAdminUsername(String username,Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending,Integer period,String selectedDate) throws  UserNotFoundException, EmployeeNotFoundException, ParseException{
	    	log.info(" EmployeeServiceImpl -> getEmployeesWorkHoursByAdminUsername() ");
	    
	  	  ArrayList<UserCompletedWorkHoursDto> userCompletedWorkHoursDtos=new ArrayList<UserCompletedWorkHoursDto>();
	  	  AdminUser adminUser=this.userDao.getUserforAuthentication(username);
	  	SimpleDateFormat monthDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    	Date today=new Date();
    	if(null==period|| period==0){
    		period=1;
    	}
    	if(null==selectedDate || selectedDate.length()<=0){		
	    	String todayDateFormat = monthDateFormat.format(today);
	    	selectedDate=todayDateFormat;
    	}
    	Date fromDate=null;
    	Date toDate=null;
    	
    	if(null!=selectedDate && selectedDate.length()>0){
	    	Calendar cal = Calendar.getInstance();
			Date newFormatDate=monthDateFormat.parse(selectedDate);
			cal.setTime(newFormatDate);
			cal.add(Calendar.DATE, -(period));
			Date dateBeforeDays = cal.getTime();
			fromDate=dateBeforeDays;
			toDate=today;
    	}
    	
	  	  ArrayList<UserCompletedWorkHours> userCompletedWorkHours=this.employeeDao.getEmployeesWorkHoursByCompany(adminUser.getCompany(), pageNo, pageSize, searchBy, sortBy, ascending,fromDate,toDate);
	  	  for(UserCompletedWorkHours userCompletedWorkHour:userCompletedWorkHours){ // need to modify the List method instead of for loop.
	  		UserCompletedWorkHoursDto userCompletedWorkHoursDto=UserCompletedWorkHoursDto.populateUserCompletedWorkHoursDto(userCompletedWorkHour);
	  		userCompletedWorkHoursDtos.add(userCompletedWorkHoursDto);
	  	  }
	  	  userCompletedWorkHoursDtos.get(0).setTotalUsers( userCompletedWorkHours.get(0).getTotalUsers());
	  	  return userCompletedWorkHoursDtos;
	    }
}