package com.idlemonitor.frontend.employee.service;

import java.text.ParseException;

import java.util.ArrayList;

import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.common.exceptions.EmployeeNotFoundException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserScreenCapturesNotFoundException;
import com.idlemonitor.frontend.user.dto.UserLoginsDto;
import com.idlemonitor.frontend.user.dto.UserScreenCapturesDto;
import com.idlemonitor.frontend.user.dto.UserCompletedWorkHoursDto;
import com.idlemonitor.frontend.user.dto.UserDto;
/**
 * Created By BHagya
 * @author user
 * Interface class for Employee services
 */
public interface EmployeeService{
	 public ArrayList<UserDto> getEmployeeUsersByAdminUsername(String username,Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws  UserNotFoundException, EmployeeNotFoundException;
	 public ArrayList<UserLoginsDto> getEmployeeUserLoginsByEmployeeUserId(Integer employeeUserId,Integer period,String selectedDate) throws EmployeeNotFoundException,ParseException, ParseException;
	 public UserDto getEmployeeUserDetailsByEmployeeUserId(Integer employeeUserId) throws EmployeeNotFoundException;
	 public ArrayList<UserScreenCapturesDto> getScreenshotsOfUser(String loggedInAdminUsername,Integer userId,Integer pageNo, Integer pageSize,Integer period,String selectedDate) throws UserScreenCapturesNotFoundException, UserNotFoundException,ParseException;
	 public ArrayList<UserDto> getListOfUsers() throws UserNotFoundException;
	 public Integer deleteEmployeeUserScreenCaptureImages(ArrayList<Integer> dataIds) throws Exception;
	 public Integer deleteMultipleScreenshotsByCronJob();
	 public ArrayList<UserDto> getUsersWithWorkedHoursByCurrentDate(Company company) throws EmployeeNotFoundException, UserNotFoundException;
	 public void sendDailyStatusReportEmailToAdmin();
	 public Integer changeEmployeeUserAccountStatus(Integer employeeUserId,Boolean isActive) throws EmployeeNotFoundException, UserNotSavedOrUpdatedException;
	 public Integer deleteEmployeeUser(Integer employeeUserId);
	 public void saveUsersCompletedWorkedHours();
	 public ArrayList<UserCompletedWorkHoursDto> getEmployeesWorkHoursByAdminUsername(String username,Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending,Integer period,String selectedDate) throws  UserNotFoundException, EmployeeNotFoundException, ParseException;
}