package com.idlemonitor.frontend.employee.dto;

/**
 * Craeted By Bhagya
 * @author user
 * Dto Class for employee Idle Time
 */

public class EmployeeIdleTimeDto{
	
	private String idleDate;
	private Integer idleHours;
	private Integer idleMinutes;
	private Integer idleSeconds;
	
	
	
	public String getIdleDate() {
		return idleDate;
	}
	public void setIdleDate(String idleDate) {
		this.idleDate = idleDate;
	}
	public Integer getIdleHours() {
		return idleHours;
	}
	public void setIdleHours(Integer idleHours) {
		this.idleHours = idleHours;
	}
	public Integer getIdleMinutes() {
		return idleMinutes;
	}
	public void setIdleMinutes(Integer idleMinutes) {
		this.idleMinutes = idleMinutes;
	}
	public Integer getIdleSeconds() {
		return idleSeconds;
	}
	public void setIdleSeconds(Integer idleSeconds) {
		this.idleSeconds = idleSeconds;
	}
	
	
	
}