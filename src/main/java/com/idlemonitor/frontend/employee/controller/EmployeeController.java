package com.idlemonitor.frontend.employee.controller;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.idlemonitor.common.exceptions.EmployeeNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeNumberNotFoundException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserScreenCapturesNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
import com.idlemonitor.frontend.user.dto.UserLoginsDto;
import com.idlemonitor.frontend.user.dto.UserScreenCapturesDto;
import com.idlemonitor.frontend.user.dto.WeeklyWorkHoursDto;
import com.idlemonitor.frontend.user.service.UserService;
import com.idlemonitor.frontend.user.dto.UserCompletedWorkHoursDto;
import com.idlemonitor.frontend.user.dto.UserDto;
import com.idlemonitor.frontend.employee.service.EmployeeService;
import com.idlemonitor.frontend.utility.dto.DisplayListBeanDto;
/**
 * Created By BHagya on june 03r, 2020
 * @author user
 * Controller class for employee
 */
@Controller("employeeController")
@RequestMapping("/employee")
public class EmployeeController{
	
private static Logger log=Logger.getLogger(EmployeeController.class);
	
	@Resource(name="employeeService")
	private EmployeeService employeeService;
	
	@Resource(name="userService")
	private UserService userService;
	
	/**
	 * Created By Bhagya on june 03rd, 2020
	 * @param map
	 * @return
	 * 
	 * Method for to get the list of employees based on login company
	 */
	@RequestMapping(value="/viewemployees.do")
	public String viewListOfEmployees(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto){
		log.info("EmployeeController -> viewListOfEmployees()");
		Integer totalResults;
		try{
			
			Authentication auth=SecurityContextHolder.getContext().getAuthentication();
			String username=auth.getName();
			if(null==listBeanDto.getSortBy()){
				listBeanDto.setSortBy("userId");
			}
			
			ArrayList<UserDto> employeeUsersDtos=this.employeeService.getEmployeeUsersByAdminUsername(username,listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
					listBeanDto.getSearchBy(),listBeanDto.getSortBy(),listBeanDto.getSortDirection());
			totalResults=employeeUsersDtos.get(0).getTotalUsers();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("employeeDtos", employeeUsersDtos);
			map.put("employeesSize",employeeUsersDtos.get(0).getTotalUsers());
			return "employee/viewEmployees";
		}
		catch(UserNotFoundException e){
			String error="User Not Found";
			map.put("message",error);
			return "error";
		}
		catch(EmployeeNotFoundException e){
			String error="Employee Not Found";
			map.put("message",error);
			return "employee/viewEmployees";
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while getting employees  "+e.getMessage());
			String error="Error while getting employees";
			map.put("message",error);
			return "error";
		}
		
	}
	/**
	 * Created by Bhagya on June 09th, 2020
	 * @param employeeUserId
	 * @return
	 * 
	 * Controller Service for to get the employee user idle timings by employee UserId
	 */
	@RequestMapping(value="/employeeidletimings.do")
	public String  getEmployeeUserIdleTimingsByUserId(Map<String,Object> map,@RequestParam("employeeUserId")Integer employeeUserId,
			@RequestParam(value="period",required=false) Integer period,@RequestParam(value="selectedDate",required=false) String selectedDate){
		log.info("EmployeeController -> getEmployeeUserIdleTimingsByUserId");
		String title="Yesterday";
		UserDto employeeUserDto=null;
		try{
			
			employeeUserDto =this.employeeService.getEmployeeUserDetailsByEmployeeUserId(employeeUserId);
			ArrayList<UserLoginsDto> employeeUserDetailsDtos=this.employeeService.getEmployeeUserLoginsByEmployeeUserId(employeeUserId,period,selectedDate);
			map.put("employeeUserDetailsDtos",employeeUserDetailsDtos);
			map.put("employeeUserId", employeeUserId);
			map.put("employeeName", employeeUserDto.getEmployeeName());
			
			if(null!=period){
				if(period==6){
					title="7 Days";
				}
				if(period==13){
					title="14 Days";
				}
				if(period==31){
					title="One Month";
				}
			}
			
			if(null!=selectedDate){
				map.put("selectedDate", selectedDate);
				SimpleDateFormat monthDateFormat = new SimpleDateFormat("MM-dd-yyyy");
		    	Date today=new Date();
				title="From "+selectedDate+" To "+monthDateFormat.format(today);
			}
			map.put("title", title);
			map.put("period", period);
			return "employee/employeeIdleTimeDetails";
		}
		catch(EmployeeNotFoundException e){
			e.printStackTrace();
			map.put("title", title);
			String error=" User Login Details Not Found Based On Date Criteria";
			map.put("message",error);
			map.put("period", period);
			map.put("employeeUserId", employeeUserId);
			map.put("employeeName", employeeUserDto.getEmployeeName());
			return "employee/employeeIdleTimeDetails";
		}
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while getting employees idle timing Details "+e.getMessage());
			String error="Error while getting employees idle timing Details";
			map.put("message",error);
			map.put("period", period);
			return "error";
		}
		
		
	}
	/**
	 * Created By Bhagya
	 * @param map
	 * @param listBeanDto
	 * @param userId
	 * @param period
	 * @param selectedDate
	 * @return
	 * 
	 * Service for to view screenshots of user
	 */
	
	@RequestMapping(value="/viewscreenshots.do")
	public String viewScreenshotsOfEmployees(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto,@RequestParam("userId")Integer userId,
			@RequestParam(value="period",required=false) Integer period,@RequestParam(value="selectedDate",required=false) String selectedDate){
		log.info("EmployeeController -> viewScreenshotsOfEmployees()");
		Integer totalResults;
		String title="Today";
		//System.out.println(" userId "+userId +" selctedDate "+selectedDate);
		String selectedUser="";
		ArrayList<UserDto> userDtos=new ArrayList<UserDto>();
		Authentication auth=SecurityContextHolder.getContext().getAuthentication();
		String loggedInAdminUsername=auth.getName();
		try{
			if(null==period){
	    		period=0; //Today
	    	}
			
			
			if(null!=period){
				if(period==6){
					title="7 Days";
				}
				if(period==13){
					title="14 Days";
				}
				if(period==31){
					title="One Month";
				}
			}
			if(null!=selectedDate && selectedDate.length()>0){
				map.put("selectedDate", selectedDate);
				SimpleDateFormat monthDateFormat = new SimpleDateFormat("MM-dd-yyyy");
		    	Date today=new Date();
				title="From "+selectedDate+" To "+monthDateFormat.format(today);
			}
			map.put("title", title);
			map.put("period", period);
			map.put("userId", userId);
			
			if(null!=userId && userId>0){
				UserDto userDto=this.employeeService.getEmployeeUserDetailsByEmployeeUserId(userId);
				selectedUser=userDto.getEmployeeName();
			}
			map.put("selectedUser", selectedUser);
			userDtos=this.employeeService.getListOfUsers();
			map.put("userDtos", userDtos);
			ArrayList<UserScreenCapturesDto> screenCapturesDtos=this.employeeService.getScreenshotsOfUser(loggedInAdminUsername,userId, listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),period,selectedDate);
			totalResults=screenCapturesDtos.get(0).getTotalCaptures();
			listBeanDto.getPagerDto().setTotalItems(totalResults);
			map.put("screenCaptureDtos", screenCapturesDtos);
			map.put("screenCapturesSize",screenCapturesDtos.get(0).getTotalCaptures());
			return "employee/employeeScreenshots";
		}
		catch(UserNotFoundException e){
			map.put("title", title);
			map.put("period", period);
			map.put("selectedUser", selectedUser);
			map.put("userId", userId);
			map.put("userDtos", userDtos);
			String error="Employees Not Found";
			map.put("message",error);
			if(null!=selectedDate){
				map.put("selectedDate", selectedDate);
			}
			return "employee/employeeScreenshots";
		}
		catch(UserScreenCapturesNotFoundException e){
			map.put("title", title);
			map.put("period", period);
			map.put("selectedUser", selectedUser);
			map.put("userId", userId);
			map.put("userDtos", userDtos);
			if(null!=selectedDate){
				map.put("selectedDate", selectedDate);
			}
			String error="Employee Screen Capture Images Not Found";
			map.put("message",error);
			return "employee/employeeScreenshots";
		}
		
		catch(Exception e){
			e.printStackTrace();
			log.error("Error while getting employees Screenshots "+e.getMessage());
			String error="Error while getting employees screenshots";
			map.put("message",error);
			return "error";
		}
		
	}
	
	  /**
	   * Created By Bhagya on october 19th, 2020
	   * @param userId
	   * @param isActive
	   * @return
	   * 
	   * Service for to change the user account status
	   */
	  
	  @ResponseBody
		@RequestMapping("/changeuserstatus.do")
		public String changeUserStatus(@RequestParam("userId")Integer userId,@RequestParam("isActive") Boolean isActive){
			log.info(" EmployeeController -> changeUserStatus()");
			try{
				Integer savedResult=this.employeeService.changeEmployeeUserAccountStatus(userId, isActive);
				return "SUCCESS";
			}
			catch(EmployeeNotFoundException e){
				log.error(" User Not Found For Changing the user Status");
				return "ERROR";
			}
			catch(UserNotSavedOrUpdatedException e){
				log.error("User Status Is Not Saved Or Updated");
				return "ERROR";
			}
			catch(Exception e){
				e.printStackTrace();
				return "ERROR";
			}
			
		}
	  /**
	   * Created By Bhagya on october 20th, 2020
	   * @param userId
	   * @return
	   * 
	   * Service for to delete the employee user
	   */
	  @ResponseBody
		@RequestMapping("/deleteemployeeuser.do")
		public String deleteEmployeeUser(@RequestParam("userId")Integer userId){
			log.info(" EmployeeController -> deleteEmployeeUser()");
			try{
				Integer deletedResult=this.employeeService.deleteEmployeeUser(userId);
				if(deletedResult>0) {
					return "SUCCESS";	
				}
				else {
					return "ERROR";
				}
			}
			catch(Exception e){
				e.printStackTrace();
				return "ERROR";
			}
			
		}
	  
	  /**
		 * Created By Bhagya on october 22nd, 2020
		 * @param map
		 * @return
		 * 
		 * Method for to get the employee work hours
		 */
		@RequestMapping(value="/viewworkhours.do")
		public String viewEmployeesWorkHours(Map<String,Object> map,@ModelAttribute("displayListBean") DisplayListBeanDto listBeanDto,
				@RequestParam(value="period",required=false) Integer period,@RequestParam(value="selectedDate",required=false) String selectedDate){
			log.info("EmployeeController -> viewEmployeesWorkHours()");
			String title="Yesterday";
			Integer totalResults;
			
			try{
				
				Authentication auth=SecurityContextHolder.getContext().getAuthentication();
				String username=auth.getName();
				if(null==listBeanDto.getSortBy()){
					listBeanDto.setSortBy("workDate");
				}
				ArrayList<UserCompletedWorkHoursDto> userCompletedWorkHoursDtos=this.employeeService.getEmployeesWorkHoursByAdminUsername(username,listBeanDto.getPagerDto().getPageNo(), listBeanDto.getPagerDto().getRange(),
						listBeanDto.getSearchBy(),listBeanDto.getSortBy(),listBeanDto.getSortDirection(),listBeanDto.getPeriod(),listBeanDto.getSelectedDate());
				totalResults=userCompletedWorkHoursDtos.get(0).getTotalUsers();
				listBeanDto.getPagerDto().setTotalItems(totalResults);
				
				if(null!=period){
					if(period==6){
						title="7 Days";
					}
					if(period==13){
						title="14 Days";
					}
					if(period==31){
						title="One Month";
					}
				}
				else if(null==selectedDate|| selectedDate.length()<0){
					period=0;
					title="Yesterday";
				}
				
				if(null!=selectedDate){
					map.put("selectedDate", selectedDate);
					SimpleDateFormat monthDateFormat = new SimpleDateFormat("MM-dd-yyyy");
			    	Date today=new Date();
					title="From "+selectedDate+" To "+monthDateFormat.format(today);
				}
				
				map.put("title", title);
				map.put("period", period);
				
				map.put("userCompletedWorkHoursDtos", userCompletedWorkHoursDtos);
				map.put("userCompletedWorkHoursDtosSize",userCompletedWorkHoursDtos.get(0).getTotalUsers());
				return "employee/viewWorkHours";
			}
			catch(UserNotFoundException e){
				String error="User Not Found";
				map.put("message",error);
				return "error";
			}
			catch(EmployeeNotFoundException e){
				if(null==selectedDate|| selectedDate.length()<0){
					period=0;
					title="Yesterday";
				}
				
				map.put("title", title);
				String error=" User Work Hours Not Found Based On Date Criteria";
				map.put("message",error);
				map.put("period", period);
				return "employee/viewWorkHours";
			}
			
			catch(Exception e){
				e.printStackTrace();
				log.error("Error while getting user work hours  "+e.getMessage());
				String error="Error while getting user work hours";
				map.put("message",error);
				return "error";
			}
			
		}
		/**
		 * Created By Bhagya On October 28th, 2020
		 * @param dataIds
		 * @param userId
		 * @param period
		 * @param selectedDate
		 * @return
		 * 
		 * Service for to delete screenshots 
		 */
		  @ResponseBody
		  @RequestMapping(value="/deletescreenshots.do") 
		  public String deleteScreenshots(@RequestParam("dataIds") String dataIdsString,@RequestParam("userId")Integer userId,
		  @RequestParam("period") Integer period,@RequestParam("selectedDate") String selectedDate){
		  log.info(" EmployeeController -> deleteScreenshots()");
		  try{
		    ArrayList<Integer> myList=new ArrayList<Integer>(); 
			  StringTokenizer tokenizer= new StringTokenizer(dataIdsString, ","); 
			  int n = tokenizer.countTokens();
			  for (int i = 0; i < n; i++) { 
				  String token = tokenizer.nextToken();
				  myList.add( Integer.parseInt(token)); 
			  }
			
			  Integer deletedResult=this.employeeService.deleteEmployeeUserScreenCaptureImages(myList);
			 
			  if(deletedResult>0) {
					return "SUCCESS";	
				}
				else {
					return "ERROR";
				}
			
		  } 
		  catch(Exception e){
			  log.info(" EmployeeController -> deleteScreenshots() -> Exception -> "+e.getMessage());
			  e.printStackTrace();
				return "ERROR";
		  }
		  }
		  

			/**
		 	 * Created By Harshitha on 11th May 2021
		 	 * Service for to get the weekly worked hours by user
		 	 */
			@RequestMapping(value="viewweeklyworkedhours.do",method=RequestMethod.POST)
			public String weeklyWorkedHours(@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,Map<String,Object> map) {
				log.info("EmployeeController -> weeklyWorkedHours() ");
				try{
					System.out.println("from date in controller "+fromDate +"todate in controller "+toDate);
					ArrayList<WeeklyWorkHoursDto> weeklyWorkHoursDtos = this.userService.getWeeklyWorkHours(fromDate, toDate);
					
					WeeklyWorkHoursDto weeklyWorkHoursTitleDto=this.weeklyWorkHoursTitles(fromDate, toDate);
					//map.put("titleDto",weeklyWorkHoursTitleDto)​;
					map.put("titleDto", weeklyWorkHoursTitleDto);
					map.put("weeklyWorkHoursDtos", weeklyWorkHoursDtos);
					if(weeklyWorkHoursDtos.size()==0) {
					
						throw new UserWorkHoursNotFoundException();		
						
					}
					
					for(WeeklyWorkHoursDto weekWorkHoursDto:weeklyWorkHoursDtos ) {
					System.out.println("day1Hours" + weekWorkHoursDto.getDay1Hours());
					System.out.println("day2Hours" + weekWorkHoursDto.getDay2Hours());
					System.out.println("day3Hours" + weekWorkHoursDto.getDay3Hours());
					System.out.println("day4Hours" + weekWorkHoursDto.getDay4Hours());
					System.out.println("day5Hours" + weekWorkHoursDto.getDay5Hours());
					System.out.println("day6Hours" + weekWorkHoursDto.getDay6Hours());
					System.out.println("day7Hours" + weekWorkHoursDto.getDay7Hours());
					}
					return "employee/viewWeeklyWorkHours";
				}
				
				  catch(Exception e){
					e.printStackTrace();
					log.error("Error while getting weekly worked hours  "+e.getMessage());
					String error="Error while getting weekly worked hours";
					map.put("message",error);
					return "error";
				}
				
			}
			
			private Exception UserWorkHoursNotFoundException() {
				// TODO Auto-generated method stub
				return null;
			}
			/**
		 	 * Created By Harshitha on 6th June 2021
		 	 * Service for to get the weekly worked hours report
		 	 */
			
			  @RequestMapping(value="weeklyworkedhoursreport.do",method=RequestMethod.GET)
			  public String weeklyWorkedHoursReport(Map<String,Object> map) throws UserWorkHoursNotFoundException {
			  log.info("EmployeeController -> weeklyWorkedHoursReport() "); 
			  try{
				 System.out.println(" Inside controller");
			  return "employee/weeklyWorkedHoursReport";
			  }
			  
			  
			  
			  catch(Exception e)
			  {
			  e.printStackTrace();
			  log.error("Error while getting weekly worked hours  "+e.getMessage()); 
			  String error="Error while getting weekly worked hours"; 
			  map.put("message",error);
			  return "error";
			  }
			  
			  
			  }
			 	
			  public WeeklyWorkHoursDto weeklyWorkHoursTitles(String fromDate,String toDate) {
				  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy"); 
					
					//convert String to LocalDate 
					LocalDate localFromDate = LocalDate.parse(fromDate, formatter);
					LocalDate localToDate = LocalDate.parse(toDate, formatter);
					Integer count=1;
					WeeklyWorkHoursDto weeklyWorkHoursTitleDto=new WeeklyWorkHoursDto();
					for(LocalDate date = localFromDate ; date.isBefore(localToDate); date = date.plusDays(1)) {
						if(count==1) {
							
							weeklyWorkHoursTitleDto.setDay1Title(date.toString());
						}
						if(count==2) {
							System.out.println("inside controller if block - 2nd tittle");
							weeklyWorkHoursTitleDto.setDay2Title(date.toString());
						}
						if(count==3) {
							weeklyWorkHoursTitleDto.setDay3Title(date.toString());
						}
						if(count==4) {
							weeklyWorkHoursTitleDto.setDay4Title(date.toString());
						}
						if(count==5) {
							weeklyWorkHoursTitleDto.setDay5Title(date.toString());
						}
						if(count==6) {
							weeklyWorkHoursTitleDto.setDay6Title(date.toString());
						}
						if(count==7) {
							weeklyWorkHoursTitleDto.setDay7Title(date.toString());
						}
						count=count+1;
					}
					
			  
					return weeklyWorkHoursTitleDto;
			  }
			  
}