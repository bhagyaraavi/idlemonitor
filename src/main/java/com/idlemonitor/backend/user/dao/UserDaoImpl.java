package com.idlemonitor.backend.user.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserScreenCapturesNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotSavedOrUpdatedException;
import com.idlemonitor.frontend.user.dto.UserDto;
import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.backend.user.model.IdleTimeConfiguration;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserCompletedWorkHours;
import com.idlemonitor.backend.user.model.UserLogins;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
import com.idlemonitor.backend.user.model.UserWorkHours;
import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.CompanyNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.EmployeeEmailIdNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeNumberNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserCompletedWorkHoursNotFoundException;
import com.idlemonitor.common.exceptions.UserCompletedWorkHoursNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserLoginsNotFoundException;
import com.idlemonitor.common.exceptions.UserLoginsNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;

/**
 * Created By Bhagya on may 27th 2020
 * @author user
 * Dao Implementation class for user
 */
@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao{
	
	private static Logger log=Logger.getLogger(UserDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * created By bhagya on may 27th, 2020
	 * @param adminUser
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Service for to save or update admin users
	 */
	
	public Integer saveOrUpdateAdminUser(AdminUser adminUser) throws UserNotSavedOrUpdatedException{
		log.info("UserDaoImpl-> saveOrUpdateAdminUser");
		sessionFactory.getCurrentSession().saveOrUpdate(adminUser);
		sessionFactory.getCurrentSession().flush();
		return adminUser.getAdminUserId();
	}
	
	/**
	 * Created By Bhagya on May27th,2020
	 * 
	 * Service for to get user based on username or email
	 */
	@SuppressWarnings("unchecked")
	public AdminUser getUserforAuthentication(String username)throws UserNotFoundException{
		log.info("UserDaoImpl-> getUserforAuthentication() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(AdminUser.class);
		criteria.add(Restrictions.or(Restrictions.eqOrIsNull("contactEmail", username),Restrictions.eq("username", username)));
		ArrayList<AdminUser> users = (ArrayList<AdminUser>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya on May 28th, 20202
	 * @param adminUserId
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to get the admin user by admin userId
	 */
	@SuppressWarnings("unchecked")
	public AdminUser getAdminUserByAdminUserId(Integer adminUserId) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getAdminUserByAdminUserId ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(AdminUser.class);
		criteria.add(Restrictions.eq("adminUserId", adminUserId));
		ArrayList<AdminUser> adminUsers = (ArrayList<AdminUser>) criteria.list();
		if(!adminUsers.isEmpty()){
			return adminUsers.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
		
	}
	
	/**Created By Bhagya On may 28th, 2020
	 * Returns the user's  one who has given Password reset token
	 * password reset token sent to the user
	 * 
	 * @param token
	 * @throws UserNotFoundException 
	 * @throws Exception
	 *             if there is no user with given token OR any kind of exception
	 *             while interacting with db
	 */
	@SuppressWarnings("unchecked")
	public AdminUser getAdminUserByPasswordResetToken(String token) throws UserNotFoundException {
		log.info("UserDaoImpl-> getAdminUserByPasswordResetToken()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(AdminUser.class);
		criteria.add(Restrictions.eq("passwordToken", token));
		ArrayList<AdminUser> users=(ArrayList<AdminUser>) criteria.list();
		if (!users.isEmpty()) {
			return users.get(0);
		} else {
			throw new UserNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya on june 02nd, 2020
	 * Method to Retreive User based on accountToken..
	 */
     @SuppressWarnings("unchecked")
	public AdminUser getAdminUserByAccountToken(String token)throws UserNotFoundException{
    	 log.info("UserDaoImpl-> getAdminUserByAccountToken()");
    	 ArrayList<AdminUser> adminUser=(ArrayList<AdminUser>) sessionFactory.getCurrentSession().createCriteria(AdminUser.class)
    			 .add(Restrictions.eq("accountActivationToken",token))
    			 .list();
    	 
    	 if(!adminUser.isEmpty()){
    		 return adminUser.get(0);
    	 }
    	 else{
    		 throw new UserNotFoundException();
    	 }    	 
     }
     
     /**
 	 * created By bhagya on june 04th, 2020
 	 * @param adminUser
 	 * @return
 	 * @throws CompanyNotSavedOrUpdatedException
 	 * 
 	 * Service for to save or update company details
 	 */
 	
 	public Integer saveOrUpdateCompany(Company company) throws CompanyNotSavedOrUpdatedException{
 		log.info("UserDaoImpl-> saveOrUpdateCompany");
 		sessionFactory.getCurrentSession().saveOrUpdate(company);
 		sessionFactory.getCurrentSession().flush();
 		return company.getCompanyId();
 	}
 	
 	/**
	 * Created By Bhagya on June 05th,2020
	 * 
	 * Service for to get company based on Company Name
	 */
	@SuppressWarnings("unchecked")
	public Company getCompanybyCompanyName(String companyName)throws CompanyNotFoundException{
		log.info("UserDaoImpl-> getCompanybyCompanyName() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Company.class);
		criteria.add(Restrictions.ilike("companyName", companyName));
		ArrayList<Company> companies = (ArrayList<Company>) criteria.list();
		if(!companies.isEmpty()){
			return companies.get(0);
		}
		else{
			throw new CompanyNotFoundException();
		}
	}
	
	/**
	 * created By bhagya on may 27th, 2020
	 * @param adminUser
	 * @return
	 * @throws IdleTimeConfigurationNotSavedOrUpdatedException
	 * 
	 * Service for to save or update idletimeconfiguration
	 */
	
	public Integer saveOrUpdateIdleTimeConfiguration(IdleTimeConfiguration idleTimeConfiguration) throws IdleTimeConfigurationNotSavedOrUpdatedException{
		log.info("UserDaoImpl-> saveOrUpdateIdleTimeConfiguration");
		sessionFactory.getCurrentSession().saveOrUpdate(idleTimeConfiguration);
		sessionFactory.getCurrentSession().flush();
		return idleTimeConfiguration.getConfigId();
	}
	
	/**
	 * Created By Bhagya on June 22nd, 2020
	 * @param configId
	 * @return
	 * @throws IdleTimeConfigurationNotFoundException
	 * 
	 * Service for to get the idle time configuration by config id
	 */
	@SuppressWarnings("unchecked")
	public IdleTimeConfiguration getIdleTimeConfigurationByConfigId(Integer configId) throws IdleTimeConfigurationNotFoundException{
		log.info("Inside UserDaoImpl -> getIdleTimeConfigurationByConfigId ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(IdleTimeConfiguration.class);
		criteria.add(Restrictions.eq("configId", configId));
		ArrayList<IdleTimeConfiguration> idleTimeConfigurations = (ArrayList<IdleTimeConfiguration>) criteria.list();
		if(!idleTimeConfigurations.isEmpty()){
			return idleTimeConfigurations.get(0);
		}
		else{
			throw new IdleTimeConfigurationNotFoundException();
		}
		
	}
	/**
	 * created by bhagya on june 24th, 2020
	 * @param adminUser
	 * @return
	 * @throws IdleTimeConfigurationNotFoundException
	 * 
	 * service for to get the idle time configuration by
	 *  adminuser
	 */
	
	@SuppressWarnings("unchecked")
	public IdleTimeConfiguration getIdleTimeConfigurationByAdminUser(AdminUser adminUser) throws IdleTimeConfigurationNotFoundException{
		log.info("Inside UserDaoImpl -> getIdleTimeConfigurationByAdminUser ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(IdleTimeConfiguration.class);
		criteria.add(Restrictions.eq("adminUser", adminUser));
		ArrayList<IdleTimeConfiguration> idleTimeConfigurations = (ArrayList<IdleTimeConfiguration>) criteria.list();
		if(!idleTimeConfigurations.isEmpty()){
			return idleTimeConfigurations.get(0);
		}
		else{
			throw new IdleTimeConfigurationNotFoundException();
		}
		
	}
	/**
	 * created By Bhagya on june 29th, 2020
	 * @param user
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Service for to save or update user info
	 */
	public Integer saveOrUpdateUserInfo(User user) throws UserNotSavedOrUpdatedException{
		log.info("UserDaoImpl-> saveOrUpdateUserInfo");
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		sessionFactory.getCurrentSession().flush();
		return user.getUserId();
	}
	/**
	 * Created By bhagya on june29th. 2020
	 * @param adminUserId
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to get the user by userId
	 */
	@SuppressWarnings("unchecked")
	public User getUserByUserId(Integer userId) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getUserByUserId ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("userId", userId));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
		
	}
	/**
	 * Created By bhagya On June 29th, 2020
	 * Service for to get the client user by user name
	 */
	@SuppressWarnings("unchecked")
	public User getClientUserByUserName(String username) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getclientUserByUserName ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya on june 30th, 2020
	 * @param username
	 * @param encryptedPassword
	 * @return
	 * 
	 * Service for to get the client user for authentication
	 * @throws UserNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public User getClientUserForAuthentication(String username,String encryptedPassword) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getClientUserForAuthentication ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		criteria.add(Restrictions.eq("password", encryptedPassword));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
	}
	/**
	 * Created By Bhagya On July 01st,2020
	 * @param userLogins
	 * @return
	 * @throws UserNotSavedOrUpdatedException
	 * 
	 * Service for to save or update user Logins Info
	 */
	public Integer saveOrUpdateUserLoginsInfo(UserLogins userLogins) throws UserLoginsNotSavedOrUpdatedException{
		log.info("UserDaoImpl-> saveOrUpdateUserLoginsInfo");
		sessionFactory.getCurrentSession().saveOrUpdate(userLogins);
		sessionFactory.getCurrentSession().flush();
		return userLogins.getDataId();
	}
	/**
	 * Created By bhagya on july 01st, 2020
	 * @param userId
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to get client user by userId
	 */
	@SuppressWarnings("unchecked")
	public User getClientUserByUserId(Integer userId) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getClientUserByUserId ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("userId", userId));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya 
	 * Service for to get user logins by userid and dataId
	 */
	@SuppressWarnings("unchecked")
	public UserLogins getUserLoginsInfoByUserIdAndDataId(User user,Integer dataId) throws UserLoginsNotFoundException{
		log.info("Inside UserDaoImpl -> getUserLoginsInfoByUserIdAndDataId ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserLogins.class);
		criteria.add(Restrictions.eq("user", user));
		criteria.add(Restrictions.eq("dataId", dataId));
		criteria.add(Restrictions.isNull("idleDateTime"));
		ArrayList<UserLogins> userLogins = (ArrayList<UserLogins>) criteria.list();
		if(!userLogins.isEmpty()){
			return userLogins.get(0);
		}
		else{
			throw new UserLoginsNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya
	 * Service for to get userlogins and work date
	 */
	@SuppressWarnings("unchecked")
	public UserLogins getUserLoginsInfoByUserIdAndWorkDate(User user,Date workDate) throws UserLoginsNotFoundException{
		log.info("Inside UserDaoImpl -> getUserLoginsInfoByUseridAndWorkDate ");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserLogins.class);
		criteria.add(Restrictions.eq("user", user));
		criteria.add(Restrictions.eq("workDate", workDate));
		criteria.add(Restrictions.isNull("idleDateTime"));
		criteria.addOrder(Order.desc("loginDateTime"));
		criteria.setMaxResults(1);
		ArrayList<UserLogins> userLogins = (ArrayList<UserLogins>) criteria.list();
		if(!userLogins.isEmpty()){
			return userLogins.get(0);
		}
		else{
			throw new UserLoginsNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya On July 01st, 2020
	 * @param company
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * service for to get the adminuserBycompany
	 */
	
	@SuppressWarnings("unchecked")
	public AdminUser getAdminUserByCompany(Company company) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getAdminUserByCompany ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(AdminUser.class);
		criteria.add(Restrictions.eq("company", company));
		ArrayList<AdminUser> adminUsers = (ArrayList<AdminUser>) criteria.list();
		if(!adminUsers.isEmpty()){
			return adminUsers.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya On july 01st, 2020
	 * @param userScreenCaptures
	 * @return
	 * @throws UserScreenCapturesNotSavedOrUpdatedException
	 * 
	 * Service for to save or update user screen captures
	 */
	public Integer saveOrUpdateUserScreenCapturesInfo(UserScreenCaptures userScreenCaptures) throws UserScreenCapturesNotSavedOrUpdatedException{
		log.info("UserDaoImpl-> saveOrUpdateUserScreenCapturesInfo");
		sessionFactory.getCurrentSession().saveOrUpdate(userScreenCaptures);
		sessionFactory.getCurrentSession().flush();
		return userScreenCaptures.getDataId();
	}
	/**
	 * Created By Bhagya on july 01st,2020
	 * @param user
	 * @param workDate
	 * @return
	 * @throws UserWorkHoursNotFoundException
	 * 
	 * Service for to get user work hours by user and work date
	 */
	@SuppressWarnings("unchecked")
	public UserWorkHours getUserWorkHoursByUserAndworkDate(User user,Date workDate) throws UserWorkHoursNotFoundException{
		log.info("UserDaoImpl-> getUserWorkHoursByUserAndworkDate()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserWorkHours.class);
		criteria.add(Restrictions.eq("user", user));
		criteria.add(Restrictions.eq("workDate", workDate));
		ArrayList<UserWorkHours> userWorkHours = (ArrayList<UserWorkHours>) criteria.list();
		if(!userWorkHours.isEmpty()){
			return userWorkHours.get(0);
		}
		else{
			throw new UserWorkHoursNotFoundException();
		}
	}
	/**
	 * Created By Bhagya on july 01st, 2020
	 * @param userWorkHours
	 * @return
	 * @throws UserWorkHoursNotSavedOrUpdatedException
	 * 
	 * Service for to save or update user Work Hours
	 */
	public Integer saveOrUpdateUserWorkHours(UserWorkHours userWorkHours) throws UserWorkHoursNotSavedOrUpdatedException{
		log.info("UserDaoImpl-> saveOrUpdateUserWorkHours");
		sessionFactory.getCurrentSession().saveOrUpdate(userWorkHours);
		sessionFactory.getCurrentSession().flush();
		return userWorkHours.getDataId();
	}
	/**
	 * Created By Bhagya on july 03rd,2020
	 * @param user
	 * @param workDate
	 * @return
	 * @throws UserWorkHoursNotFoundException
	 * 
	 * Service for to get the list of user work hours by work date and user
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserWorkHours> getListOfUserWorkHoursByWorkDateAndUser(User user,Date workDate) throws UserWorkHoursNotFoundException{
		log.info("UserDaoImpl -> getListOfUserWorkHoursByWorkDateAndUser()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserWorkHours.class);
		criteria.add(Restrictions.eq("user", user));
		if(null!=workDate) {
			criteria.add(Restrictions.eq("workDate", workDate));
		}
		ArrayList<UserWorkHours> userWorkHours = (ArrayList<UserWorkHours>) criteria.list();
		if(!userWorkHours.isEmpty()){
			return userWorkHours;
		}
		else{
			throw new UserWorkHoursNotFoundException();
		}	
		
	}
	
	/**
	 * Created By Bhagya on July 24th,2020
	 * 
	 * Service for to get company based on Company Code
	 */
	@SuppressWarnings("unchecked")
	public Company getCompanybyCompanyCode(String companyCode)throws CompanyNotFoundException{
		log.info("UserDaoImpl-> getCompanybyCompanyCode() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Company.class);
		criteria.add(Restrictions.ilike("companyCode", companyCode));
		ArrayList<Company> companies = (ArrayList<Company>) criteria.list();
		if(!companies.isEmpty()){
			return companies.get(0);
		}
		else{
			throw new CompanyNotFoundException();
		}
	}
	/**
	 * Created By bHagya on july 25th, 2019
	 * @param username
	 * @param companyCode
	 * @return
	 * @throws UserNotFoundException
	 * 
	 * Service for to get the client user by username and company 
	 */
	@SuppressWarnings("unchecked")
	public User getClientUserByUserNameAndCompany(String username,Company company) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getClientUserByUserNameAndCompany ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		criteria.add(Restrictions.eq("company", company));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
		
	}
	
	/**
	 * Created By bhagya On July 25th, 2020
	 * Service for to get the client user by email
	 */
	@SuppressWarnings("unchecked")
	public User getClientUserByEmail(String email) throws UserNotFoundException{
		log.info("Inside UserDaoImpl -> getClientUserByEmail ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("emailId", email));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new UserNotFoundException();
		}
		
	}
	
	
	/**
	 * Created By Bhagya on july 28th, 2020
	 * Method to Retreive cliet User based on accountToken..
	 */
     @SuppressWarnings("unchecked")
	public User getClientUserByAccountToken(String token)throws UserNotFoundException{
    	 log.info("UserDaoImpl-> getClientUserByAccountToken()");
    	 ArrayList<User> user=(ArrayList<User>) sessionFactory.getCurrentSession().createCriteria(User.class)
    			 .add(Restrictions.eq("accountActivationToken",token))
    			 .list();
    	 
    	 if(!user.isEmpty()){
    		 return user.get(0);
    	 }
    	 else{
    		 throw new UserNotFoundException();
    	 }    	 
     }
     /**
      * Created By Bhagya on August 13th, 2020
      * @param user
      * @param workDate
      * @return
      * @throws UserWorkHoursNotFoundException
      * Service for to get user work hours by work date and user
      */
     @SuppressWarnings("unchecked")
 	public UserWorkHours getUserWorkHoursByWorkDateAndUser(User user,Date workDate) throws UserWorkHoursNotFoundException{
 		log.info("UserDaoImpl -> getUserWorkHoursByWorkDateAndUser()");
 		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserWorkHours.class);
 		criteria.add(Restrictions.eq("user", user));
 		criteria.add(Restrictions.eq("workDate", workDate));
 		ArrayList<UserWorkHours> userWorkHours = (ArrayList<UserWorkHours>) criteria.list();
 		if(!userWorkHours.isEmpty()){
 			return userWorkHours.get(0);
 		}
 		else{
 			throw new UserWorkHoursNotFoundException();
 		}	
 		
 	}
     
     @SuppressWarnings("unchecked")
 	public UserWorkHours getUserWorkHoursByUserAndWorkDateAndIsAppClosed(User user,Date workDate) throws UserWorkHoursNotFoundException{
 		log.info("UserDaoImpl-> getUserWorkHoursByUserAndworkDate()");
 		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserWorkHours.class);
 		criteria.add(Restrictions.eq("user", user));
 		criteria.add(Restrictions.eq("workDate", workDate));
 		criteria.add(Restrictions.eq("isAppClosed",false));
 		ArrayList<UserWorkHours> userWorkHours = (ArrayList<UserWorkHours>) criteria.list();
 		if(!userWorkHours.isEmpty()){
 			return userWorkHours.get(0);
 		}
 		else{
 			throw new UserWorkHoursNotFoundException();
 		}
 	}
     /**
      * Created By Bhagya On october 20, 2020
      * @param user
      * @return
      * @throws UserLoginsNotFoundException
      * 
      * Service for to get the lsit of user logins info by employee User
      */
     @SuppressWarnings("unchecked")
 	public ArrayList<UserLogins> getListOfUserLoginsInfoByUser(User user) throws UserLoginsNotFoundException{
 		log.info("Inside UserDaoImpl -> getListOfUserLoginsInfoByUser() ");
 		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserLogins.class);
 		criteria.add(Restrictions.eq("user", user));
 		ArrayList<UserLogins> userLogins = (ArrayList<UserLogins>) criteria.list();
 		if(!userLogins.isEmpty()){
 			return userLogins;
 		}
 		else{
 			throw new UserLoginsNotFoundException();
 		}
 		
 	}
     
     @SuppressWarnings("unchecked")
 	public ArrayList<UserWorkHours> getListOfUserWorkHoursBasedOnDateCriteria(User user,Date fromDate,Date toDate) throws UserWorkHoursNotFoundException{
 		log.info("UserDaoImpl -> getListOfUserWorkHoursBasedOnDateCriteria()");
 		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserWorkHours.class);
 		criteria.add(Restrictions.eq("user", user));
 		if(null!=fromDate && null!=toDate) {
 			criteria.add(Restrictions.ge("workDate", fromDate));
 			criteria.add(Restrictions.le("workDate",toDate));
 		}
 		ArrayList<UserWorkHours> userWorkHours = (ArrayList<UserWorkHours>) criteria.list();
 		if(!userWorkHours.isEmpty()){
 			return userWorkHours;
 		}
 		else{
 			throw new UserWorkHoursNotFoundException();
 		}	
 		
 	}
     /**
      * Created By Bhagya on october 22nd, 2020
      * @param userCompletedWorkHours
      * @return
      * @throws UserCompletedWorkHoursNotSavedOrUpdatedException
      * 
      * Service for to save or update user completed work hours
      */
     public Integer saveOrUpdateUserCompletedWorkHours(UserCompletedWorkHours userCompletedWorkHours) throws UserCompletedWorkHoursNotSavedOrUpdatedException{
 		log.info("UserDaoImpl-> saveOrUpdateUserCompletedWorkHours()");
 		sessionFactory.getCurrentSession().saveOrUpdate(userCompletedWorkHours);
 		sessionFactory.getCurrentSession().flush();
 		return userCompletedWorkHours.getDataId();
 	}
     /**
      * Created By Bhagya On October 30, 2020
      * @param user
      * @return
      * @throws UserCompletedWorkHoursNotFoundException
      * 
      * Service for to get the list of user completed work hours based on user
      */
     @SuppressWarnings("unchecked")
 	public ArrayList<UserCompletedWorkHours> getListOfUserCompletedWorkHoursByUser(User user) throws UserCompletedWorkHoursNotFoundException{
 		log.info("UserDaoImpl -> getListOfUserCompletedWorkHoursByWorkDateAndUser()");
 		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserCompletedWorkHours.class);
 		criteria.add(Restrictions.eq("user", user));
 		ArrayList<UserCompletedWorkHours> userCompletedWorkHours = (ArrayList<UserCompletedWorkHours>) criteria.list();
 		if(!userCompletedWorkHours.isEmpty()){
 			return userCompletedWorkHours;
 		}
 		else{
 			throw new UserCompletedWorkHoursNotFoundException();
 		}	
 		
 	}
     /**
	  * Created By Harshitha on Febraury 22nd, 2021
	  * Service for to get the user's Updated Account Date based on user
	  */
     
     @SuppressWarnings("unchecked")
 	public User getAccountUpdatedDate(Integer userId) throws UserNotFoundException{
 		log.info("Inside UserDaoImpl -> getAccountUpdatedDate ");
 		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
 		criteria.add(Restrictions.eq("userId", userId));
 		ArrayList<User> users = (ArrayList<User>) criteria.list();
 		if(!users.isEmpty()){
 			return users.get(0);
 		}
 		else{
 			throw new UserNotFoundException();
 		}
 		
 	}
    
   /***
	 * Created By Harshitha on March 3rd,2021
	 * 
	 * Service to get the employee number for validation
	 */
	@SuppressWarnings("unchecked")
	public User getEmployeeNumber(String employeeNumber)throws EmployeeNumberNotFoundException{
		log.info("UserDaoImpl-> getEmployeeNumber() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("employeeNumber", employeeNumber));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new EmployeeNumberNotFoundException();
		}
	}
	
	 /***
		 * Created By Harshitha on March 4th,2021
		 * Service to get the employee email id for validation
		 */
		@SuppressWarnings("unchecked")
		public User getEmployeeEmailId(String emailId)throws EmployeeEmailIdNotFoundException{
			log.info("UserDaoImpl-> getEmployeeEmailId() ");
			Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
			criteria.add(Restrictions.eq("emailId", emailId));
			ArrayList<User> users = (ArrayList<User>) criteria.list();
			if(!users.isEmpty()){
				return users.get(0);
			}
			else{
				throw new EmployeeEmailIdNotFoundException();
			}
		}
		
		/**
		 * Created By Harshitha on may 12th,2021
		 * Service for to get the list of user weekly work hours by from date, to date and user
		 */
		@SuppressWarnings("unchecked")
		public ArrayList<UserCompletedWorkHours> getListOfUserWorkHoursByFromDateToDateAndUser(User user,Date fromDate, Date toDate) throws UserWorkHoursNotFoundException{
			log.info("UserDaoImpl -> getListOfUserWorkHoursByWeekWorkedDateAndUser()");
			Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserCompletedWorkHours.class);
			criteria.add(Restrictions.eq("user", user));
			System.out.println(" Dao from date"+fromDate +"toDate" +toDate);
			if(null!=fromDate||null!=toDate) {
					criteria.add(Restrictions.ge("workDate", fromDate));
					criteria.add(Restrictions.le("workDate", toDate));
				
			}
			ArrayList<UserCompletedWorkHours> userCompletedWorkHours = (ArrayList<UserCompletedWorkHours>) criteria.list();
			if(!userCompletedWorkHours.isEmpty()){
				System.out.println(" dao work hours size "+userCompletedWorkHours.size());
				return userCompletedWorkHours;
			}
			else{
				throw new UserWorkHoursNotFoundException();
			}	
			
		}
	
		
}