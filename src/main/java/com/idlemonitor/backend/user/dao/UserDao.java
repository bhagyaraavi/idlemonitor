package com.idlemonitor.backend.user.dao;

import java.util.Date;
import java.util.ArrayList;

import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.backend.user.model.IdleTimeConfiguration;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserCompletedWorkHours;
import com.idlemonitor.backend.user.model.UserLogins;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
import com.idlemonitor.backend.user.model.UserWorkHours;
import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.CompanyNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.EmployeeEmailIdNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotFoundException;
import com.idlemonitor.common.exceptions.IdleTimeConfigurationNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserCompletedWorkHoursNotFoundException;
import com.idlemonitor.common.exceptions.UserCompletedWorkHoursNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserLoginsNotFoundException;
import com.idlemonitor.common.exceptions.UserLoginsNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserScreenCapturesNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotSavedOrUpdatedException;
import com.idlemonitor.frontend.user.dto.UserDto;
import com.idlemonitor.common.exceptions.EmployeeNumberNotFoundException;
/**
 * Created By BHagya on may 27th, 2020
 * @author user
 * Dao Interface for User Services
 *
 */
public interface UserDao{
	public Integer saveOrUpdateAdminUser(AdminUser adminUser) throws UserNotSavedOrUpdatedException;
	public AdminUser getUserforAuthentication(String username)throws UserNotFoundException;
	public AdminUser getAdminUserByAdminUserId(Integer adminUserId) throws UserNotFoundException;
	public AdminUser getAdminUserByPasswordResetToken(String token) throws UserNotFoundException;
	public AdminUser getAdminUserByAccountToken(String token)throws UserNotFoundException;
	public Integer saveOrUpdateCompany(Company company) throws CompanyNotSavedOrUpdatedException;
	public Company getCompanybyCompanyName(String companyName)throws CompanyNotFoundException;
	public Integer saveOrUpdateIdleTimeConfiguration(IdleTimeConfiguration idleTimeConfiguration) throws IdleTimeConfigurationNotSavedOrUpdatedException;
	public IdleTimeConfiguration getIdleTimeConfigurationByConfigId(Integer configId) throws IdleTimeConfigurationNotFoundException;
	public IdleTimeConfiguration getIdleTimeConfigurationByAdminUser(AdminUser adminUser) throws IdleTimeConfigurationNotFoundException;
	public Integer saveOrUpdateUserInfo(User user) throws UserNotSavedOrUpdatedException;
	public User getUserByUserId(Integer userId) throws UserNotFoundException;
	public User getClientUserByUserName(String username) throws UserNotFoundException;
	public User getClientUserForAuthentication(String username,String encryptedPassword) throws UserNotFoundException;
	public Integer saveOrUpdateUserLoginsInfo(UserLogins userLogins) throws UserLoginsNotSavedOrUpdatedException;
	public User getClientUserByUserId(Integer userId) throws UserNotFoundException;
	public UserLogins getUserLoginsInfoByUserIdAndDataId(User user,Integer dataId) throws UserLoginsNotFoundException;
	public UserLogins getUserLoginsInfoByUserIdAndWorkDate(User user,Date workDate) throws UserLoginsNotFoundException;
	public AdminUser getAdminUserByCompany(Company company) throws UserNotFoundException;
	public Integer saveOrUpdateUserScreenCapturesInfo(UserScreenCaptures userScreenCaptures) throws UserScreenCapturesNotSavedOrUpdatedException;
	public Integer saveOrUpdateUserWorkHours(UserWorkHours userWorkHours) throws UserWorkHoursNotSavedOrUpdatedException;
	public UserWorkHours getUserWorkHoursByUserAndworkDate(User user,Date workDate) throws UserWorkHoursNotFoundException;
	public ArrayList<UserWorkHours> getListOfUserWorkHoursByWorkDateAndUser(User user,Date workDate) throws UserWorkHoursNotFoundException;
	public Company getCompanybyCompanyCode(String companyCode)throws CompanyNotFoundException;
	public User getClientUserByUserNameAndCompany(String username,Company company) throws UserNotFoundException;
	public User getClientUserByEmail(String email) throws UserNotFoundException;
	public User getClientUserByAccountToken(String token)throws UserNotFoundException;
	public UserWorkHours getUserWorkHoursByWorkDateAndUser(User user,Date workDate) throws UserWorkHoursNotFoundException;
	public UserWorkHours getUserWorkHoursByUserAndWorkDateAndIsAppClosed(User user,Date workDate) throws UserWorkHoursNotFoundException;
	public ArrayList<UserLogins> getListOfUserLoginsInfoByUser(User user) throws UserLoginsNotFoundException;
	public ArrayList<UserWorkHours> getListOfUserWorkHoursBasedOnDateCriteria(User user,Date fromDate,Date toDate) throws UserWorkHoursNotFoundException;
	 public Integer saveOrUpdateUserCompletedWorkHours(UserCompletedWorkHours userCompletedWorkHours) throws UserCompletedWorkHoursNotSavedOrUpdatedException;
	 public ArrayList<UserCompletedWorkHours> getListOfUserCompletedWorkHoursByUser(User user) throws UserCompletedWorkHoursNotFoundException;
	 /**
	  * Created By Harshitha on Febraury 22nd, 2021
	  * Dao for Updated Account Date in the Update operation.
	  */
	 public User getAccountUpdatedDate(Integer userId) throws UserNotFoundException;
	 /**
	  * Created By Harshitha on March 3rd, 2021
	  * Dao to get employee number validation in the Update operation.
	  */
	public User getEmployeeNumber(String employeeNumber) throws EmployeeNumberNotFoundException;
	/**
	  * Created By Harshitha on March 4rd, 2021
	  * Dao to get employee email id validation in the Update operation.
	  */
	public User getEmployeeEmailId(String emailId) throws EmployeeEmailIdNotFoundException;
	/*
	 * Created By Harshitha on May 11th , 2021
	 * Dao to get the Weekly worked hours details on the basis of user Id
	 */
	public ArrayList<UserCompletedWorkHours> getListOfUserWorkHoursByFromDateToDateAndUser(User user,Date fromDate, Date toDate) throws UserWorkHoursNotFoundException;
}