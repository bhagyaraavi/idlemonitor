package com.idlemonitor.backend.user.model;

/**
 * 
 * @author KNS-ACCONTS
 * 
 *  Created by Bhagya on may 26th, 2020
 *  
 *  Enum for USerRoles..
 *  
 *  Since UserRoles are Fixed. Saving them in ENUM, instead of getting them from seperate Table.
 *
 */
public enum UserRole {

	UNKNOWN(0),ROLE_IDLE_TIMER(1),ROLE_MONITOR_TOOL(2),ROLE_BOTH(3);
	
	
	private int value;
	
	private UserRole(int value){
		this.value=value; 
	}
	
	public int getUserRole(){
		return value;
	}
	
	
	
	
	

};
