package com.idlemonitor.backend.user.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.idlemonitor.backend.admin.model.Company;
/**
 * Created by bhagya on may 26th,2020
 * @author user
 *
 *Model class for Admin User
 */
@Entity
@Table(name ="admin_user")
public class AdminUser implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name="admin_user_id")
	private Integer adminUserId;
	
	@ManyToOne(targetEntity=Company.class)
	@JoinColumn(name="company_id")
	private Company company;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "password")
	private String password;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="user_role")
	private UserRole userRole;
	
	@Column(name = "contact_email")
	private String contactEmail;
	
	@Column(name = "contact_number")
	private String contactNumber;
	
	@Column(name = "company_address")
	private String companyAddress;
	
	@Column(name="password_token")
	private String passwordToken;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="password_token_expiry_date")
	private Date passwordTokenExpiryDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="account_creation_date")
	private Date accountCreationDate;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="account_activation_token")
	private String accountActivationToken;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="activation_token_expiry_date")
	private Date activationTokenExpiryDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="account_modified_date")
	private Date accountModifiedDate;
		
	public Integer getAdminUserId() {
		return adminUserId;
	}
	public void setAdminUserId(Integer adminUserId) {
		this.adminUserId = adminUserId;
	}
	
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	public String getPasswordToken() {
		return passwordToken;
	}
	public void setPasswordToken(String passwordToken) {
		this.passwordToken = passwordToken;
	}
	public Date getPasswordTokenExpiryDate() {
		return passwordTokenExpiryDate;
	}
	public void setPasswordTokenExpiryDate(Date passwordTokenExpiryDate) {
		this.passwordTokenExpiryDate = passwordTokenExpiryDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getAccountActivationToken() {
		return accountActivationToken;
	}
	public void setAccountActivationToken(String accountActivationToken) {
		this.accountActivationToken = accountActivationToken;
	}
	public Date getActivationTokenExpiryDate() {
		return activationTokenExpiryDate;
	}
	public void setActivationTokenExpiryDate(Date activationTokenExpiryDate) {
		this.activationTokenExpiryDate = activationTokenExpiryDate;
	}
	public Date getAccountCreationDate() {
		return accountCreationDate;
	}
	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}
	public Date getAccountModifiedDate() {
		return accountModifiedDate;
	}
	public void setAccountModifiedDate(Date accountModifiedDate) {
		this.accountModifiedDate = accountModifiedDate;
	}
	
}