package com.idlemonitor.backend.user.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.idlemonitor.backend.admin.model.Company;
/**
 * Created By Bhagya
 * 
 * @author user
 * Model class for User Screen Captures - Screenshots of user/employee
 *
 */
@Entity
@Table(name ="user_screen_captures")
public class UserScreenCaptures implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="data_id")
	private Integer dataId;
	
	@ManyToOne(targetEntity=User.class)
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="capture_date_time")
	private Timestamp captureDateTime;
	
	@Column(name="capture_screen_image")
	private String captureScreenImage;
	
	@Transient
	private Integer totalCaptures;
	
	@ManyToOne(targetEntity=Company.class)
	@JoinColumn(name="company_id")
	private Company company;
	
	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Timestamp getCaptureDateTime() {
		return captureDateTime;
	}

	public void setCaptureDateTime(Timestamp captureDateTime) {
		this.captureDateTime = captureDateTime;
	}

	public String getCaptureScreenImage() {
		return captureScreenImage;
	}

	public void setCaptureScreenImage(String captureScreenImage) {
		this.captureScreenImage = captureScreenImage;
	}

	public Integer getTotalCaptures() {
		return totalCaptures;
	}

	public void setTotalCaptures(Integer totalCaptures) {
		this.totalCaptures = totalCaptures;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	
}