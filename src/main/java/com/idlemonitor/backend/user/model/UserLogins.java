package com.idlemonitor.backend.user.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * Created By BHagya
 * @author user
 * Model Class for EMployee User logins
 */
@Entity
@Table(name ="user_logins")
public class UserLogins implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="data_id")
	private Integer dataId;
	
	@ManyToOne(targetEntity=User.class)
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="login_date_time")
	private Timestamp loginDateTime;
	
	@Column(name="idle_date_time")
	private Timestamp idleDateTime;
	
	@Column(name="shutdown_date_time")
	private Timestamp shutdownDateTime;
	
	@Column(name="work_date")
	private Date workDate;
	
	@Column(name="description")
	private String description;
	
	@Column(name="logout_date_time")
	private Timestamp logoutDateTime;

	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Timestamp getLoginDateTime() {
		return loginDateTime;
	}

	public void setLoginDateTime(Timestamp loginDateTime) {
		this.loginDateTime = loginDateTime;
	}

	public Timestamp getIdleDateTime() {
		return idleDateTime;
	}

	public void setIdleDateTime(Timestamp idleDateTime) {
		this.idleDateTime = idleDateTime;
	}

	public Timestamp getShutdownDateTime() {
		return shutdownDateTime;
	}

	public void setShutdownDateTime(Timestamp shutdownDateTime) {
		this.shutdownDateTime = shutdownDateTime;
	}

	public Date getWorkDate() {
		return workDate;
	}

	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getLogoutDateTime() {
		return logoutDateTime;
	}

	public void setLogoutDateTime(Timestamp logoutDateTime) {
		this.logoutDateTime = logoutDateTime;
	}
	
	
	
	
}