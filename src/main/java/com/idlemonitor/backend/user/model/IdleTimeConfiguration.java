package com.idlemonitor.backend.user.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created By Bhagya
 * @author user
 * Model class for Idle Time configuration
 */
@Entity
@Table(name ="idle_time_configuration")
public class IdleTimeConfiguration implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name="config_id")
	private Integer configId;
	
	@ManyToOne(targetEntity=AdminUser.class)
	@JoinColumn(name="admin_user_id")
	private AdminUser adminUser;
	
	@Column(name="idle_minutes")
	private String idleMinutes;

	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	

	public AdminUser getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(AdminUser adminUser) {
		this.adminUser = adminUser;
	}

	public String getIdleMinutes() {
		return idleMinutes;
	}

	public void setIdleMinutes(String idleMinutes) {
		this.idleMinutes = idleMinutes;
	}
	
	
}