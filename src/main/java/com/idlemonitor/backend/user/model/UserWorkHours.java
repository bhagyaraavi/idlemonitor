package com.idlemonitor.backend.user.model;

import java.io.Serializable;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * Created By Bhagya
 * @author user
 * 
 * Model Class For User Work Hours 
 *
 */
@Entity
@Table(name ="user_work_hours")
public class UserWorkHours implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name="data_id")
	private Integer dataId;
	
	@ManyToOne(targetEntity=User.class)
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="work_date")
	private Date workDate;
	
	@Column(name="work_hours")
	private String workHours;
	
	@Column(name="is_app_closed")
	private Boolean isAppClosed;
	
		
	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getWorkDate() {
		return workDate;
	}

	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}

	public String getWorkHours() {
		return workHours;
	}

	public void setWorkHours(String workHours) {
		this.workHours = workHours;
	}

	public Boolean getIsAppClosed() {
		return isAppClosed;
	}

	public void setIsAppClosed(Boolean isAppClosed) {
		this.isAppClosed = isAppClosed;
	}

}