package com.idlemonitor.backend.user.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.idlemonitor.backend.admin.model.Company;
/**
 * Created By Bhagya 
 * @author user
 * Model class for Employee User
 *
 */
@Entity
@Table(name ="users")
public class User implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	@ManyToOne(targetEntity=Company.class)
	@JoinColumn(name="company_id")
	private Company company;
	
	@Column(name="employee_number")
	private String employeeNumber;
	
	@Column(name="employee_name")
	private String employeeName;
	
	@Column(name="email_id")
	private String emailId;
	
	@Column(name="status")
	private String status;
	
	@Column(name="status_updated_date")
	private Timestamp statusUpdatedDate;
	
	@Column(name="creation_date")
	private Timestamp creationDate;
	
	/**
	  * Created By Harshitha on Febraury 22nd, 2021
	  * creating column for the user's Updated Account Date.
	  */
	@Column(name="account_updated_date")
	private Timestamp accountUpdatedDate;
	
	@Transient
	private Integer totalUsers;
	
	@Column(name="password_token")
	private String passwordToken;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="password_token_expiry_date")
	private Date passwordTokenExpiryDate;
	
	@Column(name="account_activation_token")
	private String accountActivationToken;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="activation_token_expiry_date")
	private Date activationTokenExpiryDate;
	
	@Column(name="is_active")
	private Boolean isActive;

	/*
	 * Created by Harshitha on 30 Aug 2021 for multi device login issue
	 */
	@Column(name="is_app_running")
	private Boolean isAppRunning;

	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getStatusUpdatedDate() {
		return statusUpdatedDate;
	}

	public void setStatusUpdatedDate(Timestamp statusUpdatedDate) {
		this.statusUpdatedDate = statusUpdatedDate;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getTotalUsers() {
		return totalUsers;
	}

	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}

	public String getPasswordToken() {
		return passwordToken;
	}

	public void setPasswordToken(String passwordToken) {
		this.passwordToken = passwordToken;
	}

	public Date getPasswordTokenExpiryDate() {
		return passwordTokenExpiryDate;
	}

	public void setPasswordTokenExpiryDate(Date passwordTokenExpiryDate) {
		this.passwordTokenExpiryDate = passwordTokenExpiryDate;
	}

	public String getAccountActivationToken() {
		return accountActivationToken;
	}

	public void setAccountActivationToken(String accountActivationToken) {
		this.accountActivationToken = accountActivationToken;
	}

	public Date getActivationTokenExpiryDate() {
		return activationTokenExpiryDate;
	}

	public void setActivationTokenExpiryDate(Date activationTokenExpiryDate) {
		this.activationTokenExpiryDate = activationTokenExpiryDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	/**
	  * Created By Harshitha on Febraury 22nd, 2021
	  * For user's Updated Account Date based on user
	  */
	public Timestamp getAccountUpdatedDate() {
		return accountUpdatedDate;
	}

	public void setAccountUpdatedDate(Timestamp accountUpdatedDate) {
		this.accountUpdatedDate = accountUpdatedDate;
	}

	public Boolean getIsAppRunning() {
		return isAppRunning;
	}

	public void setIsAppRunning(Boolean isAppRunning) {
		this.isAppRunning = isAppRunning;
	}	
	
	
	
}