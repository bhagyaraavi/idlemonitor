package com.idlemonitor.backend.employee.dao;

import java.util.ArrayList;

import java.util.Date;

import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.backend.admin.model.ProjectMapping;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserCompletedWorkHours;
import com.idlemonitor.backend.user.model.UserLogins;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
import com.idlemonitor.backend.user.model.UserWorkHours;
import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeNotFoundException;
import com.idlemonitor.common.exceptions.ProjectMappingNotFoundException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserScreenCapturesNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
/**
 * Created By Bhagya
 * @author user
 * 
 * Interface class for employee dao
 *
 */
public interface EmployeeDao{
	public ArrayList<User> getUsersByCompanyId(Company company, Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws EmployeeNotFoundException;
	public User getEmployeeUserforAuthentication(String username)throws EmployeeNotFoundException;
	public ArrayList<UserLogins> getEmployeeUserLoginsByUserId(User employeeUser,Date fromDate ,Date toDate) throws EmployeeNotFoundException;
	public User getEmployeeUserByUserId(Integer employeeUserId)throws EmployeeNotFoundException;
	public ArrayList<UserScreenCaptures> getScreenshotsOfUsers(Company company,User user, Integer pageNo, Integer pageSize,Date fromDate,Date toDate) throws UserScreenCapturesNotFoundException;
	public ArrayList<User> getListOfUsers() throws UserNotFoundException;
	public Integer deleteUserScreenCaptures(ArrayList<UserScreenCaptures> userScreenCaptures) throws Exception;
	public ArrayList<UserScreenCaptures> getUserScreenCapturesByDataIds(ArrayList<Integer> dataIds) throws UserScreenCapturesNotFoundException;
	public ArrayList<UserScreenCaptures> getUserScreenCapturesByUsers(ArrayList<User> users) throws UserScreenCapturesNotFoundException;
	public UserWorkHours getUsersWorkHoursByCurrentDateAndUsers(User user,Date currentDate) throws UserWorkHoursNotFoundException;
	public ArrayList<Company> getListOfCompanies() throws CompanyNotFoundException;
	
	public Integer deleteUserWorkHours(ArrayList<UserWorkHours> userWorkHours);
	public Integer deleteUserLogins(ArrayList<UserLogins> userLogins);
	public Integer deleteProjectMappingsOfUser(ArrayList<ProjectMapping> projectMappings);
	public Integer deleteEmployeeUser(User employeeUser);
	public ArrayList<UserScreenCaptures> getUserScreenCapturesByUser(User employeeUser) throws UserScreenCapturesNotFoundException;
	public ArrayList<ProjectMapping> getListOfProjectMappingsOfUser(User user) throws ProjectMappingNotFoundException;
	public ArrayList<UserCompletedWorkHours> getEmployeesWorkHoursByCompany(Company company, Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending,Date fromDate,Date toDate) throws EmployeeNotFoundException;
	public Integer deleteUserCompletedWorkHours(ArrayList<UserCompletedWorkHours> userCompletedWorkHours);
}