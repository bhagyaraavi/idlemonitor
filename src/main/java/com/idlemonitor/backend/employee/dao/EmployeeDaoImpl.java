package com.idlemonitor.backend.employee.dao;

import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.backend.admin.model.ProjectMapping;
import com.idlemonitor.backend.user.model.User;
import com.idlemonitor.backend.user.model.UserCompletedWorkHours;
import com.idlemonitor.backend.user.model.UserLogins;
import com.idlemonitor.backend.user.model.UserScreenCaptures;
import com.idlemonitor.backend.user.model.UserWorkHours;
import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.common.exceptions.CompanyNotFoundException;
import com.idlemonitor.common.exceptions.EmployeeNotFoundException;
import com.idlemonitor.common.exceptions.ProjectMappingNotFoundException;
import com.idlemonitor.common.exceptions.UserLoginsNotFoundException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
import com.idlemonitor.common.exceptions.UserScreenCapturesNotFoundException;
import com.idlemonitor.common.exceptions.UserWorkHoursNotFoundException;
/**
 * Created By Bhagya on june 03rd 2020
 * @author user
 * 
 * Implementation class for Employee Dao
 *
 */
@Repository("employeeDao")
@Transactional
public class EmployeeDaoImpl implements EmployeeDao{
	
private static Logger log=Logger.getLogger(EmployeeDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	
	/**
	 * Created By Bhagya On june 03rd, 2020
	 * @param company
	 * @return
	 * @throws EmployeeNotFoundException
	 * 
	 * Method for to get the employees based on company
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<User> getUsersByCompanyId(Company company, Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws EmployeeNotFoundException{
		log.info("EmployeeDaoImpl -> getUsersByCompanyId()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("company", company));
		
		
		if (searchBy != null && !searchBy.isEmpty()) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("employeeName", searchBy,MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("employeeNumber", searchBy,MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("status", searchBy,MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("emailId", searchBy,MatchMode.ANYWHERE));
			criteria.add(disjunction);
		}
		
		if(null!=sortBy){
			
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));			
			}
			else{
				
				criteria.addOrder(Order.desc(sortBy));
				
			}				
		}
		else {
			criteria.add(Restrictions.eq("isActive", true));
			criteria.addOrder(Order.asc("employeeName"));
		}
		Integer totalUsers=criteria.list().size();
		
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<User> users=(ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			users.get(0).setTotalUsers(totalUsers);
			return users;
		}
		else{
			 throw new EmployeeNotFoundException();
		}
		
	}
	
	/**
	 * Created By Bhagya on june 03rd,2020
	 * 
	 * Service for to get employee user based on username or email
	 */
	@SuppressWarnings("unchecked")
	public User getEmployeeUserforAuthentication(String username)throws EmployeeNotFoundException{
		log.info("EmployeeDaoImpl-> getEmployeeUserforAuthentication() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.or(Restrictions.eqOrIsNull("emailId", username),Restrictions.eq("username", username)));
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new EmployeeNotFoundException();
		}
	}
	/**
	 * Created by Bhagya on june 09th, 2020
	 * @param employeeUser
	 * @return
	 * @throws EmployeeNotFoundException
	 * 
	 * Service for to get list of employees user logins data by employeeUserId
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserLogins> getEmployeeUserLoginsByUserId(User employeeUser,Date fromDate ,Date toDate) throws EmployeeNotFoundException{
		log.info("EmployeeDaoImpl -> getEmployeeUserLoginsByUserId() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserLogins.class);
		criteria.add(Restrictions.eq("user", employeeUser));
		criteria.add(Restrictions.ge("loginDateTime", fromDate));
		criteria.add(Restrictions.le("loginDateTime",toDate));
		ArrayList<UserLogins> employeeUserLogins=(ArrayList<UserLogins>) criteria.list();
		if(!employeeUserLogins.isEmpty()){
			
			return employeeUserLogins;
		}
		else{
			 throw new EmployeeNotFoundException();
		}
		
	}
	/**
	 * created by bhagya on June 09th, 2020
	 * @param employeeUserId
	 * @return
	 * @throws EmployeeNotFoundException
	 * 
	 * Service for to get the employeeUser by employeeUserId
	 */
	
	
	@SuppressWarnings("unchecked")
	public User getEmployeeUserByUserId(Integer employeeUserId)throws EmployeeNotFoundException{
		log.info("EmployeeDaoImpl -> getEmployeeUserByUserId() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("userId", employeeUserId));	
		ArrayList<User> users = (ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users.get(0);
		}
		else{
			throw new EmployeeNotFoundException();
		}
	}
	/**
	 * created by bhagya on june 04th 2020
	 * @param employeeUser
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws EmployeeNotFoundException
	 * Service for to get employee userlogins
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<UserLogins> getEmployeeUserLogins(User employeeUser,Date fromDate ,Date toDate) throws EmployeeNotFoundException{
		log.info("EmployeeDaoImpl -> getEmployeeUserLogins() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserLogins.class);
		criteria.add(Restrictions.eq("user", employeeUser));
		criteria.add(Restrictions.ge("loginDateTime", fromDate));
		criteria.add(Restrictions.le("loginDateTime",toDate));
		criteria.setProjection(Projections.projectionList()
	            .add(Projections.groupProperty("loginDateTime")));
		ArrayList<UserLogins> employeeUserLogins=(ArrayList<UserLogins>) criteria.list();
		if(!employeeUserLogins.isEmpty()){
			return employeeUserLogins;
		}
		else{
			 throw new EmployeeNotFoundException();
		}
		
	}
	
	/**
	 * Created By Bhagya on July 07th, 2020
	 * @param user
	 * @param pageNo
	 * @param pageSize
	 * @param searchBy
	 * @param sortBy
	 * @param ascending
	 * @return
	 * @throws UserScreenCapturesNotFoundException
	 * 
	 * Service for to get the screenshots images of employee users
	 */
	
	@SuppressWarnings("unchecked")
	public ArrayList<UserScreenCaptures> getScreenshotsOfUsers(Company company,User user, Integer pageNo, Integer pageSize,Date fromDate,Date toDate) throws UserScreenCapturesNotFoundException{
		log.info("EmployeeDaoImpl -> getScreenshotsOfUsers()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserScreenCaptures.class);
		criteria.add(Restrictions.eq("company",company));
		criteria.add(Restrictions.ge("captureDateTime", fromDate));
		criteria.add(Restrictions.le("captureDateTime",toDate));
		
		if(null!=user){
			criteria.add(Restrictions.eq("user", user));
		}
		criteria.addOrder(Order.desc("captureDateTime"));
		Integer totalCaptures=criteria.list().size();
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<UserScreenCaptures> userScreenCaptures=(ArrayList<UserScreenCaptures>) criteria.list();
		
		if(!userScreenCaptures.isEmpty()){
			userScreenCaptures.get(0).setTotalCaptures(totalCaptures);
			return userScreenCaptures;
		}
		else{
			 throw new UserScreenCapturesNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya on july 08th, 2020
	 * Service for to get the list of users or employees
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<User> getListOfUsers() throws UserNotFoundException{
		log.info("EmployeeDaoImpl -> getListOfUsers()");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("isActive", true));
		criteria.addOrder(Order.asc("employeeName"));
		ArrayList<User> users=(ArrayList<User>) criteria.list();
		if(!users.isEmpty()){
			return users;
		}
		else{
			throw new UserNotFoundException();
		}
	
	}
	/**
	 * Created By Bhagya on july 08th, 2020
	 * @param userScreenCaptures
	 * @return
	 * @throws Exception
	 * 
	 * service for to delete the user Screenshot images
	 */
	
	
	public Integer deleteUserScreenCaptures(ArrayList<UserScreenCaptures> userScreenCaptures) {
		log.info("EmployeeDaoImpl -> deleteUserScreenCaptures()");
		for(UserScreenCaptures userScreenCapture:userScreenCaptures){
		sessionFactory.getCurrentSession().delete(userScreenCapture);
		sessionFactory.getCurrentSession().flush();
		}
		return userScreenCaptures.get(0).getDataId();
	}
	/**
	 * Created By Bhagya on july 08th, 2020
	 * @param dataId
	 * @return
	 * @throws UserScreenCapturesNotFoundException
	 * 
	 * Service for to get the user screen captures by dataId
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserScreenCaptures> getUserScreenCapturesByDataIds(ArrayList<Integer> dataIds) throws UserScreenCapturesNotFoundException{
		log.info("EmployeeDaoImpl -> getUserScreenCaptureByDataIds() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserScreenCaptures.class);
		
		criteria.add(Restrictions.in("dataId", dataIds));
		ArrayList<UserScreenCaptures> userScreenCaptures=(ArrayList<UserScreenCaptures>) criteria.list();
		if(!userScreenCaptures.isEmpty()){
			return userScreenCaptures;
		}
		else{
			throw new UserScreenCapturesNotFoundException();
		}
	}
	/**
	 * Created by bhagya on july 18th, 2020
	 * @param users
	 * @return
	 * @throws UserScreenCapturesNotFoundException
	 * 
	 * Service for to delete the user screen captures by users
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserScreenCaptures> getUserScreenCapturesByUsers(ArrayList<User> users) throws UserScreenCapturesNotFoundException{
		log.info("EmployeeDaoImpl -> getUserScreenCapturesByUsers() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserScreenCaptures.class);
		Calendar cal = Calendar.getInstance();
    	Date today=new Date();
		cal.setTime(today);
		cal.add(Calendar.DATE, -(30)); //last30days (previous month)
		Date dateBeforeDays = cal.getTime();
		criteria.add(Restrictions.in("user", users));
		criteria.add(Restrictions.le("captureDateTime", dateBeforeDays));
		ArrayList<UserScreenCaptures> userScreenCaptures=(ArrayList<UserScreenCaptures>) criteria.list();
		if(!userScreenCaptures.isEmpty()){
			return userScreenCaptures;
		}
		else{
			throw new UserScreenCapturesNotFoundException();
		}
	}
	/**
	 * Created By Bhagya on August 14th, 2020
	 * @param users
	 * @param currentDate
	 * @return
	 * @throws UserWorkHoursNotFoundException
	 * 
	 * Service for to get the list of user work hours by users list and current date
	 */
	@SuppressWarnings("unchecked")
	public UserWorkHours getUsersWorkHoursByCurrentDateAndUsers(User user,Date currentDate) throws UserWorkHoursNotFoundException{
		log.info("EmployeeDaoImpl -> getUsersWorkHoursByCurrentDate() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserWorkHours.class);
		if(null!=currentDate) {
			criteria.add(Restrictions.eq("workDate", currentDate));
		}
		criteria.add(Restrictions.eq("user", user));
		ArrayList<UserWorkHours> userWorkHours=(ArrayList<UserWorkHours>) criteria.list();
		if(!userWorkHours.isEmpty()){
			return userWorkHours.get(0);
		}
		else{
			throw new UserWorkHoursNotFoundException();
		}
	}
	/**
	 * Created By Bhagya on august 14th, 2020
	 * @return
	 * @throws CompanyNotFoundException
	 * 
	 * Service for to get the list of companies
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Company> getListOfCompanies() throws CompanyNotFoundException{
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Company.class);
		ArrayList<Company> companies=(ArrayList<Company>) criteria.list();
		if(!companies.isEmpty()){
			return companies;
		}
		else{
			throw new CompanyNotFoundException();
		}
	}
	/**
	 * Created By Bhagya on october 20, 2020
	 * @param userWorkHours
	 * @return
	 * Service for to delete the list of user workHours
	 */
	public Integer deleteUserWorkHours(ArrayList<UserWorkHours> userWorkHours) {
		log.info("EmployeeDaoImpl -> deleteUserWorkHours()");
		for(UserWorkHours userWorkHour:userWorkHours){
		sessionFactory.getCurrentSession().delete(userWorkHour);
		sessionFactory.getCurrentSession().flush();
		}
		return userWorkHours.get(0).getDataId();
	}
	/**
	 *  Created By Bhagya on october 20, 2020
	 * @param userLogins
	 * @return
	 * Service for to delete the list of user logins
	 */
	
	public Integer deleteUserLogins(ArrayList<UserLogins> userLogins) {
		log.info("EmployeeDaoImpl -> deleteUserLogins()");
		for(UserLogins userLogin:userLogins){
		sessionFactory.getCurrentSession().delete(userLogin);
		sessionFactory.getCurrentSession().flush();
		}
		return userLogins.get(0).getDataId();
	}
	/**
	 * Created By Bhagya On October 20, 2020
	 * @param projectMappings
	 * @return
	 * 
	 * Service for to delete the list of project mappings of user
	 */
	
	public Integer deleteProjectMappingsOfUser(ArrayList<ProjectMapping> projectMappings) {
		log.info("EmployeeDaoImpl -> deleteProjectMappingsOfUser()");
		for(ProjectMapping projectMapping:projectMappings){
		sessionFactory.getCurrentSession().delete(projectMapping);
		sessionFactory.getCurrentSession().flush();
		}
		return projectMappings.get(0).getMappingId();
	}
	/**
	 * Created By Bhagya On october 20, 2020
	 * @param employeeUser
	 * @return
	 * Service for to delete the employee user
	 */
	public Integer deleteEmployeeUser(User employeeUser) {
		log.info("EmployeeDaoImpl -> deleteEmployeeUser()");
		sessionFactory.getCurrentSession().delete(employeeUser);
		sessionFactory.getCurrentSession().flush();
		return employeeUser.getUserId();
	}
	/**
	 * Created By BHagya on October 20, 2020
	 * @param employeeUser
	 * @return
	 * @throws UserScreenCapturesNotFoundException
	 * 
	 * Service for get the list of user screen captures by user
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<UserScreenCaptures> getUserScreenCapturesByUser(User employeeUser) throws UserScreenCapturesNotFoundException{
		log.info("EmployeeDaoImpl -> getUserScreenCapturesByUsers() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserScreenCaptures.class);
		criteria.add(Restrictions.eq("user", employeeUser));
		ArrayList<UserScreenCaptures> userScreenCaptures=(ArrayList<UserScreenCaptures>) criteria.list();
		if(!userScreenCaptures.isEmpty()){
			return userScreenCaptures;
		}
		else{
			throw new UserScreenCapturesNotFoundException();
		}
	}
	/**
	 * Created By Bhagya on october 20, 2020
	 * @param user
	 * @return
	 * @throws ProjectMappingNotFoundException
	 * 
	 * Service for get the list of project mappings of user
	 */
	 @SuppressWarnings("unchecked")
	  	public ArrayList<ProjectMapping> getListOfProjectMappingsOfUser(User user) throws ProjectMappingNotFoundException{
	  		log.info("Inside UserDaoImpl -> getListOfProjectMappingsOfUser() ");
	  		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ProjectMapping.class);
	  		criteria.add(Restrictions.eq("user", user));
	  		ArrayList<ProjectMapping> projectMappings = (ArrayList<ProjectMapping>) criteria.list();
	  		if(!projectMappings.isEmpty()){
	  			return projectMappings;
	  		}
	  		else{
	  			throw new ProjectMappingNotFoundException();
	  		}
	  		
	  	}
	 
	 /**
		 * Created By Bhagya On june 03rd, 2020
		 * @param company
		 * @return
		 * @throws EmployeeNotFoundException
		 * 
		 * Method for to get the employees work hours based on company
		 */
		@SuppressWarnings("unchecked")
		public ArrayList<UserCompletedWorkHours> getEmployeesWorkHoursByCompany(Company company, Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending,Date fromDate,Date toDate) throws EmployeeNotFoundException{
			log.info("EmployeeDaoImpl -> getEmployeesWorkHoursByCompany()");
			
			Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(UserCompletedWorkHours.class);
			criteria.add(Restrictions.eq("company", company));
			criteria.add(Restrictions.ge("workDate", fromDate));
			criteria.add(Restrictions.le("workDate",toDate));
			if (searchBy != null && !searchBy.isEmpty()) {
				Disjunction disjunction = Restrictions.disjunction();
				disjunction.add(Restrictions.ilike("user.employeeName", searchBy,MatchMode.ANYWHERE));
				disjunction.add(Restrictions.ilike("user.employeeNumber", searchBy,MatchMode.ANYWHERE));
				criteria.add(disjunction);
			}
			
			if(null!=sortBy){
				
				if(ascending){
					criteria.addOrder(Order.asc(sortBy));			
				}
				else{
					
					criteria.addOrder(Order.desc(sortBy));
					
				}				
			}

			Integer totalUsers=criteria.list().size();
			if(null!=pageNo){
				criteria.setFirstResult(pageNo*pageSize);
				criteria.setMaxResults(pageSize);
			}	
			ArrayList<UserCompletedWorkHours> userCompletedWorkHours=(ArrayList<UserCompletedWorkHours>) criteria.list();
			if(!userCompletedWorkHours.isEmpty()){
				userCompletedWorkHours.get(0).setTotalUsers(totalUsers);
				return userCompletedWorkHours;
			}
			else{
				 throw new EmployeeNotFoundException();
			}
			
		}
		/**
		 * Created By Bhagya on october 30, 2020
		 * @param userCompletedWorkHours
		 * @return
		 * Service for to delete the list of user Completed workHours
		 */
		public Integer deleteUserCompletedWorkHours(ArrayList<UserCompletedWorkHours> userCompletedWorkHours) {
			log.info("EmployeeDaoImpl -> deleteUserCompletedWorkHours()");
			for(UserCompletedWorkHours userCompletedWorkHour:userCompletedWorkHours){
			sessionFactory.getCurrentSession().delete(userCompletedWorkHour);
			sessionFactory.getCurrentSession().flush();
			}
			return userCompletedWorkHours.get(0).getDataId();
		}
}