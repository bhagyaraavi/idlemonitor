package com.idlemonitor.backend.admin.dao;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.backend.admin.model.Project;
import com.idlemonitor.backend.admin.model.ProjectMapping;
import com.idlemonitor.backend.admin.model.Task;
import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.backend.user.model.UserCompletedWorkHours;
import com.idlemonitor.common.exceptions.ProjectMappingNotSavedOrUpdateException;
import com.idlemonitor.common.exceptions.ProjectNotFoundException;
import com.idlemonitor.common.exceptions.ProjectNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.TaskNotFoundException;
import com.idlemonitor.common.exceptions.TaskNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;

/**
 * Created on AUgust 03rd, 2020
 * @author Bhagya
 * Dao Implementation class for Project module
 */

@Repository("projectDao")
@Transactional
public class ProjectDaoImpl implements ProjectDao{
	
private static Logger log=Logger.getLogger(ProjectDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Created By Bhagya on August 03rd, 2020
	 * @param project
	 * @return
	 * @throws ProjectNotSavedOrUpdatedException
	 * 
	 * Dao Service for save or update project Information or data
	 */
	
	public Integer saveOrUpdateProject(Project project) throws ProjectNotSavedOrUpdatedException{
		log.info("ProjectDaoImpl-> saveOrUpdateProject");
		sessionFactory.getCurrentSession().saveOrUpdate(project);
		sessionFactory.getCurrentSession().flush();
		return project.getProjectId();
	}
	/**
	 * Created By Bhagya On August 04th, 2020
	 * @param projectId
	 * @return
	 * 
	 * Service for to get projects by projectId
	 * @throws ProjectNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public Project getProjectByProjectId(Integer projectId) throws ProjectNotFoundException {
		log.info("ProjectDaoImpl-> getProjectByProjectId ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Project.class);
		criteria.add(Restrictions.eq("projectId", projectId));
		ArrayList<Project> projects=(ArrayList<Project>) criteria.list();
		if(!projects.isEmpty()){
			return projects.get(0);
		}
		else{
			 throw new ProjectNotFoundException();
		}
		
	}
	/**
	 * Created By BHagya on august 04th, 2020
	 * @param company
	 * @return
	 * @throws ProjectNotFoundException
	 * 
	 * Service for to get the list of projects By Company
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Project> getListOfProjectsByCompany(Company company) throws ProjectNotFoundException{
		log.info("ProjectDaoImpl-> getListOfProjectsByCompany ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Project.class);
		criteria.add(Restrictions.eq("company", company));
		ArrayList<Project> projects=(ArrayList<Project>) criteria.list();
		if(!projects.isEmpty()){
			return projects;
		}
		else{
			 throw new ProjectNotFoundException();
		}
	}
	/**
	 * Created By Bhagya on AUgust 04th, 2020
	 * @param projectMapping
	 * @return
	 * @throws ProjectMappingNotSavedOrUpdateException
	 * 
	 * Service for to save or update project mapping
	 */
	public Integer saveOrUpdateProjectMapping(ProjectMapping projectMapping) throws ProjectMappingNotSavedOrUpdateException{
		log.info("ProjectDaoImpl-> saveOrUpdateProjectMapping");
		sessionFactory.getCurrentSession().saveOrUpdate(projectMapping);
		sessionFactory.getCurrentSession().flush();
		return projectMapping.getMappingId();
	}
	
	/**
	 * Created by bhagya on August 04th 2020
	 * @param task
	 * @return
	 * @throws TaskNotSavedOrUpdatedException
	 * 
	 * Service for to save or update task
	 */
	
	public Integer saveOrUpdateTask(Task task) throws TaskNotSavedOrUpdatedException{
		log.info("ProjectDaoImpl-> saveOrUpdateTask");
		sessionFactory.getCurrentSession().saveOrUpdate(task);
		sessionFactory.getCurrentSession().flush();
		return task.getTaskId();
	}
	
	/**
	 * Created By Bhagya on August 04th, 2020
	 * @param taskId
	 * @return
	 * @throws TaskNotFoundException
	 * 
	 * Service for to get task by taskId
	 */
	@SuppressWarnings("unchecked")
	public Task getTaskByTaskId(Integer taskId) throws TaskNotFoundException {
		log.info("ProjectDaoImpl-> getTaskByTaskId ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Task.class);
		criteria.add(Restrictions.eq("taskId", taskId));
		ArrayList<Task> tasks=(ArrayList<Task>) criteria.list();
		if(!tasks.isEmpty()){
			return tasks.get(0);
		}
		else{
			 throw new TaskNotFoundException();
		}
		
	}
	/**
	 * Created By Bhagya on April 27th, 2021
	 * 
	 * Dao Service for to get the list of projects in admin web application
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Project> getProjectsListByAdminUser(AdminUser adminUser, Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws ProjectNotFoundException{
		log.info("ProjectDaoImpl -> getProjectsListByUserId()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Project.class);
		criteria.add(Restrictions.eq("adminUser", adminUser));
		
		if (searchBy != null && !searchBy.isEmpty()) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("projectName", searchBy,MatchMode.ANYWHERE));
			disjunction.add(Restrictions.ilike("clientName", searchBy,MatchMode.ANYWHERE));
			criteria.add(disjunction);
		}
		
		if(null!=sortBy){
			
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));			
			}
			else{
				
				criteria.addOrder(Order.desc(sortBy));
				
			}				
		}
		else {
			criteria.add(Restrictions.eq("isActive", true));
			criteria.addOrder(Order.asc("projectId"));
		}
		Integer totalProjects=criteria.list().size();
		
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<Project> projects=(ArrayList<Project>) criteria.list();
		if(!projects.isEmpty()){
			projects.get(0).setTotalProjects(totalProjects);
			return projects;
		}
		else{
			 throw new ProjectNotFoundException();
		}
		
	}
	/**
	 * Created BY Bhagya on APril 27th, 2021
	 * @param project
	 * @return
	 * @throws TaskNotFoundException
	 * 
	 * Get list of tasks by project
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Task> getListOfTasksByProject(Project project) throws TaskNotFoundException{
		log.info("ProjectDaoImpl->getListOfTasksByProject ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Task.class);
		criteria.add(Restrictions.eq("project", project));
		ArrayList<Task> tasks=(ArrayList<Task>) criteria.list();
		if(!tasks.isEmpty()){
			return tasks;
		}
		else{
			 throw new TaskNotFoundException();
		}
	}
	
	/**
	 * Created By Bhagya on april 27th, 2021
	 * @param tasks
	 * @return
	 * Service for to delete the list of tasks
	 */
	public Integer deleteTasks(ArrayList<Task> tasks) {
		log.info("ProjectDaoImpl -> deleteTasks()");
		for(Task task:tasks){
		sessionFactory.getCurrentSession().delete(task);
		sessionFactory.getCurrentSession().flush();
		}
		return tasks.get(0).getTaskId();
	}
	/**
	 * Created By BHagya on APril 27th, 2021
	 * @param project
	 * @return
	 * 
	 * Service for to delete the project
	 */
	public Integer deleteProject(Project project) {
		log.info("ProjectDaoImpl -> deleteProject()");
		sessionFactory.getCurrentSession().delete(project);
		sessionFactory.getCurrentSession().flush();
		return project.getProjectId();
	}
	
	/**
	 * Created By Harshitha on April 29th,2021
	 * 
	 * Service for to get project based on project id or project name
	 */
	@SuppressWarnings("unchecked")
	public Project getProjectDetailsforAuthentication(String projectName)throws ProjectNotFoundException{
		log.info("ProjectDaoImpl-> getProjectDetailsforAuthentication() ");
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Project.class);
		criteria.add(Restrictions.or(Restrictions.eqOrIsNull("projectId", projectName),Restrictions.eq("projectName", projectName)));
		ArrayList<Project> projects = (ArrayList<Project>) criteria.list();
		if(!projects.isEmpty()){
			return projects.get(0);
		}
		else{
			throw new ProjectNotFoundException();
		}
	}
	
	/**
	 * Created By harshitha on April 28th, 2021
	 * 
	 * Dao Service for to get the list of Task in admin web application
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Task> getTasksListByProjectforAdmin(Project project, Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws TaskNotFoundException{
		log.info("ProjectDaoImpl -> getTasksListByProjectforAdmin()");
		
		Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(Task.class);
		criteria.add(Restrictions.eq("project", project));
		
		if (searchBy != null && !searchBy.isEmpty()) {
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.ilike("taskName", searchBy,MatchMode.ANYWHERE));
			criteria.add(disjunction);
		}
		
		if(null!=sortBy){
			
			if(ascending){
				criteria.addOrder(Order.asc(sortBy));			
			}
			else{
				
				criteria.addOrder(Order.desc(sortBy));
				
			}				
		}
		else {
			criteria.add(Restrictions.eq("isActive", true));
			criteria.addOrder(Order.asc("taskId"));
		}
		Integer totalTasks=criteria.list().size();
		
		if(null!=pageNo){
			criteria.setFirstResult(pageNo*pageSize);
			criteria.setMaxResults(pageSize);
		}	
		ArrayList<Task> tasks=(ArrayList<Task>) criteria.list();
		if(!tasks.isEmpty()){
			tasks.get(0).setTotalTasks(totalTasks);
			return tasks;
		}
		else{
			 throw new TaskNotFoundException();
		}
		
	}
}