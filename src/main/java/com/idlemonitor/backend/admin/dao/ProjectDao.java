package com.idlemonitor.backend.admin.dao;

import java.util.ArrayList;

import com.idlemonitor.backend.admin.model.Company;
import com.idlemonitor.backend.admin.model.Project;
import com.idlemonitor.backend.admin.model.ProjectMapping;
import com.idlemonitor.backend.admin.model.Task;
import com.idlemonitor.backend.user.model.AdminUser;
import com.idlemonitor.common.exceptions.ProjectMappingNotSavedOrUpdateException;
import com.idlemonitor.common.exceptions.ProjectNotFoundException;
import com.idlemonitor.common.exceptions.ProjectNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.TaskNotFoundException;
import com.idlemonitor.common.exceptions.TaskNotSavedOrUpdatedException;
import com.idlemonitor.common.exceptions.UserNotFoundException;
/**
 * Created on August 03rd, 2020
 * @author Bhagya
 * 
 * Interface class for Project Dao services
 *
 */
public interface ProjectDao{
	public Integer saveOrUpdateProject(Project project) throws ProjectNotSavedOrUpdatedException;
	public Project getProjectByProjectId(Integer projectId) throws ProjectNotFoundException ;
	public ArrayList<Project> getListOfProjectsByCompany(Company company) throws ProjectNotFoundException;
	public Integer saveOrUpdateProjectMapping(ProjectMapping projectMapping) throws ProjectMappingNotSavedOrUpdateException;
	public Integer saveOrUpdateTask(Task task) throws TaskNotSavedOrUpdatedException;
	public Task getTaskByTaskId(Integer taskId) throws TaskNotFoundException;
	public ArrayList<Project> getProjectsListByAdminUser(AdminUser adminUser, Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws ProjectNotFoundException;
	public ArrayList<Task> getListOfTasksByProject(Project project) throws TaskNotFoundException;
	public Integer deleteTasks(ArrayList<Task> tasks);
	public Integer deleteProject(Project project);
	
	public Project getProjectDetailsforAuthentication(String projectName)throws ProjectNotFoundException ;
	public ArrayList<Task> getTasksListByProjectforAdmin(Project project, Integer pageNo, Integer pageSize, String searchBy,String sortBy, Boolean ascending) throws TaskNotFoundException;
}