package com.idlemonitor.backend.admin.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
 * @author bhagya
 * Created By Bhagya on may 12th, 2020
 * Model class for company
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name ="company")
public class Company implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="company_id")
	private Integer companyId;
	
	@Column(name="company_name")
	private String companyName;
	
	@Column(name="company_code")
	private String companyCode;
	
	@Column(name="admin_email_ids")
	private String adminEmailIds;
	
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getAdminEmailIds() {
		return adminEmailIds;
	}

	public void setAdminEmailIds(String adminEmailIds) {
		this.adminEmailIds = adminEmailIds;
	}
	
	
}