package com.idlemonitor.backend.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Created on August 04th, 2020
 * @author Bhagya
 * 
 * Model class for Task
 */

@Entity
@Table(name ="tasks")
public class Task implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="task_id")
	private Integer taskId;
	
	@Column(name="task_name")
	private String taskName;
	
	@ManyToOne(targetEntity=Project.class)
	@JoinColumn(name="project_id")
	private Project project;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="task_creation_date")
	private Date taskCreationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="task_modification_date")
	private Date taskModificationDate;
	
	@Transient
	private Integer totalTasks;
	
	/**
	 * Created by harshitha on May 3rd, 2021
	 * To know the status of a task
	 */
	@Column(name="is_active")
	private Boolean isActive;

	public Integer getTaskId() {
		return taskId;
	}

	public Integer getTotalTasks() {
		return totalTasks;
	}

	public void setTotalTasks(Integer totalTasks) {
		this.totalTasks = totalTasks;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Date getTaskCreationDate() {
		return taskCreationDate;
	}

	public void setTaskCreationDate(Date taskCreationDate) {
		this.taskCreationDate = taskCreationDate;
	}

	public Date getTaskModificationDate() {
		return taskModificationDate;
	}

	public void setTaskModificationDate(Date taskModificationDate) {
		this.taskModificationDate = taskModificationDate;
	}
	
	/**
	 * Created by harshitha on May 3rd, 2021
	 * To know the status of a task
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
}