package com.idlemonitor.backend.admin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.idlemonitor.backend.user.model.AdminUser;
/**
 * 
 * @author Bhagya
 * Created On August 03rd, 2020
 * Model class for projects
 *
 */

@Entity
@Table(name ="projects")
public class Project implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="project_id")
	private Integer projectId;
	
	@Column(name="project_name")
	private String projectName;
	
	@Column(name="client_name")
	private String clientName;
	
	@ManyToOne(targetEntity=Company.class)
	@JoinColumn(name="company_id")
	private Company company;
	
	@ManyToOne(targetEntity=AdminUser.class)
	@JoinColumn(name="admin_user_id")
	private AdminUser adminUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="project_creation_date")
	private Date projectCreationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="project_modification_date")
	private Date projectModificationDate;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Transient
	private Integer totalProjects;

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public AdminUser getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(AdminUser adminUser) {
		this.adminUser = adminUser;
	}

	public Date getProjectCreationDate() {
		return projectCreationDate;
	}

	public void setProjectCreationDate(Date projectCreationDate) {
		this.projectCreationDate = projectCreationDate;
	}

	public Date getProjectModificationDate() {
		return projectModificationDate;
	}

	public void setProjectModificationDate(Date projectModificationDate) {
		this.projectModificationDate = projectModificationDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public Integer getTotalProjects() {
		return totalProjects;
	}

	public void setTotalProjects(Integer totalProjects) {
		this.totalProjects = totalProjects;
	}
	
	
	
}