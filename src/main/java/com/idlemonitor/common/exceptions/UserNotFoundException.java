package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On May 18th ,2020
 *	This Exception Handles the condition when the employee/admin user not found
 */
public class UserNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Not Found";
	}
}