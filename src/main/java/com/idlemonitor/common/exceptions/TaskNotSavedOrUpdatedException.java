package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On August 03rd, 2020
 * Exception class for TaskNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when project not saved or updated
 */
public class TaskNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Task Not Saved Or Updated ";
	}
}