package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On August 04th ,2020
 *	This Exception Handles the condition when the task data not found
 */
public class TaskNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Task Not Found";
	}
}