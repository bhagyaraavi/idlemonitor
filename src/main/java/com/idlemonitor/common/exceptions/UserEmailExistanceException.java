package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On April 09th ,2021
 *	This Exception Handles the condition when the user email already exist in db
 */
public class UserEmailExistanceException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Email Already Exist";
	}
}