package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On May 18th ,2020
 *	This Exception Handles the condition when the company not found
 */
public class CompanyNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Company Not Found";
	}
}