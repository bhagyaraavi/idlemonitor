package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On June 22nd, 2020
 *	This Exception Handles the condition when the idle time configuration not found
 */
public class IdleTimeConfigurationNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "IdleTimeConfiguration Not Found";
	}
}