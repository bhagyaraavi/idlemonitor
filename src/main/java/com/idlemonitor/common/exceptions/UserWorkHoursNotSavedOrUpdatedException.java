package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On july 01st, 2020
 * Exception class for UserWorkHoursNotSavedOrUpdatedException
 * This UserDefined Exception handles the condition when user work hours not saved or updated
 */
public class UserWorkHoursNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Work Hours Info Not Saved Or Updated ";
	}
}