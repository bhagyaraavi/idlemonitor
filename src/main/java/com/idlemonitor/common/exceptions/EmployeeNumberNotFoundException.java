package com.idlemonitor.common.exceptions;
/***
 * Created By Harshitha On March 2nd ,2021
 *	This Exception Handles the condition when the employee number is not found
 */
public class EmployeeNumberNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	
	public String toString(){
		return "Employee Number Not Found";
	}
}