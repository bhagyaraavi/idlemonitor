package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On October 20th, 2020
 * Exception class for ProjectMappingNotFound Exception
 * This UserDefined Exception handles the condition when project mapping not found
 */
public class ProjectMappingNotFoundException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "ProjectMapping Not Found ";
	}
}