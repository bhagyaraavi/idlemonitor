package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On May 18th ,2020
 *	This Exception Handles the condition when the user logins data not found
 */
public class UserLoginsNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "UserLogins Not Found";
	}
}