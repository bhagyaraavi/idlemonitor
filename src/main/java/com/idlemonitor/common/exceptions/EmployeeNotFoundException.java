package com.idlemonitor.common.exceptions;
/***
 * Created By Bhagya On May 18th ,2020
 *	This Exception Handles the condition when the employee not found
 */
public class EmployeeNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	
	public String toString(){
		return "Employee Not Found";
	}
}
