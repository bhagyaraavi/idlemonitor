package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On May 18th ,2020
 *	This Exception Handles the condition when the user Work hours not found
 */
public class UserWorkHoursNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Work Hours Not Found";
	}
}