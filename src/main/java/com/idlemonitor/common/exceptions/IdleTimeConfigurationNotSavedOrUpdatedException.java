package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On june22nd, 2020
 * Exception class for IdleTimeConfigurationNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when idletimeconfiguration not saved or updated
 */
public class IdleTimeConfigurationNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Idle Time Configuration Not Saved Or Updated ";
	}
}