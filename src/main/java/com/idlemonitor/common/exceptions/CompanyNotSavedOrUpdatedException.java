package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On June 04th, 2020
 * Exception class for CompanyNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when company not saved or updated
 */
public class CompanyNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Company Not Saved Or Updated ";
	}
}