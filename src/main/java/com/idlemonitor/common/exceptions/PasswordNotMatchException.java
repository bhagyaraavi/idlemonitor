package com.idlemonitor.common.exceptions;

public class PasswordNotMatchException extends Exception{

	
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {		
		return "Password Do not match Exception";
	}
}