package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On july 01st, 2020
 * Exception class for UserScreenCapturesNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when user screen captures not saved or updated
 */
public class UserScreenCapturesNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Screen Captures Info Not Saved Or Updated ";
	}
}