package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On july 01st, 2020
 * Exception class for UserLoginsNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when userLogins data not saved or updated
 */
public class UserLoginsNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "UserLogins Info Not Saved Or Updated ";
	}
}