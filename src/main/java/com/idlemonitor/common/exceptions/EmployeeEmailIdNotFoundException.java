package com.idlemonitor.common.exceptions;

/***
 * Created By Harshitha On March 4th,2021
 *	This Exception Handles the condition when the employee email id is not found
 */
public class EmployeeEmailIdNotFoundException extends Exception{

private static final long serialVersionUID = 1L;
	
	
	public String toString(){
		return "Employee Number Not Found";
	}
}
