package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On may28th, 2020
 * Exception class for UserNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when user not saved or updated
 */
public class UserNotSavedOrUpdatedException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Not Saved Or Updated ";
	}
}