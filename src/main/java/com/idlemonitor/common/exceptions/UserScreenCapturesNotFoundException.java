package com.idlemonitor.common.exceptions;
/***
 * Created By Bhagya On May 28th ,2020
 *	This Exception Handles the condition when the User Screen Captures not found
 */
public class UserScreenCapturesNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	
	public String toString(){
		return "User Screen Captures Not Found";
	}
}
