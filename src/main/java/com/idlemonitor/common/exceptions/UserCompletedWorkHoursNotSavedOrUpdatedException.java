package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On october 22nd ,2020
 *	This Exception Handles the condition when the user Work hours not saved or updated
 */
public class UserCompletedWorkHoursNotSavedOrUpdatedException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Completed Work Hours Not Saved or Updated ";
	}
}