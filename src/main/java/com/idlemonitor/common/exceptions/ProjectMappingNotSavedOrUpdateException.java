package com.idlemonitor.common.exceptions;
/**
 * 
 * Created By Bhagya On August 04th, 2020
 * Exception class for ProjectMappingNotSavedOrUpdated Exception
 * This UserDefined Exception handles the condition when project mapping not saved or updated
 */
public class ProjectMappingNotSavedOrUpdateException extends Exception{
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "ProjectMapping Not Saved Or Updated ";
	}
}