package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On October 20 ,2020
 *	This Exception Handles the condition when the user completed Work hours not found
 */
public class UserCompletedWorkHoursNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "User Completed Work Hours Not Found";
	}
}