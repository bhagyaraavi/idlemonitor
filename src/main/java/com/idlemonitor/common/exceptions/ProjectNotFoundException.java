package com.idlemonitor.common.exceptions;

/***
 * Created By Bhagya On August 04th ,2020
 *	This Exception Handles the condition when the project data not found
 */
public class ProjectNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	@Override
	public String toString(){
		return "Project Not Found";
	}
}